package rltools;

import java.util.Arrays;
import java.util.Iterator;

/**
 *
 * @author dean
 */
public class CombinationIterator implements Iterator<double[]> {
    private int dimensions;
    private int minValue;
    private int maxValue;
    private long curr = 0;
    private long combinations = -1;
    private double currValue[];

    public CombinationIterator(int dimensions, int minValue, int maxValue) {
        this.dimensions = dimensions;
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.curr = 0;
        this.currValue = new double[dimensions];
        Arrays.fill(currValue, minValue);
    }

    public long getNumCombinations() {
        if (combinations == -1) {
            int possibleNumbers = (maxValue - minValue) + 1;
            combinations = (long) Math.pow(possibleNumbers, dimensions);
        }
        return combinations;
    }

    @Override
    public boolean hasNext() {
        if (curr < getNumCombinations()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public double[] next() {
        double ret[] = currValue.clone();
        iterate(currValue.length - 1);
        curr++;
        return ret;
    }

    public void iterate(int index) {
        if (index == -1) {
            return;
        }
        currValue[index]++;
        if (currValue[index] > maxValue) {
            currValue[index] = minValue;
            iterate(index - 1);
        }
    }
    
        @Override
    public void remove() {
        throw new UnsupportedOperationException("Not supported yet. And never will be mwhahahaha.");
    }
}
