package functionapproximation;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import rl.ElegibilityTrace;
import rl.state.State;

/**
 *
 * @author Dean
 */
public class VApproximator implements Serializable {

    private QApproximator q;
    public FunctionApproximator fa;

    public VApproximator(FunctionApproximator fa) {
        this.q = new QApproximator(new FunctionApproximator[]{fa});
        this.fa = fa;
    }

    /**
     * Gets the weight array 
     * @return A double array of weights associated with the function approximator
     */
    public double[] getWeights() {
        return q.getWeights();
    }

    /**
     * Sets the weights of the function approximator to the given double array. There is no length checking, so care must be taken to make sure the length is the same as the old weight array.
     * @param weights The weight array to set.
     */
    public void setWeights(double weights[]) {
        q.setWeights(weights);
    }

    /**
     * Calculates the array of phi values for the given state.
     * @param s The state at which the function will be evaluated.
     * @return The array of phi values at the given state.
     */
    public double[] calculateStateValue(State s) {
        return q.calculateStateValue(s);
    }

    /**
     * Evaluates the V function at s. Calculates the phi values using calculateStateValue, and multiplies it by the weight array to get a single double value of the given state.
     * @param s The state to evaluate the function at.
     * @return The value of the state as a double.
     */
    public double valueAt(State s) {
        return q.valueAt(s, 0);
    }

    /**
     * Gets the total number of basis terms in the value function.
     * @return An integer value for the number of basis terms in the value function.
     */
    public int getNumTerms() {
        return q.getNumTerms();
    }

    /**
     * Gets the shrink array for the basis terms in this value function. Each shrink value in the array corresponds to a basis function, and indicates the reduction in the learning rate that should be applied for that basis function. Alpha/shrink[i] is the correct alpha value for basis function i. The specific case of shrink[i] = 1 for all i gives the unmodified version. The shrink value is a property of the basis functions being used.
     * @return A double array of shrink values.
     */
    public double[] getShrink() {
        return q.getShrink();
    }

    /**
     * Adds a basis function to the value function with 0 weight.
     * @param bf The basis function to add.
     */
    public void addBasis(BasisFunction bf) {
        q.addBasis(bf);
    }

    /**
     * Adds a basis function to the value function with the given weight.
     * @param bf The basis function to add.
     * @param weight The initial weight to set it at.
     */
    public void addBasis(BasisFunction bf, double weight) {
        q.addBasis(bf, weight);
    }

    /**
     * Creates a new elegibility trace and adds it to a list. Could for example be used to manage multiple agents using the same value function.
     * @return The new elegibility trace.
     */
    public ElegibilityTrace getNewTrace() {
        return q.getNewTrace();
    }
    
    public void removeBasis(BasisFunction bf) {
        try {
            q.removeBasis(bf, 0);
        } catch (MissingBasisException ex) {
            Logger.getLogger(VApproximator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
        public void printFunction() {
            q.printFunction();
        }
}
