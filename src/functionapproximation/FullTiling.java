/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package functionapproximation;

import java.util.HashMap;
import java.util.Vector;
import rl.state.State;

/**
 *
 * @author dean
 */
public class FullTiling extends FunctionApproximator {

    HashMap<TileBasis, Integer> allBasis = new HashMap<TileBasis, Integer>();
    int tiles = 0;

    public FullTiling(int tiles, int numFeatures) {
        super(calculateNumTerms(tiles, numFeatures), numFeatures);
        this.tiles = tiles;
        initialiseTerms();
    }

    public static int calculateNumTerms(int tiles, int numFeatures) {
        return (int) Math.pow(tiles, numFeatures);
    }

    public void initialiseTerms() {
        super.initialiseTerms();
        BasisFunction terms[] = getTerms();
        int iterator[] = new int[getNumFeatures()];
        int pos = 0;
        while (iterator[0] < tiles) {
            double term[][] = new double[this.getNumFeatures()][2];
            for (int i = 0; i < iterator.length; i++) {
                term[i][0] = ((double) (iterator[i])) / tiles;
                term[i][1] = ((double) (iterator[i] + 1)) / tiles;
            }

            TileBasis t = new TileBasis(term);
            terms[pos] = t;
            allBasis.put(t, pos);
            pos++;

            int count = getNumFeatures() - 1;
            iterator[count] += 1;
            while (iterator[count] == tiles && count != 0) {
                iterator[count] = 0;
                count--;
                iterator[count] += 1;
            }
        }
    }
    
    @Override
    public double[] calculateStateValue(State s) {
        double term[][] = new double[this.getNumFeatures()][2];
        for (int i = 0; i < s.getState().length; i++) {
            double unscaledValue = Math.max(Math.min(s.getState()[i],1),0) * tiles;
            
            double start = Math.floor(unscaledValue);
            double end = Math.ceil(unscaledValue);
            if (start == end) {
                start = end - 1;
            }
            if (start == -1) {
                start = 0;
                end = 1;
            }
            term[i][0] = ((double) (start)) / tiles;
            term[i][1] = ((double) (end)) / tiles;
        }
        TileBasis t = new TileBasis(term);
        int index = allBasis.get(t);
        double returnArray[] = new double[numTerms];
        returnArray[index] = 1;

        return returnArray;
    }

    @Override
    public void addBasis(BasisFunction bf) {
        super.addBasis(bf);
        reloadHashmap();
    }

    @Override
    public void removeBasis(BasisFunction bf) {
        super.removeBasis(bf);
        reloadHashmap();
    }

    protected void reloadHashmap() {
        allBasis.clear();
        for (int i = 0; i < terms.length; i++) {
            allBasis.put((TileBasis) terms[i], i);
        }
    }
}
