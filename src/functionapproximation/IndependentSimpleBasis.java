package functionapproximation;


public class IndependentSimpleBasis extends FunctionApproximator {
  
  public IndependentSimpleBasis(int numFeatures) {
    super(calculateNumTerms(numFeatures), numFeatures);
    initialiseTerms();
  }
  
 
  public static int calculateNumTerms(int numFeatures) {
    return numFeatures;
  }
  
  public void initialiseTerms() {
    super.initialiseTerms();
    BasisFunction terms[] = getTerms();
    for (int i = 0; i < getNumFeatures(); i++) {
        terms[i] = new SimpleBasis(i);
    }
  }
  
  public void printArray(double arr[]) {
    String str = "";
    for (int i = 0; i < arr.length; i++) {
      str = str + arr[i] + " ";
    }
    System.out.println(str);
  }
}
