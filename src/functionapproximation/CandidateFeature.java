/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package functionapproximation;

import rl.state.State;

/**
 *
 * @author dean
 */
public class CandidateFeature {
    CombinationBasis cb;
    double potential = 0;
    public CandidateFeature(CombinationBasis cb) {
        this.cb = cb;
    }
    
    public CombinationBasis getCombined() {
        return cb;
    }
    
    public void addPotential(double p) {
        potential += p;
    }
    
    public double getPotential() {
        return potential;
    }
    
    public void decayPotential() {
        potential *= 0.99;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof CandidateFeature) {
            CandidateFeature t = (CandidateFeature) o;
            if (t.cb.equals(this.cb)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + (this.cb != null ? this.cb.hashCode() : 0);
        return hash;
    }

}
