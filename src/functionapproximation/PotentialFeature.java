package functionapproximation;

import functionapproximation.iFFDBasis;

/**
 *
 * @author dean
 */
public class PotentialFeature {

    iFFDBasis bf;
    iFFDBasis parent1;
    iFFDBasis parent2;
    double potential;

    public PotentialFeature(iFFDBasis bf, iFFDBasis parent1, iFFDBasis parent2) {
        this.bf = bf;
        this.parent1 = parent1;
        this.parent2 = parent2;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + (this.bf != null ? this.bf.hashCode() : 0);
        return hash;
    }
    
    public iFFDBasis getBf() {
        return bf;
    }


    @Override
    public boolean equals(Object o) {
        if (o instanceof PotentialFeature) {
            PotentialFeature t = (PotentialFeature) o;
            if (t.bf.equals(this.bf)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    public double getPotential() {
        return potential;
    }

    public void incrementPotential(double amount) {
        //potential = potential + (1.0/10)*(amount - potential);
        potential += amount;
    }
    
    public void setPotential(double potential) {
        this.potential = potential;
    }
}
