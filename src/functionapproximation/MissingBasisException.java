

package functionapproximation;

/**
 *
 * @author Dean
 */
class MissingBasisException extends Exception {

    public MissingBasisException(String string) {
        super(string);
    }
    
}
