package functionapproximation;

import java.util.Arrays;

public class IndependentRBF extends FunctionApproximator {
  int order;

  public IndependentRBF(int order, int numFeatures) {
    super(calculateNumTerms(order, numFeatures), numFeatures);
    this.order = order;
    initialiseTerms();
  }

  public static int calculateNumTerms(int order, int numFeatures) {
    return order * numFeatures + 1 - 1;
  }

  public void initialiseTerms() {
    double variancesquared = 1.0 / (order - 1);
    super.initialiseTerms();
    BasisFunction terms[] = getTerms();
    double term[] = new double[this.getNumFeatures()];
    Arrays.fill(term, -1);
    terms[0] = new RadialBasis(term.clone(), variancesquared);
    int pos = 0;
    for (int i = 0; i < getNumFeatures(); i++) {
      for (int j = 0; j < order; j++) {
        term = new double[this.getNumFeatures()];
        Arrays.fill(term, -1);
        term[i] = j * (1.0/(order-1));
        terms[pos] = new RadialBasis(term, variancesquared);
        pos++;
      }
    }
  }

  public int getOrder() {
    return order;
  }

  public void printArray(double arr[]) {
    String str = "";
    for (int i = 0; i < arr.length; i++) {
      str = str + arr[i] + " ";
    }
    System.out.println(str);
  }
}
