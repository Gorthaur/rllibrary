package functionapproximation;


public class PairSimpleBasis extends FunctionApproximator {
  
  public PairSimpleBasis(int numFeatures) {
    super(calculateNumTerms(numFeatures), numFeatures);
    initialiseTerms();
  }
  
 
  public static int calculateNumTerms(int numFeatures) {
    return numFeatures*(numFeatures-1)/2;
  }
  
  public void initialiseTerms() {
    super.initialiseTerms();
    BasisFunction terms[] = getTerms();
    int pos = 0;
    for (int i = 0; i < getNumFeatures(); i++) {
        for (int j = i+1; j < getNumFeatures(); j++) {
        terms[pos] = new SimpleBasis(i,j);
        pos++;
        }
    }
  }
  
  public void printArray(double arr[]) {
    String str = "";
    for (int i = 0; i < arr.length; i++) {
      str = str + arr[i] + " ";
    }
    System.out.println(str);
  }
}
