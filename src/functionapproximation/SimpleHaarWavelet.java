
package functionapproximation;

/**
 *
 * @author Dean
 */
public class SimpleHaarWavelet extends Wavelet<SimpleHaarWavelet> {

    public SimpleHaarWavelet(int scale, int shift, int dimension) {
        super(scale, shift, dimension);
    }

    @Override
    public double getValue(double t) {
        if (t >= 0 && t < 0.5) {
            return 1;
        }
        else if (t >= 0.5 && t < 1) {
            return -1;
        }
        else {
            return 0;
        }
    }

    @Override
    public double getNormalisation(int scale) {
        return 1;
    }

    @Override
    public double getShrink() {
        return 1 + pow;
    }

    @Override
    public String getBasisString() {
        return String.format("Haar %d %d %d", this.dimension, this.scale, this.shift);
    }

    @Override
    public SimpleHaarWavelet createChild(int scale, int shift, int dimension) {
        return new SimpleHaarWavelet(scale, shift, dimension);
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj) && obj instanceof SimpleHaarWavelet; 
    }


    @Override
    public int hashCode() {
        int hash = super.hashCode()*7 + 11;
        return hash;
    }

    
    
    
}
