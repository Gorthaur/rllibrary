package functionapproximation;


import java.util.Arrays;

public class FullRBF extends FunctionApproximator {
  int order;
  
  public FullRBF(int order, int numFeatures) {
    super(calculateNumTerms(order,numFeatures), numFeatures);
    this.order = order;
    initialiseTerms();
  }
  
 
  public static int calculateNumTerms(int order, int numFeatures) {
    return (int)Math.pow(order+1, numFeatures) + 1;
  }
  
  public void initialiseTerms() {
    super.initialiseTerms();
    //double variancesquared = Math.pow(1.0 / (order + 1),2);
    double variancesquared = 1/(2*Math.pow(order+1, 3) - 4*Math.pow(order+1,2) + 2*(order+1));
    BasisFunction terms[] = getTerms();
    double c[] = new double[this.getNumFeatures()];
    Arrays.fill(c, 0);
    terms[0] = new FourierBasis(c.clone());
    terms[1] = new RadialBasis(c.clone(), variancesquared);
    int pos = 2;
    iterate(c, numFeatures, order);
    while (c[0] <= (order)) {
      terms[pos] = new RadialBasis(divide(c.clone(),order), variancesquared);
      pos++;
      iterate(c, numFeatures, order);
    }
  }
  
  public void iterate(double [] c, int NVariables, int order)
  {
    c[NVariables - 1] += 1;

    if(c[NVariables - 1] > (order))
    {
      if(NVariables > 1)
      {
        c[NVariables - 1]  = 0;
        iterate(c, NVariables - 1, order);
      }
    }
  }

  public double[] divide(double arr[], double amount) {
      for (int i = 0; i < arr.length; i++) {
          arr[i] = arr[i]/amount;
      }
      return arr;
  }
  
  public int getOrder() {
    return order;
  }
  
  public void printArray(double arr[]) {
    String str = "";
    for (int i = 0; i < arr.length; i++) {
      str = str + arr[i] + " ";
    }
    System.out.println(str);
  }
}