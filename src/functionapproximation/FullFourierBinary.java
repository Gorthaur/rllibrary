package functionapproximation;


public class FullFourierBinary extends FullFourier {
  public FullFourierBinary(int order, int numFeatures) {
    super(order, numFeatures);
  }
  
  public static int calculateNumTerms(int order, int numFeatures) {
    int num = (int)Math.pow(order + 1, numFeatures-1);
    num = 2*num - 1;
    return num;
  }
}
