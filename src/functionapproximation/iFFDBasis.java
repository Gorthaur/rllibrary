package functionapproximation;

/**
 *
 * @author dean
 */
public interface iFFDBasis<T> {
   public iFFDBasis combine(T t);
   public void addChild(T t);
   public boolean hasChild(T t);
}
