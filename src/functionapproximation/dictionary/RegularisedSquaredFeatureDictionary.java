/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package functionapproximation.dictionary;

import functionapproximation.BasisFunction;
import functionapproximation.QApproximator;
import functionapproximation.VApproximator;
import java.util.LinkedList;
import rl.Sample;

/**
 *
 * @author Dean
 */
public class RegularisedSquaredFeatureDictionary extends GeneralFeatureDictionary {

    @Override
    public BasisFunction getMostCorrelated(LinkedList<Sample> samples, VApproximator v, double gamma) {
        double maxCorrelation = 0;
        BasisFunction mostCorrelated = null;
                double residual[] = getTDResidual(samples, v, gamma);
        for (BasisFunction bf : dictionary) {
            double bot = 0;
            double top = 0;
            int count = 0;
            for (Sample s : samples) {
                double eval = bf.getValue(s.s);
                top += eval * residual[count];
                bot += eval * eval;
                count++;
            }
            bot = Math.sqrt(bot);
            double correlation = Math.abs(top)/bot;
            correlation = correlation / Math.pow(bf.getShrink(),2);
            if (correlation >= maxCorrelation) {
                mostCorrelated = bf;
                maxCorrelation = correlation;
            }
        }
        return mostCorrelated;
    }

    @Override
    public BasisFunction getMostCorrelated(LinkedList<Sample> samples, QApproximator q, double gamma, int action) {

        double maxCorrelation = 0;
        BasisFunction mostCorrelated = null;
        double residual[] = new double[samples.size()];
        int count = 0;
        for (Sample s : samples) {
            double maxActionVal = Double.NEGATIVE_INFINITY;
            int maxAction = 0;
            for (int i = 0; i < q.getNumActions(); i++) {
                double val = q.valueAt(s.nextState, i);
                if (val > maxActionVal) {
                    maxAction = i;
                    maxActionVal = val;
                }
            }
            residual[count] = -q.valueAt(s.s, s.action) + gamma * maxActionVal + s.reward;
            count++;
        }

        for (BasisFunction bf : dictionary) {

            double bot = 0;
            double top = 0;
            count = 0;
            for (Sample s : samples) {
                if (s.action != action) {
                    continue;
                }
                double eval = bf.getValue(s.s);

                top += eval * residual[count];
                bot += eval * eval;
                count++;
            }
            bot = Math.sqrt(bot);
            double correlation = Math.abs(top / bot);
            correlation = correlation / Math.pow(bf.getShrink(), 2);
            if (correlation >= maxCorrelation) {
                mostCorrelated = bf;
                maxCorrelation = correlation;
            }
        }
        return mostCorrelated;
    }
}
