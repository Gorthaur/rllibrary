/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package functionapproximation.dictionary;

import functionapproximation.BasisFunction;
import functionapproximation.FourierBasis;
import functionapproximation.QApproximator;
import functionapproximation.VApproximator;
import java.util.LinkedList;
import rl.Sample;
import rl.state.State;

/**
 *
 * @author Dean
 */
public class FourierDerivative extends GeneralFeatureDictionary {

    public FourierDerivative() {
    }

    @Override
    public BasisFunction getMostCorrelated(LinkedList<Sample> samples, VApproximator v, double gamma) {
        double maxCorrelation = 0;
        BasisFunction mostCorrelated = null;
        double residual[][] = getDifferenceResidual(samples, v, gamma);
        for (BasisFunction bf : dictionary) {
            double tempcorr = 0;
            FourierBasis fb = (FourierBasis)bf;
            for (int i = 0; i < v.fa.numFeatures; i++) {
                double bot = 0;
                double top = 0;
                int count = 0;
                double sum = 0;
                for (Sample s : samples) {
                    sum += getDerivativeValue(fb,s.s, i);
                    double eval = getDerivativeValue(fb, s.s, i);
                    top += eval * (residual[count][i]);
                    bot += eval * eval;
                    count++;
                }
                tempcorr += Math.pow(top, 2) / bot;
                            //System.out.println(sum + " " + tempcorr);
            }

            double correlation = tempcorr; 
            if (correlation >= maxCorrelation) {
                mostCorrelated = bf;
                maxCorrelation = correlation;
            }
        }
        System.out.println(mostCorrelated.getBasisString());
        return mostCorrelated;
    }
    
    public double getDerivativeValue(FourierBasis bf, State s, int partial) {
                //FourierState p = (FourierState)s;
        double state[] = s.getState();
        double val = 0;
        for (int i = 0; i < bf.featureArr.length; i++) {
            val += state[i] * bf.featureArr[i];
        }
        double returnVal = -1*bf.featureArr[partial]*Math.PI*Math.sin((Math.PI) * val);
        return returnVal;
    }

    @Override
    public BasisFunction getMostCorrelated(LinkedList<Sample> samples, QApproximator q, double gamma, int action) {
        double maxCorrelation = 0;
        BasisFunction mostCorrelated = null;
        double residual[] = new double[samples.size()];
        int count = 0;
        for (Sample s : samples) {
            double maxActionVal = Double.NEGATIVE_INFINITY;
            int maxAction = 0;
            for (int i = 0; i < q.getNumActions(); i++) {
                double val = q.valueAt(s.nextState, i);
                if (val > maxActionVal) {
                    maxAction = i;
                    maxActionVal = val;
                }
            }
            residual[count] = -q.valueAt(s.s, s.action) + gamma * maxActionVal + s.reward;
            count++;
        }

        for (BasisFunction bf : dictionary) {

            double bot = 0;
            double top = 0;
            count = 0;
            for (Sample s : samples) {
                if (s.action != action) {
                    continue;
                }
                double eval = bf.getValue(s.s);

                top += eval * residual[count];
                bot += eval * eval;
                count++;
            }
            double correlation = Math.pow(top, 2) / bot;
            if (correlation >= maxCorrelation) {
                mostCorrelated = bf;
                maxCorrelation = correlation;
            }
        }
        return mostCorrelated;
    }
}
