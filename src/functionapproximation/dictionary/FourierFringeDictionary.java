package functionapproximation.dictionary;

import functionapproximation.BasisFunction;
import functionapproximation.FourierBasis;
import functionapproximation.NormComparator;
import functionapproximation.QApproximator;
import functionapproximation.VApproximator;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import rl.Sample;

/**
 *
 * @author Dean
 */
public class FourierFringeDictionary extends GeneralFeatureDictionary {

    LinkedList<BasisFunction> smallerDictionary = new LinkedList<BasisFunction>();
    LinkedList<BasisFunction> biggerDictionary = new LinkedList<BasisFunction>();

    @Override
    public boolean addFeatures(BasisFunction[] terms) {
        biggerDictionary.addAll(Arrays.asList(terms));
        return super.addFeatures(terms); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean addFeature(BasisFunction feature) {
        biggerDictionary.add(feature);
        return super.addFeature(feature); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean remove(BasisFunction feature) {
        boolean ret = super.remove(feature); //To change body of generated methods, choose Tools | Templates.
        FourierBasis bf = (FourierBasis) feature;

        for (int i = 0; i < bf.featureArr.length; i++) {
            double newFeatureArr[] = bf.featureArr.clone();
            newFeatureArr[i] -= 1;
            FourierBasis newbf = new FourierBasis(newFeatureArr);
            if (biggerDictionary.contains(newbf)) {
                smallerDictionary.add(newbf);
                biggerDictionary.remove(newbf);
            }
            newFeatureArr = bf.featureArr.clone();
            newFeatureArr[i] += 1;
            newbf = new FourierBasis(newFeatureArr);
            if (biggerDictionary.contains(newbf)) {
                smallerDictionary.add(newbf);
                biggerDictionary.remove(newbf);
            }
        }
        smallerDictionary.remove(feature);
        return ret;
    }

    @Override
    public BasisFunction getMostCorrelated(LinkedList<Sample> samples, VApproximator v, double gamma) {
        if (smallerDictionary.size() == 0) {
            initialiseDict(v.fa.terms);
        }
        LinkedList<BasisFunction> temp = dictionary;
        dictionary = smallerDictionary;
        BasisFunction bf = super.getMostCorrelated(samples, v, gamma); //To change body of generated methods, choose Tools | Templates.
        dictionary = temp;
        return bf;
    }

    @Override
    public BasisFunction getMostCorrelated(LinkedList<Sample> samples, QApproximator q, double gamma, int action) {
        if (smallerDictionary.size() == 0) {
            initialiseDict(q.FAs[0].terms);
        }
        LinkedList<BasisFunction> temp = dictionary;
        dictionary = smallerDictionary;
        BasisFunction bf = super.getMostCorrelated(samples, q, gamma, action); //To change body of generated methods, choose Tools | Templates.
        dictionary = temp;
        return bf;
    }

    @Override
    public BasisFunction getMostCorrelated(LinkedList<Sample> samples, QApproximator q, double gamma) {
        if (smallerDictionary.size() == 0) {
            initialiseDict(q.FAs[0].terms);
        }
        LinkedList<BasisFunction> temp = dictionary;
        dictionary = smallerDictionary;
        BasisFunction bf = super.getMostCorrelated(samples, q, gamma); //To change b        dictionary = temp;
                dictionary = temp;
        return bf;

    }

    @Override
    public BasisFunction getMostCorrelated(LinkedList<Sample> samples) {
        if (smallerDictionary.size() == 0) {
            initialiseDict();
        }
        LinkedList<BasisFunction> temp = dictionary;
        dictionary = smallerDictionary;
        BasisFunction bf = super.getMostCorrelated(samples); //To change body of generated methods, choose Tools | Templates.
        dictionary = temp;
        return bf;
    }

    public void initialiseDict() {
        Collections.sort(biggerDictionary, new NormComparator());
        int dimensions = ((FourierBasis) biggerDictionary.getFirst()).featureArr.length;
        for (int i = 0; i < dimensions; i++) {
            if (biggerDictionary.size() > 0) {
                smallerDictionary.add(biggerDictionary.pop());
            }
        }
    }
    
    public void initialiseDict(BasisFunction addedAlready[]) {
        Collections.sort(biggerDictionary, new NormComparator());
        for (int i = 0; i < addedAlready.length; i++) {
            //FourierBasis fb = (FourierBasis)addedAlready[i];
            remove(addedAlready[i]);
        }

    }


    @Override
    public int getDictionarySize() {
        return smallerDictionary.size();
    }

}
