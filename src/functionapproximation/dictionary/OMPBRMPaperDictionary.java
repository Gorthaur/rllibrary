package functionapproximation.dictionary;

import functionapproximation.BasisFunction;
import functionapproximation.VApproximator;
import java.util.LinkedList;

/**
 *
 * @author Dean
 */
import rl.Sample;
public class OMPBRMPaperDictionary extends GeneralFeatureDictionary {
        @Override
    public BasisFunction getMostCorrelated(LinkedList<Sample> samples, VApproximator v, double gamma) {
        double maxCorrelation = 0;
        BasisFunction mostCorrelated = null;
        double residual[] = getTDResidual(samples, v, gamma);
        
        for (BasisFunction bf : dictionary) {
            double bot = 0;
            double top = 0;
            int count = 0;
            for (Sample s : samples) {
                double eval = bf.getValue(s.s) - gamma*bf.getValue(s.nextState); //this is correct
                top += eval * (residual[count]);
                bot += eval * eval;
                count++;
            }
            bot = Math.sqrt(bot);
            double correlation = Math.abs(top)/bot;
            if (correlation >= maxCorrelation) {
                mostCorrelated = bf;
                maxCorrelation = correlation;
            }
        }
        return mostCorrelated;
    }
}
