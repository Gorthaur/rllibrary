/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package functionapproximation.dictionary;

import functionapproximation.BasisFunction;
import functionapproximation.VApproximator;
import java.util.LinkedList;
import rl.Sample;

/**
 *
 * @author Dean
 */
public class RegularisedFeatureDictionary extends GeneralFeatureDictionary {

    @Override
    public BasisFunction getMostCorrelated(LinkedList<Sample> samples, VApproximator v, double gamma) {
        double maxCorrelation = 0;
        BasisFunction mostCorrelated = null;

        double residual[] = getTDResidual(samples, v, gamma);

        for (BasisFunction bf : dictionary) {
            double bot = 0;
            double top = 0;
            int count = 0;
            for (Sample s : samples) {
                double eval = bf.getValue(s.s);
                top += eval * (residual[count]);
                bot += eval * eval;
                count++;
            }
            bot = Math.sqrt(bot);
            double correlation = Math.abs(top) / bot;
            correlation = correlation / Math.abs(bf.getShrink());
            if (correlation >= maxCorrelation) {
                mostCorrelated = bf;
                maxCorrelation = correlation;
            }
        }
        return mostCorrelated;
    }
}
