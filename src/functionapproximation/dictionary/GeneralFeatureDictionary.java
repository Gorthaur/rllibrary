package functionapproximation.dictionary;

import functionapproximation.BasisFunction;
import functionapproximation.QApproximator;
import functionapproximation.VApproximator;
import java.util.LinkedList;
import rl.Sample;

/**
 *
 * @author Dean
 */
public class GeneralFeatureDictionary extends FeatureDictionary {

    public LinkedList<BasisFunction> dictionary;

    public GeneralFeatureDictionary() {
        dictionary = new LinkedList<BasisFunction>();
    }

    @Override
    public BasisFunction getMostCorrelated(LinkedList<Sample> samples) {

        double maxCorrelation = 0;
        BasisFunction mostCorrelated = null;
        for (BasisFunction bf : dictionary) {
            double bot = 0;
            double top = 0;
            for (Sample s : samples) {
                double eval = bf.getValue(s.s);
                top += eval * s.error;
                bot += eval * eval;
            }
            bot = Math.sqrt(bot);
            double correlation = Math.abs(top / bot);

            if (correlation >= maxCorrelation) {
                mostCorrelated = bf;
                maxCorrelation = correlation;
            }
        }
        return mostCorrelated;
    }

    @Override
    public BasisFunction getMostCorrelated(LinkedList<Sample> samples, QApproximator q, double gamma) {

        double maxCorrelation = 0;
        BasisFunction mostCorrelated = null;
        for (BasisFunction bf : dictionary) {

            double bot = 0;
            double top = 0;
            for (Sample s : samples) {
                double eval = bf.getValue(s.s);
                top += eval * (-q.valueAt(s.s, s.action) + gamma * q.valueAt(s.nextState, s.nextAction) + s.reward);
                bot += eval * eval;
            }
            bot = Math.sqrt(bot);
            double correlation = Math.abs(top / bot);
            if (correlation >= maxCorrelation) {
                mostCorrelated = bf;
                maxCorrelation = correlation;
            }
        }
        return mostCorrelated;
    }

    @Override
    public BasisFunction getMostCorrelated(LinkedList<Sample> samples, QApproximator q, double gamma, int action) {
        double maxCorrelation = 0;
        BasisFunction mostCorrelated = null;
        double residual[] = new double[samples.size()];
        int count = 0;
        for (Sample s : samples) {
            double maxActionVal = Double.NEGATIVE_INFINITY;
            int maxAction = 0;
            for (int i = 0; i < q.getNumActions(); i++) {
                double val = q.valueAt(s.nextState, i);
                if (val > maxActionVal) {
                    maxAction = i;
                    maxActionVal = val;
                }
            }
            residual[count] = -q.valueAt(s.s, s.action) + gamma * maxActionVal + s.reward;
            count++;
        }


        for (BasisFunction bf : dictionary) {

            double bot = 0;
            double top = 0;
            count = 0;
            for (Sample s : samples) {
                if (s.action != action) {
                    continue;
                }
                double eval = bf.getValue(s.s);

                top += eval * residual[count];
                bot += eval * eval;
                count++;
            }
            bot = Math.sqrt(bot);
            double correlation = Math.abs(top / bot);
            if (correlation >= maxCorrelation) {
                mostCorrelated = bf;
                maxCorrelation = correlation;
            }
        }
        return mostCorrelated;
    }

    @Override
    public BasisFunction getMostCorrelated(LinkedList<Sample> samples, VApproximator v, double gamma) {
        double residual[] = getTDResidual(samples, v, gamma);
        double maxCorrelation = 0;
        BasisFunction mostCorrelated = null;
        
        for (BasisFunction bf : dictionary) {
            double bot = 0;
            double top = 0;
            int count = 0;
            for (Sample s : samples) {
                double eval = bf.getValue(s.s);
                top += eval * residual[count];
                bot += eval * eval;
                count++;
            }
            bot = Math.sqrt(bot);
            double correlation = Math.abs(top / bot);
            if (correlation >= maxCorrelation) {
                mostCorrelated = bf;
                maxCorrelation = correlation;
            }
        }
        return mostCorrelated;
    }

    /**
     * Adds a basis function to the dictionary of functions under consideration.
     * @param feature The basis function to add.
     * @return Returns false if the basis function was already in the dictionary, else it adds it and returns true.
     */
    public boolean addFeature(BasisFunction feature) {
        if (dictionary.contains(feature)) {
            return false;
        } else {
            dictionary.add(feature);
            return true;
        }
    }

    /**
     * Adds an array of basis functions to the dictionary of functions under consideration.
     * @param feature The array of basis functions to add.
     * @return Returns false if one or more of the basis function was already in the dictionary, true if all were added successfully.
     */
    public boolean addFeatures(BasisFunction[] terms) {
        boolean success = true;
        for (BasisFunction t : terms) {
            if (dictionary.contains(t)) {
                success = false;
            } else {
                dictionary.add(t);

            }
        }
        return success;
    }

    /**
     * Gets the number of basis functions in the dictionary.
     * @return The number of basis functions in the dictionary as an integer.
     */
    public int getNumFeatures() {
        return dictionary.size();
    }

    /**
     * Clears the dictionary.
     */
    public void clearDictionary() {
        dictionary.clear();
    }

    /**
     * Returns true if the dictionary already contains this feature, false otherwise.
     * @param feature The feature to check.
     * @return True if the dictionary contains this feature, else false.
     */
    public boolean contains(BasisFunction feature) {
        return dictionary.contains(feature);
    }

    /**
     * Removes the specified basis function from the dictionary if it's in there. Returns true if successfully removed, or false if the function was not in the dictionary.
     * @param feature
     * @return True if the basis function was removed, false if it wasn't there and hence couldn't be removed.
     */
    public boolean remove(BasisFunction feature) {
        return dictionary.remove(feature);
    }

    /**
     * Gets the dictionary size.
     * @return An integer value of the dictionary's size.
     */
    public int getDictionarySize() {
        return dictionary.size();
    }
}
