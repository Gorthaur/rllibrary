package functionapproximation.dictionary;

import functionapproximation.BasisFunction;
import functionapproximation.VApproximator;
import java.util.LinkedList;

/**
 *
 * @author Dean
 */
import rl.Sample;

public class OMPBRMSTOMPAdjustableDictionary extends GeneralFeatureDictionary {

    private double lambda;
    private double epsilon;

    /**
     * Selects basis functions between OMP-TD and OMP-BRM. Epsilon = 1 gives
     * brm, and epsilon = 0 gives td.\ Lambda is for STOMP
     *
     * @param lambda
     * @param epsilon
     */
    public OMPBRMSTOMPAdjustableDictionary(double lambda, double epsilon) {
        this.lambda = lambda;
        this.epsilon = epsilon;
    }

    @Override
    public BasisFunction getMostCorrelated(LinkedList<Sample> samples, VApproximator v, double gamma) {
        double maxCorrelation = 0;
        BasisFunction mostCorrelated = null;
        double residual[] = getTDResidual(samples, v, gamma);
        for (BasisFunction bf : dictionary) {
            double bot = 0;
            double top = 0;
            int count = 0;
            for (Sample s : samples) {
                double eval = epsilon * (bf.getValue(s.s) - gamma * bf.getValue(s.nextState)) + (1 - epsilon) * bf.getValue(s.s);
                top += eval * (residual[count]);;
                bot += eval * eval;
                count++;
            }
            bot = bot + lambda * (Math.pow(bf.getShrink() * Math.PI, 4) / 2);
            bot = Math.sqrt(bot);
            double correlation = Math.abs(top) / bot;
            if (correlation >= maxCorrelation) {
                mostCorrelated = bf;
                maxCorrelation = correlation;
            }
        }
        return mostCorrelated;
    }
}
