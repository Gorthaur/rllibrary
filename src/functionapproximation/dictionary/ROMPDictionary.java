/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package functionapproximation.dictionary;

import functionapproximation.BasisFunction;
import functionapproximation.VApproximator;
import functionapproximation.dictionary.GeneralFeatureDictionary;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import rl.Sample;

/**
 *
 * @author Dean
 */
public class ROMPDictionary extends GeneralFeatureDictionary {
    LinkedList<BasisFunction> toAdd = new LinkedList<BasisFunction>();
    @Override
    public BasisFunction getMostCorrelated(LinkedList<Sample> samples, VApproximator v, double gamma) {
        if (toAdd.size() > 0) {
            return toAdd.pop();
        }
        ROMPCorrelation correlations[] = new ROMPCorrelation[dictionary.size()];
        double residual[] = new double[samples.size()];
        int count = 0;
        for (Sample s : samples) {
            residual[count] = -v.valueAt(s.s) + gamma * v.valueAt(s.nextState) + s.reward;
            count++;
        }
        
        count = 0;
        for (BasisFunction bf : dictionary) {
            double bot = 0;
            double top = 0;
            int corr = 0;
            for (Sample s : samples) {
                double eval = bf.getValue(s.s);
                top += eval * (residual[corr]);
                bot += eval * eval;
                corr++;
            }
            bot = Math.sqrt(bot);
            double correlation = Math.abs(top) / bot;
            correlation = correlation / bf.getShrink();
            ROMPCorrelation c = new ROMPCorrelation(bf, correlation);
            correlations[count] = c;
            count++;
        }
        toAdd = getBestSet(correlations);
        return toAdd.pop();
    }

    private LinkedList<BasisFunction> getBestSet(ROMPCorrelation correlations[]) {
        
        Arrays.sort(correlations);
        int bestStart = 0;
        int bestEnd = 0;
        double bestTotal = 0;

        int currStart = 0;
        double currTotal = 0;
        for (int i = 0; i < correlations.length; i++) {
            currTotal += Math.pow(correlations[i].correlation, 2);
            if (Math.abs(correlations[currStart].correlation) <= 2 * Math.abs(correlations[i].correlation)) { //part of the same subset
                //do nothing
            } else {
                while (Math.abs(correlations[currStart].correlation) > 2 * Math.abs(correlations[i].correlation)) {
                    currTotal -= Math.pow(correlations[currStart].correlation, 2);
                    currStart++;
                }
            }
            if (currTotal >= bestTotal) {
                bestStart = currStart;
                bestEnd = i;
                bestTotal = currTotal;
            }
        }
        LinkedList<BasisFunction> subset = new LinkedList<BasisFunction>();
        for (int i = bestStart; i <= bestEnd; i++) {
            subset.add(correlations[i].bf);
        }
        System.out.println("Correlation length: " + subset.size());
        return subset;
    }
}

class ROMPCorrelation implements Comparable<ROMPCorrelation> {

    BasisFunction bf;
    double correlation;

    public ROMPCorrelation(BasisFunction bf, double correlation) {
        this.bf = bf;
        this.correlation = correlation;
    }

    @Override
    public int compareTo(ROMPCorrelation o) {
        if (this.correlation < o.correlation) {
            return 1;
        } else if (this.correlation > o.correlation) {
            return -1;
        } else {
            return 0;
        }
    }
}