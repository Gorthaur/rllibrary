package functionapproximation.dictionary;

import functionapproximation.BasisFunction;
import functionapproximation.FourierBasis;
import functionapproximation.QApproximator;
import functionapproximation.VApproximator;
import java.util.LinkedList;
import java.util.ListIterator;
import rl.Sample;
import rltools.CombinationIterator;

/**
 *
 * @author dean
 */
public class FastFejerFourierFeatureDictionary extends FeatureDictionary {

    private final int dimensions;
    private final int maxFrequency;
    FourierFeatureDictionary fourier;

    public FastFejerFourierFeatureDictionary(int dimensions, int maxFrequency) {
        this.dimensions = dimensions;
        this.maxFrequency = maxFrequency;
        fourier = new FourierFeatureDictionary(dimensions, maxFrequency);
    }

    public int[][] getRange() {
        int range[][] = new int[dimensions][2];
        for (int i = 0; i < dimensions; i++) {
            range[i][0] = 0;
            range[i][1] = maxFrequency;
        }
        return range;
    }

    @Override
    public BasisFunction getMostCorrelated(LinkedList<Sample> samples) {
        //the residual error is s.error
        LinkedList<Double> residual = new LinkedList<Double>();
        for (Sample s : samples) {
            residual.add(s.error);
        }
        return fourierBisectionSearch(samples, residual, getRange());
    }

    @Override
    public BasisFunction getMostCorrelated(LinkedList<Sample> samples, QApproximator q, double gamma) {
        LinkedList<Double> residual = new LinkedList<Double>();
        for (Sample s : samples) {
            residual.add((-q.valueAt(s.s, s.action) + gamma * q.valueAt(s.nextState, s.nextAction) + s.reward));
        }
        return fourierBisectionSearch(samples, residual, getRange());
    }

    @Override
    public BasisFunction getMostCorrelated(LinkedList<Sample> samples, VApproximator v, double gamma) {
        LinkedList<Double> residual = new LinkedList<Double>();
        for (Sample s : samples) {
            residual.add((-v.valueAt(s.s) + gamma * v.valueAt(s.nextState) + s.reward));
        }
        //printData(samples, residual);
        LinkedList<Double> basisFunctionValues = getApproximateCorrelations(v, samples, residual);
        BasisFunction bf = fourierBisectionSearchSubtractAlready(v, samples, residual, getRange(), basisFunctionValues);
        //BasisFunction bf = fourierBisectionSearch(samples, residual, getRange());
        System.out.println("mine fejer" + bf.getBasisString());
        //System.out.println("exact " + fourier.getMostCorrelated(samples, v, gamma).getBasisString());
        return bf;
    }

    public void printData(LinkedList<Sample> samples, LinkedList<Double> residual) {
        StringBuilder x = new StringBuilder();
        x.append("x = [");
        for (Sample s : samples) {
            for (double d : s.s.getState()) {
                x.append(d + " ");
            }
            x.append(";");
        }
        x.append("];");
        System.out.println(x);
        StringBuilder y = new StringBuilder();
        y.append("y = [");
        for (double d : residual) {
            y.append(d + ";");
        }
        y.append("];");
        System.out.println(y);
    }

    @Override
    public BasisFunction getMostCorrelated(LinkedList<Sample> samples, QApproximator q, double gamma, int action) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public BasisFunction fourierBisectionSearchSubtractAlready(VApproximator v, LinkedList<Sample> samples, LinkedList<Double> residual, int range[][], LinkedList<Double> basisFunctionValues) {
        //range[0][0] is the minimum frequency in dimension 0 and range[0][1] is the maximum frequency in dimension 0
        for (int i = 0; i < range.length; i++) {
            int min = range[i][0];
            int max = range[i][1];
            while (min < max) {
                int half = (min + max) / 2;
                range[i][0] = min;
                range[i][1] = half;
                double sizeLeft = calcSize(range);
                double left = sum(multiply(multiDimensionCosFejerSum(0, range, samples, residual), residual));
                left = left - getSubtract(v, basisFunctionValues, range);
                left = Math.abs(left);
                left = left / sizeLeft;
                //double left = Math.abs(sum(multiply(multiDimensionCosSum(0, range, samples, residual), residual)));
                //System.out.println("left: " + left);
                range[i][0] = half + 1;
                range[i][1] = max;
                double sizeRight = calcSize(range);
                //double right = Math.abs(sum(multiply(multiDimensionCosSum(0, range, samples, residual), residual)));
                double right = sum(multiply(multiDimensionCosFejerSum(0, range, samples, residual), residual));
                right = right - getSubtract(v, basisFunctionValues, range);
                right = Math.abs(right);
                right = right / sizeRight;
               // System.out.println("Right: " + right);
                if (right > left) {
                    min = half + 1;
                } else {
                    max = half;
                }
            }
            range[i][0] = min;
            range[i][1] = max;
        }
        double frequency[] = new double[range.length];
        String line = "";
        for (int i = 0; i < range.length; i++) {
            frequency[i] = range[i][0];
            line = line + frequency[i] + " ";
        }
        //System.out.println(line);
        FourierBasis bf = new FourierBasis(frequency);
        System.out.println(bf.getBasisString());
        return bf;
    }

    public double getSubtract(VApproximator v, LinkedList<Double> basisFunctionValues, int range[][]) {
        double sub = 0;
        for (int i = 0; i < v.fa.getTerms().length; i++) {
            FourierBasis f = (FourierBasis) (v.fa.getTerms()[i]);
            double[] featureArr = f.featureArr;
            boolean inRange = true;
            for (int j = 0; j < range.length; j++) {
                if (inRange && featureArr[j] >= range[j][0] && featureArr[j] <= range[j][1]) {
                } else {
                    inRange = false;
                }
            }
            if (inRange) {
                sub += basisFunctionValues.get(i);
            }
        }
        return sub;
    }

    public LinkedList<Double> getApproximateCorrelations(VApproximator v, LinkedList<Sample> samples, LinkedList<Double> residual) {
        LinkedList<Double> values = new LinkedList<Double>();
        double weights[] = v.getWeights();
        for (int i = 0; i < v.fa.getTerms().length; i++) {
            FourierBasis bf = (FourierBasis) (v.fa.getTerms()[i]);
            int tempRange[][] = new int[dimensions][2];
            for (int j = 0; j < dimensions; j++) {
                tempRange[j][0] = (int) bf.featureArr[j];
                tempRange[j][1] = (int) bf.featureArr[j];
            }
            double corr = sum(multiply(multiDimensionCosFejerSum(0, tempRange, samples, residual), residual));
            values.add(corr);
        }
        return values;
    }

    public BasisFunction fourierBisectionSearch(LinkedList<Sample> samples, LinkedList<Double> residual, int range[][]) {
        //range[0][0] is the minimum frequency in dimension 0 and range[0][1] is the maximum frequency in dimension 0
        for (int i = 0; i < range.length; i++) {
            int min = range[i][0];
            int max = range[i][1];
            while (min < max) {
                int half = (min + max) / 2;
                range[i][0] = min;
                range[i][1] = half;
                double sizeLeft = calcSize(range);
                double left = Math.abs(sum(multiply(multiDimensionCosFejerSum(0, range, samples, residual), residual)));
                left = left / sizeLeft;
                //double left = Math.abs(sum(multiply(multiDimensionCosSum(0, range, samples, residual), residual)));
                System.out.println("left: " + left);
                range[i][0] = half + 1;
                range[i][1] = max;
                double sizeRight = calcSize(range);
                //double right = Math.abs(sum(multiply(multiDimensionCosSum(0, range, samples, residual), residual)));
                double right = Math.abs(sum(multiply(multiDimensionCosFejerSum(0, range, samples, residual), residual)));
                right = right / sizeRight;
                System.out.println("Right: " + right);
                if (right > left) {
                    min = half + 1;
                } else {
                    max = half;
                }
            }
            range[i][0] = min;
            range[i][1] = max;
        }
        double frequency[] = new double[range.length];
        String line = "";
        for (int i = 0; i < range.length; i++) {
            frequency[i] = range[i][0];
            line = line + frequency[i] + " ";
        }
        //System.out.println(line);
        FourierBasis bf = new FourierBasis(frequency);
        System.out.println(bf.getBasisString());
        return bf;
    }

    public double calcSize(int arr[][]) {
        double size = 1;
        for (int i = 0; i < arr.length; i++) {
            size = size * (arr[i][1] - arr[i][0] + 1);
        }
        return size;
    }

    public LinkedList<Double> multiDimensionCosFejerSum(int startDim, int range[][], LinkedList<Sample> samples, LinkedList<Double> residual) {
        int a = range[startDim][0];
        int b = range[startDim][1];
        if (startDim + 1 == range.length) {
            return sumFejerCos(a, b, samples, startDim);
        } else {
            LinkedList<Double> term1 = multiply(sumFejerCos(a, b, samples, startDim), multiDimensionCosSum(startDim + 1, range, samples, residual));
            LinkedList<Double> term2 = multiply(sumFejerSin(a, b, samples, startDim), multiDimensionSinSum(startDim + 1, range, samples, residual));
            LinkedList<Double> term3 = multiply(sumCos(a, b, samples, startDim), multiDimensionCosFejerSum(startDim + 1, range, samples, residual));
            LinkedList<Double> term4 = multiply(sumSin(a, b, samples, startDim), multiDimensionSinFejerSum(startDim + 1, range, samples, residual));

            return subtract(add(subtract(term1, term2), term3), term4);
        }
    }

    public LinkedList<Double> multiDimensionSinFejerSum(int startDim, int range[][], LinkedList<Sample> samples, LinkedList<Double> residual) {
        int a = range[startDim][0];
        int b = range[startDim][1];
        if (startDim + 1 == range.length) {
            return sumFejerSin(a, b, samples, startDim);
        } else {
            LinkedList<Double> term1 = multiply(sumFejerSin(a, b, samples, startDim), multiDimensionCosSum(startDim + 1, range, samples, residual));
            LinkedList<Double> term2 = multiply(sumFejerCos(a, b, samples, startDim), multiDimensionSinSum(startDim + 1, range, samples, residual));
            LinkedList<Double> term3 = multiply(sumSin(a, b, samples, startDim), multiDimensionCosFejerSum(startDim + 1, range, samples, residual));
            LinkedList<Double> term4 = multiply(sumCos(a, b, samples, startDim), multiDimensionSinFejerSum(startDim + 1, range, samples, residual));

            return add(add(add(term1, term2), term3), term4);
        }
    }

    public LinkedList<Double> sumFejerCos(int start, int end, LinkedList<Sample> samples, int dimension) {
        if (start == end) {
            return multiply(cos(start, samples, dimension), maxFrequency - start + 1);
        } else if (start == 0) {
            return add(fejerCosSumToN(end, samples, dimension), multiply(cosSumToN(end, samples, dimension), maxFrequency - end));
        } else {
            LinkedList<Double> term1 = add(fejerCosSumToN(end, samples, dimension), multiply(cosSumToN(end, samples, dimension), maxFrequency - end));
            LinkedList<Double> term2 = add(fejerCosSumToN(start - 1, samples, dimension), multiply(cosSumToN(start - 1, samples, dimension), maxFrequency - start + 1));
            return subtract(term1, term2);
        }
    }

    public LinkedList<Double> sumFejerSin(int start, int end, LinkedList<Sample> samples, int dimension) {
        if (start == end) {
            return multiply(sin(start, samples, dimension), maxFrequency - start + 1);
        } else if (start == 0) {
            return add(fejerSinSumToN(end, samples, dimension), multiply(sinSumToN(end, samples, dimension), maxFrequency - end));
        } else {
            LinkedList<Double> term1 = add(fejerSinSumToN(end, samples, dimension), multiply(sinSumToN(end, samples, dimension), maxFrequency - end));
            LinkedList<Double> term2 = add(fejerSinSumToN(start - 1, samples, dimension), multiply(sinSumToN(start - 1, samples, dimension), maxFrequency - start + 1));
            return subtract(term1, term2);
        }
    }

    public LinkedList<Double> multiDimensionCosSum(int startDim, int range[][], LinkedList<Sample> samples, LinkedList<Double> residual) {
        int a = range[startDim][0];
        int b = range[startDim][1];
        if (startDim + 1 == range.length) {
            return sumCos(a, b, samples, startDim);
        } else {
            return subtract(multiply(sumCos(a, b, samples, startDim), multiDimensionCosSum(startDim + 1, range, samples, residual)), multiply(sumSin(a, b, samples, startDim), multiDimensionSinSum(startDim + 1, range, samples, residual)));
        }
    }

    public LinkedList<Double> multiDimensionSinSum(int startDim, int range[][], LinkedList<Sample> samples, LinkedList<Double> residual) {
        int a = range[startDim][0];
        int b = range[startDim][1];
        if (startDim + 1 == range.length) {
            return sumSin(a, b, samples, startDim);
        } else {
            return add(multiply(sumSin(a, b, samples, startDim), multiDimensionCosSum(startDim + 1, range, samples, residual)), multiply(sumCos(a, b, samples, startDim), multiDimensionSinSum(startDim + 1, range, samples, residual)));
        }
    }

    public LinkedList<Double> sumCos(int start, int end, LinkedList<Sample> samples, int dimension) {
        if (start == end) {
            return cos(start, samples, dimension);
        } else if (start == 0) {
            return cosSumToN(end, samples, dimension);
        } else {
            return subtract(cosSumToN(end, samples, dimension), cosSumToN(start - 1, samples, dimension));
        }
    }

    public LinkedList<Double> sumSin(int start, int end, LinkedList<Sample> samples, int dimension) {
        if (start == end) {
            return sin(start, samples, dimension);
        } else if (start == 0) {
            return sinSumToN(end, samples, dimension);
        } else {
            return subtract(sinSumToN(end, samples, dimension), sinSumToN(start - 1, samples, dimension));
        }
    }

    public LinkedList<Double> fejerCosSumToN(int end, LinkedList<Sample> samples, int dimension) {
        LinkedList<Double> ans = new LinkedList<Double>();
        for (Sample s : samples) {
            double x = s.s.getState()[dimension];
            if (Math.abs(x) < 0.000000001) {
                ans.add((double) ((end + 1) * (end + 2)) / 2.0);
            } else {
                ans.add(end + 1 - (end + 1) / 2.0 + 0.5 * ((1 - Math.cos((end + 1) * Math.PI * x)) / (1 - Math.cos(Math.PI * x))));
            }
        }

        return ans;
    }

    public LinkedList<Double> fejerSinSumToN(int end, LinkedList<Sample> samples, int dimension) {
        LinkedList<Double> ans = new LinkedList<Double>();
        for (Sample s : samples) {
            double x = s.s.getState()[dimension];
            if (Math.abs(x) < 0.000000001) {
                ans.add((double) 0.0);
            } else {
                ans.add(((end + 1) / 2.0) * ((1.0 / (Math.tan(Math.PI * x / 2.0))) - (Math.sin((end + 1) * Math.PI * x) / ((end + 1) * (1 - Math.cos(Math.PI * x))))));
            }
        }
        //System.out.println("Test " + end + " " + sum(ans));
        return ans;
    }

    public LinkedList<Double> cosSumToN(int end, LinkedList<Sample> samples, int dimension) {
        LinkedList<Double> ans = new LinkedList<Double>();
        for (Sample s : samples) {
            if (Math.abs(s.s.getState()[dimension]) < 0.000000001) {
                ans.add((double) end);
            } else {
                ans.add(0.5 + (Math.sin((end + 0.5) * Math.PI * s.s.getState()[dimension]) / (2 * Math.sin(Math.PI * s.s.getState()[dimension] / 2.0))));
            }
        }
        return ans;
    }

    public LinkedList<Double> sinSumToN(int end, LinkedList<Sample> samples, int dimension) {
        LinkedList<Double> ans = new LinkedList<Double>();
        for (Sample s : samples) {
            if (Math.abs(s.s.getState()[dimension]) < 0.0000001) {
                ans.add((double) Math.sin(s.s.getState()[dimension]));
            } else {
                ans.add(0.5 / Math.tan(Math.PI * s.s.getState()[dimension] / 2) - (Math.cos((end + 0.5) * Math.PI * s.s.getState()[dimension]) / (2 * Math.sin(Math.PI * s.s.getState()[dimension] / 2))));
            }
        }
        return ans;
    }

    public LinkedList<Double> cos(int frequency, LinkedList<Sample> samples, int dimension) {
        LinkedList<Double> ans = new LinkedList<Double>();
        for (Sample s : samples) {
            ans.add(Math.cos(Math.PI * frequency * s.s.getState()[dimension]));
        }
        return ans;
    }

    public LinkedList<Double> sin(int frequency, LinkedList<Sample> samples, int dimension) {
        LinkedList<Double> ans = new LinkedList<Double>();
        for (Sample s : samples) {
            ans.add(Math.sin(Math.PI * frequency * s.s.getState()[dimension]));
        }
        return ans;
    }

    public LinkedList<Double> subtract(LinkedList<Double> from, LinkedList<Double> what) {
        LinkedList<Double> sub = new LinkedList<Double>();
        ListIterator<Double> it = from.listIterator();
        ListIterator<Double> it2 = what.listIterator();
        while (it.hasNext()) {
            sub.add(it.next() - it2.next());
        }
        return sub;
    }

    public LinkedList<Double> add(LinkedList<Double> from, LinkedList<Double> what) {
        LinkedList<Double> sub = new LinkedList<Double>();
        ListIterator<Double> it = from.listIterator();
        ListIterator<Double> it2 = what.listIterator();
        while (it.hasNext()) {
            sub.add(it.next() + it2.next());
        }
        return sub;
    }

    public LinkedList<Double> multiply(LinkedList<Double> from, LinkedList<Double> what) {
        LinkedList<Double> sub = new LinkedList<Double>();
        ListIterator<Double> it = from.listIterator();
        ListIterator<Double> it2 = what.listIterator();
        while (it.hasNext()) {
            sub.add(it.next() * it2.next());
        }
        return sub;
    }

    public LinkedList<Double> multiply(LinkedList<Double> from, double x) {
        LinkedList<Double> sub = new LinkedList<Double>();
        ListIterator<Double> it = from.listIterator();
        while (it.hasNext()) {
            sub.add(it.next() * x);
        }
        return sub;
    }

    public double sum(LinkedList<Double> from) {
        double ans = 0;
        ListIterator<Double> it = from.listIterator();

        while (it.hasNext()) {
            ans += it.next();
        }
        return ans;
    }
}
