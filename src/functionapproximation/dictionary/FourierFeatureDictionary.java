package functionapproximation.dictionary;

import functionapproximation.BasisFunction;
import functionapproximation.FourierBasis;
import functionapproximation.QApproximator;
import functionapproximation.VApproximator;
import java.util.LinkedList;
import rl.Sample;
import rltools.CombinationIterator;

/**
 *
 * @author dean
 */
public class FourierFeatureDictionary extends FeatureDictionary {
    private final int dimensions;
    private final int maxFrequency;
    public FourierFeatureDictionary(int dimensions, int maxFrequency) {
        this.dimensions = dimensions;
        this.maxFrequency = maxFrequency;
    }
    @Override
    public BasisFunction getMostCorrelated(LinkedList<Sample> samples) {
        CombinationIterator it = new CombinationIterator(dimensions, 0, maxFrequency);

        double maxCorrelation = 0;
        BasisFunction mostCorrelated = null;
        while (it.hasNext()) {
            double curr[] = it.next();
            FourierBasis bf = new FourierBasis(curr);
            double bot = 0;
            double top = 0;
            for (Sample s: samples) {
                double eval = bf.getValue(s.s);
                top += eval*s.error;
                bot += eval*eval;
            }
            bot = Math.sqrt(bot);
            double correlation = Math.abs(top/bot);

            if (correlation >= maxCorrelation) {
                mostCorrelated = bf;
                maxCorrelation = correlation;
            }
        }
        return mostCorrelated;
    }

    @Override
    public BasisFunction getMostCorrelated(LinkedList<Sample> samples, QApproximator q, double gamma) {
        CombinationIterator it = new CombinationIterator(dimensions, 0, maxFrequency);
        double maxCorrelation = 0;
        BasisFunction mostCorrelated = null;
        while (it.hasNext()) {
            double curr[] = it.next();
            FourierBasis bf = new FourierBasis(curr);
            double bot = 0;
            double top = 0;
            for (Sample s: samples) {
                double eval = bf.getValue(s.s);
                top += eval*(-q.valueAt(s.s, s.action) + gamma*q.valueAt(s.nextState, s.nextAction) + s.reward);
                bot += eval*eval;
            }
            bot = Math.sqrt(bot);
            double correlation = Math.abs(top/bot);
            if (correlation >= maxCorrelation) {
                mostCorrelated = bf;
                maxCorrelation = correlation;
            }
        }
        return mostCorrelated;
    }

    @Override
    public BasisFunction getMostCorrelated(LinkedList<Sample> samples, VApproximator v, double gamma) {
        CombinationIterator it = new CombinationIterator(dimensions, 0, maxFrequency);
        double maxCorrelation = 0;
        BasisFunction mostCorrelated = null;
        while (it.hasNext()) {
            double curr[] = it.next();
            FourierBasis bf = new FourierBasis(curr);
            double bot = 0;
            double top = 0;
            for (Sample s: samples) {
                double eval = bf.getValue(s.s);
                top += eval*(-v.valueAt(s.s) + gamma*v.valueAt(s.nextState) + s.reward);
                bot += eval*eval;
            }
            bot = Math.sqrt(bot);
            double correlation = Math.abs(top/bot);
            if (correlation >= maxCorrelation) {
                mostCorrelated = bf;
                maxCorrelation = correlation;
            }
        }
        //System.out.println(mostCorrelated.getBasisString());
        return mostCorrelated;
    }

    @Override
    public BasisFunction getMostCorrelated(LinkedList<Sample> samples, QApproximator q, double gamma, int action) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
