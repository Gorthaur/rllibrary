package functionapproximation.dictionary;

import functionapproximation.BasisFunction;
import functionapproximation.QApproximator;
import functionapproximation.VApproximator;
import java.io.Serializable;
import java.util.LinkedList;
import rl.Sample;

/**
 *
 * @author dean
 */
public abstract class FeatureDictionary implements Serializable {

    /**
     * Returns the most correlated basis function in the dictionary that's
     * correlated with the error value in the sample
     *
     * @param samples
     * @return
     */
    public abstract BasisFunction getMostCorrelated(LinkedList<Sample> samples);

    /**
     * Returns the most correlated basis function in the dictionary that's
     * correlated with the current residual using the given QApproximator to
     * find the residual.
     *
     * @param samples
     * @param q
     * @return
     */
    public abstract BasisFunction getMostCorrelated(LinkedList<Sample> samples, QApproximator q, double gamma);

    /**
     * Returns the most correlated basis function in the dictionary that's
     * correlated with the current residual using the given VApproximator to
     * find the residual.
     *
     * @param samples
     * @param v
     * @return
     */
    public abstract BasisFunction getMostCorrelated(LinkedList<Sample> samples, VApproximator v, double gamma);

    public abstract BasisFunction getMostCorrelated(LinkedList<Sample> samples, QApproximator q, double gamma, int action);

    public int getDictionarySize() {
        return 0;
    }

    public boolean remove(BasisFunction d) {
        return false;
    }

    public double[] getTDResidual(LinkedList<Sample> samples, VApproximator v, double gamma) {
        double residual[] = new double[samples.size()];
        int count = 0;
        for (Sample s : samples) {
            residual[count] = -v.valueAt(s.s) + gamma * v.valueAt(s.nextState) + s.reward;
            count++;
        }
        return residual;
    }

    public double[][] getDifferenceResidual(LinkedList<Sample> samples, VApproximator v, double gamma) {
        double residual[][] = new double[samples.size()][v.fa.numFeatures];
        int count = 0;
        for (Sample s : samples) {
            //double diff = -v.valueAt(s.s) + gamma * v.valueAt(s.nextState) + s.reward;
            double diff = (gamma);
            for (int i = 0; i < v.fa.numFeatures; i++) {
                double distance = s.nextState.getState()[i] - s.s.getState()[i];
                if (Math.abs(distance) < 0.0001) {
                    residual[count][i] = 0;
                } else {
                    residual[count][i] = diff / distance;
                }
                //System.out.println(diff + " " + residual[count][i] + " " + s.s.getState()[0] + " " + s.nextState.getState()[0]);
            }
            count++;
        }
        return residual;
    }
    

    public double[] getTDResidual(LinkedList<Sample> samples, QApproximator q, double gamma) {
        double residual[] = new double[samples.size()];
        int count = 0;
        for (Sample s : samples) {
            double maxActionVal = Double.NEGATIVE_INFINITY;
            int maxAction = 0;
            for (int i = 0; i < q.getNumActions(); i++) {
                double val = q.valueAt(s.nextState, i);
                if (val > maxActionVal) {
                    maxAction = i;
                    maxActionVal = val;
                }
            }
            residual[count] = -q.valueAt(s.s, s.action) + gamma * maxActionVal + s.reward;
            count++;
        }
        return residual;
    }
}
