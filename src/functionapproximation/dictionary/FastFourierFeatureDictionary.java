package functionapproximation.dictionary;

import functionapproximation.BasisFunction;
import functionapproximation.FourierBasis;
import functionapproximation.QApproximator;
import functionapproximation.VApproximator;
import java.util.LinkedList;
import java.util.ListIterator;
import rl.Sample;
import rltools.CombinationIterator;

/**
 *
 * @author dean
 */
public class FastFourierFeatureDictionary extends FeatureDictionary {

    private final int dimensions;
    private final int maxFrequency;

    FourierFeatureDictionary fourier;
    BasisFunction lastBf = null;
    double lastCorrelation = 0;

    public FastFourierFeatureDictionary(int dimensions, int maxFrequency) {
        this.dimensions = dimensions;
        this.maxFrequency = maxFrequency;
        fourier = new FourierFeatureDictionary(dimensions, maxFrequency);
    }

    public int[][] getRange() {
        int range[][] = new int[dimensions][2];
        for (int i = 0; i < dimensions; i++) {
            range[i][0] = 0;
            range[i][1] = maxFrequency;
        }
        return range;
    }

    @Override
    public BasisFunction getMostCorrelated(LinkedList<Sample> samples) {
        //the residual error is s.error
        LinkedList<Double> residual = new LinkedList<Double>();
        for (Sample s : samples) {
            residual.add(s.error);
        }
        return fourierBisectionSearch(samples, residual, getRange());
    }

    @Override
    public BasisFunction getMostCorrelated(LinkedList<Sample> samples, QApproximator q, double gamma) {
        LinkedList<Double> residual = new LinkedList<Double>();
        for (Sample s : samples) {
            residual.add((-q.valueAt(s.s, s.action) + gamma * q.valueAt(s.nextState, s.nextAction) + s.reward));
        }
        return fourierBisectionSearch(samples, residual, getRange());
    }

    @Override
    public BasisFunction getMostCorrelated(LinkedList<Sample> samples, VApproximator v, double gamma) {
        //if (lastBf != null) {
        //    System.out.println(lastBf.getBasisString() + " last correlation: " + lastCorrelation + " correlation now: " + getCorrelation(lastBf, samples, v, gamma));
        //}
        LinkedList<Double> residual = new LinkedList<Double>();
        for (Sample s : samples) {
            residual.add((-v.valueAt(s.s) + gamma * v.valueAt(s.nextState) + s.reward));
        }
        //printData(samples, residual);
        LinkedList<Double> basisFunctionCorrelations = getApproximateCorrelations(v, samples, residual);
        BasisFunction bf = fourierBisectionSearchSubtractAlready(v, samples, residual, getRange(), basisFunctionCorrelations);
        //BasisFunction bf = fourierBisectionSearch(samples, residual, getRange());
        System.out.println("mine fourier " + bf.getBasisString());
        //System.out.println("exact " + fourier.getMostCorrelated(samples, v, gamma).getBasisString());
        lastBf = bf;
        lastCorrelation = getCorrelation(bf, samples, v, gamma);

        return bf;
    }

    public double getCorrelation(BasisFunction bf, LinkedList<Sample> samples, VApproximator v, double gamma) {
        double bot = 0;
        double top = 0;
        for (Sample s : samples) {
            double eval = bf.getValue(s.s);
            top += eval * (-v.valueAt(s.s) + gamma * v.valueAt(s.nextState) + s.reward);
            bot += eval * eval;
        }
        bot = Math.sqrt(bot);
        double correlation = Math.abs(top / bot);
        return correlation;
    }

    public void printData(LinkedList<Sample> samples, LinkedList<Double> residual) {
        String x = "x = [";
        for (Sample s : samples) {
            for (double d : s.s.getState()) {
                x = x + d + " ";
            }
            x = x + ";";
        }
        x = x + "];";
        System.out.println(x);
        String y = "y = [";
        for (double d : residual) {
            y = y + d + ";";
        }
        y = y + "];";
        System.out.println(y);
    }

    @Override
    public BasisFunction getMostCorrelated(LinkedList<Sample> samples, QApproximator q, double gamma, int action) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public BasisFunction fourierBisectionSearchSubtractAlready(VApproximator v, LinkedList<Sample> samples, LinkedList<Double> residual, int range[][], LinkedList<Double> basisFunctionValues) {

        //range[0][0] is the minimum frequency in dimension 0 and range[0][1] is the maximum frequency in dimension 0
        for (int i = 0; i < range.length; i++) {
            int min = range[i][0];
            int max = range[i][1];
            while (min < max) {
                int half = (min + max) / 2;
                range[i][0] = min;
                range[i][1] = half;
                double sizeLeft = calcSize(range);
                double left = sum(multiply(multiDimensionCosSum(0, range, samples, residual), residual));
                left = left - getSubtract(v, basisFunctionValues, range);
                left = Math.abs(left);
                left = left / sizeLeft;
                range[i][0] = half + 1;
                range[i][1] = max;
                double sizeRight = calcSize(range);
                double right = sum(multiply(multiDimensionCosSum(0, range, samples, residual), residual));
                right = right - getSubtract(v, basisFunctionValues, range);
                right = Math.abs(right);
                right = right / sizeRight;
                if (right > left) {
                    min = half + 1;
                } else {
                    max = half;
                }
            }
            range[i][0] = min;
            range[i][1] = max;
        }
        double frequency[] = new double[range.length];

        //String line = "";
        for (int i = 0; i < range.length; i++) {
            frequency[i] = range[i][0];
            //line = line + frequency[i] + " ";
        }

        //System.out.println(line);
        FourierBasis bf = new FourierBasis(frequency);
        //System.out.println(bf.getBasisString());
        return bf;
    }

    public double getSubtract(VApproximator v, LinkedList<Double> basisFunctionValues, int range[][]) {
        double sub = 0;
        for (int i = 0; i < v.fa.getTerms().length; i++) {
            FourierBasis f = (FourierBasis) (v.fa.getTerms()[i]);
            double[] featureArr = f.featureArr;
            boolean inRange = true;
            for (int j = 0; j < range.length; j++) {
                if (inRange && featureArr[j] >= range[j][0] && featureArr[j] <= range[j][1]) {
                } else {
                    inRange = false;
                }
            }
            if (inRange) {
                sub += basisFunctionValues.get(i);
            }
        }
        return sub;
    }

    public LinkedList<Double> getApproximateCorrelations(VApproximator v, LinkedList<Sample> samples, LinkedList<Double> residual) {
        LinkedList<Double> values = new LinkedList<Double>();
        double weights[] = v.getWeights();
        for (int i = 0; i < v.fa.getTerms().length; i++) {
            double corr = 0;
            for (int j = 0; j < samples.size(); j++) {
                Sample s = samples.get(j);
                double res = residual.get(j);
                //corr = corr + (v.fa.getTerms()[i].getValue(s.s)*weights[i]*res);
                corr = corr + (v.fa.getTerms()[i].getValue(s.s) * res);
            }
            values.add(corr);
        }
        return values;
    }

    public BasisFunction fourierBisectionSearch(LinkedList<Sample> samples, LinkedList<Double> residual, int range[][]) {
        //range[0][0] is the minimum frequency in dimension 0 and range[0][1] is the maximum frequency in dimension 0
        for (int i = 0; i < range.length; i++) {
            int min = range[i][0];
            int max = range[i][1];
            while (min < max) {
                int half = (min + max) / 2;
                range[i][0] = min;
                range[i][1] = half;
                double sizeLeft = calcSize(range);
                double left = Math.abs(sum(multiply(multiDimensionCosSum(0, range, samples, residual), residual)));
                left = left / sizeLeft;
                range[i][0] = half + 1;
                range[i][1] = max;
                double sizeRight = calcSize(range);
                double right = Math.abs(sum(multiply(multiDimensionCosSum(0, range, samples, residual), residual)));
                right = right / sizeRight;
                if (right > left) {
                    min = half + 1;
                } else {
                    max = half;
                }
            }
            range[i][0] = min;
            range[i][1] = max;
        }
        double frequency[] = new double[range.length];
        String line = "";
        for (int i = 0; i < range.length; i++) {
            frequency[i] = range[i][0];
            line = line + frequency[i] + " ";
        }
        //System.out.println(line);
        FourierBasis bf = new FourierBasis(frequency);
        //System.out.println(bf.getBasisString());
        return bf;
    }

    public double calcSize(int arr[][]) {
        double size = 1;
        for (int i = 0; i < arr.length; i++) {
            size = size * (arr[i][1] - arr[i][0] + 1);
        }
        return size;
    }

    public LinkedList<Double> multiDimensionCosSum(int startDim, int range[][], LinkedList<Sample> samples, LinkedList<Double> residual) {
        int a = range[startDim][0];
        int b = range[startDim][1];
        if (startDim + 1 == range.length) {
            return sumCos(a, b, samples, startDim);
        } else {
            return subtract(multiply(sumCos(a, b, samples, startDim), multiDimensionCosSum(startDim + 1, range, samples, residual)), multiply(sumSin(a, b, samples, startDim), multiDimensionSinSum(startDim + 1, range, samples, residual)));
        }
    }

    public LinkedList<Double> multiDimensionSinSum(int startDim, int range[][], LinkedList<Sample> samples, LinkedList<Double> residual) {
        int a = range[startDim][0];
        int b = range[startDim][1];
        if (startDim + 1 == range.length) {
            return sumSin(a, b, samples, startDim);
        } else {
            return add(multiply(sumSin(a, b, samples, startDim), multiDimensionCosSum(startDim + 1, range, samples, residual)), multiply(sumCos(a, b, samples, startDim), multiDimensionSinSum(startDim + 1, range, samples, residual)));
        }
    }

    public LinkedList<Double> sumCos(int start, int end, LinkedList<Sample> samples, int dimension) {
        if (start == end) {
            return cos(start, samples, dimension);
        } else if (start == 0) {
            return cosSumToN(end, samples, dimension);
        } else {
            return subtract(cosSumToN(end, samples, dimension), cosSumToN(start - 1, samples, dimension));
        }
    }

    public LinkedList<Double> sumSin(int start, int end, LinkedList<Sample> samples, int dimension) {
        if (start == end) {
            return sin(start, samples, dimension);
        } else if (start == 0) {
            return sinSumToN(end, samples, dimension);
        } else {
            return subtract(sinSumToN(end, samples, dimension), sinSumToN(start - 1, samples, dimension));
        }
    }

    public LinkedList<Double> cosSumToN(int end, LinkedList<Sample> samples, int dimension) {
        LinkedList<Double> ans = new LinkedList<Double>();
        for (Sample s : samples) {
            if (Math.abs(s.s.getState()[dimension]) < 0.000000001) {
                ans.add((double) end);
            } else {
                ans.add(0.5 + (Math.sin((end + 0.5) * Math.PI * s.s.getState()[dimension]) / (2 * Math.sin(Math.PI * s.s.getState()[dimension] / 2.0))));
            }
        }
        return ans;
    }

    public LinkedList<Double> sinSumToN(int end, LinkedList<Sample> samples, int dimension) {
        LinkedList<Double> ans = new LinkedList<Double>();
        for (Sample s : samples) {
            if (Math.abs(s.s.getState()[dimension]) < 0.0000001) {
                ans.add((double) Math.sin(s.s.getState()[dimension]));
            } else {
                ans.add(0.5 / Math.tan(Math.PI * s.s.getState()[dimension] / 2) - (Math.cos((end + 0.5) * Math.PI * s.s.getState()[dimension]) / (2 * Math.sin(Math.PI * s.s.getState()[dimension] / 2))));
            }
        }
        return ans;
    }

    public LinkedList<Double> cos(int frequency, LinkedList<Sample> samples, int dimension) {
        LinkedList<Double> ans = new LinkedList<Double>();
        for (Sample s : samples) {
            ans.add(Math.cos(Math.PI * frequency * s.s.getState()[dimension]));
        }
        return ans;
    }

    public LinkedList<Double> sin(int frequency, LinkedList<Sample> samples, int dimension) {
        LinkedList<Double> ans = new LinkedList<Double>();
        for (Sample s : samples) {
            ans.add(Math.sin(Math.PI * frequency * s.s.getState()[dimension]));
        }
        return ans;
    }

    public LinkedList<Double> subtract(LinkedList<Double> from, LinkedList<Double> what) {
        LinkedList<Double> sub = new LinkedList<Double>();
        ListIterator<Double> it = from.listIterator();
        ListIterator<Double> it2 = what.listIterator();
        while (it.hasNext()) {
            sub.add(it.next() - it2.next());
        }
        return sub;
    }

    public LinkedList<Double> add(LinkedList<Double> from, LinkedList<Double> what) {
        LinkedList<Double> sub = new LinkedList<Double>();
        ListIterator<Double> it = from.listIterator();
        ListIterator<Double> it2 = what.listIterator();
        while (it.hasNext()) {
            sub.add(it.next() + it2.next());
        }
        return sub;
    }

    public LinkedList<Double> multiply(LinkedList<Double> from, LinkedList<Double> what) {
        LinkedList<Double> sub = new LinkedList<Double>();
        ListIterator<Double> it = from.listIterator();
        ListIterator<Double> it2 = what.listIterator();
        while (it.hasNext()) {
            sub.add(it.next() * it2.next());
        }
        return sub;
    }

    public double sum(LinkedList<Double> from) {
        double ans = 0;
        ListIterator<Double> it = from.listIterator();

        while (it.hasNext()) {
            ans += it.next();
        }
        return ans;
    }
}
