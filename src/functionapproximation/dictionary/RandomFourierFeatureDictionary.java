package functionapproximation.dictionary;

import functionapproximation.BasisFunction;
import functionapproximation.FourierBasis;
import functionapproximation.QApproximator;
import functionapproximation.VApproximator;
import java.util.LinkedList;
import rl.Sample;
import rltools.CombinationIterator;

/**
 *
 * @author dean
 */
public class RandomFourierFeatureDictionary extends FeatureDictionary {
    private final int dimensions;
    private final int maxFrequency;
    public RandomFourierFeatureDictionary(int dimensions, int maxFrequency) {
        this.dimensions = dimensions;
        this.maxFrequency = maxFrequency;
    }
    @Override
    public BasisFunction getMostCorrelated(LinkedList<Sample> samples) {
        CombinationIterator it = new CombinationIterator(dimensions, 0, maxFrequency);
        double randNum = Math.random() * it.getNumCombinations();
        BasisFunction mostCorrelated = null;
        for (int i = 0; i <= randNum; i++) {
            double curr[] = it.next();
            FourierBasis bf = new FourierBasis(curr);
            mostCorrelated = bf;
        }
        return mostCorrelated;
    }

    @Override
    public BasisFunction getMostCorrelated(LinkedList<Sample> samples, QApproximator q, double gamma) {
        CombinationIterator it = new CombinationIterator(dimensions, 0, maxFrequency);
        double randNum = Math.random() * it.getNumCombinations();
        BasisFunction mostCorrelated = null;
        for (int i = 0; i <= randNum; i++) {
            double curr[] = it.next();
            FourierBasis bf = new FourierBasis(curr);
            mostCorrelated = bf;
        }
        return mostCorrelated;
    }

    @Override
    public BasisFunction getMostCorrelated(LinkedList<Sample> samples, VApproximator v, double gamma) {
        CombinationIterator it = new CombinationIterator(dimensions, 0, maxFrequency);
        double randNum = Math.random() * it.getNumCombinations();
        BasisFunction mostCorrelated = null;
        for (int i = 0; i <= randNum; i++) {
            double curr[] = it.next();
            FourierBasis bf = new FourierBasis(curr);
            mostCorrelated = bf;
        }
        return mostCorrelated;
    }

    @Override
    public BasisFunction getMostCorrelated(LinkedList<Sample> samples, QApproximator q, double gamma, int action) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
