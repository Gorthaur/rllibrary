package functionapproximation.dictionary;

import functionapproximation.BasisFunction;
import functionapproximation.VApproximator;
import java.util.LinkedList;

/**
 *
 * @author Dean
 */
import rl.Sample;
public class OMPPaperDictionary extends GeneralFeatureDictionary {
        @Override
    public BasisFunction getMostCorrelated(LinkedList<Sample> samples, VApproximator v, double gamma) {
        double maxCorrelation = 0;
        BasisFunction mostCorrelated = null;
        for (BasisFunction bf : dictionary) {
            double bot = 0;
            double top = 0;
            for (Sample s : samples) {
                double eval = bf.getValue(s.s);
                top += eval * (-v.valueAt(s.s) + gamma * v.valueAt(s.nextState) + s.reward);
                bot += eval * eval;
            }
            bot = Math.sqrt(bot);
            double correlation = Math.abs(top);
            if (correlation >= maxCorrelation) {
                mostCorrelated = bf;
                maxCorrelation = correlation;
            }
        }
        return mostCorrelated;
    }
}
