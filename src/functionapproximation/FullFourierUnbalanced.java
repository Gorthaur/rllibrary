package functionapproximation;

public class FullFourierUnbalanced extends FunctionApproximator {
  int order[];
  public FullFourierUnbalanced(int order[], int numFeatures) {
    super(calculateNumTerms(order,numFeatures), numFeatures);
    this.order = order;
    initialiseTerms();
  }
  
 
  public static int calculateNumTerms(int order[], int numFeatures) {
      int total = 1;
      for (int i = 0; i < order.length; i++) {
          total *= (order[i]+1);
      }
    return total;
  }
  
  public void initialiseTerms() {
    super.initialiseTerms();
    BasisFunction terms[] = getTerms();
    double c[] = new double[numFeatures];
    terms[0] = new FourierBasis(c.clone());
    int pos = 1;
    iterate(c, numFeatures, order);
    while (c[0] < order[0] + 1) {
      terms[pos] = new FourierBasis(c.clone());
      pos++;
      iterate(c, numFeatures, order);
    }
  }
  
  public void iterate(double [] c, int NVariables, int Degree[])
  {
    (c[NVariables - 1])++;

    if(c[NVariables - 1] > Degree[NVariables - 1])
    {
      if(NVariables > 1)
      {
        c[NVariables - 1]  = 0;
        iterate(c, NVariables - 1, Degree);
      }
    }
  }
  
  public int[] getOrder() {
    return order;
  }
  
  public void printArray(double arr[]) {
    String str = "";
    for (int i = 0; i < arr.length; i++) {
      str = str + arr[i] + " ";
    }
    System.out.println(str);
  }
}
