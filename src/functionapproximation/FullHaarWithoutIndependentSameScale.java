package functionapproximation;

import java.util.ArrayList;

/**
 * A full tiling of haar wavelets and scaling functions up to some order
 * doesn't work yet.
 * @author Dean
 */
public class FullHaarWithoutIndependentSameScale extends FunctionApproximator {

    private final int maxScale;
    private final int baseScale;
    private final int dimensions;

    public FullHaarWithoutIndependentSameScale(int baseScale, int maxScale, int dimensions) {
        super(calculateNumTerms(baseScale, maxScale, dimensions), dimensions);
        this.maxScale = maxScale;
        this.baseScale = baseScale;
        this.dimensions = dimensions;
        initialiseTerms();
    }

    public static int calculateNumTerms(int baseScale, int maxScale, int dimensions) {

        int num = (int) Math.pow((int) Math.pow(2, baseScale), dimensions);
        int num2 = 0;
        for (int i = baseScale; i <= maxScale; i++) {
            num2 += Math.pow(Math.pow(2, i), dimensions);
        }
        num += num2;
        return num;
    }

    public void initialiseTerms() {
        super.initialiseTerms();
        BasisFunction terms[] = getTerms();
        BasisFunction tempTerms[] = new BasisFunction[FullHaarFunctionApproximation.calculateNumTerms(baseScale, maxScale, dimensions)];
        System.out.println(terms.length);


        //intialise scaling functions
        ArrayList<BasisFunction> bfBuilder = new ArrayList<BasisFunction>();
        for (int i = dimensions - 1; i >= 0; i--) { //just go backwards. gives a nice order in the array this way.
            ArrayList<BasisFunction> bfBuilder2 = new ArrayList<BasisFunction>();
            for (int j = 0; j < Math.pow(2, baseScale); j++) {
                HaarScalingFunction h = new HaarScalingFunction(baseScale, j, i); //scale translation dimension
                for (BasisFunction b : bfBuilder) { //add all combinations in
                    bfBuilder2.add(new CombinationBasis(h, (WiFDDBasisFunction)b));
                }
                if (bfBuilder.isEmpty()) {
                    bfBuilder2.add(h);
                }
            }
            bfBuilder = bfBuilder2;
        }
        int pos = 0;
        for (BasisFunction b : bfBuilder) {
            terms[pos] = b;
            pos++;
        }

        for (int l = baseScale; l <= maxScale; l++) { //go through all scales

            bfBuilder = new ArrayList<BasisFunction>();
            for (int i = dimensions - 1; i >= 0; i--) { //just go backwards. gives a nice order in the array this way.
                ArrayList<BasisFunction> bfBuilder2 = new ArrayList<BasisFunction>();
                for (int j = 0; j < Math.pow(2, l); j++) { // go through all translations
                    HaarWavelet h = new HaarWavelet(l, j, i);
                    for (BasisFunction b : bfBuilder) { //add all combinations in
                        bfBuilder2.add(new CombinationBasis(h, (WiFDDBasisFunction)b));
                    }
                    if (bfBuilder.isEmpty()) {
                        bfBuilder2.add(h);
                    }
                }
                bfBuilder = bfBuilder2;
            }
            for (BasisFunction b : bfBuilder) {
                terms[pos] = b;
                pos++;
            }

        }
        System.out.println(bfBuilder.size());


        System.out.println(pos);
        System.out.println(terms.length);
        for (int i = 0; i < terms.length; i++) {
            System.out.println(terms[i].getBasisString());
        }

    }
}
