package functionapproximation;

import java.util.Vector;
import rl.state.State;


/**
 *
 * @author dean
 */
public class IndependentTiling extends FunctionApproximator implements iFFDFunctionApproximator {

    int tiles = 0;

    public IndependentTiling(int tiles, int numFeatures) {
        super(calculateNumTerms(tiles, numFeatures), numFeatures);
        this.tiles = tiles;
        initialiseTerms();
    }

    public static int calculateNumTerms(int tiles, int numFeatures) {
        return tiles * numFeatures;
    }

    public void initialiseTerms() {
        super.initialiseTerms();
        BasisFunction terms[] = getTerms();

        int pos = 0;
        for (int i = 0; i < getNumFeatures(); i++) {
            double previous = 0;

            for (int j = 0; j < tiles; j++) {
                double term[][] = new double[this.getNumFeatures()][2];
                for (int k = 0; k < this.getNumFeatures(); k++) {
                    if (k != i) {
                        term[k][0] = 0;
                        term[k][1] = 1;
                    } else {
                        term[k][0] = previous;
                        term[k][1] = ((double) (j + 1)) / tiles;
                        previous = term[k][1];
                    }
                }
                TileBasis t = new TileBasis(term);
                terms[pos] = t;
                pos++;
            }
        }
    }

    @Override
    public Vector<iFFDBasis> getActiveBasisFunctions(State s) {
        Vector<iFFDBasis> active = new Vector<iFFDBasis>();
        for (int i = 0; i < terms.length; i++) {
            if (terms[i].getValue(s) == 1) {
                active.add((iFFDBasis)terms[i]);
            }
        }
        return active;
    }
}
