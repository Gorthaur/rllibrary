package functionapproximation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Vector;
import rl.state.State;

/**
 *
 * @author dean
 */
public class TileBasis implements BasisFunction, iFFDBasis<TileBasis>, WiFDDBasisFunction {

    public double coverage[][];
    Vector<TileBasis> children;
    double shrink = 1;
    double area = 0;
    ArrayList<Integer> dimensions;
    double activation = 1;
    private int arrayIndex;

    public TileBasis(double coverage[][]) {
        this.coverage = coverage; //nx2 array where for each state k, the coverage[k][0] is the min and coverage[k][1] is the max value it activates on.
        for (int i = 0; i < coverage.length; i++) {
            if (coverage[i][0] != 0 || coverage[i][1] != 1) {
                area += 1;
            }
        }
        //shrink = -0.3312*area + 1.3312;
        children = new Vector<TileBasis>();
    }

    @Override
    public double getValue(State s) {
        if (s == null) {
            return 0;
        }
        /*
        for (TileBasis c : children) {
            if (c.getValue(s) == 1) {
               return 0; //deactivate this basis function if its children are active
            }
        }
         * */
        double stateVector[] = s.getState();
        for (int i = 0; i < stateVector.length; i++) {
            if (!(stateVector[i] >= coverage[i][0] && stateVector[i] < coverage[i][1])) {
                if (stateVector[i] >= 1 && coverage[i][1] >= 1) {
                    continue; //if it's greater than 1, then it does belong to this tile
                }
                return 0;
            }
            
        }

        return 1;
    }

    @Override
    public double getShrink() {
        return shrink;
    }
            @Override
    public int getArrayIndex() {
        return this.arrayIndex;
    }

    @Override
    public void setArrayIndex(int index) {
        this.arrayIndex = index;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof TileBasis) {
            TileBasis t = (TileBasis) o;
            for (int i = 0; i < coverage.length; i++) {
                if (coverage[i][0] != t.coverage[i][0]) {
                            
                    return false;
                } else if (coverage[i][1] != t.coverage[i][1]) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Arrays.deepHashCode(this.coverage);
        return hash;
    }

    @Override
    public TileBasis combine(TileBasis t) {
        double newCoverage[][] = new double[coverage.length][2];
        for (int i = 0; i < coverage.length; i++) {
            newCoverage[i][0] = Math.max(coverage[i][0], t.coverage[i][0]);
            newCoverage[i][1] = Math.min(coverage[i][1], t.coverage[i][1]);
            if (newCoverage[i][0] >= newCoverage[i][1]) {
                return null; //they don't overlap
            }
        }
        TileBasis nt = new TileBasis(newCoverage);
        if (children.contains(nt)) {
            nt = children.get(children.lastIndexOf(nt));
        }
        return nt;
    }

    @Override
    public void addChild(TileBasis child) {
        if (!hasChild(child)) {
            children.add(child);
        }
    }

    @Override
    public boolean hasChild(TileBasis child) {
        if (children.contains(child)) {
            return true;
        } else {
            return false;
        }
    }

    public void print() {

        System.out.println(getBasisString());
    }

    @Override
    public String getBasisString() {
        String output = "";
        for (int i = 0; i < coverage.length; i++) {
            output += "(" + coverage[i][0] + "," + coverage[i][1] + ")";
        }
        return output;
    }

    @Override
    public ArrayList<Integer> getDimensions() {
        if (dimensions != null) {
            return dimensions;
        }
        else {
            dimensions = new ArrayList<Integer>();
            for (int i = 0; i < coverage.length; i++) {
                if (coverage[i][0] != 0.0 || coverage[i][1] != 1.0) {
                    dimensions.add(i);
                }
            }
            return dimensions;
        }
    }

    @Override
    public double getActivation() {
        return activation;
    }

    @Override
    public void setActivation(double activation) {
        this.activation = activation;
    }

    @Override
    public void decreaseActivation(double amount) {
        this.activation = this.activation - amount;
        this.activation = Math.max(0, activation);
    }
}
