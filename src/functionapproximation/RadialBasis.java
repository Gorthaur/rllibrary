package functionapproximation;

import rl.state.State;


public class RadialBasis implements BasisFunction {
  private double featureArr[];
  private double variance;
  private double shrink;
  private double normalisation;
  public RadialBasis(double featureArr[], double variancesquared) {
    this.featureArr = featureArr;
    this.variance = variancesquared;
    this.shrink = Math.sqrt(1/variancesquared);
    this.normalisation = Math.pow(Math.PI, featureArr.length/4.0) * Math.pow(variancesquared, featureArr.length/4.0); //because variance squared.
  }
  
  public double getValue(State s) {
    //FourierState p = (FourierState)s;
    double state[] = s.getState();
    double val = 0;
    int dimcount = 0;
    for (int i = 0; i < featureArr.length; i++) {
      if (featureArr[i] == -1) {
        continue; //ignore all dimensions with -1
      }
      dimcount++;
      val+= Math.pow(state[i] - featureArr[i],2);
    }
    double top = Math.exp(val / (-2*variance));
    double bottom = normalisation;
    double returnVal = top / bottom;
    return returnVal;
  }
  
    @Override
  public double getShrink() {
    return shrink;
  }
  
  public void printArr(double arr[]) {
    String result = "";
    for (int i = 0; i < arr.length; i++) {
      result += arr[i] + " ";
    }
    System.out.println(result);
  }

    @Override
    public String getBasisString() {
        String str = "";
        for (int i = 0; i < featureArr.length; i++) {
            str = str + featureArr[i] + " ";
        }
        str = "(" + str.trim() + ")";
        return str;
    }
}
