package functionapproximation;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import rl.state.State;

/**
 *
 * @author Dean
 */
public class TensorWavelet  implements BasisFunction {

    private final Wavelet[] independentWavelets;

    public TensorWavelet(Wavelet... independentWavelets) {
        this.independentWavelets = independentWavelets;
        Arrays.sort(independentWavelets);
    }

    public List<TensorWavelet> getChildren() {
        LinkedList<TensorWavelet> children = new LinkedList<>();
        int i = 0;
        for (Wavelet w : independentWavelets) {
            List<Wavelet> childList = w.getChildren();
            for (Wavelet c : childList) {
                Wavelet waveletArray[] = Arrays.copyOf(independentWavelets, independentWavelets.length);
                waveletArray[i] = c;
                children.add(new TensorWavelet(waveletArray));
            }
            i++;
        }
        return children;
    }

    @Override
    public double getValue(State s) {
        double value = 1;
        for (Wavelet w : independentWavelets) {
            value *= w.getValue(s);
        }
        return value;
    }

    @Override
    public double getShrink() {
        double total = 0;
        for (Wavelet w : independentWavelets) {
            total += w.getShrink();
        }
        return total / independentWavelets.length;
    }

    @Override
    public String getBasisString() {
        StringBuilder b = new StringBuilder();
        for (Wavelet w : independentWavelets) {
            b.append(w.getBasisString()).append(System.lineSeparator());
        }
        return b.toString();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Arrays.deepHashCode(this.independentWavelets);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TensorWavelet other = (TensorWavelet) obj;
        if (!Arrays.deepEquals(this.independentWavelets, other.independentWavelets)) {
            return false;
        }
        return true;
    }


}
