
package functionapproximation;

/**
 *
 * @author Dean
 */
public class HaarFatherWavelet extends FatherWavelet<SimpleHaarWavelet> {

    public HaarFatherWavelet(int scale, int shift, int dimension) {
        super(scale, shift, dimension);
    }

    @Override
    public SimpleHaarWavelet createChild(int scale, int shift, int dimension) {
        return new SimpleHaarWavelet(scale, shift, dimension);
    }

    @Override
    public double getValue(double t) {
        if (t <1 && t >= 0) {
            return 1;
        }
        else {
            return 0;
        }
    }

    @Override
    public double getNormalisation(int scale) {
        return 1;
    }

    @Override
    public double getShrink() {
        return 1 + pow;
    }

    @Override
    public String getBasisString() {
        return String.format("Haar Father %d %d %d", this.dimension, this.scale, this.shift);
    }

        @Override
    public boolean equals(Object obj) {
        return super.equals(obj) && obj instanceof HaarFatherWavelet; 
    }


    @Override
    public int hashCode() {
        int hash = super.hashCode()*12 + 5;
        return hash;
    }

    
}
