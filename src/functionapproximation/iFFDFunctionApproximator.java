package functionapproximation;

import java.util.Vector;
import rl.state.State;

/**
 *
 * @author dean
 */
public interface iFFDFunctionApproximator {
   public Vector<iFFDBasis> getActiveBasisFunctions(State s);
}
