/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package functionapproximation;

import java.util.Comparator;

/**
 *
 * @author Dean
 */
public class NormComparator implements Comparator<BasisFunction> {

    @Override
    public int compare(BasisFunction o1, BasisFunction o2) {
        FourierBasis fb = (FourierBasis) o1;
        FourierBasis fb2 = (FourierBasis) o2;
        if (fb.shrink > fb2.shrink) {
            return 1;
        } else if (fb.shrink < fb2.shrink) {
            return -1;
        } else {
            return 0;
        }
    }
}
