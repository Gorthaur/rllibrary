package functionapproximation;

import java.util.ArrayList;


public class FullDaubWithoutIndependent extends FunctionApproximator {

    private final int maxScale;
    private final int baseScale;
    private final int dimensions;
    private final int order;

    public FullDaubWithoutIndependent(int baseScale, int maxScale, int dimensions, int order) {
        super(calculateNumTerms(baseScale, maxScale, dimensions, order), dimensions);
        this.maxScale = maxScale;
        this.baseScale = baseScale;
        this.dimensions = dimensions;
        this.order = order;
        initialiseTerms();
    }

    public static int calculateNumTerms(int baseScale, int maxScale, int dimensions, int order) {
        int numPhi = (int)Math.pow(2,baseScale) + order - 2;
        if (baseScale > maxScale) return (int)Math.pow(numPhi, dimensions);
        int numPsi = (int)Math.pow(2, maxScale+1) + (maxScale-baseScale+2)*(order-2) - numPhi;
        return (int) Math.pow(numPsi,dimensions) + (int)Math.pow(numPhi, dimensions);
    
    }

    public void initialiseTerms() {
        super.initialiseTerms();
        BasisFunction terms[] = getTerms();
        BasisFunction tempTerms[] = new BasisFunction[FullDaubFunctionApproximation.calculateNumTerms(baseScale, maxScale, dimensions, order)];

        //intialise scaling functions
        ArrayList<BasisFunction> bfBuilder = new ArrayList<BasisFunction>();
        for (int i = dimensions-1; i >= 0; i--) { //just go backwards. gives a nice order in the array this way.
            ArrayList<BasisFunction> bfBuilder2 = new ArrayList<BasisFunction>();
            for (int j = -order+2; j < Math.pow(2, baseScale); j++) {
                DaubNScalingFunction h = new DaubNScalingFunction(baseScale, j, i, order); //scale translation dimension
                for (BasisFunction b : bfBuilder) { //add all combinations in
                    bfBuilder2.add(new CombinationBasis(h,(WiFDDBasisFunction)b));
                }
                if (bfBuilder.isEmpty()) {
                    bfBuilder2.add(h);
                }
            }
            bfBuilder = bfBuilder2;
        }
        int pos = 0;
        for (BasisFunction b : bfBuilder) {
            terms[pos] = b;
            pos++;
        }
        if (baseScale <= maxScale) {
        bfBuilder = new ArrayList<BasisFunction>();
        for (int i = dimensions-1; i >= 0; i--) { //just go backwards. gives a nice order in the array this way.
            ArrayList<BasisFunction> bfBuilder2 = new ArrayList<BasisFunction>();
            for (int l = baseScale; l <= maxScale; l++) { //go through all scales
                for (int j = -order+2; j < Math.pow(2, l); j++) { // go through all translations
                    DaubNWavelet h = new DaubNWavelet(l, j, i, order);
                    for (BasisFunction b : bfBuilder) { //add all combinations in
                        bfBuilder2.add(new CombinationBasis(h,(WiFDDBasisFunction)b));
                    }
                    if (bfBuilder.isEmpty()) {
                        bfBuilder2.add(h);
                    }
                }
            }
            bfBuilder = bfBuilder2;
        }
        for (BasisFunction b : bfBuilder) {
            terms[pos] = b;
            pos++;
        }
    }
    }
}
