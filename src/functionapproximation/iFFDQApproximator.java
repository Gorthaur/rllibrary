package functionapproximation;

import java.util.Vector;
import rl.state.State;

/**
 *
 * @author dean
 */
public class iFFDQApproximator extends QApproximator {

    public iFFDQApproximator(IndependentTiling FAs[]) {
        super((FunctionApproximator[]) FAs);
    }

    public void addBasis(PotentialFeature bf) {
        for (int i = 0; i < FAs.length; i++) {
            addBasis(bf, i);
        }
    }

    public void addBasis(PotentialFeature f, int fa) {
        int previousTerms = FAs[fa].getNumTerms();
        FAs[fa].addBasis((BasisFunction) (f.bf));
        double newWeights[] = new double[getNumTerms()];
        double newTraces[][] = new double[traces.size()][getNumTerms()];
        int pos = 0; //position in total weight array
        int newpos = 0; //position in new weight array
        
        for (int i = 0; i < FAs.length; i++) {
            double newWeight = 0;
            for (int j = 0; j < FAs[i].getNumTerms(); j++) {
                if (i == fa && (FAs[fa].getNumTerms()-1) == j) {
                    newWeights[newpos] = newWeight; //combined total
                    for (int k = 0; k < traces.size(); k++) {
                        newTraces[k][newpos] = 0;
                    }
                } else {
                    if (f.parent1.equals(FAs[i].terms[j])) {
                        newWeight += weights[pos];
                        ((iFFDBasis) FAs[i].terms[j]).addChild(f.bf);
                    } else if (f.parent2.equals(FAs[i].terms[j])) {
                        newWeight += weights[pos];
                        ((iFFDBasis) FAs[i].terms[j]).addChild(f.bf);
                    }
                    newWeights[newpos] = weights[pos];
                    for (int k = 0; k < traces.size(); k++) {
                        newTraces[k][newpos] = traces.get(k).getTrace()[pos];
                    }
                    pos++;
                }
                newpos++;
            }
        }
        weights = newWeights;
        for (int i = 0; i < traces.size(); i++) {
            traces.get(i).setTrace(newTraces[i]);
        }
    }

    public Vector<iFFDBasis> getActiveBasisFunctions(State s) {
        return ((iFFDFunctionApproximator) FAs[0]).getActiveBasisFunctions(s);
    }
}
