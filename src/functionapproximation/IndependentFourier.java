package functionapproximation;


public class IndependentFourier extends FunctionApproximator {
  int order;
  
  public IndependentFourier(int order, int numFeatures) {
    super(calculateNumTerms(order,numFeatures), numFeatures);
    this.order = order;
    initialiseTerms();
  }
  
 
  public static int calculateNumTerms(int order, int numFeatures) {
    return order*numFeatures + 1;
  }
  
  public void initialiseTerms() {
    super.initialiseTerms();
    BasisFunction terms[] = getTerms();
    terms[0] = new FourierBasis(new double[this.getNumFeatures()]);
    int pos = 1;
    for (int i = 0; i < getNumFeatures(); i++) {
      for (int j = 1; j <= order; j++) {
        double term[] = new double[this.getNumFeatures()];
        term[i] = j;
        terms[pos] = new FourierBasis(term);
        pos++;
      }
    }
  }
  
  public int getOrder() {
    return order;
  }
  
  public void printArray(double arr[]) {
    String str = "";
    for (int i = 0; i < arr.length; i++) {
      str = str + arr[i] + " ";
    }
    System.out.println(str);
  }
}
