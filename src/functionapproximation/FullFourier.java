package functionapproximation;

public class FullFourier extends FunctionApproximator {
  int order;
  
  public FullFourier(int order, int numFeatures) {
    super(calculateNumTerms(order,numFeatures), numFeatures);
    this.order = order;
    initialiseTerms();
  }
  
 
  public static int calculateNumTerms(int order, int numFeatures) {
    return (int)Math.pow(order + 1, numFeatures);
  }
  
  public void initialiseTerms() {
    super.initialiseTerms();
    BasisFunction terms[] = getTerms();
    double c[] = new double[numFeatures];
    terms[0] = new FourierBasis(c.clone());
    int pos = 1;
    iterate(c, numFeatures, order);
    while (c[0] < order+1) {
      terms[pos] = new FourierBasis(c.clone());
      pos++;
      iterate(c, numFeatures, order);
    }
  }
  
  public void iterate(double [] c, int NVariables, int Degree)
  {
    (c[NVariables - 1])++;

    if(c[NVariables - 1] > Degree)
    {
      if(NVariables > 1)
      {
        c[NVariables - 1]  = 0;
        iterate(c, NVariables - 1, Degree);
      }
    }
  }
  
  public int getOrder() {
    return order;
  }
  
  public void printArray(double arr[]) {
    String str = "";
    for (int i = 0; i < arr.length; i++) {
      str = str + arr[i] + " ";
    }
    System.out.println(str);
  }
}
