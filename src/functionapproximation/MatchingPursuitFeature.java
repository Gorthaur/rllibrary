/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package functionapproximation;

/**
 *
 * @author Dean
 */
public class MatchingPursuitFeature implements Comparable<MatchingPursuitFeature> {
    BasisFunction bf;
    double weight;
    double potential;
    public MatchingPursuitFeature(BasisFunction bf) {
        this.bf = bf;
    }
    
    public BasisFunction getBf() {
        return bf;
    }
    
    public double getPotential() {
        return potential;
    }
    
    public void setPotential(double potential) {
        this.potential = Math.abs(potential);
    }
    
    public void setWeight(double weight) {
        this.weight = weight;
    }
    
    public double getWeight() {
        return this.weight;
    }

    @Override
    public int compareTo(MatchingPursuitFeature o) {
        if (this.potential > o.potential) {
            return -1;
        }
        else if (this.potential < o.potential) {
            return 1;
        }
        else {
            return 0;
        }
    }
    
}
