package functionapproximation;

import java.io.Serializable;
import rl.state.State;

public class FourierBasis implements BasisFunction{

    public double featureArr[];
    public double shrink = 1;
    public double decayShrink = 0;
    static final long serialVersionUID = -4372949038505027760l;

    public FourierBasis(double featureArr[]) {
        this.featureArr = featureArr.clone();
        setShrink();
    }

    public double getValue(State s) {
        //FourierState p = (FourierState)s;
        double state[] = s.getState();
        double val = 0;
        for (int i = 0; i < featureArr.length; i++) {
            val += state[i] * featureArr[i];
        }
        double returnVal = Math.cos((Math.PI) * val);
        return returnVal;
    }

    public void setShrink() {
        double shrink = 0;
        for (int i = 0; i < featureArr.length; i++) {
            shrink += Math.pow(featureArr[i], 2);
        }
        if (shrink == 0) {
            this.shrink = 1;
            return;
        }
        //shrink = 1;
        this.shrink = Math.sqrt(shrink);
        //this.shrink = shrink;
    }

    public double getShrink() {
        decayShrink = decayShrink*0.95;
        return shrink + decayShrink;
    }

    public void setShrink(double shrink) {
        this.shrink = shrink;
    }
    
    public void setDecayShrink(double shrink) {
        this.decayShrink = shrink;
    }

    public void printBasis() {
        String str = "";
        for (int i = 0; i < featureArr.length; i++) {
            str += featureArr[i] + " ";
        }
        System.out.println(str);
    }

    @Override
    public String getBasisString() {
        String str = "new FourierBasis(new double[]{";
        for (int i = 0; i < featureArr.length; i++) {
            str += featureArr[i] + ", ";
        }
        return str.trim() + "})";
    }

    @Override
    public String toString() {
        return getBasisString();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof FourierBasis) {
            FourierBasis b = (FourierBasis) o;
            if (b.featureArr.length == this.featureArr.length) {
                for (int i = 0; i < b.featureArr.length; i++) {
                    if (featureArr[i] != b.featureArr[i]) {
                        return false;
                    }
                }
                return true;
            }
            return false;
        } else {
            return false;
        }


    }
}
