package functionapproximation;

import java.util.ArrayList;
import rl.state.State;

/**
 * @author Dean
 */
public class WiFDDQApproximator extends QApproximator {
    //private final FunctionApproximator FAs[] FAs;

    public WiFDDQApproximator(FunctionApproximator FAs[]) {
        super(FAs);
    }

    public void addBasis(BasisFunction bf, int fa, double weight) {
        super.addBasis(bf, fa, weight);
        int pos = 0;
        for (int i = 0; i < getNumActions(); i++) {
            BasisFunction terms[] = FAs[i].getTerms();
            for (int k = 0; k < terms.length; k++) {
                ((WiFDDBasisFunction) terms[k]).setArrayIndex(pos);
                pos++;
            }
        }
    }

    @Override
    public double[] calculateStateValue(State s, int action) {
        double returnArray[] = new double[getNumTerms()];
        BasisFunction terms[] = FAs[action].getTerms();
        for (BasisFunction b : terms) {
            ((WiFDDBasisFunction) b).setActivation(1);
        }
        double answer[] = FAs[action].calculateStateValue(s);
        int copyPosition = getArrayStartPosition(action);
        for (int i = 0; i < FAs[action].getNumTerms(); i++) {
            returnArray[i + copyPosition] = answer[i] * (((WiFDDBasisFunction) (FAs[action].getTerms()[i])).getActivation());
            //((WiFDDBasisFunction) (FAs[action].getTerms()[i])).setActivation(1);
        }
        return returnArray;
    }
    /*
    public void deactivateAll(WiFDDBasisFunction b) {
        if (b instanceof CombinationBasis) {
            CombinationBasis cb = (CombinationBasis)b;
            ArrayList<BasisFunction> bfs = cb.getBFs();
        }
    }
     * 
     */

    @Override
    public double[] calculateStateValue(State s) {
        double returnArray[] = new double[getNumTerms()];
        int pos = 0;

        for (int i = 0; i < getNumActions(); i++) {
            System.out.println("action");
            BasisFunction terms[] = FAs[i].getTerms();
            for (BasisFunction b : terms) {
                ((WiFDDBasisFunction) b).setActivation(1);
            }
            double answer[] = FAs[i].calculateStateValue(s);
            for (int k = 0; k < answer.length; k++) {
                returnArray[pos] = answer[k] * (((WiFDDBasisFunction) (FAs[i].getTerms()[k])).getActivation());
                System.out.println("Vals: " + returnArray[pos] + FAs[i].getTerms()[k].getBasisString());
                /*
                if (returnArray[((WiFDDBasisFunction) (FAs[i].getTerms()[k])).getArrayIndex()] == 1) {
                    if (((WiFDDBasisFunction) (FAs[i].getTerms()[k])) instanceof CombinationBasis) {
                        CombinationBasis b = (CombinationBasis) (FAs[i].getTerms()[k]);
                        WiFDDBasisFunction bf1 = b.getBF1();
                        WiFDDBasisFunction bf2 = b.getBF2();
                        if (bf1.getActivation() != 0) {
                            System.out.println("WOOOOOAAAAH1");
                        }
                        if (bf2.getActivation() != 0) {
                            System.out.println("WOOOOOAAAAH2");
                        }

                    }
                }
                //System.out.println("WOOOAAAH");
                 * 
                 */
            }

            //((WiFDDBasisFunction) (FAs[i].getTerms()[k])).setActivation(1);
            pos++;
        }
        return returnArray;
    }
}
