
package functionapproximation;

import java.util.LinkedList;
import java.util.List;
import rl.state.State;

/**
 *
 * @author Dean
 * @param <T>
 */
public abstract class FatherWavelet<T extends Wavelet<T>> extends Wavelet<T> implements BasisFunction {

    public FatherWavelet(int scale, int shift, int dimension) {
        super(scale, shift,dimension);
    }

    @Override
    public List<T> getChildren() {
        LinkedList<T> children = new LinkedList<>();
        children.add(createChild(scale, shift, dimension));
        return children;
    }


    @Override
    public double getValue(State s) {
        return getValue(pow * s.getState()[dimension] + shift);
    }

}
