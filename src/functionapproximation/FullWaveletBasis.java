
package functionapproximation;

import java.util.LinkedList;

/**
 *
 * @author Dean
 */
public class FullWaveletBasis extends FunctionApproximator {

    public FullWaveletBasis(TensorWavelet parent, int desiredScale, int numFeatures) {
        super(0, numFeatures);
        //assume parent is at scale 0
        addBasis(parent);
        LinkedList<TensorWavelet> parents = new LinkedList<>();
        parents.add(parent);
        for (int i = 0; i < desiredScale; i++) {
            LinkedList<TensorWavelet> children = new LinkedList<>();
            for (TensorWavelet p: parents) {
                children.addAll(p.getChildren());
            }
            for (TensorWavelet c: children) {
                addBasis(c);
            }
            parents = children;
        }
    }
    
}
