package functionapproximation;

import java.util.LinkedList;
import java.util.List;
import rl.state.State;

/**
 *
 * @author Dean
 * @param <T>
 */
public abstract class Wavelet<T extends Wavelet<T>> implements BasisFunction, Comparable<Wavelet> {

    protected final int scale;
    protected final int shift;
    protected final int dimension;
    protected final double pow;

    public Wavelet(int scale, int shift, int dimension) {
        this.scale = scale;
        this.shift = shift;
        this.dimension = dimension;
        this.pow = Math.pow(2, scale);
    }

    public List<T> getChildren() {
        LinkedList<T> children = new LinkedList<>();
        children.add(createChild(scale + 1, shift * 2, dimension));
        children.add(createChild(scale + 1, shift * 2 + 1, dimension));
        return children;
    }

    public abstract T createChild(int scale, int shift, int dimension);

    @Override
    public double getValue(State s) {
        return getValue(pow * s.getState()[dimension] - shift);
    }

    public abstract double getValue(double t);

    public abstract double getNormalisation(int scale);

    @Override
    public int compareTo(Wavelet o) {
        if (this.scale < o.scale) {
            return -1;
        } else if (this.scale > o.scale) {
            return 1;
        } else {
            if (this.shift < o.shift) {
                return -1;
            } else if (this.shift > o.shift) {
                return 1;
            } else {
                if (this.dimension < o.dimension) {
                    return -1;
                } else if (this.dimension > o.dimension) {
                    return 1;
                } else {
                    return 0;
                }
            }
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.scale;
        hash = 97 * hash + this.shift;
        hash = 97 * hash + this.dimension;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Wavelet<?> other = (Wavelet<?>) obj;
        if (this.scale != other.scale) {
            return false;
        }
        if (this.shift != other.shift) {
            return false;
        }
        if (this.dimension != other.dimension) {
            return false;
        }
        return true;
    }
    
    

}
