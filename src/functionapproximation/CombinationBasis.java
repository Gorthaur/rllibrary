/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package functionapproximation;

import java.util.ArrayList;
import rl.state.State;

/**
 *
 * @author Dean
 */
public class CombinationBasis implements WiFDDBasisFunction {

    private WiFDDBasisFunction bf1;
    private WiFDDBasisFunction bf2;
    private double activation = 1;
    private int arrayIndex;
    ArrayList<Integer> dims;
    ArrayList<WiFDDBasisFunction> parents;

    public CombinationBasis(WiFDDBasisFunction bf1, WiFDDBasisFunction bf2) {
        this.bf1 = bf1;
        this.bf2 = bf2;
        parents = new ArrayList<WiFDDBasisFunction>();
        parents.add(bf1);
        parents.add(bf2);
    }
    
    public void updateParents(WiFDDBasisFunction bf1, WiFDDBasisFunction bf2) {
        if (this.bf1.equals(bf1) || this.bf2.equals(bf2)) {
            return;
        }
        this.bf1 = bf1;
        this.bf2 = bf2;
    }
    
    public void addParents(WiFDDBasisFunction bf1, WiFDDBasisFunction bf2) {
        parents.add(bf1);
        parents.add(bf2);
    }
    
    public ArrayList<WiFDDBasisFunction> getParents() {
        return parents;
    }
    
    public void addParent(WiFDDBasisFunction p) {
        parents.add(p);
    }

    @Override
    public double getValue(State s) {
        double value = bf1.getValue(s) * bf2.getValue(s);
        for (WiFDDBasisFunction wf: parents) {
            wf.decreaseActivation(Math.abs(value));
        }
        //bf1.decreaseActivation(Math.abs(value));
        //bf2.decreaseActivation(Math.abs(value)); //not sure if we want to decrease it like this.
        return value;
    }

    @Override
    public double getShrink() {
        return 1;
    }

    @Override
    public String getBasisString() {
        return bf1.getBasisString() + " " + bf2.getBasisString();
    }

    public ArrayList<BasisFunction> getBFs() {
        ArrayList<BasisFunction> bfs = new ArrayList<BasisFunction>();
        if (bf1 instanceof CombinationBasis) {
            bfs.addAll(((CombinationBasis) bf1).getBFs());
        } else {
            bfs.add(bf1);
        }
        if (bf2 instanceof CombinationBasis) {
            bfs.addAll(((CombinationBasis) bf2).getBFs());
        } else {
            bfs.add(bf2);
        }
        return bfs;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof CombinationBasis) {
            CombinationBasis t = (CombinationBasis) o;
            ArrayList<BasisFunction> bfs = getBFs();
            ArrayList<BasisFunction> bfs2 = t.getBFs();
            for (BasisFunction b : bfs) {
                if (!bfs2.contains(b)) {
                    return false;
                }
            }
            if (bfs.size() != bfs2.size()) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash = hash + (this.bf1 != null ? this.bf1.hashCode() : 0) + (this.bf2 != null ? this.bf2.hashCode() : 0);
        return hash;
    }

    @Override
    public ArrayList<Integer> getDimensions() {
        if (dims == null) {
            ArrayList<Integer> bf1dims = (ArrayList<Integer>)(bf1.getDimensions().clone());
            ArrayList<Integer> bf2dims = bf2.getDimensions();
            for (int w : bf2dims) {
               // if (!bf1dims.contains(w)) {
                    bf1dims.add(w);
                //}
            }
            dims = bf1dims;
            return bf1dims;
        } else {
            return dims;
        }
    }

    @Override
    public double getActivation() {
        return activation;
    }

    @Override
    public void setActivation(double activation) {
        this.activation = activation;
    }

    @Override
    public void decreaseActivation(double amount) {
        this.activation = this.activation - amount;
        this.activation = Math.max(0, activation);
        for (WiFDDBasisFunction wf: parents) {
            wf.decreaseActivation(Math.abs(amount));
        }
    }

    @Override
    public int getArrayIndex() {
        return this.arrayIndex;
    }

    @Override
    public void setArrayIndex(int index) {
        this.arrayIndex = index;
    }

    public WiFDDBasisFunction getBF1() {
        return bf1;
    }

    public WiFDDBasisFunction getBF2() {
        return bf2;
    }
}
