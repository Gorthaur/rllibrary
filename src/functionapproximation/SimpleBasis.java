package functionapproximation;

import java.io.Serializable;
import java.util.Arrays;
import rl.state.State;

public class SimpleBasis implements BasisFunction {

    private int[] indices;

    public SimpleBasis(int... index) {
        this.indices = index;
        setShrink();
    }

    @Override
    public double getValue(State s) {
        //FourierState p = (FourierState)s;
        double state[] = s.getState();
        double val = 1;
        for (int i = 0; i < indices.length; i++) {
            if (state.length > indices[i]) {
                val *= (state[indices[i]]);
            }
            else {
                val = 0;
            }
        }
        return val;
    }

    public void setShrink() {

    }

    @Override
    public double getShrink() {
        return 1;
    }

    public void printBasis() {
        String str = Arrays.toString(indices);
    }

    public String getBasisString() {
        String str = "Arrays.asList(";
        for (int i = 0; i < indices.length; i++) {
            str += "" + indices[i] + ",";
        }
        str = str.substring(0, str.length() - 1) + ")";

        return str;
        /*
         String str = "new SimpleBasis(";
         for (int i = 0; i < indices.length; i++) {
         str += indices[i] + ",";
         }
         str = str.substring(0, str.length() -1);
         str += ")";

         return str;
         */
    }

    @Override
    public String toString() {
        return getBasisString();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SimpleBasis other = (SimpleBasis) obj;
        if (!Arrays.equals(this.indices, other.indices)) {
            return false;
        }
        return true;
    }

}
