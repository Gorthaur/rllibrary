package domains.mountaincar3d;

import domains.Domain;
import domains.mountaincarND.MountainCarND;
import java.util.LinkedList;
import rl.state.ScaledState;
import rl.state.State;

/**
 *
 * @author Dean
 */
/*
 * 
 *  	0 - coast
 *	1 - left
 *	2 - right
 *	3 - down
 *	4 - up
 */
public class MountainCar3D extends MountainCarND {
    public MountainCar3D() {
        super(2);
    }
}
