/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package domains;

import domaintools.ValueFunctionPlotter;
import domains.discontinuousroom.WallWorld;
import experimental.SarsaLambdaTester;
import functionapproximation.FullDaubWithoutIndependent;
import functionapproximation.FullFourier;
import functionapproximation.FunctionApproximator;
import functionapproximation.QApproximator;
import java.util.logging.Level;
import java.util.logging.Logger;
import rl.policy.EpsilonGreedy;
import rl.policy.Policy;
import rl.learningalgorithms.SarsaLambda;
import rl.state.State;

/**
 *
 * @author dean
 */
public class DiscontinuousRoomRun {

    public static void main(String args[]) {
        int experiments = 1;
        int episodes = 10000;
        int maxSteps = 20000;
        double alpha = 1;
        double gamma = 0.99;
        double lambda = 0.9;

        int fourierOrder = 7;

        int dimensions = WallWorld.getNumDimensions();

        double totalSteps[] = new double[episodes];
        for (int i = 0; i < experiments; i++) {
            FunctionApproximator FAs[] = new FunctionApproximator[WallWorld.getNumActions()];
            for (int k = 0; k < WallWorld.getNumActions(); k++) {
                FAs[k] = new FullFourier(fourierOrder, dimensions);
                //System.out.println(dimensions);
                //FAs[k] = new FullDaubWithoutIndependent(0, -1, dimensions,8);
                System.out.println("Action " + k + " number of terms: " + FAs[k].getNumTerms());
            }
            QApproximator q = new QApproximator(FAs);
            Policy p = new EpsilonGreedy(new WallWorld(), q, 0.1);
            SarsaLambdaTester learner = new SarsaLambdaTester(q, p, alpha, gamma, lambda);
            for (int j = 0; j < episodes; j++) {
                WallWorld room = new WallWorld();
                //room.x = (int)(Math.random()*20)*0.5;
                //room.y = (int)(Math.random()*12)*0.5;
                learner.startEpisode();
                int steps = 0;
                State currState = room.getState();
                int move = learner.nextMove(currState);
                double ret = 0;
                while (steps < maxSteps && !currState.isTerminal()) {
                    double reward = room.step(move);
                    ret += reward;
                    State newState = room.getState();
                    int nextMove = learner.nextMove(newState);
                    learner.addSample(currState, move, newState, nextMove, reward);
                    currState = newState;
                    move = nextMove;
                    steps++;
                    //.out.println(currState.getState()[1]);
                }


                totalSteps[j] += steps;
                System.out.println("Discontinuous Room Episode " + j + " took " + steps + " steps with return: " + ret);
            }
            ValueFunctionPlotter plot = new ValueFunctionPlotter(q, "data/discontinuous");
            plot.writeValuesMatFile();
        }
    }
}
