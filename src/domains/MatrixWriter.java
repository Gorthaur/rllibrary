package domains;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This program will be run at least 100 times each with a different experimentNum parameter.
 * The files will be written using this experimentNum parameter, and the concatenated at the end.
 * @author Dean
 */
public class MatrixWriter {
    public static void main(String args[]) {
        int experimentNum = 1;
        if (args.length > 0) {
            experimentNum = Integer.parseInt(args[0]);
        }
        int n = 10;
        double results[][][] = new double[3][2][n];
        //instead of these 2 lines, actually do the stuff to get the matrix
        
        writeResults(experimentNum, results);
    }
    
    public static void writeResults(int experimentNum, double results[][][]) {
        for (int method = 0; method < results.length; method++) {
            for (int metric = 0; metric < results[method].length; metric++) {
                BufferedWriter writer = null;
                try {
                    String result = arrayToString(results[method][metric]);
                    result = experimentNum + " " + result;
                    File output = new File("mitchresults/method" + method + "_metric" + metric + "_experiment" + experimentNum + ".txt");
                    output = output.getAbsoluteFile();
                    File parent = output.getParentFile();
                    parent.mkdir();
                    writer = new BufferedWriter(new FileWriter(output));
                    writer.write(result);
                } catch (IOException ex) {
                    Logger.getLogger(MatrixWriter.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    try {
                        writer.close();
                    } catch (IOException ex) {
                        Logger.getLogger(MatrixWriter.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }
    
    public static String arrayToString(double array[]) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            builder.append(array[i]);
            builder.append(" ");
        }
        return builder.toString().trim();
    }
    
}
