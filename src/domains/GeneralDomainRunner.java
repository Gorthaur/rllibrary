package domains;

import PinBallDomain.PinBall;
import domains.acrobot.Acrobot;
import domains.cartpole.CartPole;
import domains.discontinuousroom.WallWorld;
import domains.mountaincar.MountainCar;
import domains.mountaincar3d.MountainCar3D;
import domains.pinball.PinballWrapper;
import domains.mountaincarND.MountainCarND;
import domains.mountaincarND.MountainCarNDPolicy;
import domaintools.ValueFunctionPlotter;
import functionapproximation.FullDaubNoTails;
import functionapproximation.FullDaubWithoutIndependent;
import functionapproximation.FullFourier;
import functionapproximation.FullRBF;
import functionapproximation.FullTiling;
import functionapproximation.FunctionApproximator;
import functionapproximation.IndependentDaubFunctionApproximation;
import functionapproximation.IndependentFourier;
import functionapproximation.IndependentRBF;
import functionapproximation.IndependentTiling;
import functionapproximation.QApproximator;
import java.io.File;
import rl.policy.EpsilonGreedy;
import rl.policy.Policy;
import rl.Sample;
import rl.learningalgorithms.SarsaLambdaAlphaScale;
import rl.state.State;

/**
 *
 * @author Dean
 */
public class GeneralDomainRunner {

    Domain domain;
    Policy policy;
    QApproximator qApproximator;
    SarsaLambdaAlphaScale learner;
    int numEpisodes = 1000;
    double alpha = 1;
    double gamma = 1;
    double lambda = 0.95;

    public static void main(String args[]) {
        GeneralDomainRunner runner;
        if (args.length > 0) {
            runner = new GeneralDomainRunner(args);
        } else {
            runner = new GeneralDomainRunner();
        }
        //get the return of each
        double results[] = runner.runExperiment();
        ValueFunctionPlotter plot = new ValueFunctionPlotter(runner.qApproximator, "data/mcarvaluefourier20");
        plot.writeActionsMatFile();
        plot.writeValuesMatFile();
    }

    /**
     * Constructor for running local experiments. Set the domain you want to test here.
     * Anything to do with the learner will have to be done in the runExperiment method.
     */
    public GeneralDomainRunner() {
        String pinballMapFile = "./pinballfiles/pinball_hard_single.cfg";
        //domain = new PinballWrapper(pinballMapFile); //pinball had to have a wrapper around it because it's George's external code I don't want to change.
        //domain = new Acrobot();
        domain = new MountainCar();
        //domain = new WallWorld();
        //domain = new MountainCarND(8);
        int numActions = domain.getTotalNumActions();
        int numDimensions = domain.getTotalNumDimensions();
        FunctionApproximator[] FAs = getFunctionApproximators(numActions, numDimensions);
        qApproximator = new QApproximator(FAs);
        policy = new EpsilonGreedy(domain, qApproximator, 0.01);
        //policy = new MountainCarNDPolicy(domain);
    }

    /**
     * Constructor for cluster. Can be ignored mostly.
     * @param args The command line arguments indicating which experiment it is, and other variable parameters.
     */
    public GeneralDomainRunner(String args[]) {
        String outputFileName = "";
        String pinballMapFile = "./pinballfiles/pinball_hard_single.cfg";
        domain = new PinballWrapper(pinballMapFile); //pinball had to have a wrapper around it because it's George's external code I don't want to change.
        //domain = new Acrobot();
        //domain = new MountainCar();
        //domain = new WallWorld();
        
        int numActions = domain.getTotalNumActions();
        int numDimensions = domain.getTotalNumDimensions();
        String dom = domain.getDescriptionString();
        int experimentNum = Integer.parseInt(args[0]);

        FunctionApproximator[] FAs = new FunctionApproximator[numDimensions];
        for (int j = 0; j < numActions; j++) {
            if (args.length >= 3) {
                int order = Integer.parseInt(args[2]);
                if (args[1].equals("fullfourier")) {
                    FAs[j] = new FullFourier(order, numDimensions);
                    outputFileName = "fullfourier" + order + "_lambda_" + lambda + "_" + dom + "_" + experimentNum;
                } else if (args[1].equals("independentfourier")) {
                    FAs[j] = new IndependentFourier(order, numDimensions);
                    outputFileName = "independentfourier" + order + "_lambda_" + lambda + "_" + dom + "_" + experimentNum;
                } else if (args[1].equals("fullrbf")) {
                    FAs[j] = new FullRBF(order, numDimensions);
                    outputFileName = "fullrbf" + order + "_lambda_" + lambda + "_" + dom + "_" + experimentNum;
                } else if (args[1].equals("independentrbf")) {
                    FAs[j] = new IndependentRBF(order, numDimensions);
                    outputFileName = "independentrbf" + order + "_lambda_" + lambda + "_" + dom + "_" + experimentNum;
                } else if (args[1].equals("fulltiling")) {
                    FAs[j] = new FullTiling(order, numDimensions);
                    outputFileName = "fulltiling" + order + "_lambda_" + lambda + "_" + dom + "_" + experimentNum;
                } else if (args[1].equals("independenttiling")) {
                    FAs[j] = new IndependentTiling(order, numDimensions);
                    outputFileName = "independenttiling" + order + "_lambda_" + lambda + "_" + dom + "_" + experimentNum;
                } else if (args[1].equals("fulldaub")) {
                    int basescale = Integer.parseInt(args[3]);
                    int maxscale = Integer.parseInt(args[4]);
                    FAs[j] = new FullDaubWithoutIndependent(basescale, maxscale, numDimensions, order);
                    outputFileName = "fulldaub" + order + "_m" + basescale + "_n" + maxscale + "_lambda_" + lambda + "_" + dom + "_" + experimentNum;
                } else if (args[1].equals("independentdaub")) {
                    int basescale = Integer.parseInt(args[3]);
                    int maxscale = Integer.parseInt(args[4]);
                    FAs[j] = new IndependentDaubFunctionApproximation(basescale, maxscale, numDimensions, order);
                    outputFileName = "independentdaub" + order + "_m" + basescale + "_n" + maxscale + "_lambda_" + lambda + "_" + dom + "_" + experimentNum;
                } else if (args[1].equals("fulldaubnotails")) {
                    int basescale = Integer.parseInt(args[3]);
                    int maxscale = Integer.parseInt(args[4]);
                    FAs[j] = new FullDaubNoTails(basescale, maxscale, numDimensions, order);
                    outputFileName = "fulldaubnotails" + order + "_m" + basescale + "_n" + maxscale + "_lambda_" + lambda + "_" + dom + "_" + experimentNum;
                }
            } else {
                FAs[j] = new IndependentFourier(5, numDimensions);
            }
        }
        qApproximator = new QApproximator(FAs);
        policy = new EpsilonGreedy(domain, qApproximator, 0.01);
    }

    public void resetFAs() {
        FunctionApproximator FAs[] = qApproximator.getFAs();
        for (int i = 0; i < FAs.length; i++) {
            FAs[i].initialiseWeights();
        }
    }

    /**
     * Gets the function approximators for this experiment. Feel free to change this to whatever function approximator you're using. This is for running non-cluster tests. The other one is for cluster tests.
     * @param numActions The number of actions available in the domain.
     * @param numDimensions The number of dimensions the state of the domain lies in.
     * @return An array of FunctionApproximator objects.
     */
    public FunctionApproximator[] getFunctionApproximators(int numActions, int numDimensions) {
        int fourierOrder = 100;
        FunctionApproximator functionApproximators[] = new FunctionApproximator[numActions];
        for (int i = 0; i < numActions; i++) {
            functionApproximators[i] = new FullFourier(fourierOrder, numDimensions);
        }
        return functionApproximators;
    }

    /**
     * Runs an experiment. Feel free to change the code inside to run whatever you want.
     * @return The return of each episode run in the experiment
     */
    public double[] runExperiment() {
        double experimentResults[] = new double[numEpisodes];
        for (int i = 0; i < numEpisodes; i++) {
            experimentResults[i] = runAndLearnEpisode();
            System.out.println("Experiment " + i + " gave return " + experimentResults[i]);
        }
        return experimentResults;
    }

    /**
     * Run a single episode and return the total reward (return). No learning in this one. Good for evaluating a policy or collecting samples from a policy, but the SampleCollector class is better for that.
     * @return 
     */
    public double runEpisode() {
        domain.resetDomain();
        double ret = 0;
        State currState = domain.getState();
        if (currState.isTerminal()) {
            return 0;
        }

        int nextAction = policy.select(currState);
        while (!currState.isTerminal()) {
            int action = nextAction;
            double reward = domain.step(action);
            ret += reward;
            State nextState = domain.getState();
            if (!nextState.isTerminal()) {
                nextAction = policy.select(nextState);
            }
            //this is where you would have the full sample. Not doing anything with the sample right now.
            Sample sample = new Sample(currState, action, nextState, nextAction, reward);
            currState = nextState;
        }
        return ret;
    }

    /**
     * Runs an episode in the domain while learning using some online learning algorithm.
     * @return The total reward accumulated in the episode (the return).
     */
    public double runAndLearnEpisode() {


        domain.resetDomain();
        if (learner == null) {
            learner = new SarsaLambdaAlphaScale(qApproximator, policy, alpha, gamma, lambda);
        } else {
            //we don't create a new learner each time because alpha may have changed.
            learner.startEpisode(); //clears traces and stuff
        }
        //domain.setState(domain.getRandomState());
        double ret = 0;
        State currState = domain.getState();
        if (currState.isTerminal()) {
            return 0;
        }

        int nextAction = policy.select(currState);
        int count = 0;
        while (!currState.isTerminal()) {
            int action = nextAction;
            //System.out.println(action);
            double reward = domain.step(action);
            ret += reward;
            State nextState = domain.getState();
            if (!nextState.isTerminal()) {
                nextAction = policy.select(nextState);
            }
            //this is where you would have the full sample. Not doing anything with the sample right now.
            Sample sample = new Sample(currState, action, nextState, nextAction, reward);
            learner.addSample(currState, action, nextState, nextAction, reward);
            currState = nextState;
            count++;
            if (count == 10000) {
                break;
            }
        }
        //System.out.println("Number of steps: " + count);
        return ret;
    }
}
