package domains;

import java.util.LinkedList;
import rl.state.State;

/**
 *
 * @author Dean
 */
public abstract class Domain {
    public abstract State getState();
    public abstract double step(int action);
    
    /**
     * Set the state of the domain to the given state object.
     * @param s The state the domain should be in. Should be of the correct dimensionality.
     */
    public abstract void setState(State s);
    
    public abstract boolean isEpisodeOver();
    /**
     * Returns a random state object that is valid in this domain. The state is a non-terminal state.
     * @return A random state object that is valid in this domain.
     */
    public abstract State getRandomState();
    
    /**
     * Returns the number of actions available in this domain.
     * @return The number of actions available in this domain.
     */
    public static int getNumActions() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    /**
     * Returns the number of dimensions the state of this domain lies in.
     * @return The number of dimensions this domain lies in.
     */
    public static int getNumDimensions() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    /**
     * Gets the number of actions available in the given state.
     * @param s The state at which the actions are available.
     * @return The number of actions available in the given state.
     */
    public abstract int getNumActions(State s);
    
    /**
     * Gets a list of available actions in the given state.
     * @param s The state at which the actions are available.
     * @return A list of available actions.
     */
    public abstract LinkedList<Integer> getAvailableActions(State s);
    
    /**
     * Gets the total number of unique actions that are available in the domain.
     * @return The total number of unique actions in the domain.
     */
    public abstract int getTotalNumActions();
    
    /**
     * Resets the domain to one of its starting positions.
     */
    public abstract void resetDomain();
    
    /**
     * Gets the domain's description for use in file names etc.
     * @return The domain description. Eg: mountaincar, or acrobotm1-0.75m2-0.3
     */
    public abstract String getDescriptionString();
    
    public abstract int getTotalNumDimensions();
}
