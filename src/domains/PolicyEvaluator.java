package domains;

import rl.policy.Policy;
import rl.Sample;
import rl.state.State;

/**
 *
 * @author Dean
 */
public class PolicyEvaluator {

    Policy policy;
    Domain domain;

    public PolicyEvaluator(Policy p, Domain domain) {
        this.policy = p;
        this.domain = domain;
    }

    public double getAverageReturn(int totalRuns) {
        double total = 0;
        for (int i = 0; i < totalRuns; i++) {
            total += runEpisode();
        }
        return total / totalRuns;

    }

    public double runEpisode() {
        domain.resetDomain();
        double ret = 0;
        State currState = domain.getState();
        if (currState.isTerminal()) {
            return 0;
        }

        int nextAction = policy.select(currState);
        while (!currState.isTerminal()) {
            int action = nextAction;
            double reward = domain.step(action);
            ret += reward;
            State nextState = domain.getState();
            if (!nextState.isTerminal()) {
                nextAction = policy.select(nextState);
            }
            //this is where you would have the full sample. Not doing anything with the sample right now.
            Sample sample = new Sample(currState, action, nextState, nextAction, reward);
            currState = nextState;
        }
        return ret;
    }
}
