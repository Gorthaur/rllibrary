/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package domains.mountaincar;

import domains.Domain;
import java.util.Arrays;
import rl.policy.Policy;
import rl.state.State;

/**
 *
 * @author mitch
 */
    
public class MCPolicy extends Policy {
    public MCPolicy(Domain domain) {
        super(domain);
    }
    
    @Override
    public double[] getProbabilities(State s) {
        double probs[] = new double[this.getDomain().getTotalNumActions()];
        if (s.getState()[1]>0.5) probs[2]=1;
        else probs[0]=1;
        return probs;
    }
    
    
}


