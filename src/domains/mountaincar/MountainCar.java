package domains.mountaincar;

import domains.Domain;
import java.util.LinkedList;
import rl.state.EndState;
import rl.state.ScaledState;
import rl.state.State;

public class MountainCar extends Domain {

    public MountainCar() {
        x = -0.5 + (Math.random() - 0.5)/100000;
        xdot = 0.0 + (Math.random() - 0.5)/100000;
    }

    @Override
    public State getState() {
        if (x >= mcar_goal_position) {
            return new EndState(new double[]{x, xdot}, new double[]{mcar_min_position, mcar_min_velocity}, new double[]{mcar_max_position, mcar_max_velocity});
        } else {
            return new ScaledState(new double[]{x, xdot}, new double[]{mcar_min_position, mcar_min_velocity}, new double[]{mcar_max_position, mcar_max_velocity});
        }
    }

    @Override
    public double step(int a) // Take action a, update state of car
    {
        xdot += (a - 1) * 0.001 + Math.cos(3 * x) * (-0.0025);

        if (xdot > mcar_max_velocity) {
            xdot = mcar_max_velocity;
        }
        if (xdot < -mcar_max_velocity) {
            xdot = -mcar_max_velocity;
        }
        x += xdot;

        if (x > mcar_max_position) {
            x = mcar_max_position;
        }
        if (x < mcar_min_position) {
            x = mcar_min_position;
        }
        if ((x == mcar_min_position) && xdot < 0) {
            xdot = 0;
        }

        if (atGoal()) {
            return 0;
        }
        return -1;
    }

    boolean atGoal() {
        return x >= mcar_goal_position;
    }

    public boolean isEpisodeOver() {
        return atGoal();
    }
    double x;
    double xdot;

    public static int getNumActions() {
        return 3;
    }

    public static int getNumDimensions() {
        return 2;
    }
    public static final double mcar_min_position = -1.2;
    public static final double mcar_max_position = 0.6;
    public static final double mcar_max_velocity = 0.07;
    public static final double mcar_min_velocity = -0.07;
    public static final double mcar_goal_position = 0.5;

    @Override
    public State getRandomState() {
        double x = Math.random() * (mcar_max_position - mcar_min_position) + mcar_min_position;
        if (x >= mcar_goal_position) {
            return getRandomState();
        }
        double y = Math.random() * (mcar_max_velocity - mcar_min_velocity) + mcar_min_velocity;
        ScaledState f = new ScaledState(new double[]{x, y}, new double[]{mcar_min_position, mcar_min_velocity}, new double[]{mcar_max_position, mcar_max_velocity});
        return f;
    }

    @Override
    public void setState(State s) {
        if (s.getStateLength() == getNumDimensions()) {
            //state is scaled between 0 and 1. Unscale it.
            this.x = s.getState()[0] * (mcar_max_position - mcar_min_position) + mcar_min_position;
            this.xdot = s.getState()[1] * (mcar_max_velocity - mcar_min_velocity) + mcar_min_velocity;
        } else {
            throw new IllegalStateException("State is illegal");
        }
    }

    @Override
    public int getNumActions(State s) {
        return MountainCar.getNumActions();
    }

    @Override
    public LinkedList<Integer> getAvailableActions(State s) {
        LinkedList<Integer> actions = new LinkedList<Integer>();
        for (int i = 0; i < MountainCar.getNumActions(); i++) {
            actions.add(i);
        }
        return actions;
    }

    @Override
    public int getTotalNumActions() {
        return MountainCar.getNumActions();
    }

    @Override
    public void resetDomain() {
        x = -0.5 + (Math.random() - 0.5)/100000;
        xdot = 0.0 + (Math.random() - 0.5)/100000;
    }

    @Override
    public String getDescriptionString() {
        return "mountain_car";
    }

    @Override
    public int getTotalNumDimensions() {
        return MountainCar.getNumDimensions();
    }
}
