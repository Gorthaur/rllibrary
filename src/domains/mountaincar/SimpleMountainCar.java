/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package domains.mountaincar;

/**
 *
 * @author Dean
 */
import domaintools.ValueFunctionPlotter;
import functionapproximation.QApproximator;
import functionapproximation.FunctionApproximator;
import functionapproximation.IndependentFourier;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import rl.policy.EpsilonGreedy;
import rl.policy.Policy;
import experimental.SarsaLambdaTester;
import experimental.SarsaWiFDD;
import functionapproximation.FullDaubFunctionApproximation;
import functionapproximation.FullDaubWithoutIndependent;
import functionapproximation.FullFourier;
import functionapproximation.FullHaarFunctionApproximation;
import functionapproximation.FullHaarWithoutIndependent;
import functionapproximation.FullHaarWithoutIndependentSameScale;
import functionapproximation.FullTiling;
import functionapproximation.IndependentDaubFunctionApproximation;
import functionapproximation.IndependentHaarFunctionApproximation;
import functionapproximation.IndependentHaarMathIT;
import functionapproximation.IndependentTiling;
import functionapproximation.WiFDDQApproximator;
import rl.learningalgorithms.SarsaLambda;
import rl.state.State;

public class SimpleMountainCar {

    public static void main(String args[]) throws IOException {
        int maxSteps = 10000; //max steps in mountain car episode before calling it quits
        int numEpisodes = 2000;
        int numExperiments = 1;

        int baseScale = 2;
        int numFeatures = 2; //num state variables/dimensions
        int maxScale = -1;



        double alpha = 1; //learning rate
        //double alpha = 1;
        double gamma = 1; //discount (1 = no discount)
        double lambda = 0.9; //eligibility trace

        double maxPotential = 20000;

        for (int i = 0; i < numExperiments; i++) {
            System.out.println("Start Experiment " + (i + 1));
            FunctionApproximator FAs[] = new FunctionApproximator[3];
            for (int j = 0; j < 3; j++) {
                //FAs[j] = new FullFourier(15, numFeatures);
                //FAs[j] = new FullHaarFunctionApproximation(baseScale, maxScale, numFeatures);
                //FAs[j] = new IndependentHaarFunctionApproximation(baseScale, maxScale, numFeatures);
                //FAs[j] = new FullHaarWithoutIndependent(baseScale, maxScale, numFeatures);
                //FAs[j] = new FullHaarWithoutIndependentSameScale(baseScale, maxScale, numFeatures);
                //FAs[j] = new IndependentHaarMathIT(baseScale, maxScale, numFeatures);
                //FAs[j] = new IndependentDaubFunctionApproximation(baseScale, maxScale, numFeatures,2);
                //FAs[j] = new FullDaubFunctionApproximation(baseScale, maxScale, numFeatures, 4);
                //FAs[j] = new FullDaubWithoutIndependent(baseScale, maxScale, numFeatures, 8);
                FAs[j] = new FullFourier(10, numFeatures);
                System.out.println("Action " + j + " number of terms: " + FAs[j].getNumTerms());
            }
            State n = new State(new double[]{0, 0});
            QApproximator q = new QApproximator(FAs);
            //WiFDDQApproximator q = new WiFDDQApproximator(FAs);
            Policy p = new EpsilonGreedy(new MountainCar(), q, 0.1);
            // SarsaWiFDD learner = new SarsaWiFDD(q, p, alpha, gamma, lambda, maxPotential);
            SarsaLambdaTester learner = new SarsaLambdaTester(q, p, alpha, gamma, lambda);
            //SarsaLambda learner = new SarsaLambda(q, p, alpha, gamma, lambda);

            //start an episode
            for (int k = 0; k < numEpisodes; k++) {
                System.out.print(".");
                learner.startEpisode();
                MountainCar m = new MountainCar();
                int steps = 0;
                State currState = m.getState();
                int move = learner.nextMove(currState);
                while (steps < maxSteps && !currState.isTerminal()) {
                    double reward = m.step(move);
                    State newState = m.getState();
                    //learner.addSample(currState, move, newState, reward); //GQ
                    int nextMove = learner.nextMove(newState);

                    learner.addSample(currState, move, newState, nextMove, reward);

                    currState = newState;
                    move = nextMove;
                    steps++;
                }
                System.out.println("Episode " + k + " took " + steps + " steps");
            }
            //q.printFunction();
            ValueFunctionPlotter vp = new ValueFunctionPlotter(q, "data/mcvalue-tiling");
            //vp.writeActionsMatFile();
            vp.writeValuesMatFile();
        }
    }
}
