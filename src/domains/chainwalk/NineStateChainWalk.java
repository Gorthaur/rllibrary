package domains.chainwalk;

/**
 *
 * @author Dean
 */
public class NineStateChainWalk extends ChainWalk {
    public NineStateChainWalk(double gamma) {
        super(gamma);
    }
    @Override
    public int getNumStates() {
        return 9;
    }

    @Override
    public double[] getRewards() {
        //I number states from 0 to 49
        double rewards[] = new double[getNumStates()];
        rewards[4] = 1;
        return rewards;
    }

    @Override
    public double[] getInitialValues() {
        return new double[getNumStates()];
    }

    @Override
    public int getInitialState() {
        int st = (int)Math.random()*(getNumStates());
        if (st == getNumStates()) {
            st --;
        }
        return st;
    }

    @Override
    public int[] getInitialPolicy() {
        return new int[getNumStates()];
    }
    
   
}
