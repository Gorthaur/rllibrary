package domains.chainwalk;

import domains.Domain;
import functionapproximation.BasisFunction;
import functionapproximation.FunctionApproximator;
import functionapproximation.QApproximator;
import functionapproximation.TileBasis;
import functionapproximation.VApproximator;
import java.util.Arrays;
import java.util.LinkedList;
import rl.policy.EpsilonGreedy;
import rl.policy.Policy;
import rl.state.State;

/**
 *
 * @author Dean
 */
public abstract class ChainWalk extends Domain {

    int numStates;
    public double values[];
    double rewards[];
    int state;
    final int actionLeft = 0;
    final int actionRight = 1;
    int policy[];
    double gamma;
    double policyEvalStop = 0.00001;
    VApproximator v;
    QApproximator q;

    /**
     * The chainwalk domain
     *
     * @param gamma The gamma or discount factor for this domain. It should be
     * 0.8 if you want to compare to LSPI or OMP papers.
     */
    public ChainWalk(double gamma) {
        this.gamma = gamma;
        this.numStates = getNumStates();
        this.rewards = getRewards();
        this.values = getInitialValues();
        this.state = getInitialState();
        this.policy = getInitialPolicy();
    }

    /**
     * Returns the number of states in chain walk.
     *
     * @return
     */
    public abstract int getNumStates();

    public abstract double[] getRewards();

    public abstract double[] getInitialValues();

    public abstract int getInitialState();

    public abstract int[] getInitialPolicy();

    @Override
    public State getState() {
        return new State(new double[]{(double) state / (numStates - 1)});
    }

    @Override
    public double step(int action) {
        int stepDir = 0;
        if (action == actionLeft) {
            stepDir = -1;
        } else {
            stepDir = 1;
        }
        if (Math.random() > 0.9) {
            stepDir *= -1;
        }
        if (state + stepDir >= numStates || state + stepDir < 0) {
            stepDir = 0;
        }
        state = state + stepDir;
        return rewards[state];
    }

    @Override
    public void setState(State s) {
        state = (int) Math.round(s.getState()[0] * (numStates - 1));
    }

    @Override
    public boolean isEpisodeOver() {
        return false;
    }

    @Override
    public State getRandomState() {
        int s = (int) (Math.random() * numStates);
        if (s == numStates) {
            s--;
        }
        return new State(new double[]{(double) s / (numStates - 1)});
    }

    @Override
    public int getNumActions(State s) {
        return 2;
    }

    @Override
    public LinkedList<Integer> getAvailableActions(State s) {
        LinkedList<Integer> available = new LinkedList<Integer>();
        available.add(0);
        available.add(1);
        return available;
    }

    @Override
    public int getTotalNumActions() {
        return 2;
    }

    @Override
    public void resetDomain() {
        setState(getRandomState());
    }

    @Override
    public String getDescriptionString() {
        return "chain_walk";
    }

    @Override
    public int getTotalNumDimensions() {
        return 1;
    }

    public VApproximator getOptimalVApproximator() {
        if (v == null) {
            findValues();
        }
        return v;
    }

    public QApproximator getOptimalQApproximator() {
        if (q == null) {
            findValues();
        }
        return q;
    }

    /**
     * Gets the state value of this state.
     *
     * @param s The state to evaluate at.
     * @return The true value of being in this state.
     */
    public double getOptimalValue(State s) {
        int st = (int) Math.round(s.getState()[0] * (numStates - 1));
        return values[st];
    }

    /**
     * Returns the optimal policy for this chain walk problem.
     *
     * @return Optimal policy p for this chain walk.
     */
    public Policy getOptimalPolicy() {
        QApproximator opt = getOptimalQApproximator();
        return new EpsilonGreedy(this, opt, 0);
    }

    /**
     * Gets the state-action value at the given state for the given action.
     *
     * @param s The state to evaluate at.
     * @param action The action taken from that state
     * @return The value of taking the given action in the given state.
     */
    public double getOptimalValue(State s, int action) {
        int st = (int) Math.round(s.getState()[0] * (numStates - 1));
        double rightValue = 0;
        if (st < numStates - 1) {
            rightValue = gamma * values[st + 1] + rewards[st + 1];
        } else {
            rightValue = gamma * values[st] + rewards[st]; //don't move
        }
        double leftValue = 0;
        if (st > 0) {
            leftValue = gamma * values[st - 1] + rewards[st - 1];
        } else {
            leftValue = gamma * values[st] + rewards[st];
        }
        if (action == actionRight) {
            rightValue *= 0.9;
            leftValue *= 0.1;
        } else {
            rightValue *= 0.1;
            leftValue *= 0.9;
        }
        return leftValue + rightValue;
    }
    /*
     private double[] findValues() {
     while (true) {
     boolean stable = true;
     for (int i = 0; i < numStates; i++) {
     int act = policy[i];
     double rightValue = 0;
     if (i < numStates - 1) {
     rightValue = gamma * values[i + 1] + rewards[i + 1];
     } else {
     rightValue = gamma * values[i] + rewards[i]; //don't move
     }
     double leftValue = 0;
     if (i > 0) {
     leftValue = gamma * values[i - 1] + rewards[i - 1];
     } else {
     leftValue = gamma * values[i] + rewards[i];
     }
     double actRight = rightValue * 0.9 + leftValue * 0.1;
     double actLeft = rightValue * 0.1 + leftValue * 0.9;
     if (actRight > actLeft) {
     policy[i] = actionRight;
     } else {
     policy[i] = actionLeft;
     }
     if (policy[i] != act) {
     stable = false;
     }
     }
     if (stable) {
     setVApproximator();
     setQApproximator();
     return values;
     } else {
     evaluatePolicy(policy);
     }
     }

     }
     */

    private double[] findValues() {
        boolean stable = false;
        while (true) {

            for (int i = 0; i < numStates; i++) {
                int act = policy[i];
                double rightValue = 0;
                if (i < numStates - 1) {
                    rightValue = gamma * values[i + 1] + rewards[i];
                } else {
                    rightValue = gamma * values[i] + rewards[i]; //don't move
                }
                double leftValue = 0;
                if (i > 0) {
                    leftValue = gamma * values[i - 1] + rewards[i];
                } else {
                    leftValue = gamma * values[i] + rewards[i];
                }
                double actRight = rightValue * 0.9 + leftValue * 0.1;
                double actLeft = rightValue * 0.1 + leftValue * 0.9;
                if (actRight > actLeft) {
                    policy[i] = actionRight;
                } else {
                    policy[i] = actionLeft;
                }
                if (policy[i] != act) {
                    stable = false;
                }
            }
            if (stable) {
                evaluatePolicy(policy);
                setVApproximator();
                setQApproximator();
                return values;
            } else {
                evaluatePolicy(policy);
            }
            stable = true;
        }

    }
    /*
     private double[] evaluatePolicy(int policy[]) {
     while (true) {
     double delta = 0;
     for (int i = 0; i < numStates; i++) {
     double val = values[i];
     double rightValue = 0;
     if (i < numStates - 1) {
     rightValue = gamma * values[i + 1] + rewards[i + 1];
     } else {
     rightValue = gamma * values[i] + rewards[i]; //don't move
     }
     double leftValue = 0;
     if (i > 0) {
     leftValue = gamma * values[i - 1] + rewards[i - 1];
     } else {
     leftValue = gamma * values[i] + rewards[i];
     }
     if (policy[i] == actionRight) {
     rightValue *= 0.9;
     leftValue *= 0.1;
     } else {
     rightValue *= 0.1;
     leftValue *= 0.9;
     }
     values[i] = rightValue + leftValue;
     delta = Math.max(delta, Math.abs(val - values[i]));
     }
     if (delta < policyEvalStop) {
     break;
     }
     }
     return values;
     }
     */

    private double[] evaluatePolicy(int policy[]) {
        while (true) {
            double delta = 0;
            for (int i = 0; i < numStates; i++) {
                double val = values[i];
                double rightValue = 0;
                if (i < numStates - 1) {
                    rightValue = gamma * values[i + 1] + rewards[i];
                } else {
                    rightValue = gamma * values[i] + rewards[i]; //don't move
                }
                double leftValue = 0;
                if (i > 0) {
                    leftValue = gamma * values[i - 1] + rewards[i];
                } else {
                    leftValue = gamma * values[i] + rewards[i];
                }
                if (policy[i] == actionRight) {
                    rightValue *= 0.9;
                    leftValue *= 0.1;
                } else {
                    rightValue *= 0.1;
                    leftValue *= 0.9;
                }
                values[i] = rightValue + leftValue;
                delta = Math.max(delta, Math.abs(val - values[i]));
            }
            if (delta < policyEvalStop) {
                break;
            }
        }
        return values;
    }

    /**
     * Gets an array of optimal values for each state.
     *
     * @return The array of optimal values for each state.
     */
    private double[] getOptimalValues() {
        return values;
    }

    /**
     * Finds the state values for the given policy where policy is a numStates*2
     * array representing the probability of selecting each action in each of
     * the states. Returns the value as an array and also stores it internally.
     * GetOptimalValue functions will use the values given by this policy for
     * their results until you call findValues() again to find the optimal
     * values.
     *
     * @param policy
     */
    public double[] findValues(double policy[][]) {
        while (true) {
            double delta = 0;
            for (int i = 0; i < numStates; i++) {
                double val = values[i];
                double rightValue = 0;
                if (i < numStates - 1) {
                    rightValue = gamma * values[i + 1] + rewards[i + 1];
                } else {
                    rightValue = gamma * values[i] + rewards[i]; //don't move
                }
                double leftValue = 0;
                if (i > 0) {
                    leftValue = gamma * values[i - 1] + rewards[i - 1];
                } else {
                    leftValue = gamma * values[i] + rewards[i];
                }

                values[i] = policy[i][0] * (rightValue * 0.1 + leftValue * 0.9) + policy[i][1] * (rightValue * 0.9 + leftValue * 0.1);
                delta = Math.max(delta, Math.abs(val - values[i]));
            }
            if (delta < policyEvalStop) {
                break;
            }
        }
        setVApproximator();
        setQApproximator();
        return values;
    }

    private void setVApproximator() {
        BasisFunction bf[] = new BasisFunction[numStates];
        double mid[] = new double[numStates];
        for (int i = 0; i < numStates; i++) {
            double tileRange[][] = new double[1][2];
            tileRange[0][0] = ((double) i / (numStates - 1)) - (0.5 / (numStates - 1));
            tileRange[0][1] = ((double) i / (numStates - 1)) + (0.5 / (numStates - 1));
            mid[i] = (tileRange[0][0]+tileRange[0][1])/2.0;
            bf[i] = new TileBasis(tileRange);
        }
        System.out.println(Arrays.toString(mid));

        FunctionApproximator fa = new FunctionApproximator(bf, 1);
        v = new VApproximator(fa);
        v.setWeights(values.clone());
    }

    private void setQApproximator() {
        BasisFunction bf[] = new BasisFunction[numStates];
        for (int i = 0; i < numStates; i++) {
            double tileRange[][] = new double[1][2];
            tileRange[0][0] = ((double) i / (numStates - 1)) - (0.5 / (numStates - 1));
            tileRange[0][1] = ((double) i / (numStates - 1)) + (0.5 / (numStates - 1));
            bf[i] = new TileBasis(tileRange);
        }

        FunctionApproximator fa1 = new FunctionApproximator(bf, 1);
        FunctionApproximator fa2 = new FunctionApproximator(bf, 1);
        q = new QApproximator(new FunctionApproximator[]{fa1, fa2});
        double weights[] = q.getWeights();
        for (int i = 0; i < numStates; i++) {
            double val = values[i];
            double rightValue = 0;
            if (i < numStates - 1) {
                rightValue = gamma * values[i + 1] + rewards[i + 1];
            } else {
                rightValue = gamma * values[i] + rewards[i]; //don't move
            }
            double leftValue = 0;
            if (i > 0) {
                leftValue = gamma * values[i - 1] + rewards[i - 1];
            } else {
                leftValue = gamma * values[i] + rewards[i];
            }
            weights[i] = leftValue * 0.9 + rightValue * 0.1;
            weights[i + numStates] = rightValue * 0.9 + leftValue * 0.1;
        }
    }
}
