
package domains.chainwalk;

/**
 * Chain walk domain from 2003 LSPI Paper Lagoudakis, Michail G. and Parr, Ronald. Least-squares
policy iteration. Journal of Machine Learning Research,
 * @author Dean
 */
public class FiftyStateChainWalk extends ChainWalk {
    public FiftyStateChainWalk(double gamma) {
        super(gamma);
    }
    @Override
    public int getNumStates() {
        return 50;
    }

    @Override
    public double[] getRewards() {
        //I number states from 0 to 49
        double rewards[] = new double[getNumStates()];
        rewards[9] = 1;
        rewards[40] = 1;
        return rewards;
    }

    @Override
    public double[] getInitialValues() {
        return new double[getNumStates()];
    }

    @Override
    public int getInitialState() {
        int st = (int)Math.random()*(getNumStates());
        if (st == getNumStates()) {
            st --;
        }
        return st;
    }

    @Override
    public int[] getInitialPolicy() {
        return new int[getNumStates()];
    }
    
   
}
