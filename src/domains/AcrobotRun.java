/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package domains;

import domains.acrobot.Acrobot;
import experimental.SarsaLambdaTester;
import functionapproximation.FullDaubWithoutIndependent;
import functionapproximation.FullFourier;
import functionapproximation.FullTiling;
import functionapproximation.FunctionApproximator;
import functionapproximation.QApproximator;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import rl.policy.EpsilonGreedy;
import rl.policy.Policy;
import rl.state.State;

/**
 *
 * @author dean
 */
public class AcrobotRun {

    public static void main(String args[]) {
        int experiments = 100;
        int episodes = 100;
        int maxSteps = 20000;
        double alpha = 1;
        double gamma = 1;
        double lambda = 0.9;
        int minscale = 3;
        int maxscale = -1;

        int fourierOrder = 5;
        int dimensions = Acrobot.getNumDimensions();
        int experimentStart = 0;
        int experimentEnd = 200;
        //String experiment = "1";
        if (args.length == 1) {
            experimentStart = Integer.parseInt(args[0]);
            experimentEnd = experimentStart + 1;
        }
        
        for (int i = experimentStart; i < experimentEnd; i++) {
            double totalSteps[] = new double[episodes];
            FunctionApproximator FAs[] = new FunctionApproximator[Acrobot.getNumActions()];
            for (int k = 0; k < Acrobot.getNumActions(); k++) {
                // FAs[k] = new FullFourier(fourierOrder, dimensions);
                //FAs[k] = new FullDaubWithoutIndependent(minscale, maxscale, dimensions, 8);
                FAs[k] = new FullTiling(5,dimensions);
                System.out.println("Action " + k + " number of terms: " + FAs[k].getNumTerms());
            }
            QApproximator q = new QApproximator(FAs);
                    Policy p = new EpsilonGreedy(new Acrobot(), q,0.01);
            SarsaLambdaTester learner = new SarsaLambdaTester(q, p, alpha, gamma, lambda);
            double rets[] = new double[episodes];
            for (int j = 0; j < episodes; j++) {
                double ret = 0;
                Acrobot acro = new Acrobot();
                learner.startEpisode();
                int steps = 0;
                State currState = acro.getState();
                int move = learner.nextMove(currState);
                while (steps < maxSteps && !currState.isTerminal()) {
                    double reward = acro.step(move);
                    ret += reward;
                    State newState = acro.getState();
                    int nextMove = learner.nextMove(newState);
                    learner.addSample(currState, move, newState, nextMove, reward);
                    currState = newState;
                    move = nextMove;
                    steps++;
                }
                rets[j] = ret;
                totalSteps[j] = steps;
                System.out.println("Acrobot Episode " + j + " took " + steps + " steps");
            }
            FileWriter f = null;
            StringBuilder b = new StringBuilder();
            String output = "";
            for (int k = 0; k < episodes; k++) {
                //output += experiment + " " + (k + 1) + " " + (averageReturn[k] / numExperiments) + "\n";
                //b = b.append(i + 1).append(" ").append(k + 1).append(" ").append(averageReturn[k] / numExperiments).append("\n");
                b = b.append(i + 1).append(" ").append(k + 1).append(" ").append(totalSteps[k]).append("\n");
            }
            output = b.toString();


            String outfile = "fulltiling5_acrobot_lambda_" + lambda + "_" + i + ".dat";
            f = null;
            try {
                f = new FileWriter(new File(outfile));

                f.write(output);
                f.flush();
                f.close();
            } catch (IOException ex) {
                Logger.getLogger(AcrobotRun.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
