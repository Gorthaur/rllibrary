package domains.mountaincarND;

import domains.Domain;
import java.util.LinkedList;
import rl.state.State;

/**
 *
 * @author Dean
 */
public class MountainCarND extends Domain {

    private int dimensions = 1;
    final static double minVelocity = -0.07;
    final static double maxVelocity = 0.07;
    final static double minPosition = -1.2;
    final static double maxPosition = 0.6;
    final static double goalPosition = 0.5;
    final static double initialPosition = -Math.PI / 6;
    final static double initialVelocity = 0;
    double position[];
    double velocity[];
    LinkedList<Integer> availableActions;

    public MountainCarND() {
        this(1);
    }

    public MountainCarND(int n) {
        super();
        this.dimensions = n;
        this.position = new double[dimensions];
        this.velocity = new double[dimensions];
        availableActions = new LinkedList<Integer>();
        for (int i = 0; i <= 2*dimensions; i++) {
            availableActions.add(i);
        }
        resetDomain();
    }

    @Override
    public State getState() {
        double state[] = new double[dimensions * 2];
        for (int i = 0; i < position.length; i++) {
            state[i] = (position[i] - minPosition) / (maxPosition - minPosition);
            state[dimensions + i] = (velocity[i] - minVelocity) / (maxVelocity - minVelocity);
        }
        State s = new State(state);
        if (isTerminal()) {
            s.setTerminal(true);
        }
        return s;
    }

    @Override
    public double step(int action) {
        //0 is always neutral, else 1,2 belong to dim 1 (left and right respectively), 2,3 dim 2 etc.
        
        /* For example in 3d
         *    	0 - coast
         *	1 - left
         *	2 - right
         *	3 - down
         *	4 - up
         */
        //update all velocities
        for (int i = 0; i < dimensions; i++) {
            velocity[i] += Math.cos(3 * position[i]) * (-0.0025);
            int actLeft = 2*i + 1;
            int actRight = 2*i + 2;
            if (action == actLeft) {
                velocity[i] -= 0.001;
            }
            else if (action == actRight) {
                velocity[i] += 0.001;
            }
            velocity[i] = Math.max(Math.min(velocity[i], maxVelocity), minVelocity);
        }
        
        //update all positions
        for (int i = 0; i < dimensions; i++) {
            position[i] += velocity[i];
            position[i] = Math.max(Math.min(position[i], maxPosition), minPosition);
            if (position[i] == maxPosition && velocity[i] > 0) {
                velocity[i] = 0;
            }
            else if (position[i] == minPosition && velocity[i] < 0) {
                velocity[i] = 0;
            }
        }
        
        if (isTerminal()) {
            return 0;
        }
        else {
            return -1;
        }
    }

    @Override
    public void setState(State s) {
        if (s.getStateLength() != 2*dimensions) {
            throw new IllegalStateException("State dimensions don't match the domain.");
        }
        for (int i = 0; i < dimensions; i++) {
            position[i] = s.getState()[i] * (maxPosition - minPosition) + minPosition;
            velocity[i] = s.getState()[dimensions+i]*(maxVelocity - minVelocity) + minVelocity;
        }
    }

    public boolean isTerminal() {
        for (int i = 0; i < dimensions; i++) {
            if (position[i] < goalPosition) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public boolean isEpisodeOver() {
        return isTerminal();
    }

    @Override
    public State getRandomState() {
        double state[] = new double[dimensions*2];
        boolean terminal = true;
        for (int i = 0; i < state.length; i++) {
            state[i] = Math.random();
            if (i < dimensions) {
                double mCarPos = state[i] * (maxPosition - minPosition) + minPosition;
                if (mCarPos < goalPosition) {
                    terminal = false;
                }
            }
        }
        if (terminal) {
            return getRandomState();
        }
        else {
            return new State(state);
        }
    }

    @Override
    public int getNumActions(State s) {
        return 2*dimensions + 1;
    }

    @Override
    public LinkedList<Integer> getAvailableActions(State s) {
        return availableActions;
    }

    @Override
    public int getTotalNumActions() {
        return 2*dimensions+1;
    }

    @Override
    public void resetDomain() {
        for (int i = 0; i < dimensions; i++) {
            position[i] = initialPosition + (Math.random() - 0.5)/100;
            velocity[i] = initialVelocity;
        }
    }

    @Override
    public String getDescriptionString() {
        return "mountaincar_" + dimensions + "d";
    }

    @Override
    public int getTotalNumDimensions() {
        return dimensions*2;
    }
}
