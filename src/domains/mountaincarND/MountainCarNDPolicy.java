/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package domains.mountaincarND;

import domains.Domain;
import rl.policy.Policy;
import rl.state.State;



/**
 *
 * @author dean
 */
public class MountainCarNDPolicy extends Policy {
    public MountainCarNDPolicy(Domain d) {
        super(d);
    }

    @Override
    public double[] getProbabilities(State s) {
        double max = -1;
        int maxIndex = -1;
        Domain d = getDomain();
        double probabilities[] = new double[d.getTotalNumActions()];
        //second half are 
        for (int i = s.getStateLength()/2; i < s.getStateLength(); i++) {
            double val = Math.abs(s.getState()[i] - 0.5);
            if (val > max) {
                max = val;
                maxIndex = i;
            }
        }
        
        if (s.getState()[maxIndex] > 0.5) { //go right
            int actionIndex = 2*(maxIndex - (d.getTotalNumDimensions()/2))+2;
            probabilities[actionIndex] = 1;
        }
        else { //go left
            int actionIndex = 2*(maxIndex - (d.getTotalNumDimensions()/2))+1;
            probabilities[actionIndex] = 1;
        }
        return probabilities;
    }
    
}
