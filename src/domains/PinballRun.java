package domains;

import PinBallDomain.PinBall;
import PinBallDomain.PinBallGUI;
import PinBallDomain.PinBallState;
import domains.pinball.PinballWrapper;
import experimental.SarsaLambdaFourierAdd;
import experimental.SarsaLambdaTester;
import experimental.SarsaLambdaTester2;
import experimental.SarsaWiFDD;
import functionapproximation.FourierBasis;
import functionapproximation.FullDaubFunctionApproximation;
import functionapproximation.FullDaubNoTails;
import functionapproximation.FullDaubWithoutIndependent;
import functionapproximation.FullFourier;
import functionapproximation.FullRBF;
import functionapproximation.FullTiling;
import functionapproximation.FunctionApproximator;
import functionapproximation.IndependentDaubFunctionApproximation;
import functionapproximation.IndependentFourier;
import functionapproximation.IndependentRBF;
import functionapproximation.IndependentTiling;
import functionapproximation.QApproximator;
import functionapproximation.WiFDDQApproximator;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import rl.policy.EpsilonGreedy;
import rl.policy.KEpsilonGreedy;
import rl.policy.Policy;
import rl.learningalgorithms.SarsaLambdaAlphaScale;
import rl.state.State;

public class PinballRun {

    public static void main(String args[]) throws IOException {
        //String pinballDomain = "./pinballfiles/pinball-maze1.cfg";
        //String pinballDomain = "./pinballfiles/pinball_simple_single.cfg";
        //String pinballDomain = "./pinballfiles/pinball_hard_single.cfg";
        String pinballDomain = "./pinballfiles/pinball-diagonalhard-botleft.cfg";
        //PinBallGUI pg = new PinBallGUI(pinballDomain);
        //pg.setVisible(true);
        int experimentStart = 0;
        int experimentEnd = 200;
        //String experiment = "1";
        if (args.length >= 1) {
            experimentStart = Integer.parseInt(args[0]);
            experimentEnd = experimentStart + 1;
        }
        System.out.println(experimentEnd);
        double alphas[] = new double[]{1};
        double lambdas[] = new double[]{0.9};
        for (double alph : alphas) {
            for (double lamb : lambdas) {
                System.out.println("Alpha " + alph);

                int numEpisodes = 550;


                int numFeatures = 4; //num state variables
                int numActions = 5;



                
                //Policy p = new KEpsilonGreedy(0.01, 10);
                double alpha = alph; //learning rate
                double gamma = 1; //discount (1 = no discount)
                double lambda = lamb; //eligibility trace
                double potential = 0; //50000

                double averageReturn[] = new double[numEpisodes];
                int baseScale = 2;
                int maxScale = 1;

                //String dispFile = "gametrace.dat";
                FileWriter f = null;
                FileWriter g = null;

                ArrayList<SarsaLambdaFourierAdd.AddedBfs>[] addedBfs = new ArrayList[numActions];
                
                for (int i = experimentStart; i < experimentEnd; i++) {
                    double rets[] = new double[numEpisodes];
                    double numbfs[] = new double[numEpisodes];
                    System.out.println("Start Experiment " + (i + 1));
                    String foutfile = "fourieradd_maze_lambda_" + lambda + "_" + i;
                    String goutfile = foutfile + "_bfs";
                    FunctionApproximator FAs[] = new FunctionApproximator[numActions];
                    for (int j = 0; j < numActions; j++) {
                        //FAs[j] = new IndependentDaubFunctionApproximation(baseScale, maxScale, numFeatures, 8);
                        //FAs[j] = new FullDaubWithoutIndependent(baseScale, maxScale, numFeatures, 8);
                        
                        //FAs[j] = new IndependentFourier(9, numFeatures);
                        //FAs[j] = new FullTiling(10, numFeatures);
                        //FAs[j] = new IndependentTiling(10, numFeatures);
                        //FAs[j] = new IndependentRBF(10, numFeatures);
                        //FAs[j] = new FullRBF(10, numFeatures);
                        //FAs[j] = new FullDaubNoTails(baseScale, maxScale, numFeatures, 8);
                        if (args.length >= 3) {
                            int order = Integer.parseInt(args[2]);
                            File pinf = new File(pinballDomain);
                            String dom = pinf.getName();
                            if (args[1].equals("fullfourier")) {
                                FAs[j] = new FullFourier(order, numFeatures);
                                foutfile = "fullfourier" + order + "_lambda_" + lambda + "_" + dom + "_" + i;
                            } else if (args[1].equals("independentfourier")) {
                                FAs[j] = new IndependentFourier(order, numFeatures);
                                foutfile = "independentfourier" + order + "_lambda_" + lambda + "_" + dom + "_" + i;
                            } else if (args[1].equals("fullrbf")) {
                                FAs[j] = new FullRBF(order, numFeatures);
                                foutfile = "fullrbf" + order + "_lambda_" + lambda + "_" + dom + "_" + i;
                            } else if (args[1].equals("independentrbf")) {
                                FAs[j] = new IndependentRBF(order, numFeatures);
                                foutfile = "independentrbf" + order + "_lambda_" + lambda + "_" + dom + "_" + i;
                            } else if (args[1].equals("fulltiling")) {
                                FAs[j] = new FullTiling(order, numFeatures);
                                foutfile = "fulltiling" + order + "_lambda_" + lambda + "_" + dom + "_" + i;
                            } else if (args[1].equals("independenttiling")) {
                                FAs[j] = new IndependentTiling(order, numFeatures);
                                foutfile = "independenttiling" + order + "_lambda_" + lambda + "_" + dom + "_" + i;
                            } else if (args[1].equals("fulldaub")) {
                                int basescale = Integer.parseInt(args[3]);
                                int maxscale = Integer.parseInt(args[4]);
                                FAs[j] = new FullDaubWithoutIndependent(basescale, maxscale, numFeatures, order);
                                foutfile = "fulldaub" + order + "_m" + basescale + "_n" + maxscale + "_lambda_" + lambda + "_" + dom + "_" + i;
                            } else if (args[1].equals("independentdaub")) {
                                int basescale = Integer.parseInt(args[3]);
                                int maxscale = Integer.parseInt(args[4]);
                                FAs[j] = new IndependentDaubFunctionApproximation(basescale, maxscale, numFeatures, order);
                                foutfile = "independentdaub" + order + "_m" + basescale + "_n" + maxscale + "_lambda_" + lambda + "_" + dom + "_" + i;
                            }
                            else if (args[1].equals("fulldaubnotails")) {
                                int basescale = Integer.parseInt(args[3]);
                                int maxscale = Integer.parseInt(args[4]);
                                FAs[j] = new FullDaubNoTails(basescale, maxscale, numFeatures, order);
                                foutfile = "fulldaubnotails" + order + "_m" + basescale + "_n" + maxscale + "_lambda_" + lambda + "_" + dom + "_" + i;
                            }
                        }
                        else {
                            //FAs[j] = new IndependentTiling(5, numFeatures);
                            
                            //FAs[j] = new IndependentTiling(2, numFeatures);
                            FAs[j] = new IndependentFourier(5, numFeatures);
                        }
                    }


                    //WiFDDQApproximator q = new WiFDDQApproximator(FAs);
                    
                    QApproximator q = new QApproximator(FAs);
                    Policy p = new EpsilonGreedy(new PinballWrapper(pinballDomain), q, 0.15);
                    /*
                    q.addBasis(new FourierBasis(new double[]{1,1,0,0}), 0); //1
                    q.addBasis(new FourierBasis(new double[]{0,1,1,1}), 0); //2
                    q.addBasis(new FourierBasis(new double[]{2,2,0,0}), 0); //3
                    q.addBasis(new FourierBasis(new double[]{1,0,2,0}), 0); //4
                    q.addBasis(new FourierBasis(new double[]{0,4,0,1}), 0); //5
                    q.addBasis(new FourierBasis(new double[]{2,4,0,0}), 0); //6
                    q.addBasis(new FourierBasis(new double[]{0,4,1,1}), 0); //7
                    q.addBasis(new FourierBasis(new double[]{0,0,2,1}), 0); //8
                    q.addBasis(new FourierBasis(new double[]{1,4,0,0}), 0); //9
                    q.addBasis(new FourierBasis(new double[]{0,3,2,0}), 0); //10
                    
                    
                    q.addBasis(new FourierBasis(new double[]{0,1,1,1}), 1); //1
                    q.addBasis(new FourierBasis(new double[]{0,2,0,1}), 1); //2
                    q.addBasis(new FourierBasis(new double[]{1,1,0,1}), 1); //3
                    q.addBasis(new FourierBasis(new double[]{1,0,1,0}), 1); //4
                    q.addBasis(new FourierBasis(new double[]{0,4,0,1}), 1); //5
                    q.addBasis(new FourierBasis(new double[]{1,1,1,0}), 1); //6
                    q.addBasis(new FourierBasis(new double[]{0,3,0,1}), 1); //7
                    q.addBasis(new FourierBasis(new double[]{1,2,0,0}), 1); //8
                    q.addBasis(new FourierBasis(new double[]{4,1,1,0}), 1); //9
                    q.addBasis(new FourierBasis(new double[]{4,0,1,0}), 1); //10
                    
                    q.addBasis(new FourierBasis(new double[]{2,0,1,0}), 2); //1
                    q.addBasis(new FourierBasis(new double[]{2,0,0,1}), 2); //2
                    q.addBasis(new FourierBasis(new double[]{1,0,1,1}), 2); //3
                    q.addBasis(new FourierBasis(new double[]{2,1,0,0}), 2); //4
                    q.addBasis(new FourierBasis(new double[]{0,2,0,1}), 2); //5
                    q.addBasis(new FourierBasis(new double[]{4,0,1,0}), 2); //6
                    q.addBasis(new FourierBasis(new double[]{0,1,0,1}), 2); //7
                    q.addBasis(new FourierBasis(new double[]{4,0,0,1}), 2); //8
                    q.addBasis(new FourierBasis(new double[]{1,1,0,0}), 2); //9
                    q.addBasis(new FourierBasis(new double[]{0,1,0,3}), 2); //10

                    q.addBasis(new FourierBasis(new double[]{1,3,0,0}), 3); //1
                    q.addBasis(new FourierBasis(new double[]{0,1,0,2}), 3); //2
                    q.addBasis(new FourierBasis(new double[]{1,0,1,1}), 3); //3
                    q.addBasis(new FourierBasis(new double[]{1,1,1,0}), 3); //4
                    q.addBasis(new FourierBasis(new double[]{1,4,0,0}), 3); //5
                    q.addBasis(new FourierBasis(new double[]{1,0,0,2}), 3); //6
                    q.addBasis(new FourierBasis(new double[]{1,1,1,1}), 3); //7
                    q.addBasis(new FourierBasis(new double[]{0,0,1,2}), 3); //8
                    q.addBasis(new FourierBasis(new double[]{1,2,0,0}), 3); //9
                    q.addBasis(new FourierBasis(new double[]{4,1,2,0}), 3); //10

                    q.addBasis(new FourierBasis(new double[]{2,0,0,1}), 4); //1
                    q.addBasis(new FourierBasis(new double[]{2,1,0,0}), 4); //2
                    q.addBasis(new FourierBasis(new double[]{1,0,1,1}), 4); //3
                    q.addBasis(new FourierBasis(new double[]{2,1,1,0}), 4); //4
                    q.addBasis(new FourierBasis(new double[]{2,0,1,0}), 4); //5
                    q.addBasis(new FourierBasis(new double[]{0,2,0,1}), 4); //6
                    q.addBasis(new FourierBasis(new double[]{0,2,1,0}), 4); //7
                    q.addBasis(new FourierBasis(new double[]{4,0,1,0}), 4); //8
                    q.addBasis(new FourierBasis(new double[]{0,1,1,1}), 4); //9
                    q.addBasis(new FourierBasis(new double[]{0,4,0,1}), 3); //10

                    */
                    
                    //q.printFunction();
                    System.out.println("Terms per Action: " + FAs[0].getNumTerms());
                    System.out.println("All Terms: " + q.getNumTerms());
                    //SarsaLambdaTester learner = new SarsaLambdaTester(q, p, alpha, gamma, lambda);
                    //SarsaLambdaTester2 learner = new SarsaLambdaTester2(q, p, alpha, gamma, lambda);
                    //SarsaLambda learner = new iFFDSarsa(q, p, alpha, gamma, lambda, eta);
                    //SarsaWiFDD learner = new SarsaWiFDD(q, p, alpha, gamma, lambda, potential);
                    //SarsaLambdaFourierAdd learner = new SarsaLambdaFourierAdd(q, p, alpha, gamma, lambda);
                    SarsaLambdaAlphaScale learner = new SarsaLambdaAlphaScale(q,p,alpha,gamma,lambda);
                    //learner.addBFs(addedBfs);
                    for (int k = 0; k < numEpisodes; k++) {
                        //if (k == 50) {
                        //if (k > 0 && k % 150 == 0) {
                        //if (k > 0 && k % 20 == 0) {
                            //learner.addBFs(addedBfs);
                            //learner.clearErrors();
                        //}
                        PinBall pin = new PinBall(pinballDomain);
                        //PinBall pin = new PinBall("./pinballfiles/pinball-maze2.cfg");

                        System.out.print(".");
                        learner.startEpisode();
                        int steps = 0;
                        PinBallState pinstate = pin.getState();
                        State currState = new State(pinstate.getDescriptor());
                        int move = learner.nextMove(currState);
                        double ret = 0;
                        while (!pinstate.endState()) {
                            //if (k > 30) {
                            //    f.write("" + pin.getBall().getX() + " " + pin.getBall().getY() + " " + move + "\n");
                            //}
                            double reward = pin.step(move);
                            //totalAverage += reward;
                            ret += reward;
                            pinstate = pin.getState();
                            State newState = new State(pinstate.getDescriptor());

                            int nextMove = learner.nextMove(newState);
                            learner.addSample(currState, move, newState, nextMove, reward);
                            currState = newState;
                            move = nextMove;
                            steps++;
                            /*
                            if (steps > 1500000) {
                            for (int x = 0; x < 1 ; x+=0.2) {
                            for (int y = 0; y < 1; y +=0.2) {
                            ValueFunctionPlotter plot = new ValueFunctionPlotter(q, "data/" + x + "-" + y + "-");
                            plot.dimStart[0] = x;
                            plot.dimEnd[0] = x;
                            plot.dimStart[1] = y;
                            plot.dimEnd[1] = y;
                            plot.dimStep[0] = 0;
                            plot.dimStep[1] = 0;
                            plot.writeValuesMatFile();
                            }
                            }
                            break;
                            }
                             * 
                             */
                            
                        }
                        System.out.println("All terms: " + q.getNumTerms());
                        System.out.println("Terms per action: " + q.getNumTerms(0));
                        numbfs[k] = q.getNumTerms();
                        averageReturn[k] += ret;
                        rets[k] = ret;
                        System.out.println("Episode " + k + " took " + steps + " steps with return " + ret);
                    }
                    /*
                    for (int j = 0; j < addedBfs.length; j++) {
                        System.out.println("Action " + j);
                        Collections.sort(addedBfs[j]);
                        for (SarsaLambdaFourierAdd.AddedBfs abs: addedBfs[j]) {
                            System.out.println(abs.getTimes() + " " + abs.getBasis().getBasisString());
                        }
                    }
                     * 
                     */
                    //q.printFunction();

                    StringBuilder b = new StringBuilder();  
                    StringBuilder gout = new StringBuilder();
                    String output = "";
                    String goutput = "";
                    for (int k = 0; k < numEpisodes; k++) {
                        //output += experiment + " " + (k + 1) + " " + (averageReturn[k] / numExperiments) + "\n";
                        //b = b.append(i + 1).append(" ").append(k + 1).append(" ").append(averageReturn[k] / numExperiments).append("\n");
                        b = b.append(i + 1).append(" ").append(k + 1).append(" ").append(rets[k]).append("\n");
                        gout = gout.append(i + 1).append(" ").append(k + 1).append(" ").append(numbfs[k]).append("\n");
                    }
                    output = b.toString();
                    goutput = gout.toString();
                    goutfile = foutfile + "_bfs.dat";
                    foutfile = foutfile + ".dat";
                    
                    f = null;

                    f = new FileWriter(new File(foutfile));
                    f.write(output);
                    f.flush();
                    f.close();
                    
                    g =  null;
                    g = new FileWriter(new File(goutfile));
                    g.write(goutput);
                    g.flush();
                    g.close();
                    
                }
            }

        }

    }
}
