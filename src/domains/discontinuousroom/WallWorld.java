package domains.discontinuousroom;

import domains.Domain;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import rl.state.State;

public class WallWorld extends Domain {

    public WallWorld() {
        x = 1.0;
        y = 1.0;
    }

    public WallWorldState getState() {
        return new WallWorldState(x, y);
    }

    public WallWorldDiscreteState getDiscreteState() {
        return new WallWorldDiscreteState(x, y);
    }
   

    public double step(int a) {
        double newx = x;
        double newy = y;

        switch (a) {
            case UP:
                newy += 0.5;
                break;
            case DOWN:
                newy -= 0.5;
                break;
            case LEFT:
                newx -= 0.5;
                break;
            case RIGHT:
                newx += 0.5;
                break;
        }

        if (collision(x, y, newx, newy)) {
            return -1.0;
        }

        x = newx;
        y = newy;

        if (atGoal()) {
            return 1000.0;
        }
        return -1.0;
    }

    boolean atGoal() {
        if (Math.abs(x - goal_x) > 1) {
            return false;
        }
        if (Math.abs(y - goal_y) > 1) {
            return false;
        }

        return true;
    }

    boolean collision(double x, double y, double xx, double yy) {
        if (xx < 0.0) {
            return true;
        }
        if (xx > 10.0) {
            return true;
        }

        if (yy < 0.0) {
            return true;
        }
        if (yy > 6.0) {
            return true;
        }

        if (x <= 8.0) {
            if (yy == 3.0) {
                return true;
            }
        } else if ((xx == 8.0) && (yy == 3.0)) {
            return true;
        }

        return false;
    }
    double x;
    double y;

    public static int getNumActions() {
        return 4;
    }

    public static int getNumDimensions() {
        return 2;
    }

    public static int stateNumber(double x, double y) {
        int snum = (int) ((x * 2.0 * 2.0 * 6.5) + y * 2.0);

        return snum;
    }
    public static final int UP = 0;
    public static final int DOWN = 1;
    public static final int LEFT = 2;
    public static final int RIGHT = 3;
    public static final double goal_x = 1.0;
    public static final double goal_y = 4.5;
    /*
    public static void main(String args[]) {
    WallWorld w = new WallWorld();
    
    String outfile = "../results/wall_samples.dat";
    FileWriter f = null;
    
    try {
    f = new FileWriter(new File(outfile));
    } catch (java.io.IOException e) {
    };
    
    for (double x = 0.0; x <= 10.0; x += 0.5) {
    for (double y = 0.0; y <= 6.0; y += 0.5) {
    int snum = WallWorld.stateNumber(x, y);
    System.out.println("State num " + snum);
    
    if (!((y == 3.0) && (x <= 8.0))) {
    for (int act = UP; act <= RIGHT; act++) {
    w.x = x;
    w.y = y;
    
    double r = w.step(act);
    
    double xx = w.x;
    double yy = w.y;
    
    try {
    String S = x + " " + y + " " + act + " " + xx + " " + yy + " " + r + "\n";
    f.write(S);
    } catch (IOException e) {
    };
    }
    }
    }
    }
    
    try {
    f.close();
    } catch (java.io.IOException e) {
    };
    }
     * 
     */

    @Override
    public boolean isEpisodeOver() {
        return atGoal();
    }

    @Override
    public void setState(State s) {
        if (s.getStateLength() == getNumDimensions()) {
            //state is scaled between 0 and 1. Unscale it.
            this.x = Math.round(s.getState()[0]*20)/2.0;
            this.y = Math.round(s.getState()[1]*12)/2.0; //this is just so we get back to 0.5's exactly.
        }
        else {
            throw new IllegalStateException("State is illegal");
        }
    }

    @Override
    public State getRandomState() {
        double x = (Math.round(Math.random()*20))/2.0;
        double y = (Math.round(Math.random()*12))/2.0;
        if (x <= 8.0 && y == 3.0) {
            return getRandomState();
        }
        else {
            return new WallWorldState(x,y);
        }
    }

    @Override
    public int getNumActions(State s) {
        return WallWorld.getNumActions();
    }

    @Override
    public LinkedList<Integer> getAvailableActions(State s) {
        LinkedList<Integer> actions = new LinkedList<Integer>();
        for (int i = 0; i < WallWorld.getNumActions(); i++) {
            actions.add(i);
        }
        return actions;
    }

    @Override
    public int getTotalNumActions() {
        return WallWorld.getNumActions();
    }

    @Override
    public void resetDomain() {
        x = 1.0;
        y = 1.0;
    }

    @Override
    public String getDescriptionString() {
        return "discontinuous_room";
    }

    @Override
    public int getTotalNumDimensions() {
        return WallWorld.getNumDimensions();
    }
}
