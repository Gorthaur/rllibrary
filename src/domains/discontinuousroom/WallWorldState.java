package domains.discontinuousroom;

import rl.state.State;

public class WallWorldState extends State {
    
    WallWorldState(double state[]) {
        super(state);
        x = state[0]*10;
        y = state[1]*6;
    }

    WallWorldState(double X, double Y) {
        super(new double[]{X, Y});
        x = X;
        y = Y;
    }

    public boolean endState() {
        if (Math.abs(x - WallWorld.goal_x) > 1) {
            return false;
        }
        if (Math.abs(y - WallWorld.goal_y) > 1) {
            return false;
        }

        return true;
    }

    @Override
    public double[] getState() {
        return getState(x, y);
    }

    public static double[] getState(double x, double y) {
        double[] d = new double[2];

        d[0] = x / (10.0);
        d[1] = y / (6.0);

        return d;
    }

    @Override
    public boolean isTerminal() {
        return endState();
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public State stateActionPair(double a) {
        // TODO Auto-generated method stub
        return null;
    }
    double x, y;
}
