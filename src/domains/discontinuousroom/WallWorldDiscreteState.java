package domains.discontinuousroom;

import rl.state.State;


public class WallWorldDiscreteState extends State {

	WallWorldDiscreteState(double X, double Y)
	{	
            super(new double[]{X, Y});
		x = X;
		y = Y;
		
		S = WallWorld.stateNumber(x, y);
	}
	
	public boolean endState() 
	{
		if(Math.abs(x - WallWorld.goal_x) > 1) return false;
		if(Math.abs(y - WallWorld.goal_y) > 1) return false;
		
		return true;
	}
        
        public boolean isTerminal() {
            return endState();
        }

	public double[] getState() 
	{
		double[] d = new double[1];
		d[0] = S;
		
		return d;
	}

	public double getX()
	{
		return x;
	}
	
	public double getY()
	{
		return y;
	}
	
	public int getS()
	{
		return S;
	}
	
	public State stateActionPair(double a) {
		// TODO Auto-generated method stub
		return null;
	}

	double x, y;
	int S;
}
