/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package domains.acrobot;

import domains.Domain;
import rl.policy.Policy;
import rl.state.State;

/**
 *
 * @author mitch
 */
public class AcroPolicy extends Policy {
    public AcroPolicy(Domain domain) {
        super(domain);
    }
    
    @Override
    public double[] getProbabilities(State s) {
        double probs[] = new double[this.getDomain().getTotalNumActions()];
        if (s.getState()[2]>0.5 && s.getState()[3]>0.5) probs[2]=1;  // If the main joint and second joint are swinging in the same direction, keep them swinging
        else if (s.getState()[2]<0.5 && s.getState()[3]<0.5) probs[0]=1; //as above
        else if (s.getState()[2]>0.5 && s.getState()[3]<=0.5) probs[0]=1; // otherwise, try swinging the second joint in the right direction
        else if (s.getState()[2]<0.5 && s.getState()[3]>=0.5) probs[2]=1; // as above
        else if (s.getState()[0]>0.5) probs[2]=1; // otherwise, the main joint has zero velocity. swing out of our current quadrant
        else probs[0]=1;
        return probs;
    }
}
