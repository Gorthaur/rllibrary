package domains.acrobot;

/**
 *
 * @author Dean
 */
public class Acrobot extends VariableMassAcrobot {
    /**
     * Normal acrobot, with standard link mass.
     */
    public Acrobot() {
        
    }
    
    public static int getNumActions() {
        return VariableMassAcrobot.getNumActions();
    }
    
    public static int getNumDimensions() {
        return VariableMassAcrobot.getNumDimensions();
    }
    
    public String getDescriptionString() {
        return "standard_acrobot";
    }
}
