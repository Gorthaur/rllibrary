package domains.acrobot;

import rl.state.State;

public class AcrobotState extends State {

    double theta1, theta2, theta1Dot, theta2Dot;

    /**
     * Constructor for acrobot state. Takes in an array of scaled (between 0 and 1) state values)
     * @param st A scaled array of state values.
     */
    public AcrobotState(double st[]) {
        super(st);
        // This is older, compatability code.
        theta1 = (st[0] - 0.5)*(Math.PI * 2.0);
        theta2 = (st[1] - 0.5)*(Math.PI * 2.0);
        theta1Dot = st[2]*(2 * VariableMassAcrobot.maxTheta1Dot) - VariableMassAcrobot.maxTheta1Dot;
        theta2Dot = st[3]*(2 * VariableMassAcrobot.maxTheta2Dot) - VariableMassAcrobot.maxTheta2Dot;
    }

    public AcrobotState(double theta1, double theta2, double theta1Dot, double theta2Dot) {
        super(getState(theta1,theta2,theta1Dot,theta2Dot));
        this.theta1 = theta1;
        this.theta2 = theta2;
        this.theta1Dot = theta1Dot;
        this.theta2Dot = theta2Dot;
    }

    public boolean endState() {
        double position = VariableMassAcrobot.getTip(theta1, theta2);
        return (position > VariableMassAcrobot.acrobotGoalPosition);
    }
    
    public boolean isTerminal() {
        return endState();
    }

    @Override
    public double[] getState() {
        return getState(theta1,theta2,theta1Dot,theta2Dot);
    }

    public static double[] getState(double theta1, double theta2, double theta1Dot, double theta2Dot) {
        double[] d = new double[4];

        d[0] = (theta1 / (Math.PI * 2.0)) + 0.5;
        d[1] = (theta2 / (Math.PI * 2.0)) + 0.5;
        d[2] = (theta1Dot + VariableMassAcrobot.maxTheta1Dot) / (2 * VariableMassAcrobot.maxTheta1Dot);
        d[3] = (theta2Dot + VariableMassAcrobot.maxTheta2Dot) / (2 * VariableMassAcrobot.maxTheta2Dot);

        return d;
    }

    public State stateActionPair(double a) {
        // We don't need this here
        return this;
    }
}
