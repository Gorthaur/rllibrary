package domains.acrobot;

import domains.Domain;
import java.util.LinkedList;
import rl.state.State;

/**
 * 
 * @author gdk
 *
 *  Adapted from the Acrobot Java code available on the RLGlue RL Library agents repository.
 *  That code was written by Brian Tanner, adapted from previous code by Adam White. 
 *
 */
public class VariableMassAcrobot extends Domain {

    public VariableMassAcrobot(double mass1, double mass2) {
        init(mass1, mass2);
    }

    public VariableMassAcrobot(double mass1) {
        init(mass1, 1.0);
    }

    public VariableMassAcrobot() {
        init(1.0, 1.0);
    }

    private void init(double mass1, double mass2) {
        m1 = mass1;
        m2 = mass2;

        theta1 = 0;
        theta2 = 0;
        theta1Dot = 0;
        theta2Dot = 0;
    }

    public AcrobotState getState() {
        return new AcrobotState(theta1, theta2, theta1Dot, theta2Dot);
    }

    boolean atGoal() {
        return (getTip(theta1, theta2) > acrobotGoalPosition);
    }

    public static int getNumActions() {
        return 3;
    }

    public static int getNumDimensions() {
        return 4;
    }

    public double getMass1() {
        return m1;
    }

    public double getMass2() {
        return m2;
    }

    @Override
    public double step(int a) {
        double torque = a - 1.0d;
        double d1;
        double d2;

        double phi_1;
        double phi_2;

        double theta1_ddot;
        double theta2_ddot;

        //torque is in [-1,1]
        //We'll make noise equal to at most +/- 1
        //double theNoise=transitionNoise*2.0d*(ourRandomNumber.nextDouble()-.5d);
        //
        //torque+=theNoise;

        int count = 0;
        while (!atGoal() && count < 4) {
            count++;

            d1 = m1 * Math.pow(lc1, 2) + m2 * (Math.pow(l1, 2) + Math.pow(lc2, 2) + 2 * l1 * lc2 * Math.cos(theta2)) + I1 + I2;
            d2 = m2 * (Math.pow(lc2, 2) + l1 * lc2 * Math.cos(theta2)) + I2;

            phi_2 = m2 * lc2 * g * Math.cos(theta1 + theta2 - Math.PI / 2.0);
            phi_1 = -(m2 * l1 * lc2 * Math.pow(theta2Dot, 2) * Math.sin(theta2) - 2 * m2 * l1 * lc2 * theta1Dot * theta2Dot * Math.sin(theta2)) + (m1 * lc1 + m2 * l1) * g * Math.cos(theta1 - Math.PI / 2.0) + phi_2;

            theta2_ddot = (torque + (d2 / d1) * phi_1 - m2 * l1 * lc2 * Math.pow(theta1Dot, 2) * Math.sin(theta2) - phi_2) / (m2 * Math.pow(lc2, 2) + I2 - Math.pow(d2, 2) / d1);
            theta1_ddot = -(d2 * theta2_ddot + phi_1) / d1;

            theta1Dot += theta1_ddot * dt;
            theta2Dot += theta2_ddot * dt;

            theta1 += theta1Dot * dt;
            theta2 += theta2Dot * dt;
        }

        if (Math.abs(theta1Dot) > maxTheta1Dot) {
            theta1Dot = Math.signum(theta1Dot) * maxTheta1Dot;
        }

        if (Math.abs(theta2Dot) > maxTheta2Dot) {
            theta2Dot = Math.signum(theta2Dot) * maxTheta2Dot;
        }
        /* Put a hard constraint on the Acrobot physics, thetas MUST be in [-PI,+PI]
         * if they reach a top then angular velocity becomes zero
         */
        if (Math.abs(theta2) > Math.PI) {
            theta2 = Math.signum(theta2) * Math.PI;
            theta2Dot = 0;
        }
        if (Math.abs(theta1) > Math.PI) {
            theta1 = Math.signum(theta1) * Math.PI;
            theta1Dot = 0;
        }

        if (atGoal()) {
            return 0;
        }

        return -1;
    }

    public static double getTip(double theta1, double theta2) {
        double firstJointEndHeight = l1 * Math.cos(theta1);
        double secondJointEndHeight = l2 * Math.sin(Math.PI / 2 - theta1 - theta2);

        return -(firstJointEndHeight + secondJointEndHeight);
    }

    public double getTip() {
        return getTip(theta1, theta2);
    }
    private static final double dt = 0.05;
    private static final double g = 9.8;
    private double m1 = 1.0;
    private double m2 = 1.0;
    private static final double l1 = 1.0;
    private static final double l2 = 1.0;
    private static final double lc1 = 0.5;
    private static final double lc2 = 0.5;
    private static final double I1 = 1.0;
    private static final double I2 = 1.0;
    public static final double maxTheta_1 = Math.PI;
    public static final double maxTheta_2 = Math.PI;
    public static final double maxTheta1Dot = 4.0 * Math.PI;
    public static final double maxTheta2Dot = 9.0 * Math.PI;
    public static final double thetaWidth = Math.PI / 3.0;
    public static final double thetaDotWidth_0 = 8.0 * Math.PI / 6.0;
    public static final double thetaDotWidth_1 = 3.0 * Math.PI;
    private double theta1, theta2, theta1Dot, theta2Dot;
    public static double acrobotGoalPosition = 1.0;

    public static void main(String[] args) {
        VariableMassAcrobot a = new VariableMassAcrobot();
        java.util.Random r = new java.util.Random();

        for (int j = 0; j < 1000; j++) {
            int ract = r.nextInt(a.getNumActions());
            a.step(ract);

            State s = a.getState();
            double[] ss = s.getState();

            for (int v = 0; v < ss.length; v++) {
                System.out.print(ss[v] + " ");
            }
            System.out.println();

        }
    }

    @Override
    public void setState(State s) {
        AcrobotState astate;
        if (s instanceof AcrobotState) {
            astate = (AcrobotState) s;
        } else {
            astate = new AcrobotState(s.getState());
        }
        theta1 = astate.theta1;
        theta2 = astate.theta2;
        theta1Dot = astate.theta1Dot;
        theta2Dot = astate.theta2Dot;
    }

    @Override
    public boolean isEpisodeOver() {
        return atGoal();
    }

    @Override
    public State getRandomState() {
        AcrobotState aState = new AcrobotState(new double[]{Math.random(), Math.random(), Math.random(), Math.random()});
        if (aState.isTerminal()) {
            return getRandomState();
        } else {
            return aState;
        }
    }

    @Override
    public int getNumActions(State s) {
        return VariableMassAcrobot.getNumActions();
    }

    @Override
    public LinkedList<Integer> getAvailableActions(State s) {
        LinkedList<Integer> actions = new LinkedList<Integer>();
        for (int i = 0; i < VariableMassAcrobot.getNumActions(); i++) {
            actions.add(i);
        }
        return actions;
    }

    @Override
    public int getTotalNumActions() {
        return VariableMassAcrobot.getNumActions();
    }

    @Override
    public void resetDomain() {
        init(m1,m2);
    }

    @Override
    public String getDescriptionString() {
        return "variable_acrobot_m1-" + m1 + "m2-" + m2;
    }

    @Override
    public int getTotalNumDimensions() {
        return VariableMassAcrobot.getNumDimensions();
    }
}
