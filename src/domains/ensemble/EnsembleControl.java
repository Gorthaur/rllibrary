/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package domains.ensemble;

/**
 *
 * @author mitch
 */


import domains.Domain;
import java.util.LinkedList;
import rl.state.State;
//import rl.ScaledState;

public class EnsembleControl extends Domain {
    
    private int robots;
    private double rotate[];
    private double[] positions;
    private int steps = 0;
     // xpos robot 1, ypos robot 1, direction robot 1, xpos robot 2...
    
    private final double startRotate = 45; // minimum rotation of a robot
    private final double rotateOffset = 5; // rotation differential
    private final double forward = 0.01; // how many units the robot moves
   
    
    // ensemble control of a mass of robots. Robots are points, 
    // Each robot
    // rotates at a different rate, but through ensemble control the robots
    // must all navigate to the walls (at x=0, 1 and y=0,1)
    // robots stop when they hit a wall. The reward given is - number of robots
    // not at a wall.
    
    public EnsembleControl() {
        robots = 3;
        positions = new double[3*robots];
        rotate = new double[robots];
        for (int i=0; i<robots; i++) {
            rotate[i] = startRotate +i*rotateOffset;//intrinsic rotation
        }
        for (int i=0; i<3*robots; i=i+3) {
            positions[i] = 0.25; //x position
            positions[i+1] = 0.5; //y position
            positions[i+2] = 0; //direction between 0 and 1
        }
    }
    
    public EnsembleControl(int robots) {
        this.robots=robots;
        positions = new double[3*robots];
        rotate = new double[robots];
        for (int i=0; i<robots; i++) {
            rotate[i] = startRotate +i*rotateOffset;//intrinsic rotation
        }
        for (int i=0; i<3*robots; i=i+3) {
            positions[i] = 0.25; //x position
            positions[i+1] = 0.5; //y position
            positions[i+2] = 0; //direction between 0 and 1
        }
        steps = 0;
    }
    
    
    @Override
    public State getState() {
        State s = new State(positions);
        if (isTerminal()) {
           s.setTerminal(true);
        }
        return s;
    }

    @Override
    public double step(int action) {
        if (action==0) {
            // clockwise rotation, decrease angles by 
            for (int i=0; i<robots; i++) {
                positions[3*i+2] = ((360*positions[3*i+2] - rotate[i])%360)/360;
                if (positions[3*i+2] <0) positions[3*i+2]+=1;
            }
        } else if (action==1) {
            // counterclockwise rotation
            for (int i=0; i<robots; i++) {
                positions[3*i+2] = ((360*positions[3*i+2] + rotate[i])%360)/360;
            }
        } else if (action==2) {
            // forward
            for (int i=0; i<robots; i++) {
                positions[3*i] = Math.min(Math.max(positions[3*i]+forward*Math.cos(Math.PI*2*positions[3*i+2]), 0), 1);
                positions[3*i+1] = Math.min(Math.max(positions[3*i+1]+forward*Math.sin(Math.PI*2*positions[3*i+2]), 0), 1);
            }
        } 
        steps++;
        if (steps==999) return 0;
        return robotsAtGoal() - robots; // reward.
    }

    private int robotsAtGoal() {
        int goals = 0;
        for (int i=0; i<robots; i++) {
            if (positions[3*i]==0 || positions[3*i]==1 || positions[3*i+1]==0 || positions[3*i+1]==1) goals++;
        }
        return goals;
    }
    
    @Override
    public void setState(State s) {
        this.positions = s.getState();
    }

    @Override
    public boolean isEpisodeOver() {
        return (steps==999);
    }

    private boolean isTerminal() {
        return (steps==999);
    }
    
    @Override
    public State getRandomState() {
        double newpositions[] = new double[3*robots];
        for (int i=0; i<3*robots; i=i+3) {
            newpositions[i] = Math.random(); //x position
            newpositions[i+1] = Math.random(); //y position
            newpositions[i+2] = Math.random(); //direction between 0 and 1
        }
        return new State(newpositions);
    }

    @Override
    public int getNumActions(State s) {
        return 3;
    }

    @Override
    public LinkedList<Integer> getAvailableActions(State s) {
        LinkedList<Integer> actions = new LinkedList<Integer>();
        for (int i = 0; i < EnsembleControl.getNumActions(); i++) {
            actions.add(i);
        }
        return actions;    
    }

    @Override
    public int getTotalNumActions() {
        return 3;
    }

    @Override
    public void resetDomain() {
        steps =0;
        for (int i=0; i<3*robots; i=i+3) {
            positions[i] = 0.25; //x position
            positions[i+1] = 0.5; //y position
            positions[i+2] = 0; 
        }
    }

    @Override
    public String getDescriptionString() {
        String s = "Ensemble Control "+robots;
        return s;
        }

    @Override
    public int getTotalNumDimensions() {
        return 3*robots;
    }
    
}
