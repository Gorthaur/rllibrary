package domains.rccar;

import domains.Domain;
import java.util.LinkedList;
import rl.state.ScaledState;
import rl.state.State;

/**
 *
 * @author Dean
 */
public class RCCar extends Domain {

    public double x = 0.0;
    public double y = 0.0;
    public double phi = 0.0;
    public double velocity = 0.0;
    public static final double ROOM_WIDTH = 3; //3 meters
    public static final double ROOM_HEIGHT = 2; //2 meters
    public static final double XMIN = -ROOM_WIDTH / 2;
    public static final double XMAX = ROOM_WIDTH / 2;
    public static final double YMIN = -ROOM_HEIGHT / 2;
    public static final double YMAX = ROOM_HEIGHT / 2;
    public static final double ACCELERATION = 0.1;
    public static final double TURN_ANGLE = Math.PI / 6.0;
    public static final double VELOCITY_MIN = -0.3;
    public static final double VELOCITY_MAX = 0.3;
    public static final double ANGLE_MIN = -Math.PI;
    public static final double ANGLE_MAX = Math.PI;
    public static final double[] STATE_MIN = new double[]{XMIN, YMIN, VELOCITY_MIN, ANGLE_MIN};
    public static final double[] STATE_MAX = new double[]{XMAX, YMAX, VELOCITY_MAX, ANGLE_MAX};
    public static final double STEP_REWARD = -1;
    public static final double GOAL_REWARD = 0;
    public static final double[] GOAL = new double[]{0.5, 0.5};
    public static final double GOAL_RADIUS = 0.1;
    public static final double TIMESTEP = 0.1;
    public static final double CAR_LENGTH = 0.3;
    public static final double CAR_WIDTH = 0.15;
    public static final double REAR_WHEEL = 0.05;

    public RCCar() {
    }

    @Override
    public State getState() {
        State s = new ScaledState(new double[]{x, y, velocity, phi}, STATE_MIN, STATE_MAX);
        if (isEpisodeOver()) {
            s.setTerminal(true);
        }
        return s;
    }

    @Override
    public double step(int action) {
        double acc = 0;
        double turn = 0;
        if (action == 0) { //backwards, left
            acc = -1;
            turn = -1;
        } else if (action == 1) { //backwards straight
            acc = -1;
        } else if (action == 2) { //backwards right
            acc = -1;
            turn = 1;
        } else if (action == 3) { //coast left
            turn = -1;
        } else if (action == 4) { //coast straight
        } else if (action == 5) { //coast right
            turn = 1;
        } else if (action == 6) { //forward left
            acc = 1;
            turn = -1;
        } else if (action == 7) { //forward straight
            acc = 1;
        } else { //forward right
            acc = 1;
            turn = 1;
        }
        double newVelocity = velocity + acc * ACCELERATION * TIMESTEP;
        double newX = x + newVelocity * Math.cos(phi) * TIMESTEP;
        double newY = y + newVelocity * Math.sin(phi) * TIMESTEP;
        double newPhi = phi + (newVelocity / CAR_LENGTH) * Math.tan(turn * TURN_ANGLE) * TIMESTEP;

        newX = Math.max(Math.min(newX, XMAX), XMIN);
        newY = Math.max(Math.min(newY, YMAX), YMIN);
        newVelocity = Math.max(Math.min(newVelocity, VELOCITY_MAX), VELOCITY_MIN);
        newPhi = ((newPhi - ANGLE_MIN + 2 * (ANGLE_MAX - ANGLE_MIN)) % ((ANGLE_MAX - ANGLE_MIN))) + ANGLE_MIN; //first make positive, then add some multiple of 2PI, then take modulo and subtract PI again to be between -pi and pi.

        if (newX == XMAX || newX == XMIN || newY == YMAX || newY == YMIN) {
            newVelocity = 0;
        }
        x = newX;
        y = newY;
        velocity = newVelocity;
        phi = newPhi;

        if (isEpisodeOver()) {
            return GOAL_REWARD;
        } else {
            return STEP_REWARD;
        }
    }

    public void printState() {
        System.out.println("x " + x + " y " + y + " v " + velocity + " phi " + phi);
    }

    @Override
    public void setState(State s) {
        x = s.getState()[0] * (STATE_MAX[0] - STATE_MIN[0]) + STATE_MIN[0];
        y = s.getState()[1] * (STATE_MAX[1] - STATE_MIN[1]) + STATE_MIN[1];
        velocity = s.getState()[2] * (STATE_MAX[2] - STATE_MIN[2]) + STATE_MIN[2];
        phi = s.getState()[3] * (STATE_MAX[3] - STATE_MIN[3]) + STATE_MIN[3];
    }

    @Override
    public boolean isEpisodeOver() {
        double dist = Math.sqrt(Math.pow(x - GOAL[0], 2) + (Math.pow(y - GOAL[1], 2)));
        return dist < GOAL_RADIUS;
    }

    @Override
    public State getRandomState() {
        State s = new State(new double[]{Math.random(), Math.random(), Math.random(), Math.random()});
        if (isTerminal(s)) {
            return getRandomState();
        } else {
            return s;
        }
    }

    public boolean isTerminal(State s) {
        double newX = s.getState()[0] * (STATE_MAX[0] - STATE_MIN[0]) + STATE_MIN[0];
        double newY = s.getState()[1] * (STATE_MAX[1] - STATE_MIN[1]) + STATE_MIN[1];
        double dist = Math.sqrt(Math.pow(newX - GOAL[0], 2) + (Math.pow(newY - GOAL[1], 2)));
        return dist < GOAL_RADIUS;
    }

    @Override
    public int getNumActions(State s) {
        return 9;
    }

    @Override
    public LinkedList<Integer> getAvailableActions(State s) {
        LinkedList<Integer> available = new LinkedList<Integer>();
        for (int i = 0; i < 9; i++) {
            available.add(i);
        }
        return available;
    }

    @Override
    public int getTotalNumActions() {
        return 9;
    }

    @Override
    public void resetDomain() {
        x = 0.0;
        y = 0.0;
        phi = 0.0;
        velocity = 0.0;
    }

    @Override
    public String getDescriptionString() {
        return "rc_car";
    }

    @Override
    public int getTotalNumDimensions() {
        return 4;
    }
}
