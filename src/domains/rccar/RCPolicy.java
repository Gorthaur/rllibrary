package domains.rccar;

import domains.Domain;
import java.util.Arrays;
import rl.policy.Policy;
import rl.state.State;

/**
 *
 * @author dean
 */
public class RCPolicy extends Policy {

    RCCar car;

    public RCPolicy(RCCar d) {
        super(d);
        car = new RCCar();

    }

    @Override
    public double[] getProbabilities(State s) {

        double probs[] = new double[car.getTotalNumActions()];
        double bestDistance = Double.MAX_VALUE;
        int bestAction = 0;
        for (int action = 0; action < 9; action++) {
            car.setState(s);
            car.step(action);
            if (!car.getState().equals(s)) {
                double distance = Math.pow(car.x - RCCar.GOAL[0], 2) + Math.pow(car.y - RCCar.GOAL[1], 2);

                double[] v1 = new double[]{RCCar.GOAL[0] - car.x, RCCar.GOAL[1] - car.y};
                double[] v2 = new double[]{Math.cos(car.phi), Math.sin(car.phi)};
                double top = v1[0] * v2[0] + v1[1] * v2[1];
                double angle = Math.acos(top / (Math.sqrt(v1[0] * v1[0] + v1[1] * v1[1]) * Math.sqrt(v2[0] * v2[0] + v2[1] * v2[1])));
                double angle2 = Math.PI - angle;

                    distance = distance + 2*Math.pow(Math.sin(angle), 2) * distance;
                

                if (distance < bestDistance) {
                    bestDistance = distance;
                    bestAction = action;
                }
            }
        }
                probs[bestAction] = 1;
        return probs;


    }
}
