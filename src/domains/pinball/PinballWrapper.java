package domains.pinball;

import PinBallDomain.Ball;
import PinBallDomain.Obstacle;
import PinBallDomain.PinBall;
import PinBallDomain.Point;
import PinBallDomain.Target;
import domains.Domain;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import rl.state.State;

/**
 *
 * @author Dean
 */
public class PinballWrapper extends Domain {

    public PinBall pinball;
    String configFile;

    /**
     * The pinball wrapper which wraps George's pinball class into my code.
     * @param configFile The map file for the pinball map.
     */
    public PinballWrapper(String configFile) {
        this.pinball = new PinBall(configFile);
        this.configFile = configFile;
    }

    @Override
    public State getState() {
        State state = new State(pinball.getState().getDescriptor());
        state.setTerminal(pinball.getState().endState());
        return state;
    }

    @Override
    public double step(int action) {
        return pinball.step(action);
    }

    @Override
    public boolean isEpisodeOver() {
        return pinball.episodeEnd();
    }

    @Override
    public State getRandomState() {
        double x = Math.random() * (1 - 2 * pinball.getBall().getRadius()) + pinball.getBall().getRadius(); //make sure it's at least its radius away from the walls.
        double y = Math.random() * (1 - 2 * pinball.getBall().getRadius()) + pinball.getBall().getRadius(); //make sure it's at least a its radius away from the walls.
        Point ballPos = new Point(x, y);
        Ball ball = new Ball(ballPos, pinball.getBall().getRadius());
        ArrayList<Obstacle> obstacles = pinball.getObstacles();

        for (Obstacle o : obstacles) {
            if (o.inside(ballPos)) {
                return getRandomState();
            }
            if (o.collision(ball)) {
                return getRandomState();
            }
        }
        //no collision, check if it's a terminal state
        Target pinTarget = pinball.getTarget();
        if (pinTarget.collision(ball)) {
            return getRandomState();
        }
        double xdot = Math.random();
        double ydot = Math.random();
        double state[] = new double[]{x, y, xdot, ydot};
        return new State(state);
    }

    @Override
    public void setState(State s) {
        if (s.getStateLength() != PinballWrapper.getNumDimensions()) {
            throw new IllegalStateException("State dimensions don't match the domain.");
        }
        pinball.getBall().setPosition(s.getState()[0], s.getState()[1]);
        pinball.getBall().setVelocities((s.getState()[2] * 2 - 1), (s.getState()[3] * 2 - 1));
    }

    public static int getNumDimensions() {
        return 4;
    }

    public static int getNumActions() {
        return 5;
    }

    @Override
    public int getNumActions(State s) {
        return PinballWrapper.getNumActions();
    }

    @Override
    public LinkedList<Integer> getAvailableActions(State s) {
        LinkedList<Integer> acts = new LinkedList<Integer>();
        for (int i = 0; i < PinballWrapper.getNumActions(); i++) {
            acts.add(i);
        }
        return acts;
    }

    @Override
    public int getTotalNumActions() {
        return PinballWrapper.getNumActions();
    }

    @Override
    public void resetDomain() {
        int nstarts = pinball.getStartStates().size();
        int pos = (int) (Math.random() * nstarts);
        if (pos == nstarts) {
            pos--;
        }

        double xx = pinball.getStartStates().get(pos).getDescriptor()[0];
        double yy = pinball.getStartStates().get(pos).getDescriptor()[1];

        pinball.getBall().setPosition(xx, yy);
    }

    @Override
    public String getDescriptionString() {
        File f = new File(configFile);
        return "pinball_" + f.getName();
    }

    @Override
    public int getTotalNumDimensions() {
        return PinballWrapper.getNumDimensions();
    }
}
