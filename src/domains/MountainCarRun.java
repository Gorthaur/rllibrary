package domains;

import domains.mountaincar.MountainCar;
import functionapproximation.QApproximator;
import functionapproximation.FunctionApproximator;
import functionapproximation.IndependentFourier;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import rl.policy.EpsilonGreedy;
import rl.policy.Policy;
import experimental.SarsaLambdaTester;
import functionapproximation.FullTiling;
import rl.state.State;

public class MountainCarRun {

    public static void main(String args[]) throws IOException {
        int maxSteps = 10000; //max steps in mountain car episode before calling it quits
        int numEpisodes = 100;
        int numExperiments = 1;

        int fourierOrder = 3;
        int numFeatures = 2; //num state variables

        String data = "";
        int counter = 0;
        //number of terms with independent fourier is (fourierOrder*numFeatures) + 1 for each action
        //for full fourier it's (fourierOrder+1)^numFeatures <- a lot. Don't use when there are a large number of features.

       
        //Policy p = new EpsilonGreedy(0);
        //double alpha = 0.0075; //learning rate
        double alpha = 1;
        double gamma = 1; //discount (1 = no discount)
        double lambda = 0.95; //eligibility trace
        double eta = 100;

        double averages[] = new double[numEpisodes];
        for (int i = 0; i < numExperiments; i++) {
            System.out.println("Start Experiment " + (i + 1));
            FunctionApproximator FAs[] = new FunctionApproximator[3];
            //IndependentTiling FAs[] = new IndependentTiling[3];
            for (int j = 0; j < 3; j++) {
                FAs[j] = new IndependentFourier(fourierOrder, numFeatures);
                FAs[j] = new FullTiling(10, numFeatures);
                //FAs[j] = new FullFourier(fourierOrder, numFeatures);
                //FAs[j] = new FullRBF(fourierOrder, 2);
            }
            State n = new State(new double[]{0, 0});
            QApproximator q = new QApproximator(FAs);
            Policy p = new EpsilonGreedy(new MountainCar(), q, 0.01);
            
            //iFFDQApproximator q = new iFFDQApproximator(FAs);
            //GQLambdaTester learner = new GQLambdaTester(q,p,alpha,1,0.95); //GQLambda sucks... you can try it, but make sure you use a greedy policy
            //SarsaLambda learner = new SarsaLambdaErrorPlot(q,p,alpha,gamma,lambda);
            SarsaLambdaTester learner = new SarsaLambdaTester(q, p, alpha, gamma, lambda);
            //SarsaLambda learner = new iFFDSarsa(q,p,alpha,gamma,lambda,eta);
            //start an episode
            for (int k = 0; k < numEpisodes; k++) {
                System.out.print(".");
                learner.startEpisode();
                MountainCar m = new MountainCar();
                int steps = 0;
                State currState = m.getState();
                int move = learner.nextMove(currState);
                counter++;
                while (steps < maxSteps && !currState.isTerminal()) {
                    double reward = m.step(move);
                    State newState = m.getState();
                    //learner.addSample(currState, move, newState, reward); //GQ
                    int nextMove = learner.nextMove(newState);
                    counter++;
                    learner.addSample(currState, move, newState, nextMove, reward);
                    
                    currState = newState;
                    move = nextMove;
                    steps++;

                    //learner.decayAlpha(0.9999);
                }
                data += k + " " + steps + "\n";
                System.out.println("Episode " + k + " took " + steps + " steps");
                averages[k] += steps;
            }
            q.printFunction();
            //learner.saveErrors("mcerrors");

        }
        //String output = "Episode,Steps\n";
        //for (int i = 0; i < numEpisodes; i++) {
        //  averages[i] = averages[i]/numExperiments;
        //  System.out.println("Episode " + i + " took " + averages[i] + " steps on average");
        //  output += (i+1) + "," + averages[i] + "\n";
        //}
        //Write the output in csv format to graph easily in excel
        //String filename = "results/GQLambdaFullalpha" + alpha + ".csv";
        FileWriter f = null;
        f = new FileWriter(new File("mountaincar_episode.dat"));
        f.write(data);
        f.flush();
        f.close();
    }
}
