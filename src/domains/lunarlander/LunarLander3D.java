package domains.lunarlander;

import domains.Domain;
import java.util.LinkedList;
import rl.state.ScaledState;
import rl.state.State;

/**
 *
 * @author Dean
 */
public class LunarLander3D extends Domain {

    double g = -1.622;
    double a = -2 * g;
    double maxX = 60;
    double minX = 0;
    double maxY = 60;
    double minY = 0;
    double maxZ = 60;
    double minZ = 0;
    double maxVelocities[] = getMaxVelocity(maxX, maxY, maxZ);
    double minVelocities[] = getMinVelocity(minX, minY, minZ);
    double maxTheta = 90;
    double minTheta = -90;
    double minPsi = -90;
    double maxPsi = 90;

    double safeVelocityX = 4;
    double safeVelocityY = 4;
    double safeVelocityZ = 4;
    double timeStep = 0.1;

    double landingPadLengthX = 10;
    double landingPadLengthY = 10;

    double craftLengthX = 5;
    double craftLengthY = 5;
    double craftLengthZ = 6;

    /**
     * state[0] is x position state[1] is x velocity state[2] is y position
     * state[3] is y velocity state[4] is z position state[5] is z velocity
     * state[6] is theta, angle in xz plane (degrees) state[7] is psi, angle in
     * yz plane (degrees)
     */
    double state[] = new double[8];

    double stateMin[] = new double[]{minX, minVelocities[0], minY, minVelocities[1], minZ, minVelocities[2], minTheta, minPsi};
    double stateMax[] = new double[]{maxX, maxVelocities[0], maxY, maxVelocities[1], maxZ, maxVelocities[2], maxTheta, maxPsi};

    @Override
    public State getState() {
       State s = new ScaledState(state, stateMin, stateMax);
       if (isTerminal(state)) {
           s.setTerminal(true);
       }
       return s;
    }

    @Override
    public double step(int action) {
        double nextState[] = state.clone();
        if (action == 0) {
            //none
        } else if (action == 1) {
            //thrust
        } else if (action == 2) {
            //theta, rotate anticlockwise
            nextState[6] = Math.min(nextState[6] + 15, 90);
        } else if (action == 3) {
            //theta, rotate clockwise
            nextState[6] = Math.max(nextState[6] - 15, -90);
        } else if (action == 4) {
            //phi, rotate anticlockwise
            nextState[7] = Math.min(nextState[7] + 15, 90);
        } else if (action == 5) {
            //phi, rotate clockwise
            nextState[7] = Math.max(nextState[7] - 15, -90);
        }

        double theta = nextState[6] * Math.PI / 180;
        double psi = nextState[7] * Math.PI / 180;
        if (action != 1) { //not thrust
            nextState[4] = nextState[4] + nextState[5] * timeStep + (g * Math.pow(timeStep, 2)) / 2;
            nextState[2] = nextState[2] + nextState[3] * timeStep;
            nextState[0] = nextState[0] + nextState[1] * timeStep;

            nextState[5] = nextState[5] + g * timeStep;
        } else {
            nextState[4] = nextState[4] + nextState[5] * timeStep + ((a * Math.cos(theta) * Math.cos(psi) + g) * (Math.pow(timeStep, 2))) / 2; //z position
            nextState[2] = nextState[2] + nextState[3] * timeStep + ((-a * Math.sin(psi) * Math.cos(theta)) * (Math.pow(timeStep, 2))) / 2; //y position
            nextState[0] = nextState[0] + nextState[1] * timeStep + ((-a * Math.sin(theta) * Math.cos(psi)) * (Math.pow(timeStep, 2))) / 2; //z position

            nextState[5] = nextState[5] + ((a * Math.cos(theta) * Math.cos(psi) + g)) * timeStep; //z velocity
            nextState[3] = nextState[3] + (-a * Math.sin(psi) * Math.cos(theta)) * timeStep; //y velocity
            nextState[1] = nextState[1] + (-a * Math.sin(theta) * Math.cos(psi)) * timeStep; //x velocity
        }
        for (int i = 0; i < nextState.length; i++) {
            nextState[i] = Math.min(Math.max(nextState[i], stateMin[i]), stateMax[i]);
        }
        this.state = nextState;
        if (isCrash(state)) {
            System.out.println("crash");
            return -75.0 * Math.sqrt(Math.pow(state[1], 2) + Math.pow(state[3], 2) + Math.pow(state[5], 2))  -25*Math.sqrt(Math.pow(state[0], 2) + Math.pow(state[2], 2) + Math.pow(state[4], 2));
        }
        if (isLanded(state)) {
            System.out.println("yay");
            return 1000;
        }
        if (action == 1) {
            return -1;
        } else {
            return 0;
        }
    }

    @Override
    public void setState(State s) {
        if (s.getStateLength() != 8) {
            throw new IllegalStateException("State dimensions don't match the domain.");
        }
        for (int i = 0; i < s.getStateLength(); i++) {
            state[i] = s.getState()[i] * (stateMax[i] - stateMin[i]) + stateMin[i];
        }
    }

    @Override
    public boolean isEpisodeOver() {
        return isTerminal(state);
    }

    public double[] getRandomUnscaledState() {
        double s[] = new double[8];
        s[0] = Math.random() * (maxX - minX) + minX;
        s[2] = Math.random() * (maxY - minY) + minY;
        s[4] = Math.random() * (maxZ - minZ) + minZ;
        double maxVel[] = getMaxVelocity(s[0], s[2], s[4]);
        double minVel[] = getMinVelocity(s[0], s[2], s[4]);
        s[1] = Math.random() * (maxVel[0] - minVel[0]) + minVel[0];
        s[3] = Math.random() * (maxVel[1] - minVel[1]) + minVel[1];
        s[5] = Math.random() * (maxVel[2] - minVel[2]) + minVel[2];
        int rand = (int) (Math.random() * (13));
        if (rand == 13) {
            rand = 12;
        }
        s[6] = (rand - 6) * 15;
        rand = (int) (Math.random() * (13));
        if (rand == 13) {
            rand = 12;
        }
        s[7] = (rand - 6) * 15;
        if (isTerminal(s)) {
            return getRandomUnscaledState();
        } else {
            return s;
        }
    }

    @Override
    public State getRandomState() {
        return new ScaledState(getRandomUnscaledState(), minVelocities, maxVelocities);
    }

    public State getUnscaledState() {
        return new State(state);
    }

    public boolean isTerminal(double unscaledState[]) {
        if (isLanded(unscaledState)) {
            return true;
        } else {
            return isCrash(unscaledState);
        }
    }

    public boolean isCrash(double[] unscaledState) {
        if (isLanded(unscaledState)) {
            return false;
        }

        double thetaRads = unscaledState[6] * Math.PI / 180;
        double psiRads = unscaledState[7] * Math.PI / 180;
        //if (unscaledState[4] - craftLengthZ/2 <= minZ) {
        //    return true;
        //}

        
        //check the height of each leg after rotation. There should be a better way, but this works.
        double height = -(craftLengthX / 2) * Math.sin(thetaRads) - (craftLengthY / 2) * Math.cos(thetaRads) * Math.sin(psiRads) - (craftLengthZ / 2) * Math.cos(thetaRads) * Math.cos(psiRads);
        height = height + unscaledState[4];
        if (height <= minZ) {
            return true;
        }
        height = +(craftLengthX / 2) * Math.sin(thetaRads) - (craftLengthY / 2) * Math.cos(thetaRads) * Math.sin(psiRads) - (craftLengthZ / 2) * Math.cos(thetaRads) * Math.cos(psiRads);
        height = height + unscaledState[4];
        if (height <= minZ) {
            return true;
        }
        height = -(craftLengthX / 2) * Math.sin(thetaRads) + (craftLengthY / 2) * Math.cos(thetaRads) * Math.sin(psiRads) - (craftLengthZ / 2) * Math.cos(thetaRads) * Math.cos(psiRads);
        height = height + unscaledState[4];
        if (height <= minZ) {
            return true;
        }
        height = +(craftLengthX / 2) * Math.sin(thetaRads) + (craftLengthY / 2) * Math.cos(thetaRads) * Math.sin(psiRads) - (craftLengthZ / 2) * Math.cos(thetaRads) * Math.cos(psiRads);
        height = height + unscaledState[4];
        if (height <= minZ) {
            return true;
        }
        
        return false;
    }

    public boolean isLanded(double[] unscaledState) {
        if (Math.abs(unscaledState[0] - ((maxX - minX) / 2)) <= (landingPadLengthX - (craftLengthX / 2))) {
            if (Math.abs(unscaledState[2] - ((maxY - minY) / 2)) <= (landingPadLengthY - (craftLengthY / 2))) {
                if (unscaledState[4] - (craftLengthZ / 2) <= minZ) {
                    if (unscaledState[6] == 0) {
                        if (unscaledState[7] == 0) {
                            //now check velocities
                            if (Math.abs(unscaledState[1]) <= safeVelocityX) {
                                if (Math.abs(unscaledState[3]) <= safeVelocityY) {
                                    if (Math.abs(unscaledState[5]) <= safeVelocityZ) {
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    @Override
    public int getNumActions(State s) {
        return 6;
    }

    @Override
    public LinkedList<Integer> getAvailableActions(State s) {
        LinkedList<Integer> actions = new LinkedList();
        for (int i = 0; i < 6; i++) {
            actions.add(i);
        }
        return actions;
    }

    @Override
    public int getTotalNumActions() {
        return 6;
    }

    @Override
    public void resetDomain() {
        this.state = new double[]{10+ Math.random()*2-1, Math.random()*2-1, 10 + Math.random()*2-1, Math.random()*2-1, 10+Math.random()*2-1, Math.random() + Math.random()*2-1, 0, 0};
    }

    @Override
    public String getDescriptionString() {
        return "lunarlander_3d";
    }

    @Override
    public int getTotalNumDimensions() {
        return 8;
    }

    public static int getNumDimensions() {
        return 8;
    }

    public static int getNumActions() {
        return 6;
    }

    /**
     * Gets the maximum velocity in the x, y and z dimensions at a specific
     * position.
     *
     * @param x
     * @param y
     * @param z
     * @return an array giving the maximum velocity in each of the dimensions.
     */
    private double[] getMaxVelocity(double x, double y, double z) {
        double velocities[] = new double[3];
        double xDist = x - minX;
        velocities[0] = Math.abs(a) * Math.sqrt(xDist * 2 / Math.abs(a));

        double yDist = y - minY;
        velocities[1] = Math.abs(a) * Math.sqrt(yDist * 2 / Math.abs(a));

        double zDist = z - minZ;
        velocities[2] = Math.abs(a) * Math.sqrt(zDist * 2 / Math.abs(a));
        return velocities;
    }

    /**
     * Gets the minimum velocity in the x, y and z dimensions at a specific
     * position.
     *
     * @param x
     * @param y
     * @param z
     * @return an array giving the minimum velocity in each of the dimensions.
     */
    private double[] getMinVelocity(double x, double y, double z) {
        double velocities[] = new double[3];
        double xDist = x - maxX;
        velocities[0] = -1 * Math.abs(a) * Math.sqrt(-1 * xDist * 2 / Math.abs(a));

        double yDist = y - maxY;
        velocities[1] = -1 * Math.abs(a) * Math.sqrt(-1 * yDist * 2 / Math.abs(a));

        double zDist = z - maxZ; //negative
        velocities[2] = -1 * Math.abs(a) * Math.sqrt(-1 * zDist * 2 / Math.abs(a));
        return velocities;
    }
    
    

}
