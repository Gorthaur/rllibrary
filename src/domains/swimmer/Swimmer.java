package domains.swimmer;

import domains.Domain;
import java.util.LinkedList;
import org.mathIT.algebra.Matrix;
import rl.state.State;
import rlmath.MatrixMath;

/**
 *
 * @author Dean
 */
public class Swimmer extends Domain {

    double dt = 0.03;
    int segments = 3;
    int noseSegment = 0; //nose at front
    double k1 = 7.5; //The drag coefficient for velocity in the direction perpendicular to a given stick
    double k2 = 0.3; //The drag coefficient for velocity in the direction parallel to a given stick. 
    double segmentMasses[][];
    double segmentLengths[][];
    double inertia[][];
    double goal[][];
    double Q[][]; //dunno what this is
    double A[][]; //again..
    double P[][];
    double U[][];
    double G[][];

    boolean angles[];

    int actions[][];
    
    
    /**
     * State
     */
    
    double theta[][];
    double position[];
    double velocity[];
    double angularVelocity[][];

    public Swimmer() {
        segmentMasses = MatrixMath.diag(MatrixMath.eye(segments));
        segmentLengths = MatrixMath.diag(MatrixMath.eye(segments));
        inertia = MatrixMath.divide(MatrixMath.multiply(segmentMasses, MatrixMath.multiply(segmentLengths, segmentLengths)), 12);
        goal = MatrixMath.zeros(2);
        Q = MatrixMath.toeplitz(segments, 0, -1, 1);
        Q[Q.length - 1] = MatrixMath.transpose(MatrixMath.diag(segmentMasses))[0];
        Matrix QInv = new Matrix(Q);
        QInv = QInv.inverse();
        double[][] qInv = QInv.getMatrix();

        A = MatrixMath.toeplitz(segments, 0, 1, 1);
        A[segments][segments] = 0;
        P = MatrixMath.multiply(MatrixMath.multiply(MatrixMath.multiply(qInv, A), MatrixMath.diag(segmentLengths)), 0.5);
        U = MatrixMath.toeplitz(segments, -1, 1, 0);
        U = MatrixMath.resize(U, U.length, U.length - 1);
        G = MatrixMath.multiply(MatrixMath.multiply(MatrixMath.transpose(P), MatrixMath.diag(segmentLengths)), P);

        angles = new boolean[3 + 2 * segments];
        for (int i = 2; i < segments + 1; i++) {
            angles[i] = true;
        }

        actions = new int[(int) Math.pow(segments-1, 3)][segments -1];
        //lazy method for enumerating all the actions
        for (int i = 0; i < actions.length; i++) {
            //for each link there is only 3 actions, so convert to base 3 (0, 1, 2)
            //subtract 1 and multiply by 2 to get actions -2, 0, and 2.
            String s = Integer.toString(i, 3);
            while (s.length() < segments-1) {
                s = "0" + s;
            }
            String split[] = s.split("");
            for (int j = 0; j < segments-1; j++) {
                actions[i][j] = (Integer.parseInt(split[j]) - 1) * 2;
            }
        }
        theta = new double[segments][segments];
        position = new double[]{10,0};
        velocity = new double[]{0,0};
        angularVelocity = new double[segments][segments];

    }

    @Override
    public State getState() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double step(int action) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setState(State s) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isEpisodeOver() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public State getRandomState() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getNumActions(State s) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public LinkedList<Integer> getAvailableActions(State s) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getTotalNumActions() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void resetDomain() {
        theta = new double[segments][segments];
        position = new double[]{10,0};
        velocity = new double[]{0,0};
        angularVelocity = new double[segments][segments];
    }

    @Override
    public String getDescriptionString() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getTotalNumDimensions() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    protected double[] getBodyState() {
        double[][] cth = MatrixMath.cos(theta);
        double[][] sth = MatrixMath.sin(theta);
        //double[][] M = MatrixMath.subtract(P, MatrixMath.multiply(MatrixMath.diag(position), 0.5))
        return new double[5];
    }

}
