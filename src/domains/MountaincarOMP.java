package domains;

/**
 *
 * @author Dean
 */
import PinBallDomain.PinBall;
import domains.acrobot.AcroPolicy;
import domains.acrobot.Acrobot;
import domains.chainwalk.ChainWalk;
import domains.chainwalk.FiftyStateChainWalk;
import domains.discontinuousroom.WallWorld;
import domains.mountaincar.MCPolicy;
import domains.mountaincar.MountainCar;
import domains.pinball.PinballWrapper;
import domaintools.SampleCollector;
import domaintools.ValueFunctionPlotter;
import rl.learningalgorithms.OMPTD;
import functionapproximation.BasisFunction;
import functionapproximation.FourierBasis;
import functionapproximation.dictionary.FourierFeatureDictionary;
import functionapproximation.FullFourier;
import functionapproximation.FullRBF;
import functionapproximation.FunctionApproximator;
import functionapproximation.dictionary.GeneralFeatureDictionary;
import functionapproximation.IndependentFourier;
import functionapproximation.dictionary.OMPPaperDictionary;
import functionapproximation.QApproximator;
import functionapproximation.dictionary.RandomFourierFeatureDictionary;
import functionapproximation.dictionary.RegularisedFeatureDictionary;
import functionapproximation.VApproximator;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import rl.policy.EpsilonGreedy;
import rl.learningalgorithms.LSPI;
import rl.learningalgorithms.LSTDV;
import rl.policy.Policy;
import rl.policy.RandomPolicy;
import rl.Sample;
import rl.learningalgorithms.SarsaLambda;
import rl.learningalgorithms.SarsaLambdaAlphaScale;
import rl.state.State;

/**
 *
 * @author dean
 */
public class MountaincarOMP {

    public static void main(String args[]) {
        int experimentNumber = Integer.parseInt(args[0]);
        //int experimentNumber = 1;
        Domain domain = new MountainCar();


        Policy p = new MCPolicy(domain);
        SampleCollector sampler = new SampleCollector(domain);

        double average = 0;
        double beta = 0.0001;
        int maxOrder = 10;
        LinkedList<Sample> samples = new LinkedList<Sample>();
        int desiredSamples = 5000;
        int randomWalkLength = 1000;

        int numTestSamples = 10000;
        LinkedList<Sample> testSamples = new LinkedList<Sample>();

        sampler = new SampleCollector(domain);
        double gamma = 1;
        double aver = 0;
        int total = 0;
        while (samples.size() < desiredSamples) {
            System.out.println(samples.size());
            LinkedList<Sample> currSamples = sampler.getSamplesFromWalkEvaluatedOnce(p, randomWalkLength, gamma);
            aver += currSamples.get(0).ret;
            total += 1;
            samples.addAll(currSamples);
        }
        System.out.println(aver / total);

        while (testSamples.size() < numTestSamples) {
            testSamples.addAll(sampler.getSamplesFromWalkEvaluatedOnce(p, randomWalkLength, gamma));
        }

        //System.out.println(Arrays.toString(cw.values));


        System.out.print(".");
        //do something with samples
        FunctionApproximator FAs[] = new FunctionApproximator[domain.getTotalNumActions()];
        for (int i = 0; i < domain.getTotalNumActions(); i++) {
            FAs[i] = new IndependentFourier(maxOrder, domain.getTotalNumDimensions());
        }
        //QApproximator q = new QApproximator(FAs);
        VApproximator v = new VApproximator(FAs[0]);
        VApproximator vRand = new VApproximator(new IndependentFourier(maxOrder, domain.getTotalNumDimensions()));
        //VApproximator vFull = new VApproximator(new FullFourier(maxOrder, domain.getTotalNumDimensions()));
        VApproximator vOrdered = new VApproximator(new IndependentFourier(maxOrder, domain.getTotalNumDimensions()));
        VApproximator vFullDict = new VApproximator(new IndependentFourier(maxOrder, domain.getTotalNumDimensions()));
        VApproximator vPaarOMP = new VApproximator(new IndependentFourier(maxOrder, domain.getTotalNumDimensions()));
        VApproximator vRegularised = new VApproximator(new IndependentFourier(maxOrder, domain.getTotalNumDimensions()));
        VApproximator vRegularised2 = new VApproximator(new IndependentFourier(maxOrder, domain.getTotalNumDimensions()));

        //LSTDV learner = new LSTDV(vFull, gamma);
        //learner.fullUpdate(samples);

        //OMPTD omp = new OMPTD(v, new FourierFeatureDictionary(domain.getTotalNumDimensions(), 1));

        //omp.calculateWeights(samples, 0.99, 0.001, 60);


        //need a linked list of terms already in
        //need the dictionary

        //do 1 step omp, record the error, get the basis function that was added, use that basis function to possibly generate more.

        LinkedList<BasisFunction> added = new LinkedList<BasisFunction>();
        LinkedList<BasisFunction> errorAdded = new LinkedList<BasisFunction>();
        LinkedList<BasisFunction> paarAdded = new LinkedList<BasisFunction>();
        for (BasisFunction t : v.fa.getTerms()) {
            added.add(t);
            errorAdded.add(t);
            paarAdded.add(t);
        }
        GeneralFeatureDictionary dictionary = new GeneralFeatureDictionary();
        GeneralFeatureDictionary paarDictionary = new OMPPaperDictionary();
        //FourierFeatureDictionary dictionary = new FourierFeatureDictionary(domain.getTotalNumDimensions(), maxOrder);


        //make a dictionary and a pool of potential functions to add to it from the total list. Shuffle the pool and when we add to the other dictionary we add from this one to the random dictionary.
        GeneralFeatureDictionary randomDictionary = new GeneralFeatureDictionary();
        GeneralFeatureDictionary orderedDictionary = new GeneralFeatureDictionary();
        GeneralFeatureDictionary fullDictionary = new GeneralFeatureDictionary();
        GeneralFeatureDictionary errorDictionary = new GeneralFeatureDictionary();
        RegularisedFeatureDictionary regularisedDictionary = new RegularisedFeatureDictionary();
        RegularisedFeatureDictionary regularisedDictionary2 = new RegularisedFeatureDictionary();
        LinkedList<BasisFunction> pool = new LinkedList<BasisFunction>();
        LinkedList<BasisFunction> orderedPool = new LinkedList<BasisFunction>();
        FunctionApproximator full = new FullFourier(maxOrder, domain.getTotalNumDimensions());
        for (BasisFunction t : full.getTerms()) {
            if (!added.contains(t)) {
                fullDictionary.addFeature(t);
                paarDictionary.addFeature(t);
                regularisedDictionary.addFeature(t);
                regularisedDictionary2.addFeature(t);
                pool.add(t);
                orderedPool.add(t);
            }
        }
        Collections.shuffle(pool);
        Collections.sort(orderedPool, new NormComparator());
        int numBFs = 100;

        boolean anova = true;


        if (anova) {

            //Initialise dictionary here

            for (BasisFunction t : added) {
                FourierBasis fb = (FourierBasis) t;
                double[] featureArr = fb.featureArr.clone();
                for (int j = 0; j < featureArr.length; j++) {
                    if (featureArr[j] > 0) {
                        featureArr[j]--;
                        FourierBasis newFB = new FourierBasis(featureArr.clone());
                        if (!added.contains(newFB) && featureArr[j] <= maxOrder) {
                            dictionary.addFeature(newFB);
                        }
                        featureArr[j]++;
                    }
                    featureArr[j]++;
                    FourierBasis newFB = new FourierBasis(featureArr.clone());
                    if (!added.contains(newFB) && featureArr[j] <= maxOrder) {
                        dictionary.addFeature(newFB);
                    }
                    featureArr[j]--;
                }
            }
        }
        //*********************************************************
        for (int k = 0; k < domain.getTotalNumDimensions(); k++) {
            randomDictionary.addFeature(pool.pop());
            orderedDictionary.addFeature(orderedPool.pop());
        }

        OMPTD randomOMP = new OMPTD(vRand, randomDictionary);
        OMPTD orderedOMP = new OMPTD(vOrdered, orderedDictionary);
        OMPTD fullDictOMP = new OMPTD(vFullDict, fullDictionary);
        OMPTD paarDictOMP = new OMPTD(vPaarOMP, paarDictionary);
        OMPTD regularisedOMP = new OMPTD(vRegularised, regularisedDictionary);
        OMPTD regularisedOMP2 = new OMPTD(vRegularised2, regularisedDictionary2);

        VApproximator vs[] = new VApproximator[6];
        vs[0] = vRand;
        vs[1] = vOrdered;
        vs[2] = vFullDict;
        vs[3] = vPaarOMP;
        vs[4] = vRegularised;
        vs[5] = vRegularised2;
        //vs[5] = vFull;


        //Do LSTD and find the most correlated BF to add (which gets added automatically, now update dictionary using whatever method.

        for (int i = 0; i < numBFs; i++) {
            System.out.print(":");


            LinkedList<BasisFunction> newbfsRand = randomOMP.findFirstNBasisFunctions(samples, gamma, 1);
            randomDictionary.remove(newbfsRand.get(0));

            LinkedList<BasisFunction> newbfsOrdered = orderedOMP.findFirstNBasisFunctions(samples, gamma, 1);
            orderedDictionary.remove(newbfsOrdered.get(0));

            LinkedList<BasisFunction> newbfsFull = fullDictOMP.findFirstNBasisFunctions(samples, gamma, 1);
            fullDictionary.remove(newbfsFull.get(0));

            LinkedList<BasisFunction> newbfsPaar = paarDictOMP.findFirstNBasisFunctions(samples, gamma, 1);
            paarDictionary.remove(newbfsPaar.get(0));

            LinkedList<BasisFunction> newbfsReg = regularisedOMP.findFirstNBasisFunctions(samples, gamma, 1);
            regularisedDictionary.remove(newbfsReg.get(0));

            LinkedList<BasisFunction> newbfsReg2 = regularisedOMP2.findFirstNBasisFunctions(samples, gamma, 1);
            regularisedDictionary2.remove(newbfsReg2.get(0));

            int dictionarySizes[] = new int[vs.length];
            dictionarySizes[0] = randomDictionary.getDictionarySize();
            dictionarySizes[1] = orderedDictionary.getDictionarySize();
            dictionarySizes[2] = fullDictionary.getDictionarySize();
            //dictionarySizes[5] = 0;

            writeData(vs, dictionarySizes, testSamples, experimentNumber, i + 1);

            for (int k = 0; k < domain.getTotalNumDimensions(); k++) {
                if (pool.size() > 0) {
                    randomDictionary.addFeature(pool.pop());
                    orderedDictionary.addFeature(orderedPool.pop());
                }
            }
            //==============================================

        }


        //error 2 is just to see if LSTD is actually reducing the error at all. It's just the squares of the optimal value. error2 should be greater than error.
        double error2 = 0;
        double av = 0;
        double randError = 0;
        double fullError = 0;
        double orderedError = 0;
        double fullDictError = 0;
        for (Sample samp : testSamples) {
            randError += Math.pow(samp.ret - vRand.valueAt(samp.s), 2);
            //fullError += Math.pow(samp.ret - vFull.valueAt(samp.s), 2);
            orderedError += Math.pow(samp.ret - vOrdered.valueAt(samp.s), 2);
            fullDictError += Math.pow(samp.ret - vFullDict.valueAt(samp.s), 2);
            error2 += Math.pow(samp.ret, 2);
            av += samp.ret;
        }
        error2 = Math.sqrt(error2) / Math.sqrt(testSamples.size());
        randError = Math.sqrt(randError) / Math.sqrt(testSamples.size());
        //fullError = Math.sqrt(fullError) / Math.sqrt(testSamples.size());
        orderedError = Math.sqrt(orderedError) / Math.sqrt(testSamples.size());
        fullDictError = Math.sqrt(fullDictError) / Math.sqrt(testSamples.size());
        //System.out.println(error2);
        System.out.println("Random Error:" + randError);
        System.out.println("Ordered Error:" + orderedError);
        System.out.println("Full Dictionary Error:" + fullDictError);
        System.out.println("Full Error:" + fullError);
    }
    //}

    public static void errorCombinations(OMPTD errorOMP, VApproximator v, GeneralFeatureDictionary dictionary, LinkedList<BasisFunction> added, LinkedList<Sample> samples, double gamma, int maxOrder) {
        double error = 0;
        for (Sample s : samples) {
            error += Math.abs(gamma * v.valueAt(s.nextState) - v.valueAt(s.s) + s.reward);
        }
        double averageError = error / samples.size();
        out:
        for (Sample s : samples) {
            double err = Math.abs(gamma * v.valueAt(s.nextState) - v.valueAt(s.s) + s.reward);

            if (err > averageError * 2) {
                FourierBasis bf1 = null;
                double bf1val = 0;
                FourierBasis bf2 = null;
                double bf2val = 0;

                for (BasisFunction a : added) {
                    FourierBasis b = (FourierBasis) a;
                    boolean constTerm = true;
                    for (int j = 0; j < b.featureArr.length; j++) {
                        if (b.featureArr[j] != 0) {
                            constTerm = false;
                            break;
                        }
                    }
                    if (constTerm) {
                        continue;
                    }
                    double val = Math.abs(a.getValue(s.s));
                    if (val > bf1val) {
                        bf2val = bf1val;
                        bf2 = bf1;
                        bf1val = val;
                        bf1 = b;
                    } /*
                    else if (val == bf1val) {
                    if (b.shrink < bf1.shrink) {
                    bf2val = bf1val;
                    bf2 = bf1;
                    bf1val = val;
                    bf1 = b;
                    }
                    else {
                    
                    }
                    }
                     * 
                     */ else if (val > bf2val) {
                        bf2val = val;
                        bf2 = b;
                    }
                }
                FourierBasis fbf1 = (FourierBasis) bf1;
                FourierBasis fbf2 = (FourierBasis) bf2;
                //fbf1.printBasis();
                //fbf2.printBasis();
                double featureArr[] = fbf1.featureArr.clone();
                for (int k = 0; k < featureArr.length; k++) {
                    featureArr[k] += fbf2.featureArr[k];
                    if (featureArr[k] > maxOrder) {
                        continue out;
                    }
                }
                FourierBasis combo = new FourierBasis(featureArr);
                if (combo.getValue(s.s) * err > 2 * averageError) {
                    if (!added.contains(combo)) {
                        dictionary.addFeature(combo);
                    }
                }
            }
        }
    }

    public static void writeData(VApproximator vs[], int dictionarySizes[], LinkedList<Sample> samples, int experimentNumber, int episodeNumber) {
        File output = new File("./data/OMPMountaincar");
        output = output.getAbsoluteFile();
        output.mkdirs();
        for (int i = 0; i < vs.length; i++) {

            double error = 0;
            for (Sample samp : samples) {
                error += Math.pow(samp.ret - vs[i].valueAt(samp.s), 2);
            }
            error = Math.sqrt(error) / Math.sqrt(samples.size());
            System.out.println(i + " " + error);
            File curr = new File(output.getAbsolutePath() + "/omperror_" + i + "_" + experimentNumber + ".dat");
            File currDict = new File(output.getAbsolutePath() + "/ompdictionary" + i + "_" + experimentNumber + ".dat");
            String line = experimentNumber + " " + vs[i].getNumTerms() + " " + error;
            String dictLine = experimentNumber + " " + vs[i].getNumTerms() + " " + dictionarySizes[i];

            BufferedWriter writer = null;
            try {
                writer = new BufferedWriter(new FileWriter(curr, true));
                writer.write(line + "\r\n");
            } catch (IOException ex) {
                Logger.getLogger(CollectSamples.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    writer.close();
                } catch (IOException ex) {
                    Logger.getLogger(CollectSamples.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            try {
                writer = new BufferedWriter(new FileWriter(currDict, true));
                writer.write(dictLine + "\r\n");
            } catch (IOException ex) {
                Logger.getLogger(CollectSamples.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    writer.close();
                } catch (IOException ex) {
                    Logger.getLogger(CollectSamples.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }


    }

    public static double runAndLearnEpisode(Domain domain, Policy policy, SarsaLambdaAlphaScale learner) {


        domain.resetDomain();
        //we don't create a new learner each time because alpha may have changed.
        learner.startEpisode(); //clears traces and stuff
        double ret = 0;
        State currState = domain.getState();
        if (currState.isTerminal()) {
            return 0;
        }

        int nextAction = policy.select(currState);
        int count = 0;
        while (!currState.isTerminal()) {
            int action = nextAction;
            double reward = domain.step(action);
            ret += reward;
            State nextState = domain.getState();
            if (!nextState.isTerminal()) {
                nextAction = policy.select(nextState);
            }
            //this is where you would have the full sample. Not doing anything with the sample right now.
            Sample sample = new Sample(currState, action, nextState, nextAction, reward);
            learner.addSample(currState, action, nextState, nextAction, reward);
            currState = nextState;
            count++;
        }
        //System.out.println("Number of steps: " + count);
        return ret;
    }
}
