/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package domains;

import domains.mountaincar.MCPolicy;
import domains.mountaincar.MountainCar;
import domaintools.SampleCollector;
import experimental.OMPBRM;
import functionapproximation.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import rl.policy.Policy;
import rl.policy.RandomPolicy;
import rl.Sample;

/**
 *
 * @author Dean
 */
public class MitchCollectSamples {
    public static FunctionApproximator[] CreateFAs(Domain domain, int wBaseScale, int wMaxScale) {
        int nFourier = 3;
        FunctionApproximator FAs[] = new FunctionApproximator[1];
            for (int j = 0; j < 1; j++) { // hard coded to one for v function approx
                FAs[j] = new WaveletTensorBasis(wBaseScale, wMaxScale, domain.getTotalNumDimensions(), 8);
                //FAs[j] = new FullFourier(nFourier, domain.getNumDimensions());
            }
        return FAs;
    }
    public static void main(String args[]) {
        int experimentNum = 1;
        if (args.length > 0) {
            experimentNum = Integer.parseInt(args[0]);
        }

        //String pinballMapFile = "./pinballfiles/pinball_hard_single.cfg";
        //Domain domain = new PinballWrapper(pinballMapFile); //pinball had to have a wrapper around it because it's George's external code I don't want to change.
        //Domain domain = new Acrobot();
        Domain domain = new MountainCar();
        //Domain domain = new WallWorld();
        
        Policy p = new MCPolicy(domain);
        //Policy p = new RandomPolicy(domain);
        //FunctionApproximator FAs[] = CreateFAs(domain);
        //QApproximator q = new QApproximator(FAs);
        //Policy p = new EpsilonGreedy(domain, q, 0);
        
        SampleCollector sampler = new SampleCollector(domain);
        double gamma = 1;
       
        LinkedList<Sample> samples = new LinkedList<Sample>();
        int desiredSamples = 5000; // number of episodes
        int randomWalkLength = 50; // max steps
        while (samples.size() < desiredSamples) {
            LinkedList<Sample> currSamples = sampler.getSamplesFromWalkEvaluatedOnce(p, randomWalkLength, gamma);
            samples.addAll(currSamples);
        }
        System.out.println("Samples collected");
        double[][][] results = new double[3][][];
        //Policy p = new EpsilonGreedy(domain, q, 0);
        double betas[] = {0.001, 0.0005, 0.0001, 0.00005, 0.00001, 0.000005, 0.000001};
        OMPBRM fixed = new OMPBRM(p, new QApproximator(CreateFAs(domain, 2, 3)), 1, betas, false);
        results[0] = fixed.processSamples(samples);
        System.out.println(experimentNum + " 1/3 done.");
        fixed = new OMPBRM(p, new QApproximator(CreateFAs(domain, 2, -1)), 1, betas, true, 3);
        System.gc();
        results[1] = fixed.processSamples(samples);
        System.out.println(experimentNum + " 2/3 done.");
        fixed = new OMPBRM(p, new QApproximator(CreateFAs(domain, 0, -1)), 1, betas, true,4);
        System.gc();
        results[2] = fixed.processSamples(samples);
        System.out.println(experimentNum + " 3/3 done, writing results.");
        writeResults(experimentNum, results);
    }
    
    public static void writeResults(int experimentNum, double results[][][]) {
        for (int method = 0; method < results.length; method++) {
            for (int metric = 0; metric < results[method].length; metric++) {
                BufferedWriter writer = null;
                try {
                    String result = arrayToString(results[method][metric]);
                    result = experimentNum + " " + result;
                    File output = new File("mountaincarresults/method" + method + "_metric" + metric + "_experiment" + experimentNum + ".txt");
                    output = output.getAbsoluteFile();
                    File parent = output.getParentFile();
                    parent.mkdir();
                    writer = new BufferedWriter(new FileWriter(output));
                    writer.write(result);
                } catch (IOException ex) {
                    Logger.getLogger(CollectSamples.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    try {
                        writer.close();
                    } catch (IOException ex) {
                        Logger.getLogger(CollectSamples.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }
    
    public static String arrayToString(double array[]) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            builder.append(array[i]);
            builder.append(" ");
        }
        return builder.toString().trim();
    }

}
