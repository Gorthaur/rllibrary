/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package domains.cartpole;

import domains.Domain;
import java.util.LinkedList;
import rl.state.State;
import rl.state.ScaledState;

/**
 *
 * @author Dean
 */
public class CartPole extends Domain {

    CartPoleState state;
    
    CartPoleState tester;
    double minStateValues[];
    double maxStateValues[];
    LinkedList<Integer> availableActions;

    public CartPole() {
        super();

        boolean randomStartStates = false;
        double transitionNoise = 0.0d;
        long randomSeed = 0L;
        boolean useDiscountFactor = true;

        double leftAngleBound = CartPoleState.DEFAULTLEFTANGLEBOUND;
        double rightAngleBound = CartPoleState.DEFAULTRIGHTANGLEBOUND;
        double leftCartBound = CartPoleState.DEFAULTLEFTCARTBOUND;
        double rightCartBound = CartPoleState.DEFAULTRIGHTCARTBOUND;

        state = new CartPoleState(randomStartStates, transitionNoise, randomSeed, useDiscountFactor);
        tester = new CartPoleState(randomStartStates, transitionNoise, randomSeed, useDiscountFactor);
        state.leftAngleBound = leftAngleBound;
        state.rightAngleBound = rightAngleBound;
        state.leftCartBound = leftCartBound;
        state.rightCartBound = rightCartBound;
        tester.leftAngleBound = leftAngleBound;
        tester.rightAngleBound = rightAngleBound;
        tester.leftCartBound = leftCartBound;
        tester.rightCartBound = rightCartBound;
    }

    @Override
    public State getState() {
        double vars[] = new double[]{state.getX(), state.getTheta(), state.getXDot(), state.getThetaDot()};
        if (minStateValues == null) {
            minStateValues = new double[]{state.getLeftCartBound(), state.getLeftAngleBound(), -6.0, -6.0};
            maxStateValues = new double[]{state.getRightCartBound(), state.getRightAngleBound(), 6.0, 6.0}; //the 6's are guesses according to the cartpole class
        }
        ScaledState s = new ScaledState(vars, minStateValues, maxStateValues);
        if (state.inFailure()) {
            s.setTerminal(true);
        }
        return s;
    }

    @Override
    public double step(int action) {
        state.update(action);
        return state.getReward();
    }

    @Override
    public void setState(State s) {
        setState(s, state);
    }
    
    public void setState(State s, CartPoleState setState) {
        if (minStateValues == null) {
            minStateValues = new double[]{setState.getLeftCartBound(), setState.getLeftAngleBound(), -6.0, -6.0};
            maxStateValues = new double[]{setState.getRightCartBound(), setState.getRightAngleBound(), 6.0, 6.0}; //the 6's are guesses according to the cartpole class
        }
        setState.reset();
        setState.x = (s.getState()[0])*(maxStateValues[0] - minStateValues[0]) + minStateValues[0];
        setState.theta = (s.getState()[1])*(maxStateValues[1] - minStateValues[1]) + minStateValues[1];
        setState.x_dot = (s.getState()[2])*(maxStateValues[2] - minStateValues[2]) + minStateValues[2];
        setState.theta_dot = (s.getState()[3])*(maxStateValues[3] - minStateValues[3]) + minStateValues[3];
    }

    @Override
    public boolean isEpisodeOver() {
        return state.inFailure();
    }

    @Override
    public State getRandomState() {
        double s[] = new double[this.getTotalNumDimensions()];
        for (int i = 0; i < s.length; i++) {
            s[i] = Math.random();
        }
        State st = new State(s);
        setState(st, tester);
        if (tester.inFailure()) {
            return getRandomState();
        }
        else {
            return st;
        }
    }

    @Override
    public int getNumActions(State s) {
        return 2;
    }

    @Override
    public LinkedList<Integer> getAvailableActions(State s) {
        if (this.availableActions == null) {
            availableActions = new LinkedList<Integer>();
            availableActions.add(0);
            availableActions.add(1);
        }
        return availableActions;
    }

    @Override
    public int getTotalNumActions() {
        return 2;
    }

    @Override
    public void resetDomain() {
        state.reset();
    }

    @Override
    public String getDescriptionString() {
        return "cartpole";
    }

    @Override
    public int getTotalNumDimensions() {
        return 4;
    }
    
    public static int getNumDimensions() {
        return 4;
    }

    public static int getNumActions() {
        return 2;
    }
}
