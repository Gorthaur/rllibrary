package rlmath;

/**
 *
 * @author Dean
 */
public class MatrixMath {

    //=======================================================
    //Matrix operations
    //=======================================================
    public static double[][] multiply(double arr[][], double c) {
        double returnArr[][] = new double[arr.length][arr[0].length];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                returnArr[i][j] = arr[i][j] * c;
            }
        }
        return returnArr;
    }

    public static double[] multiply(double arr[], double c) {
        double returnArr[] = new double[arr.length];
        for (int i = 0; i < arr.length; i++) {
            returnArr[i] = arr[i] * c;

        }
        return returnArr;
    }

    public static double[][] multiply(double arr[], double arr2[]) {
        double returnArr[][] = new double[arr.length][arr.length];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                returnArr[i][j] = arr[i] * arr2[j];
            }
        }
        return returnArr;
    }

    public static double[][] divide(double arr[][], double c) {
        double returnArr[][] = new double[arr.length][arr[0].length];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                returnArr[i][j] = arr[i][j] / c;
            }
        }
        return returnArr;
    }

    public static double[][] multiply(double arr1[][], double arr2[][]) {
        return multiply(arr1, false, arr2, false);
    }

    public static double[][] multiply(double arr1[][], boolean transpose1, double arr2[][], boolean transpose2) {
        int arr1width = arr1[0].length;
        int arr1height = arr1.length;
        if (transpose1) {
            int temp = arr1width;
            arr1width = arr1height;
            arr1height = temp;
        }

        int arr2width = arr2[0].length;
        int arr2height = arr2.length;
        if (transpose2) {
            int temp = arr2width;
            arr2width = arr2height;
            arr2height = temp;
        }
        if (arr1width != arr2height) {
            System.out.println("Matrix size error");
            return null;
        }
        double returnArr[][] = new double[arr1height][arr2width];

        for (int i = 0; i < returnArr.length; i++) {
            for (int j = 0; j < returnArr[i].length; j++) {
                double val = 0;
                for (int k = 0; k < arr1width; k++) {
                    int m = i;
                    int n = k;

                    if (transpose1) {
                        int temp = m;
                        m = n;
                        n = temp;
                    }

                    int p = k;
                    int q = j;

                    if (transpose2) {
                        int temp = p;
                        p = q;
                        q = temp;
                    }

                    val += arr1[m][n] * arr2[p][q];
                }
                returnArr[i][j] = val;
            }
        }
        return returnArr;
    }

    public static double[] multiply(double arr1[][], double arr2[]) {
        double result[] = new double[arr1.length];
        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr1[i].length; j++) {
                result[i] += arr1[i][j] * arr2[j];
            }
        }
        return result;
    }

    public static double[][] subtract(double arr1[][], double arr2[][]) {
        if (arr1.length != arr2.length || arr1[0].length != arr2[0].length) {
            System.out.println("Subtract error. Matrix dimensions do not match");
        }

        double returnVal[][] = new double[arr1.length][arr1[0].length];

        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr1[i].length; j++) {
                returnVal[i][j] = arr1[i][j] - arr2[i][j];
            }
        }
        return returnVal;
    }

    public static double[][] add(double arr1[][], double arr2[][]) {
        if (arr1.length != arr2.length || arr1[0].length != arr2[0].length) {
            System.out.println("Subtract error. Matrix dimensions do not match");
        }

        double returnVal[][] = new double[arr1.length][arr1[0].length];

        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr1[i].length; j++) {
                returnVal[i][j] = arr1[i][j] + arr2[i][j];
            }
        }
        return returnVal;
    }

    public static double[][] transpose(double arr[][]) {
        double returnVal[][] = new double[arr[0].length][arr.length];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                returnVal[j][i] = arr[i][j];
            }
        }
        return returnVal;
    }

    public static double[][] eye(int size) {
        double[][] returnVal = new double[size][size];
        for (int i = 0; i < size; i++) {
            returnVal[i][i] = 1;
        }
        return returnVal;
    }

    public static double[][] diag(double diag[]) {
        double[][] returnVal = new double[diag.length][diag.length];
        for (int i = 0; i < diag.length; i++) {
            returnVal[i][i] = diag[i];
        }
        return returnVal;
    }

    /**
     * Returns a k x 1 matrix A derived from the given matrix M where A(i,1) =
     * M(i,i). If M is a k x 1 matrix, a k x k matrix is returned with M on the
     * diagonal.
     *
     * @param diag
     * @return
     */
    public static double[][] diag(double diag[][]) {
        if (diag[0].length == diag.length) {
            double[][] returnVal = new double[diag.length][1];
            for (int i = 0; i < diag.length; i++) {
                returnVal[i][0] = diag[i][i];
            }
            return returnVal;
        } else if (diag[0].length == 1) {
            double[][] returnVal = new double[diag.length][diag.length];
            for (int i = 0; i < diag.length; i++) {
                returnVal[i][i] = diag[i][0];
            }
            return returnVal;
        } else {
            return null;
        }
    }

    public static double norm(double arr1[], double arr2[]) {
        double ret = 0;
        for (int i = 0; i < arr1.length; i++) {
            ret += Math.pow(arr1[i] - arr2[i], 2);
        }
        return Math.sqrt(ret);
    }

    public static double[][] zeros(int size) {
        double[][] returnVal = new double[size][size];
        return returnVal;
    }

    public static double[][] toeplitz(int size, double subDiagonal, double diagonal, double superDiagonal) {
        double[][] returnVal = new double[size][size];
        for (int i = 0; i < size; i++) {
            returnVal[i][i] = diagonal;
            if (i < size - 1) {
                returnVal[i + 1][i] = subDiagonal;
            }
            if (i > 0) {
                returnVal[i - 1][i] = superDiagonal;
            }
        }
        return returnVal;
    }

    public static double[][] resize(double matrix[][], int rows, int columns) {
        double[][] returnVal = new double[rows][columns];
        rows = Math.min(matrix.length, rows);
        columns = Math.min(matrix[0].length, columns);
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                returnVal[i][j] = matrix[i][j];
            }
        }
        return returnVal;
    }

    public static double[][] cos(double matrix[][]) {
        double[][] returnVal = new double[matrix.length][matrix[0].length];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                returnVal[i][j] = Math.cos(matrix[i][j]);
            }
        }
        return returnVal;
    }

    public static double[][] sin(double matrix[][]) {
        double[][] returnVal = new double[matrix.length][matrix[0].length];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                returnVal[i][j] = Math.sin(matrix[i][j]);
            }
        }
        return returnVal;
    }

    public static double[] subtract(double arr1[], double arr2[]) {
        double ret[] = new double[arr1.length];
        for (int i = 0; i < arr1.length; i++) {
            ret[i] = arr1[i] - arr2[i];
        }
        return ret;
    }

    public static double[] subtract(double arr1[], double a) {
        double ret[] = new double[arr1.length];
        for (int i = 0; i < arr1.length; i++) {
            ret[i] = arr1[i] - a;
        }
        return ret;
    }

    public static double[] add(double arr1[], double a) {
        double ret[] = new double[arr1.length];
        for (int i = 0; i < arr1.length; i++) {
            ret[i] = arr1[i] + a;
        }
        return ret;
    }

    public static double norm(double arr1[]) {
        double ret = 0;
        for (int i = 0; i < arr1.length; i++) {
            ret += Math.pow(arr1[i], 2);
        }
        return Math.sqrt(ret);
    }

    public static double[] normalise(double arr[]) {
        double norm = norm(arr);
        if (norm == 0) {
            return new double[arr.length]; //zeros
        }
        double ret[] = new double[arr.length];
        for (int i = 0; i < arr.length; i++) {
            ret[i] = arr[i] / norm;
        }
        return ret;
    }

    public static boolean approxEqual(double arr1[], double arr2[], double maxDiff) {
        for (int i = 0; i < arr1.length; i++) {
            if (Math.abs(arr1[i] - arr2[i]) > maxDiff) {
                return false;
            }
        }
        return true;
    }

    public static boolean approxEqual(double arr1[], double arr2[]) {
        return approxEqual(arr1, arr2, 0.000000001);
    }

    public static double dot(double arr1[], double arr2[]) {
        double result = 0;
        for (int i = 0; i < arr1.length; i++) {
            result += arr1[i] * arr2[i];
        }
        return result;
    }

    public static double[] dotTimes(double arr1[], double arr2[]) {
        double result[] = new double[arr1.length];
        for (int i = 0; i < arr1.length; i++) {
            result[i] = arr1[i] * arr2[i];
        }
        return result;
    }

    public static double frobeniusNorm(double[][] A) {
        double ans = 0;
        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < A[i].length; j++) {
                ans += Math.pow(A[i][j], 2);
            }
        }
        return ans;
    }
}
