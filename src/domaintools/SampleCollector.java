package domaintools;

import domains.Domain;
import java.util.Arrays;
import java.util.LinkedList;
import rl.policy.Policy;
import rl.Sample;
import rl.state.State;

/**
 *
 * @author Dean
 */
public class SampleCollector {

    Domain domain;

    /**
     * Constructs the Sample collector object which collects samples from the
     * given domain object.
     *
     * @param domain The domain to collect samples from.
     */
    public SampleCollector(Domain domain) {
        this.domain = domain;
    }

    /**
     * Gets one sample starting from a random state, taking an action using the
     * given policy.
     *
     * @param p The policy to be followed.
     * @return A single sample object.
     */
    public Sample getSample(Policy p) {
        State s = domain.getRandomState();
        return getSample(p, s);
    }

    /**
     * Gets one sample starting from the given state, taking an action using the
     * given policy.
     *
     * @param p The policy to be followed.
     * @param startState The state the sample is started from.
     * @return
     */
    public Sample getSample(Policy p, State startState) {
        domain.setState(startState);
        int action = p.select(startState);
        double reward = domain.step(action);
        State nextState = domain.getState();
        int nextAction = 0; //if the state is terminal, the next action is irrelevant, so set to 0.
        if (!nextState.isTerminal()) {
            nextAction = p.select(nextState);
        }
        Sample sample = new Sample(startState, action, nextState, nextAction, reward);
        return sample;
    }

    /**
     * Gets a list of samples taken from a walk starting at a random state
     * following the given policy. Starting state is guaranteed to be a
     * non-terminal state.
     *
     * @param p The policy to be followed.
     * @param maxWalkLength The maximum number of moves that should be made. May
     * be less depending if a terminal state is reached.
     * @return List of samples given in order they were traversed.
     */
    public LinkedList<Sample> getSamplesFromWalkRandomStart(Policy p, int maxWalkLength) {
        State s = domain.getRandomState();
        return getSamplesFromWalk(p, maxWalkLength, s);
    }

    /*
    public LinkedList<Sample> getSamplesFromWalkRandomStartEvaluated(Policy p, int maxWalkLength, double gamma, int monteLength) {
        State s = domain.getRandomState();
        return getSamplesFromWalkEvaluated(p, maxWalkLength, s, gamma, monteLength);
    }
    */

    /**
     * Gets a list of samples taken from a walk starting at the given state
     * following the given policy.
     *
     * @param p The policy to be followed.
     * @param maxWalkLength The maximum number of moves that should be made. May
     * be less depending if a terminal state is reached.
     * @param startState The state the walk is started from.
     * @return List of samples given in the order they were traversed.
     */
    public LinkedList<Sample> getSamplesFromWalk(Policy p, int maxWalkLength, State startState) {
        domain.setState(startState);
        return getSamplesFromWalk(p, maxWalkLength);
    }
/*
    public LinkedList<Sample> getSamplesFromWalkEvaluated(Policy p, int maxWalkLength, State startState, double gamma, int monteLength) {
        domain.setState(startState);
        return getSamplesFromWalkEvaluated(p, maxWalkLength, gamma, monteLength);
    }
    */

    /**
     * Gets a list of samples taken from a walk starting at the current state
     * following the given policy.
     *
     * @param p The policy to be followed.
     * @param maxWalkLength The maximum number of moves that should be made. May
     * be less depending if a terminal state is reached.
     * @return List of samples given in the order they were traversed.
     */
    public LinkedList<Sample> getSamplesFromWalk(Policy p, int maxWalkLength) {
        LinkedList<Sample> samples = new LinkedList<Sample>();
        int count = 0;
        State curr = domain.getState();
        while (!curr.isTerminal() && count < maxWalkLength) {
            int action = p.select(curr);
            double reward = domain.step(action);
            State nextState = domain.getState();
            int nextAction = 0; //if the state is terminal, the next action is irrelevant, so set to 0.
            if (!nextState.isTerminal()) {
                nextAction = p.select(nextState);
            }
            Sample sample = new Sample(curr, action, nextState, nextAction, reward);
            curr = nextState;
            samples.add(sample);
            count++;
        }
        return samples;
    }
/*
    public LinkedList<Sample> getSamplesFromWalkEvaluated(Policy p, int maxWalkLength, double gamma, int monteLength) {
        State curr = domain.getState();
        if (monteLength == 1) {
            LinkedList<Sample> s = getSamplesFromWalkEvaluatedOnce(p, maxWalkLength, gamma);
            LinkedList<Sample> ret = new LinkedList<Sample>();
            for (int i = 0; i < maxWalkLength; i++) {
                if (s.size() <= i) {
                    return ret;
                }
                ret.add(s.get(i));
            }
            return ret;
        }

        LinkedList<Sample> samples = new LinkedList<Sample>();
        int othercount, count = 0;

        int i;
        while (!curr.isTerminal() && count < maxWalkLength) {
            double aveReturn = 0;
            for (i = 0; i < monteLength; i++) {
                State curr1 = curr;
                othercount = 0;
                while (!curr1.isTerminal()) {
                    aveReturn += Math.pow(gamma, othercount) * domain.step(p.select(curr1));
                    othercount++;
                    curr1 = domain.getState();
                }
                domain.setState(curr);
            }
            aveReturn = aveReturn / monteLength;
            //System.out.println(aveReturn);
            int action = p.select(curr);
            double reward = domain.step(action);
            State nextState = domain.getState();
            int nextAction = 0; //if the state is terminal, the next action is irrelevant, so set to 0.
            if (!nextState.isTerminal()) {
                nextAction = p.select(nextState);
            }
            Sample sample = new Sample(curr, action, nextState, nextAction, reward, aveReturn);
            curr = nextState;
            samples.add(sample);
            count++;
        }
        return samples;
    }
    */

    /**
     * Fast implementation of working out the return for each state when you
     * have a deterministic policy.
     *
     * @param p
     * @param maxWalkLength
     * @param gamma
     * @return
     */
    public LinkedList<Sample> getSamplesFromWalkEvaluatedOnce(Policy p, int maxWalkLength, double gamma) {
        try {
            return SampleRecurse(p, maxWalkLength, gamma);
        }
        catch (StackOverflowError e) {
            return new LinkedList<Sample>();
        }

    }
    
    public LinkedList<Sample> SampleRecurse(Policy p, int maxWalkLength, double gamma) {
        
            State curr = domain.getState();
        if (curr.isTerminal()) {
            LinkedList<Sample> s = new LinkedList<Sample>();
            // s.add(new Sample(curr, 0))
            return s;
        } else {
            int action = p.select(curr);
            double reward = domain.step(action);
            State nextState = domain.getState();
            int nextAction = 0; //if the state is terminal, the next action is irrelevant, so set to 0.
            if (!nextState.isTerminal()) {
                nextAction = p.select(nextState);
            }
            double ret = reward;
            LinkedList<Sample> samples;
            try {
                samples = getSamplesFromWalkEvaluatedOnce(p, maxWalkLength - 1, gamma);
            } catch (StackOverflowError e) {
                System.out.println(Arrays.toString(curr.getState()));
                throw e;
            }
            if (samples.size() > 0) {
                ret += gamma * samples.get(0).ret;
            }

            Sample samp = new Sample(curr, action, nextState, nextAction, reward, ret);
            samples.push(samp);
            return samples;
        }
    }
    /*
     public LinkedList<Sample> getSamplesFromWalkEvaluatedOnce(Policy p, int maxWalkLength, double gamma) {
        
     State curr = domain.getState();
     while (!curr.isTerminal()) {
     double value = domain.getCurrStateValue();
            
     }
     if (curr.isTerminal()) {
     LinkedList<Sample> s = new LinkedList<Sample>();
     s.add(new Sample(curr, 0))
     } else {
     int action = p.select(curr);
     double reward = domain.step(action);
     State nextState = domain.getState();
     int nextAction = 0; //if the state is terminal, the next action is irrelevant, so set to 0.
     if (!nextState.isTerminal()) {
     nextAction = p.select(nextState);
     }
     double ret = reward;
     LinkedList<Sample> samples = getSamplesFromWalkEvaluatedOnce(p, maxWalkLength - 1, gamma);
     if (samples.size() > 0) {
     ret += gamma*samples.get(0).ret;
     }
            
     Sample samp = new Sample(curr, action, nextState, nextAction, reward, ret);
     samples.push(samp);
     return samples;
     }
     }
     */
}
