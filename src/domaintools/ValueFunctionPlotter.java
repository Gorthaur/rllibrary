package domaintools;

import domains.mountaincar.MCPolicy;
import domains.mountaincar.MountainCar;
import functionapproximation.QApproximator;
import functionapproximation.TileBasis;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import rl.Sample;
import rl.state.State;

/**
 *
 * @author dean
 */
public class ValueFunctionPlotter {

    double dimStep[];
    double dimStart[];
    double dimEnd[];
    int numDims = 0;
    QApproximator q;
    int numActions = 0;
    String baseFileName = "";

    public ValueFunctionPlotter(QApproximator q, String fileName) {
        this.q = q;
        numActions = q.getNumActions();
        this.baseFileName = fileName;
        numDims = q.getFAs()[0].getNumFeatures();
        dimStep = new double[numDims];
        dimStart = new double[numDims];
        dimEnd = new double[numDims];
        for (int i = 0; i < numDims; i++) {
            dimStep[i] = 0.01;
            dimStart[i] = 0;
            dimEnd[i] = 1;
        }

    }

    public void writeActionsMatFile() {
        for (int i = 0; i < numActions; i++) {
            String tempFileName = baseFileName + "-act" + i + ".dat";

            try {
                File file = new File(tempFileName);
                file = file.getAbsoluteFile();
                file.getParentFile().mkdirs();
                FileWriter f = new FileWriter(file);
                double currState[] = dimStart.clone();
                int currStatePos = 0;
                StringBuilder dimStrings[] = new StringBuilder[numDims];
                StringBuilder valueString = new StringBuilder();
                for (int k = 0; k < numDims; k++) {
                    dimStrings[k] = new StringBuilder();
                }
                outer:
                while (currStatePos < numDims) {
                    while (currState[currStatePos] >= dimEnd[currStatePos]) {
                        currState[currStatePos] = dimStart[currStatePos];
                        currStatePos++;
                        if (currStatePos >= numDims) {
                            break outer;
                        }
                        currState[currStatePos] += dimStep[currStatePos];

                    }
                    currStatePos = 0;
                    currState[currStatePos] += dimStep[currStatePos];

                    State s = new State(currState);
                    double value = q.valueAt(s, i);
                    valueString.append(value + " ");
                    for (int k = 0; k < numDims; k++) {
                        if (dimStep[k] == 0) {
                            continue;
                        }
                        dimStrings[k].append(currState[k] + " ");
                    }
                }
                f.write(valueString.toString().trim() + "\n");
                for (int k = 0; k < numDims; k++) {
                    f.write(dimStrings[k].toString().trim() + "\n");
                }
                f.flush();
                f.close();

            } catch (IOException ex) {
                Logger.getLogger(ValueFunctionPlotter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void writeValuesMatFile() {

        String tempFileName = baseFileName + "-value.dat";

        try {
            File file = new File(tempFileName);
            file = file.getAbsoluteFile();
            file.getParentFile().mkdirs();
            FileWriter f = new FileWriter(file);
            double currState[] = dimStart.clone();
            int currStatePos = 0;
            StringBuilder dimStrings[] = new StringBuilder[numDims];
            StringBuilder valueString = new StringBuilder();
            for (int k = 0; k < numDims; k++) {
                dimStrings[k] = new StringBuilder();
            }
            outer:
            while (currStatePos < numDims) {
                while (currState[currStatePos] > dimEnd[currStatePos]) {
                    currState[currStatePos] = dimStart[currStatePos];
                    currStatePos++;
                    if (currStatePos >= numDims) {
                        break outer;
                    }
                    currState[currStatePos] += dimStep[currStatePos];

                }
                currStatePos = 0;
                currState[currStatePos] += dimStep[currStatePos];

                State s = new State(currState);
                double val = Double.NEGATIVE_INFINITY;
                for (int i = 0; i < numActions; i++) {
                    double value = q.valueAt(s, i);
                    /*
                     SampleCollector sample = new SampleCollector(new MountainCar());
                     sample.domain.setState(s);
                     LinkedList<Sample> samps = sample.getSamplesFromWalkEvaluated(new MCPolicy(new MountainCar()), 2, 0.99, 1);

                     double value = 0;
                     if (samps.size() == 0) {
                     value = 1;
                     } else {
                     value = samps.get(0).ret;
                     }
                     * 
                     */
                    if (value > val) {
                        val = value;
                    }
                }
                valueString.append(val + " ");
                for (int k = 0; k < numDims; k++) {
                    dimStrings[k].append(currState[k] + " ");
                }
            }
            f.write(valueString.toString().trim() + "\n");
            for (int k = 0; k < numDims; k++) {
                f.write(dimStrings[k].toString().trim() + "\n");
            }
            f.flush();
            f.close();

        } catch (IOException ex) {
            Logger.getLogger(ValueFunctionPlotter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
