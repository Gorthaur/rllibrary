package experimental;


import functionapproximation.QApproximator;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import rl.policy.Policy;
import rl.learningalgorithms.SarsaLambda;
import rl.state.State;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Dean
 */
public class SarsaLambdaErrorPlot extends SarsaLambda {

    double matrix[][];
    OnlineWeightedClustering clusters;
    int framerate = 20;
    double averager = 20;
    int divisions = 15;
    int count = 0;
    String file = "mountaincar_error.dat";
    String clusterfile = "mountaincar_cluster.dat";
    String basisfile = "mountaincar_basis.dat";
    EXPAveragePoint pt;

    public SarsaLambdaErrorPlot(QApproximator q, Policy p, double alpha, double gamma, double lambda) {
        super(q, p, alpha, gamma, lambda);
        matrix = new double[divisions][divisions];
        for (int i = 0; i < divisions; i++) {
            //Arrays.fill(matrix[i], -1);
        }
        clusters = new OnlineWeightedClustering(20);
        //pt = new EXPAveragePoint(new double[]{0.9999999, 0.6});
    }

    public void addSample(State s, int action, State nextState, int nextAction, double reward) {
        double delta = reward - q.valueAt(s, action);
        if (!nextState.isTerminal()) {
            delta += gamma * q.valueAt(nextState, nextAction);
        }
        delta = Math.abs(delta);
        double state[] = s.getState();
        clusters.addDataPoint(state, delta);
        //pt.addPoint(state, delta);
        int x = (int) (state[0] * divisions);
        if (x == divisions) {
            x = divisions - 1;
        }
        int y = (int) (state[1] * divisions);
        if (y == divisions) {
            y = divisions - 1;
        }
        if (matrix[y][x] == -1) {
            matrix[y][x] = delta;
        } else {
            matrix[y][x] = matrix[y][x] + (1 / averager) * (delta - matrix[y][x]);
        }
        if (count % framerate == 0) {
            saveMatrix();
            saveClusters();
            //printClusters();
        }
        count++;
        super.addSample(s, action, nextState, nextAction, reward);
    }

    public void saveMatrix() {
        int frame = (count / framerate) + 1;
        FileWriter f = null;
        try {
            f = new FileWriter(file, true);
        } catch (IOException ex) {
            Logger.getLogger(SarsaLambdaErrorPlot.class.getName()).log(Level.SEVERE, null, ex);
        }
        BufferedWriter writer = new BufferedWriter(f);
        for (int i = 0; i < divisions; i++) {
            for (int j = 0; j < divisions; j++) {
                String line = "";
                if (matrix[j][i] != -1) {
                    line = frame + " " + (i + 1) + " " + (j + 1) + " " + matrix[j][i] + "\r\n";
                }
                try {
                    writer.write(line);
                } catch (IOException ex) {
                    Logger.getLogger(SarsaLambdaErrorPlot.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        try {
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(SarsaLambdaErrorPlot.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void saveClusters() {
        int frame = (count / framerate) + 1;
        FileWriter f = null;
        try {
            f = new FileWriter(clusterfile, true);
        } catch (IOException ex) {
            Logger.getLogger(SarsaLambdaErrorPlot.class.getName()).log(Level.SEVERE, null, ex);
        }
        BufferedWriter writer = new BufferedWriter(f);
        /*
        int x = (int) (pt.centre[0] * divisions) + 1;
        int y = (int) (pt.centre[1] * divisions) + 1;
        String line = frame + " " + (x) + " " + (y) + " " + pt.weight + "\r\n";
        try {
        writer.write(line);
        } catch (IOException ex) {
        Logger.getLogger(SarsaLambdaErrorPlot.class.getName()).log(Level.SEVERE, null, ex);
        }
         * */


        for (int i = 0; i < clusters.centres.size(); i++) {
            int x = (int) (clusters.centres.get(i).getCentre()[0] * divisions);
            x++;
            int y = (int) (clusters.centres.get(i).getCentre()[1] * divisions);
            y++;
            String line = frame + " " + (x) + " " + (y) + " " + clusters.centres.get(i).meanWeight + " " + clusters.centres.get(i).simpleVariance + " " + clusters.centres.get(i).sumWeight + "\r\n";
            try {
                writer.write(line);
            } catch (IOException ex) {
                Logger.getLogger(SarsaLambdaErrorPlot.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try {
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(SarsaLambdaErrorPlot.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void saveNewBasisFunctions() {
        int frame = (count / framerate) + 1;
        FileWriter f = null;
        try {
            f = new FileWriter(basisfile, true);
        } catch (IOException ex) {
            Logger.getLogger(SarsaLambdaErrorPlot.class.getName()).log(Level.SEVERE, null, ex);
        }
        BufferedWriter writer = new BufferedWriter(f);
        /*
        int x = (int) (pt.centre[0] * divisions) + 1;
        int y = (int) (pt.centre[1] * divisions) + 1;
        String line = frame + " " + (x) + " " + (y) + " " + pt.weight + "\r\n";
        try {
        writer.write(line);
        } catch (IOException ex) {
        Logger.getLogger(SarsaLambdaErrorPlot.class.getName()).log(Level.SEVERE, null, ex);
        }
         * */


        for (int i = 0; i < clusters.centres.size(); i++) {
            int x = (int) (((UphillCluster)(clusters.centres.get(i))).averagePosition[0] * divisions);
            x++;
            int y = (int) (((UphillCluster)(clusters.centres.get(i))).averagePosition[1] * divisions);
            y++;
            String line = frame + " " + (x) + " " + (y) + " " + clusters.centres.get(i).meanWeight + "\r\n";
            try {
                writer.write(line);
            } catch (IOException ex) {
                Logger.getLogger(SarsaLambdaErrorPlot.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try {
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(SarsaLambdaErrorPlot.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void printClusters() {
        //for (int i = 0; i < clusters.length; i++) {
        //   System.out.print("(" + clusters[i].getCentre()[0] + "," + clusters[i].getCentre()[1] + ") ");
        //}
        System.out.println();
    }
}
