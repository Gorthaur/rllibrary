package experimental;

import functionapproximation.QApproximator;
import functionapproximation.FourierBasis;
import java.util.ArrayList;
import rl.ElegibilityTrace;
import rl.policy.Policy;
import rl.state.State;

public class GQLambdaTester {

    QApproximator q;
    Policy p;
    double secondaryWeights[];
    double primaryWeights[];
    double shrink[];
    //int numTerms;
    // double alpha = 0.0001;
    // double eta = 0.01;
    double lambda;
    double gamma;
    double alpha;
    ArrayList<double[]> errors[];
    int xFrequency = 100;
    int yFrequency = 100;
    double topVals[][][] = new double[3][xFrequency][yFrequency];
    FourierBasis f[][] = new FourierBasis[xFrequency][yFrequency];
    double botVals[][][] = new double[3][xFrequency][yFrequency];
    int actionCounter[] = new int[3];
    ElegibilityTrace trace;

    public GQLambdaTester(QApproximator q, Policy p, double alpha, double gamma, double lambda) {
        this.q = q;
        this.p = p;
        //this.numTerms = q.getNumTerms();
        this.alpha = alpha;
        this.gamma = gamma;
        this.lambda = lambda;
        trace = q.getNewTrace();
        secondaryWeights = new double[q.getNumTerms()];
        primaryWeights = q.getWeights();
        shrink = q.getShrink();

        errors = new ArrayList[q.getNumActions()];
        for (int i = 0; i < q.getNumActions(); i++) {
            errors[i] = new ArrayList<double[]>();
        }

        for (int i = 0; i < xFrequency; i++) {
            for (int j = 0; j < yFrequency; j++) {
                f[i][j] = new FourierBasis(new double[]{i, j});
            }
        }
    }

    public void startEpisode() {
        trace.setTrace(new double[q.getNumTerms()]);
    }

    public int nextMove(State s) {
        return p.select( s);
    }

    public void addSample(State s, int action, State nextState, double reward) {
        primaryWeights = q.getWeights();
        double trace[] = this.trace.getTrace();
        shrink = q.getShrink();
        double probabilities[] = p.getProbabilities(nextState);
        double phi_next[] = new double[q.getNumTerms()];
        for (int i = 0; i < q.getNumActions(); i++) {
            phi_next = add(phi_next, multiply(q.calculateStateValue(nextState, i), probabilities[i]));
        }
        int greedy = p.getGreedy(q, s);
        double pi = 0;
        if (greedy != action) {
            pi = 0;
        } else {
            pi = 1;
        }
        probabilities = p.getProbabilities(s);
        double rho = pi / probabilities[action];
        double phi[] = q.calculateStateValue(s, action);
        double gammaPrime;
        if (nextState.isTerminal()) {
            gammaPrime = 0;
        } else {
            gammaPrime = 1;
        }
        double delta = GQLearn(phi, phi_next, lambda, gammaPrime, 0, reward, rho, 1);
        addError(s,action, delta);
    }

    /**
     * @author Adam White Inputs:: phi - feature vector corresponding to action
     *         a_t in state s_t phi_next - expected next state feature vector
     *         corresponding to a \in A and s_t+1 lambda - elegibility trace
     *         parameter [0,1] gamma - discount factor [0,1] z - outcome reward r
     *         - transient reward rho - ratio of target policy to behaviour policy
     *         [0,1] I - set of interest for s_t, a_t [0,1]
     **/
    public double GQLearn(double[] phi, double[] phi_next, double lambda, double gamma, double z, double r, double rho, double I) {
        double eta = 1.0; // step size parameter
        double delta;
        double trace[] = this.trace.getTrace();
        delta = r + (1 - gamma) * z + gamma * dot(primaryWeights, phi_next) - dot(primaryWeights, phi);

        for (int i = 0; i < q.getNumTerms(); i++) {
            trace[i] = rho * trace[i] + I * phi[i];
        }

        for (int i = 0; i < q.getNumTerms(); i++) {
            primaryWeights[i] += (alpha / shrink[i]) * (delta * trace[i] - gamma * (1 - lambda) * dot(secondaryWeights, trace) * phi_next[i]);
            secondaryWeights[i] += (alpha / shrink[i]) * eta * (delta * trace[i] - dot(secondaryWeights, phi) * phi[i]);
            trace[i] *= gamma * lambda;
        }
        return delta;
    }

    public void addError(State s, int action, double delta) {
        actionCounter[action]++;
        double weightMag = 0;

        for (int i = q.getArrayStartPosition(action); i < q.getArrayStartPosition(action) + q.getNumTerms(action); i++) {
            weightMag += Math.abs(primaryWeights[i]);
        }
        double err[] = new double[s.getState().length + 1];
        for (int i = 0; i < s.getState().length; i++) {
            err[i] = s.getState()[i];
        }
        err[err.length - 1] = delta;
        errors[action].add(err);
        boolean added = false;
        FourierBasis best = null;
        double bestVal = 0;
        for (int i = 0; i < xFrequency; i++) {
            for (int j = 0; j < yFrequency; j++) {
                double four = f[i][j].getValue(s);
                topVals[action][i][j] = topVals[action][i][j] + four * delta;
                botVals[action][i][j] = botVals[action][i][j] + four * four;
                if (actionCounter[action] > 200) {
                    double val = Math.abs(topVals[action][i][j] / Math.sqrt(botVals[action][i][j]));
                    if ((val / weightMag) < 0.01) {
                        continue;
                    }
                    if (val > bestVal) {
                        boolean add = true;
                        FourierBasis b = new FourierBasis(new double[]{i, j});
                        for (int k = 0; k < q.getFAs()[action].getTerms().length; k++) {
                            if (q.getFAs()[action].getTerms()[k].equals(b)) {
                                add = false;
                            }
                        }
                        if (add) {
                            best = b;
                            bestVal = val;
                        }
                    }

                }
            }
        }
        if (best != null) {
            int x = (int) best.featureArr[0];
            int y = (int) best.featureArr[1];
            double val = topVals[action][x][y] / botVals[action][x][y];
            //best = new FourierBasis(new double[]{(int)(Math.random()*100), (int)(Math.random()*100)});
            best.printBasis();
            int addPos = q.getArrayStartPosition(action+1);
            //q.addBasis(best, action, 0);
            double newSecondaryWeights[] = new double[secondaryWeights.length+1];
            int pos = 0;
            for (int i = 0; i < secondaryWeights.length; i++) {
                newSecondaryWeights[pos] = secondaryWeights[i];
                pos++;
                if (pos == addPos) {
                    pos++;
                }
            }
            //secondaryWeights = newSecondaryWeights;
            //secondaryWeights = new double[q.getNumTerms()];
            //trace.setTrace(new double[trace.getTrace().length]);
            actionCounter[action] = 0;
            for (int i = 0; i < xFrequency; i++) {
                for (int j = 0; j < yFrequency; j++) {
                    topVals[action][i][j] = 0;
                    botVals[action][i][j] = 0;

                }
            }
        }
    }

    public double[] add(double arr1[], double arr2[]) {
        double returnArr[] = new double[arr1.length];
        for (int i = 0; i < arr1.length; i++) {
            returnArr[i] = arr1[i] + arr2[i];
        }
        return returnArr;
    }

    public double dot(double arr1[], double arr2[]) {
        //if (arr1.length != arr2.length) {
        //    System.out.println("Dot product error. Arrays not equal in length");
        //    return Double.NaN;
        //}
        double returnVal = 0;
        for (int i = 0; i < arr1.length; i++) {
            returnVal += arr1[i] * arr2[i];
        }
        return returnVal;
    }

    public double[] multiply(double arr[], double m) {
        double returnArr[] = new double[arr.length];
        for (int i = 0; i < arr.length; i++) {
            returnArr[i] = arr[i] * m;
        }
        return returnArr;
    }
}
