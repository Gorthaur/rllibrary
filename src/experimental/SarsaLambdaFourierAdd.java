package experimental;

import rl.Sample;
import functionapproximation.QApproximator;
import functionapproximation.FourierBasis;
import functionapproximation.MatchingPursuitFeature;
import functionapproximation.PotentialFeature;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import rl.ElegibilityTrace;
import rl.policy.Policy;
import rl.state.State;

public class SarsaLambdaFourierAdd {

    QApproximator q;
    Policy p;
    ElegibilityTrace trace;
    double weights[];
    double shrink[];
    int numTerms;
    double lambda;
    double gamma;
    double alpha;
    LinkedList<Sample> errors[];
    double oldphi[] = null;

    public SarsaLambdaFourierAdd(QApproximator q, Policy p, double alpha, double gamma, double lambda) {
        this.q = q;
        this.p = p;
        this.numTerms = q.getNumTerms();
        this.alpha = alpha;
        this.gamma = gamma;
        this.lambda = lambda;
        trace = q.getNewTrace();
        weights = q.getWeights();
        shrink = q.getShrink();
        errors = new LinkedList[q.getNumActions()];
        for (int i = 0; i < q.getNumActions(); i++) {
            errors[i] = new LinkedList<Sample>();
        }
    }

    public void startEpisode() {
        trace.setTrace(new double[q.getNumTerms()]);
        System.out.println("alpha " + alpha);
    }

    public int nextMove(State s) {
        return p.select(s);
    }

    public void addSample(State s, int action, State nextState, int nextAction, double reward) {
        weights = q.getWeights();
        double trace[] = this.trace.getTrace();
        shrink = q.getShrink();
        double stateVals[] = q.calculateStateValue(s);
        //printArray(stateVals);
        int start = q.getArrayStartPosition(action);
        int end = start + q.getNumTerms(action);
        for (int i = 0; i < q.getNumTerms(); i++) {
            trace[i] = gamma * lambda * 1 * trace[i];
            if (i >= start && i < end) {
                trace[i] += stateVals[i];
            }
        }

        double bot = Math.abs(dot(dotDivide(trace, shrink), add(multiply(q.calculateStateValue(nextState, nextAction), gamma), multiply(q.calculateStateValue(s, action), -1))));
        //double bot = Math.abs(dot(trace, add(multiply(q.calculateStateValue(nextState, nextAction), gamma), multiply(q.calculateStateValue(s, action), -1))));
        alpha = Math.min(alpha, 1 / bot);
        //System.out.println(alpha);
        double delta = reward - q.valueAt(s, action);
        if (!nextState.isTerminal()) {
            delta += gamma * q.valueAt(nextState, nextAction);
        }
        if (java.lang.Double.isNaN(delta)) {
            System.out.println("NaN trouble");
        }


        for (int i = 0; i < q.getNumTerms(); i++) {
            weights[i] += (alpha / shrink[i]) * delta * trace[i];
        }
        addError(s, action, delta, reward);
        //printArray(weights);
    }

    public void addSample(State s, int action, State nextState, int nextAction, double reward, double trace[]) {
        double delta = reward - q.valueAt(s, action);
        if (!nextState.isTerminal()) {
            delta += gamma * q.valueAt(nextState, nextAction);
        }
        if (java.lang.Double.isNaN(delta)) {
            System.out.println("NaN trouble");
        }

        double stateVals[] = q.calculateStateValue(s);
        //printArray(stateVals);
        int start = q.getArrayStartPosition(action);
        int end = start + q.getNumTerms(action);
        for (int i = 0; i < q.getNumTerms(); i++) {
            trace[i] = gamma * lambda * 1 * trace[i];
            if (i >= start && i < end) {
                trace[i] += stateVals[i];
            }
            weights[i] += (alpha / shrink[i]) * delta * trace[i];
        }
        //printArray(weights);
    }

    public void addError(State s, int action, double delta, double reward) {
        Sample sample = new Sample();
        sample.s = s;
        sample.action = action;
        sample.error = delta;
        sample.reward = reward;
        errors[action].add(sample);
        if (errors[action].size() > 100000) {
            errors[action].remove();
        }
    }

    public void clearErrors() {
        for (int i = 0; i < q.getNumActions(); i++) {
            errors[i] = new LinkedList<Sample>();
        }
    }

    public void addBFs(ArrayList<AddedBfs>[] added) {
        int maxFreq = 5;
        for (int a = 0; a < q.getNumActions(); a++) {
            if (added[a] == null) {
                added[a] = new ArrayList<AddedBfs>();
            }
            ArrayList<MatchingPursuitFeature> pot = new ArrayList<MatchingPursuitFeature>(100000);
            for (int i = 0; i <= maxFreq; i++) {
                for (int j = 0; j <= maxFreq; j++) {
                    for (int k = 0; k <= maxFreq; k++) {
                        for (int l = 0; l <= maxFreq; l++) {
                            int temp = i + j + k + l;
                            if (temp == i || temp == j || temp == k || temp == l) {
                                continue;
                            }
                            FourierBasis fb = new FourierBasis(new double[]{i, j, k, l});
                            pot.add(getMPFeature(fb, a));
                        }
                    }
                }
            }
            //Collections.shuffle(pot);
            Collections.sort(pot);
            //for (int i = 0; i < pot.size(); i++) {
            for (int i = 0; i < 10; i++) {
                //q.addBasis(pot.get(i).getBf(), a, -1*pot.get(i).getWeight());
                q.addBasis(pot.get(i).getBf(), a, 0);
                FourierBasis fb = (FourierBasis) (pot.get(i).getBf());
                boolean add = true;
                for (AddedBfs abf : added[a]) {
                    if (abf.fb.equals(fb)) {
                        abf.times++;
                        add = false;
                    }
                }
                if (add) {
                    added[a].add(new AddedBfs(fb));
                }
                //((FourierBasis)(pot.get(i).getBf())).setDecayShrink(10000.0);
            }

        }
        //alpha = alpha /10;
        //q.printFunction();

    }

    public MatchingPursuitFeature getMPFeature(FourierBasis fb, int action) {
        double top = 0;
        double bot = 0;
        for (Sample s : errors[action]) {
            double val = fb.getValue(s.s);
            top = top + (val * s.error);
            bot = bot + Math.pow(val, 2);

        }
        MatchingPursuitFeature mp = new MatchingPursuitFeature(fb);
        mp.setPotential(top / Math.sqrt(bot));
        mp.setWeight(top / bot);
        return mp;
    }

    /*
    public void saveErrors(String filename) {
    for (int i = 0; i < errors.length; i++) {
    String fname = filename + i + ".dat";
    String out = "";
    for (double[] d : errors[i]) {
    out += getArrayString(d) + "\r\n";
    }
    FileWriter f = null;
    try {
    f = new FileWriter(fname, false);
    } catch (IOException ex) {
    Logger.getLogger(SarsaLambdaTester.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    BufferedWriter writer = new BufferedWriter(f);
    try {
    writer.write(out);
    } catch (IOException ex) {
    Logger.getLogger(SarsaLambdaTester.class.getName()).log(Level.SEVERE, null, ex);
    }
    try {
    writer.close();
    } catch (IOException ex) {
    Logger.getLogger(SarsaLambdaTester.class.getName()).log(Level.SEVERE, null, ex);
    }
    }
    }
    
     * 
     */
    public double[] add(double arr1[], double arr2[]) {
        double returnArr[] = new double[arr1.length];
        for (int i = 0; i < arr1.length; i++) {
            returnArr[i] = arr1[i] + arr2[i];
        }
        return returnArr;
    }

    public void decayAlpha(double amount) {
        alpha = alpha * amount;
    }

    public double dot(double arr1[], double arr2[]) {
        if (arr1.length != arr2.length) {
            System.out.println("Dot product error. Arrays not equal in length");
            return Double.NaN;
        }
        double returnVal = 0;
        for (int i = 0; i < arr1.length; i++) {
            returnVal += arr1[i] * arr2[i];
        }
        return returnVal;
    }

    //pairwise multiple elements in arrays and return the resulting array
    public double[] dotMultiply(double arr1[], double arr2[]) {

        if (arr1.length != arr2.length) {
            System.out.println("Dot product error. Arrays not equal in length");
            return null;
        }
        double ret[] = new double[arr1.length];
        for (int i = 0; i < arr1.length; i++) {
            ret[i] = arr1[i] * arr2[i];
        }
        return ret;
    }

    //pairwise multiple elements in arrays and return the resulting array
    public double[] dotDivide(double arr1[], double arr2[]) {

        if (arr1.length != arr2.length) {
            System.out.println("Dot product error. Arrays not equal in length");
            return null;
        }
        double ret[] = new double[arr1.length];
        for (int i = 0; i < arr1.length; i++) {
            ret[i] = arr1[i] / arr2[i];
        }
        return ret;
    }

    public double[] multiply(double arr[], double m) {
        double returnArr[] = new double[arr.length];
        for (int i = 0; i < arr.length; i++) {
            returnArr[i] = arr[i] * m;
        }
        return returnArr;
    }

    public void printArray(double arr[]) {
        String str = "";
        for (int i = 0; i < arr.length; i++) {
            str = str + arr[i] + " ";
        }
        System.out.println(str);
    }

    public String getArrayString(double arr[]) {
        String str = "";
        for (int i = 0; i < arr.length; i++) {
            str = str + arr[i] + " ";
        }
        return str;
    }

    public class AddedBfs implements Comparable<AddedBfs> {

        FourierBasis fb;
        int times = 0;

        public AddedBfs(FourierBasis fb) {
            this.fb = fb;
            times++;
        }
        
        public FourierBasis getBasis() {
            return fb;
        }
        
        public int getTimes() {
            return times;
        }

        @Override
        public int compareTo(AddedBfs o) {
            if (this.times > o.times) {
                return -1;
            } else if (this.times < o.times) {
                return 1;
            } else {
                return 0;
            }
        }
    }
}
