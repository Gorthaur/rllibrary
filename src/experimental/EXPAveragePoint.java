package experimental;


/**
 *
 * @author dean
 */
public class EXPAveragePoint {
    double centre[];
    double weight = 0;
    double alpha = 1.0/20;
   public EXPAveragePoint(double pos[]) {
       this.centre = pos;
   }

   public void addPoint(double pos[], double weight) {
       this.weight = this.weight + alpha*getKernel(pos)*(weight - this.weight);
   }

       public double getDistance(double point[]) {
        double result = 2 - 2*getKernel(point);
        result = Math.sqrt(result);
        return result;
    }

    public double getKernel(double point[]) {
        double distance = 0;
        double maxDistance = Math.sqrt(centre.length);
        double squaredVariance = 1.0 / 225;
        for (int i = 0; i < centre.length; i++) {
            distance += Math.pow(centre[i] - point[i], 2);
        }

        return Math.exp(-1*(distance / squaredVariance));
    }
}
