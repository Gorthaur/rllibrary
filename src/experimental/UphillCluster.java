package experimental;


/**
 *
 * @author dean
 */
public class UphillCluster extends Cluster {

    double alpha = 1.0 / 20;
    double averagePosition[];
    double averagePositionVariance[];

    public UphillCluster(int degree) {
        super(degree);
    }

    public UphillCluster(double centre[], double weight) {
        super(centre, weight);
        sumWeight = 0;
        this.centre = centre;
        this.meanWeight = 0;
        this.averagePosition = centre.clone();
    }

    @Override
    public double getDistance(double point[], double weight) {
        double result = 2 - 2 * getKernel(point);
        result = Math.sqrt(result);
        return result;
    }

    public double getKernel(double point[]) {
        double distance = 0;
        double maxDistance = Math.sqrt(centre.length);
        double squaredVariance = 1.0 / 225;
        for (int i = 0; i < centre.length; i++) {
            distance += Math.pow(centre[i] - point[i], 2);
        }

        return Math.exp(-1 * (distance / squaredVariance));
    }

    public void move(double point[], double weight) {
        double oldWeight = this.meanWeight;
        this.meanWeight = this.meanWeight + alpha * getKernel(point) * (weight - this.meanWeight);
        this.sumWeight = this.sumWeight + alpha*getKernel(point) * (1 - sumWeight);

        if (this.meanWeight > oldWeight) {
            double moveScale = (this.meanWeight - oldWeight) / (this.meanWeight + oldWeight);
            moveScale = Math.abs(moveScale);
            double distanceToB = getDistance(point);
            moveDistance(point, distanceToB, distanceToB * moveScale);
            for (int i = 0; i < centre.length; i++) {
                averagePosition[i] = averagePosition[i] + alpha*(centre[i] - averagePosition[i]);
            }
        }

    }

    public void merge(Cluster b) {
        double oldWeight = this.meanWeight;
        this.meanWeight = (this.sumWeight*this.meanWeight + b.sumWeight*b.meanWeight)/(b.sumWeight + this.sumWeight);
        this.sumWeight = Math.max(this.sumWeight, b.sumWeight);
        if (this.meanWeight > oldWeight) {
            double moveScale = (this.meanWeight - oldWeight) / (this.meanWeight + oldWeight);
            moveScale = Math.abs(moveScale);
            double distanceToB = getDistance(b.getCentre());
            moveDistance(b.getCentre(), distanceToB, distanceToB * moveScale);
        }
        
    }
}
