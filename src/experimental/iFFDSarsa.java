package experimental;

import functionapproximation.TileBasis;
import functionapproximation.iFFDBasis;
import functionapproximation.iFFDQApproximator;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Vector;
import rl.policy.Policy;
import functionapproximation.PotentialFeature;
import rl.policy.Policy;
import rl.learningalgorithms.SarsaLambda;
import rl.learningalgorithms.SarsaLambda;
import rl.state.State;
import rl.state.State;

/**
 *
 * @author dean
 */
public class iFFDSarsa extends SarsaLambda {

    //Vector<PotentialFeature> potentials2;
    //Vector<PotentialFeature> added2;
    //HashSet<PotentialFeature> potentials;
    HashSet<PotentialFeature> added;
    HashMap<PotentialFeature, PotentialFeature> potentials;
    double eta;

    public iFFDSarsa(iFFDQApproximator q, Policy p, double alpha, double gamma, double lambda, double eta) {
        super(q, p, alpha, gamma, lambda);
        //potentials2 = new Vector<PotentialFeature>();
        //added2 = new Vector<PotentialFeature>();
        potentials = new HashMap<PotentialFeature, PotentialFeature>(100000);
        added = new HashSet<PotentialFeature>(100000);
        this.eta = eta;
    }

    @Override
    public void addSample(State s, int action, State nextState, int nextAction, double reward) {
        weights = q.getWeights();
        double trace[] = this.trace.getTrace();
        shrink = q.getShrink();
        double delta = reward - q.valueAt(s, action);
        if (!nextState.isTerminal()) {
            delta += gamma * q.valueAt(nextState, nextAction);
        }
        if (java.lang.Double.isNaN(delta)) {
            System.out.println("NaN trouble");
        }
        double stateVals[] = q.calculateStateValue(s);
        Vector<iFFDBasis> activeBFs = ((iFFDQApproximator) q).getActiveBasisFunctions(s);
        updatePotentials(activeBFs, delta);
        //printArray(stateVals);
        int start = q.getArrayStartPosition(action);
        int end = start + q.getNumTerms(action);
        for (int i = 0; i < q.getNumTerms(); i++) {
            trace[i] = gamma * lambda * 1 * trace[i];
            if (i >= start && i < end) {
                trace[i] += stateVals[i];
            }
            weights[i] += (alpha / shrink[i]) * delta * trace[i];
        }
        addBasisFunctions();
    }

    public void updatePotentials(Vector<iFFDBasis> active, double delta) {
        delta = Math.abs(delta);
        for (int i = 0; i < active.size() - 1; i++) {
            for (int j = i + 1; j < active.size(); j++) {
                iFFDBasis combine = active.get(i).combine(active.get(j));
                if (combine != null) {
                    PotentialFeature p = new PotentialFeature(combine, active.get(i), active.get(j));
                    if (!added.contains(p)) {

                        if (potentials.containsKey(p)) {
                        } else {
                            //potentials.add(p,p);
                            potentials.put(p, p);
                            //potentials2.add(p);
                        }
                        //potentials2.get(potentials2.lastIndexOf(p)).incrementPotential(delta);
                        potentials.get(p).incrementPotential(delta);
                    }
                }
            }
        }
    }

    public void addBasisFunctions() {
        int size = potentials.size();
        Collection<PotentialFeature> pts = potentials.values();
        boolean addedP = false;
        for (PotentialFeature p : pts) {
            if (p.getPotential() > eta) {
                addedP = true;
                //printArray(this.trace.getTrace());
                added.add(p);
                System.out.println(added.size());
                ((TileBasis) p.getBf()).print();
                ((iFFDQApproximator) q).addBasis(p);
                potentials.remove(p);
                weights = q.getWeights();
                //printArray(this.trace.getTrace());
                break;
            }
        }
        if (addedP) {
            for (PotentialFeature p : pts) {
                p.setPotential(0);
            }
        }
    }
}
