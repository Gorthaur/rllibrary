package experimental;

import rl.Sample;
import functionapproximation.CandidateFeature;
import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingDeque;

/**
 *
 * @author Dean
 */
public class PotentialUpdater extends Thread {
    
    LinkedBlockingDeque<Sample> samples;
    double potentialThreshold = 0;
    LinkedBlockingDeque<CandidateFeature> newCandidates;
    LinkedBlockingDeque<CandidateFeature> addCandidates;
    ArrayList<CandidateFeature> currentCandidates;
    
    public PotentialUpdater(LinkedBlockingDeque<CandidateFeature> newCandidates, LinkedBlockingDeque<CandidateFeature> addCandidates, double potentialThreshold) {
        this.newCandidates = newCandidates;
        this.addCandidates = addCandidates;
        this.potentialThreshold = potentialThreshold;
        currentCandidates = new ArrayList<CandidateFeature>();
    }
    
    public void run() {
        while (true) {
            while(newCandidates.size() > 0) {
                currentCandidates.add(newCandidates.poll());
            }
            if (samples.size() != 0) {
                Sample samp = samples.poll();
                ArrayList<CandidateFeature> removeLater = new ArrayList<CandidateFeature>();
                for (CandidateFeature f: currentCandidates) {
                    f.decayPotential();
                    f.addPotential(f.getCombined().getValue(samp.s)*samp.error);
                    if (f.getPotential() >= potentialThreshold) {
                        addCandidates.offer(f);
                        removeLater.add(f);
                    }
                }
                for (CandidateFeature i: removeLater) {
                    currentCandidates.remove(i);
                }
            }
        }
    }
    
}
