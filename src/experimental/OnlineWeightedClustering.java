package experimental;


import java.util.ArrayList;

/**
 *
 * @author dean
 */
public class OnlineWeightedClustering {

    int maxClusters;
    ArrayList<Cluster> centres;
    int numPoints = 0;
    double averageWeight = 0;

    public OnlineWeightedClustering(int maxClusters) {
        this.maxClusters = maxClusters;
        centres = new ArrayList<Cluster>();
        for (int i = 0; i < maxClusters; i++) {
            centres.add(new UphillCluster(new double[]{Math.random(), Math.random()}, 0));
        }
    }

    public void addDataPoint(double datalocation[], double weight) {
        numPoints++;
        if (centres.size() != 0) {
            moveClosestCluster(datalocation, weight);
            //if (centres.size() == maxClusters) {
            //    mergeClosestClusters();
            //}
        }
        if (centres.size() != maxClusters) {
            centres.add(new UphillCluster(datalocation, weight));
        }
    }

    public void moveClosestCluster(double datalocation[], double weight) {
        double distance = 0;
        Cluster closest = null;
        for (Cluster c : centres) {
            double dist = c.getDistance(datalocation, weight);
            if (dist < distance || closest == null) {
                closest = c;
                distance = dist;
            }
        }


        closest.move(datalocation, weight);
    }

    public void mergeClosestClusters() {
        double distance = 0;
        Cluster a = null, b = null;
        for (int i = 0; i < centres.size() - 1; i++) {
            for (int j = i + 1; j < centres.size(); j++) {
                double dist = centres.get(i).getDistance(centres.get(j).getCentre(), centres.get(j).meanWeight);
                if (distance > dist || a == null) {
                    distance = dist;
                    a = centres.get(i);
                    b = centres.get(j);
                }
            }
        }
        if (a != null && b != null) {
            if (a.sumWeight > b.sumWeight) {
                a.merge(b);
                centres.remove(b);
            } else {
                b.merge(a);
                centres.remove(a);
            }
        }
    }
}
