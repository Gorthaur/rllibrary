package experimental;

/**
 *
 * @author dean
 */
public class KernelCluster extends Cluster {

    double averageError = 0;
    double errorWeight = 0;

    public KernelCluster(int degree) {
        super(degree);
    }
    public KernelCluster(double centre[], double weight) {
        super(centre, weight);
        sumWeight = 1;
        this.centre = centre;
        this.meanWeight = 0;
    }

    @Override
    public double getDistance(double point[], double weight) {
        double result = 2 - 2*getKernel(point, weight);
        result = Math.sqrt(result);
        return result;
    }

    public double getKernel(double point[], double pointWeight) {
        double distance = 0;
        double maxDistance = Math.sqrt(centre.length);
        double squaredVariance = maxDistance / 2;
        for (int i = 0; i < centre.length; i++) {
            distance += Math.pow(centre[i] - point[i], 2);
        }
        double distanceScale = (pointWeight - meanWeight) / (pointWeight + meanWeight) + 1;
        distance = (1.0/distanceScale)*distance;

        return Math.exp(-1*(distance / squaredVariance));
    }

    @Override
    public void move(double point[], double weight) {
        double oldError = averageError;
        averageError = averageError + (1.0/20)*(weight*getKernel(point, weight) - averageError*getKernel(point, weight));
        errorWeight = errorWeight + (1.0/20)*(1*getKernel(point, weight) - errorWeight*getKernel(point, weight));
        double weightChange = Math.abs((oldError - averageError) / (oldError - weight));
        if (weightChange > 1) {
            System.out.println("woah");
        }


        sumWeight = sumWeight - sumWeight/20;
        sumWeight += weight*getKernel(point, weight);

        for (int i = 0; i < point.length; i++) {
            double temp = centre[i] + (point[i] - centre[i])*weightChange;
            //double temp = centre[i] + (point[i] - centre[i])/sumWeight;
            if (temp > 1) {
                System.out.println(point[i] + " " + centre[i] + " " + sumWeight + " " + getKernel(point, weight)+ " " + weight);
            }
            centre[i] = temp;
        }
        //meanWeight = meanWeight + (weight - meanWeight)/sumWeight;
        sumWeight = errorWeight;
        meanWeight = averageError;
    }

    @Override
    public void merge(Cluster b) {
        double oldError = averageError;
        double totalWeight = errorWeight + ((KernelCluster)b).errorWeight;
        averageError = (errorWeight*averageError + ((KernelCluster)b).errorWeight*((KernelCluster)b).averageError)/totalWeight;
        
        double weightChange = Math.abs((oldError - averageError) / (oldError - ((KernelCluster)b).averageError));
        weightChange = 0;
        //errorWeight = totalWeight / 2;
        errorWeight = totalWeight;
        if (weightChange > 1) {
            System.out.println("woah2");
        }
        for (int i = 0; i < centre.length; i++) {
            centre[i] = centre[i] + (centre[i] - b.centre[i])*weightChange;
        }

        sumWeight = errorWeight;
        meanWeight = averageError;
        /*
        double totalWeight = sumWeight + b.sumWeight;
        meanWeight = (meanWeight*sumWeight + b.meanWeight*b.sumWeight) / totalWeight;
        for (int i = 0; i < centre.length; i++) {
            centre[i] = (centre[i]*sumWeight + b.centre[i]*b.sumWeight)/totalWeight;
        }
        sumWeight = totalWeight / 2;
         * (
         */
    }
}
