package experimental;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import rl.state.State;


public class Robot {
  String execString[];
  Process robot;
  BufferedReader reader;
  BufferedWriter writer;
  State lastState = null;
  boolean seenMiddle = false;
  
  public Robot(String execString[]) {
    this.execString = execString;
  }
  
  public void start() {
    try {
      robot = Runtime.getRuntime().exec(execString);
      reader = new BufferedReader(new InputStreamReader(robot.getInputStream()));
      writer = new BufferedWriter(new OutputStreamWriter(robot.getOutputStream()));
    }
    catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  public void stop() {
    robot.destroy();
  }
  
  public State readState() {
    try {
      String line = reader.readLine();
      String split[] = line.split(" ");
      double vals[] = new double[split.length + 1];
      for (int i = 0; i < split.length; i++) {
        vals[i] = Double.parseDouble(split[i]);
      }
      if (seenMiddle) {
        vals[split.length] = 1;
      }
      else {
        vals[split.length] = 0;
      }
      return new State(vals);
    }
    catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return null;
  }
  
  public State getState() {
    return lastState;
  }
  
  public void takeAction(int action) {
    try {
      writer.write(action + "\n");
    }
    catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }
  
  public double step(int action) {
    takeAction(action);
    lastState = readState();
    double reward = 0;
    if (lastState.getState()[0] == 1.0 && seenMiddle) {
      reward = 10;
      lastState.setTerminal(true);
    }
    else if (lastState.getState()[0] == 1.0 && !seenMiddle) {
      reward = -100;
      lastState.setTerminal(true);
    }
    else if (lastState.getState()[0] > 0.3 && lastState.getState()[0] < 0.4) {
      if (!seenMiddle) {
        seenMiddle = true;
        reward = 1;
      }
      reward = -0.01;
    }
    else {
      reward = -0.01;
    }
    return reward;
  }
  
 
}
