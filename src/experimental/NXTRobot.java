package experimental;

import functionapproximation.QApproximator;
import functionapproximation.FunctionApproximator;
import functionapproximation.FullFourier;
import java.io.IOException;
import rl.policy.EpsilonGreedy;
import rl.policy.Policy;
import rl.learningalgorithms.SarsaLambda;
import rl.state.State;

public class NXTRobot {


  public static void main(String args[]) {
    String execString[] = new String[] { "wits_robot7.exe" };
    int maxSteps = 10000; // max steps in mountain car episode before calling it
    int numEpisodes = 40;
    int numExperiments = 40;

    int fourierOrder = 10;
    int numFeatures = 3; // num state variables
    int numActions = 3;
    
    Robot roboguy = new Robot(execString);


    double alpha = 0.0075; // learning rate
    double gamma = 1; // discount (1 = no discount)
    double lambda = 0.95; // eligibility trace

    double averages[] = new double[numEpisodes];
    for (int i = 0; i < numExperiments; i++) {
      System.out.println("Start Experiment " + (i + 1));
      FunctionApproximator FAs[] = new FunctionApproximator[numActions];

      for (int j = 0; j < numActions; j++) {
        FAs[j] = new FullFourier(fourierOrder, numFeatures);
      }
      QApproximator q = new QApproximator(FAs);
          Policy p = new EpsilonGreedy(null, q, 0.01);
      SarsaLambda learner = new SarsaLambda(q, p, alpha, gamma, lambda);

      // start an episode
      for (int k = 0; k < numEpisodes; k++) {
        roboguy.start();
        System.out.print(".");
        learner.startEpisode();
        int steps = 0;
        State currState = roboguy.getState();
        int move = learner.nextMove(currState);
        while (steps < maxSteps && !currState.isTerminal()) {
          double reward = roboguy.step(move);
          State newState = roboguy.getState();

          int nextMove = learner.nextMove(newState);
          learner.addSample(currState, move, newState, nextMove, reward);
          currState = newState;
          move = nextMove;
          steps++;
        }
        averages[k] += steps;
        roboguy.stop();
      }
    }
    String output = "Episode,Steps\n";
    for (int i = 0; i < numExperiments; i++) {
      averages[i] = averages[i] / numExperiments;
      System.out.println("Episode " + i + " took " + averages[i] + " steps on average");
      output += (i + 1) + "," + averages[i] + "\n";
    }
  }



}
