package experimental;


/**
 *
 * @author dean
 */
public class GradientAccentClustering {

    double centre[];
    double weight;
    double direction[];

    public GradientAccentClustering(double centre[], double weight) {
        this.centre = centre;
        this.weight = weight;
        this.direction = new double[centre.length];
    }

    public void move(double point[], double weight) {
        double newDirection[] = new double[point.length];
        for (int i = 0; i < point.length; i++) {
            newDirection[i] = point[i] - centre[i];
        }
        double totalWeight = weight + this.weight;

    }
}
