package experimental;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Dean
 */
public class Cluster {

    double centre[];
    double weight = 0;
    double stability = 0;
    int numPoints = 0;
    double meanWeight = 0;
    double S = 0; //no idea what this is. Used in variance calculation.
    double sumWeight = 25;

    double simpleVariance = 0;

    double averager = 50;

    public Cluster(int degree) {
        centre = new double[degree];
        for (int i = 0; i < degree; i++) {
            centre[i] = Math.random();
        }
    }

    public Cluster(double centre[], double weight) {
        this.centre = new double[centre.length];
        move(centre, weight);
        this.meanWeight = weight;
    }

    public double getDistance(double point[]) {
        double distance = 0;
        for (int i = 0; i < centre.length; i++) {
            distance += Math.pow(point[i] - centre[i], 2);
        }
        return Math.sqrt(distance);
    }

    public double getDistance(double point[], double weight) {
        return getDistance(point);
    }

    public void move(double point[], double weight) {
        //http://en.wikipedia.org/wiki/Algorithms_for_calculating_variance


        //Linearly scale the weight according to distance.
        //0 distance = full weight. Max distance = 0 weight.
        double maxDistance = Math.sqrt(point.length) / 2;
        double distance = getDistance(point);
        double weightMagnitudeScale = 2*((weight - meanWeight) / (meanWeight+weight)) + 1;
        //weightMagnitudeScale = Math.pow(weightMagnitudeScale,4);
        distance = distance * (1/weightMagnitudeScale);

        double weightDistanceScale = (maxDistance - distance) / maxDistance;


        
        weightDistanceScale = Math.exp(-1*(Math.pow(distance,2)/(2*maxDistance)));
        if (weightDistanceScale < 0) {
            weightDistanceScale = 0;
        }
        /* Possibly working stuff
        //weightDistanceScale = 1;
        weightDistanceScale = Math.min(1, weightDistanceScale + weightMagnitudeScale);
        weightDistanceScale = Math.max(0, weightDistanceScale);
        //weightDistanceScale = weightMagnitudeScale;
         *
         * 
         */

        double oldMean = meanWeight;
        numPoints++;
        double temp = sumWeight + weightDistanceScale;
        if (temp < 0) {
            System.out.println("wtc");
        }
        double Q = weight - meanWeight;
        double R = 0;
        if (temp < 0.0001) {
            R = Q;
        } else {
            R = Q * weightDistanceScale / temp;
        }
        S = S + sumWeight * Q * R;
        meanWeight = meanWeight + R;
        //sumWeight = temp;  //unmodified version
        sumWeight = temp - (1.0/averager)*sumWeight*weightDistanceScale;
        double moveScale = (meanWeight - oldMean) / (meanWeight + oldMean);
        moveScale = Math.abs(moveScale);
        moveDistance(point, distance, distance * moveScale);
        simpleVariance = simpleVariance + moveScale*(Math.pow(distance,2) - simpleVariance);


        /*
        //System.out.println(this.weight + ": " + centre[0] + " " + centre[1]);
        double distance = getDistance(point);
        if (weight < 0.001) {
        return;
        }
        if (distance < 0.0001) {
        this.weight = (this.weight + weight) / 2;
        return;
        }
        double moveDist = 0.01 * (weight / (weight + this.weight)) * distance;
        double newWeight = (this.weight * (distance - moveDist) + weight * moveDist) / distance; //average weight according to distance moved

        double weightDiff = Math.abs(this.weight - newWeight);
        double stab = moveDist + weightDiff;
        stability = stab;
        double movevector[] = new double[point.length];
        for (int i = 0; i < point.length; i++) {
        movevector[i] = ((point[i] - centre[i]) / distance) * moveDist;
        centre[i] = centre[i] + movevector[i];
        }
        this.weight = newWeight;
        //System.out.println(weight + ": " + point[0] + " " + point[1]);
        //System.out.println(this.weight + ": " + centre[0] + " " + centre[1]);
        //System.out.println();
         * */

    }

    /**
     * Move the center towards point, where distance is the distance to that point, and moveDistance is the amount to move towards it.
     * @param point The point to move towards.
     * @param distance The distance to the point it's moving towards.
     * @param moveDistance The distance it must move.
     */
    public void moveDistance(double point[], double distance, double moveDistance) {
        //move the centre in the direction of point, distance units
        for (int i = 0; i < point.length; i++) {
            centre[i] = centre[i] + ((point[i] - centre[i]) / distance) * moveDistance;
        }
    }

    public double[] getCentre() {
        return this.centre;
    }

    public double getWeight() {
        return weight;
    }

    public double getVariance() {
        if (numPoints == 1) {
            return S / sumWeight;
        } else {
            return (S * numPoints) / ((numPoints - 1) * sumWeight);
        }
    }

    public void merge(Cluster b) {
        //^ Chan, Tony F.; Golub, Gene H.; LeVeque, Randall J. (1979), "Updating Formulae and a Pairwise Algorithm for Computing Sample Variances.", Technical Report STAN-CS-79-773, Department of Computer Science, Stanford University, ftp://reports.stanford.edu/pub/cstr/reports/cs/tr/79/773/CS-TR-79-773.pdf .
        //combining 2 means and 2 variances together.
        //modified to work with weighted means.


        double maxDistance = Math.sqrt(b.centre.length)/2;
        double distance = getDistance(b.getCentre());
        double weightMagnitudeScale = 2*((b.meanWeight - meanWeight) / (meanWeight+b.meanWeight)) + 1;
        //weightMagnitudeScale = Math.pow(weightMagnitudeScale,4);
        distance = distance * (1/weightMagnitudeScale);

        double weightDistanceScale = (maxDistance - distance) / maxDistance;


        weightDistanceScale = Math.exp(-1*(Math.pow(distance,2)/(2*maxDistance)));
        if (weightDistanceScale < 0) {
            weightDistanceScale = 0;
        }

        double oldMean = this.meanWeight;

        double delta = b.meanWeight - this.meanWeight;
        double totalWeight = this.sumWeight + b.sumWeight;
        double newMean = 0;

        //when the weights of both are large and almost equal, do a different update.
        //http://en.wikipedia.org/wiki/Algorithms_for_calculating_variance paralell algorithm section.
        if (this.sumWeight > 10000 && Math.abs(Math.abs(this.sumWeight) - Math.abs(b.sumWeight)) < 1) {
            newMean = (this.meanWeight * this.sumWeight + b.meanWeight * b.sumWeight) / totalWeight;
        } else {
            newMean = this.meanWeight + delta * (b.sumWeight / totalWeight);
        }
        double thisVariance = getVariance();
        double otherVariance = b.getVariance();
        double newVariance = thisVariance + otherVariance + Math.pow(delta, 2) * ((this.sumWeight * b.sumWeight) / totalWeight);
        int totalPoints = this.numPoints + b.numPoints;
        double newS = (newVariance * (totalPoints -1) * totalWeight) / totalPoints;

        this.meanWeight = meanWeight + (newMean-meanWeight)*weightDistanceScale;
        this.S = newS;
        this.numPoints = totalPoints;
        //this.sumWeight = totalWeight;

        //move the cluster
        double moveScale = (meanWeight - oldMean) / (meanWeight + oldMean);
        moveScale = Math.abs(moveScale);
        double distanceToB = getDistance(b.getCentre());
        moveDistance(b.getCentre(), distanceToB, distanceToB * moveScale);
        this.simpleVariance = (1-moveScale)*this.simpleVariance + (moveScale)*b.simpleVariance;
        //System.out.print(this.sumWeight + " ");
        //this.sumWeight = (1-moveScale)*this.sumWeight + (moveScale)*b.sumWeight;
        //this.sumWeight = Math.max(this.sumWeight, b.sumWeight);
        //System.out.println(this.sumWeight);
        this.sumWeight = totalWeight;
        if (totalWeight > averager) {
            sumWeight = averager;
        }
    }
}
