package experimental;

import functionapproximation.BasisFunction;
import functionapproximation.CandidateFeature;
import functionapproximation.CombinationBasis;
import functionapproximation.QApproximator;
import functionapproximation.WiFDDBasisFunction;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import rl.policy.Policy;
import rl.learningalgorithms.SarsaLambda;
import rl.state.State;

/**
 *
 * @author Dean
 */
public class SarsaWiFDD extends SarsaLambda {

    HashMap<WiFDDBasisFunction, WiFDDBasisFunction> addedBFs[];
    HashMap<WiFDDBasisFunction, CandidateFeature> candidateBFs;
    ArrayList<CandidateFeature> candidateUpdates;
    double potentialThreshold;

    public SarsaWiFDD(QApproximator q, Policy p, double alpha, double gamma, double lambda, double potentialThreshold) {
        super(q, p, alpha, gamma, lambda);
        addedBFs = new HashMap[q.getNumActions()];
        for (int i = 0; i < q.getNumActions(); i++) {
            addedBFs[i] = new HashMap<WiFDDBasisFunction, WiFDDBasisFunction>(2000);
        }
        candidateBFs = new HashMap<WiFDDBasisFunction, CandidateFeature>(100000);
        candidateUpdates = new ArrayList<CandidateFeature>(100000);
        this.potentialThreshold = potentialThreshold;
        this.addAllCandidates();
    }

    public void addSample(State s, int action, State nextState, int nextAction, double reward) {
        weights = q.getWeights();
        double trace[] = this.trace.getTrace();
        shrink = q.getShrink();
        double stateVals[] = q.calculateStateValue(s);
        //printArray(stateVals);
        int start = q.getArrayStartPosition(action);
        int end = start + q.getNumTerms(action);
        for (int i = 0; i < q.getNumTerms(); i++) {
            trace[i] = gamma * lambda * 1 * trace[i];
            if (i >= start && i < end) {
                trace[i] += stateVals[i];
            }
        }


        double bot = Math.abs(dot(trace, add(multiply(q.calculateStateValue(nextState, nextAction), gamma), multiply(q.calculateStateValue(s, action), -1))));
        alpha = Math.min(alpha, 1 / bot);
        //System.out.println(alpha);
        double delta = reward - q.valueAt(s, action);
        if (!nextState.isTerminal()) {
            delta += gamma * q.valueAt(nextState, nextAction);
        }
        if (java.lang.Double.isNaN(delta)) {
            System.out.println("NaN trouble");
        }


        for (int i = 0; i < q.getNumTerms(); i++) {
            weights[i] += (alpha / shrink[i]) * delta * trace[i];
        }
        addError(s, action, delta);
        //printArray(weights);
    }

    public void addError(State s, int action, double delta) {
        //System.out.println("Value Before " + q.valueAt(s, action));

        ArrayList<CandidateFeature> add = new ArrayList<CandidateFeature>(10);
        for (CandidateFeature c : candidateUpdates) {
            c.addPotential(Math.abs(c.getCombined().getValue(s) * delta));
            c.decayPotential();
            //System.out.println("Value After " + q.valueAt(s, action));
            if (c.getPotential() >= potentialThreshold) {
                add.add(c);

            }
        }


        for (CandidateFeature c : add) {
            //System.out.println("added");
            //System.out.println(c.getCombined().getBasisString());
            for (int i = 0; i < q.getNumActions(); i++) {
                WiFDDBasisFunction bf1 = addedBFs[i].get(c.getCombined().getBF1());
                WiFDDBasisFunction bf2 = addedBFs[i].get(c.getCombined().getBF2());
                CombinationBasis b = new CombinationBasis(bf1, bf2);
                for (WiFDDBasisFunction parent: c.getCombined().getParents()) {
                    b.addParent(addedBFs[i].get(parent));
                }
                q.addBasis(b, i);
                double weights[] = q.getWeights();
                double newWeight = weights[b.getBF1().getArrayIndex()] + weights[b.getBF2().getArrayIndex()]; //get its parent weights
                weights[b.getArrayIndex()] = newWeight;
                addedBFs[i].put(b, b);
            }

            //q.printFunction();
            addCandidates(c.getCombined());
            System.out.println("Hashmap size: " + candidateBFs.size());
            System.out.println("Candidate updates:" + candidateUpdates.size());
            candidateUpdates.remove(c);
            candidateBFs.remove(c.getCombined());
        }
        //System.out.println("Value After " + q.valueAt(s, action));

    }

    public void addCandidates(WiFDDBasisFunction c) {
        BasisFunction terms[] = q.getFAs()[0].getTerms();
        ArrayList<Integer> cdims = c.getDimensions();

        outer:
        for (BasisFunction bf : terms) {
            WiFDDBasisFunction t = (WiFDDBasisFunction) bf;
            ArrayList<Integer> dims = t.getDimensions();
            for (int k : cdims) {
                if (dims.contains(k)) {
                    continue outer;
                }

            }
            CombinationBasis cb = new CombinationBasis(c, t);
            if (candidateBFs.containsKey(cb)) {
                CombinationBasis cand = candidateBFs.get(cb).getCombined();
                cand.updateParents(c, t);
                continue; //we have this one already
            }
            if (addedBFs[0].containsKey(cb)) {
                for (int i = 0; i < q.getNumActions(); i++) {
                    WiFDDBasisFunction bf1 = addedBFs[i].get(c);
                    WiFDDBasisFunction bf2 = addedBFs[i].get(t);
                    ((CombinationBasis) addedBFs[i].get(cb)).addParents(bf1, bf2);
                }
                continue; //we're actually learning with this one
            }
            //System.out.println(cb.getBasisString());
            //System.out.println(cb.getDimensions().size());
            CandidateFeature cf = new CandidateFeature(cb);
            candidateBFs.put(cb, cf);
            candidateUpdates.add(cf);
        }

    }

    public void addAllCandidates() {
        for (int i = 0; i < q.getNumActions(); i++) {
            BasisFunction terms[] = (q.getFAs()[i].getTerms());
            for (BasisFunction wdd : terms) {
                addedBFs[i].put((WiFDDBasisFunction) wdd, (WiFDDBasisFunction) wdd);
            }
        }
        BasisFunction terms[] = (q.getFAs()[0].getTerms());
        for (BasisFunction wdd : terms) {
            addCandidates((WiFDDBasisFunction) wdd);
        }
        System.out.println("Hashmap size: " + candidateBFs.size());
        System.out.println("Candidate updates:" + candidateUpdates.size());
    }
}
