package experimental;

import functionapproximation.QApproximator;
import functionapproximation.FourierBasis;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import rl.ElegibilityTrace;
import rl.policy.Policy;
import rl.state.State;

public class SarsaLambdaTester3 {

    QApproximator q;
    Policy p;
    ElegibilityTrace trace;
    double weights[];
    double shrink[];
    int numTerms;
    double lambda;
    double gamma;
    double alpha;
    ArrayList<double[]> errors[];
    ArrayList<FourierBasis> candidateFunctions[] = new ArrayList[4];
    ArrayList<Double>[] topValsA = new ArrayList[4];
    ArrayList<Double>[] botValsA = new ArrayList[4];
    int actionCounter[] = new int[3];
    int numActions = 3;
    double oldphi[] = null;

    public SarsaLambdaTester3(QApproximator q, Policy p, double alpha, double gamma, double lambda) {
        this.q = q;
        this.p = p;
        this.numTerms = q.getNumTerms();
        this.alpha = alpha;
        this.gamma = gamma;
        this.lambda = lambda;
        trace = q.getNewTrace();
        weights = q.getWeights();
        shrink = q.getShrink();
        errors = new ArrayList[q.getNumActions()];
        for (int i = 0; i < q.getNumActions(); i++) {
            errors[i] = new ArrayList<double[]>();
            candidateFunctions[i] = new ArrayList<FourierBasis>();
            topValsA[i] = new ArrayList<Double>();
            botValsA[i] = new ArrayList<Double>();
        }
        addInitialCandidates();

    }

    public void startEpisode() {
        trace.setTrace(new double[q.getNumTerms()]);
    }

    public int nextMove(State s) {
        return p.select(s);
    }

    public void addSample(State s, int action, State nextState, int nextAction, double reward) {
        weights = q.getWeights();
        double trace[] = this.trace.getTrace();
        shrink = q.getShrink();
        double stateVals[] = q.calculateStateValue(s);
        //printArray(stateVals);
        int start = q.getArrayStartPosition(action);
        int end = start + q.getNumTerms(action);
        for (int i = 0; i < q.getNumTerms(); i++) {
            trace[i] = gamma * lambda * 1 * trace[i];
            if (i >= start && i < end) {
                trace[i] += stateVals[i];
            }
        }


        double bot = Math.abs(dot(trace, add(multiply(q.calculateStateValue(nextState, nextAction), gamma), multiply(q.calculateStateValue(s, action), -1))));
        alpha = Math.min(alpha, 1 / bot);
        //System.out.println(alpha);
        double delta = reward - q.valueAt(s, action);
        if (!nextState.isTerminal()) {
            delta += gamma * q.valueAt(nextState, nextAction);
        }
        if (java.lang.Double.isNaN(delta)) {
            System.out.println("NaN trouble");
        }


        for (int i = 0; i < q.getNumTerms(); i++) {
            weights[i] += (alpha / shrink[i]) * delta * trace[i];
        }
        addError(s, action, delta);
        //printArray(weights);
    }

    public void addSample(State s, int action, State nextState, int nextAction, double reward, double trace[]) {
        double delta = reward - q.valueAt(s, action);
        if (!nextState.isTerminal()) {
            delta += gamma * q.valueAt(nextState, nextAction);
        }
        if (java.lang.Double.isNaN(delta)) {
            System.out.println("NaN trouble");
        }

        double stateVals[] = q.calculateStateValue(s);
        //printArray(stateVals);
        int start = q.getArrayStartPosition(action);
        int end = start + q.getNumTerms(action);
        for (int i = 0; i < q.getNumTerms(); i++) {
            trace[i] = gamma * lambda * 1 * trace[i];
            if (i >= start && i < end) {
                trace[i] += stateVals[i];
            }
            weights[i] += (alpha / shrink[i]) * delta * trace[i];
        }
        //printArray(weights);
    }

    public void addError(State s, int action, double delta) {
        actionCounter[action]++;
        double weightMag = 0;

        for (int i = q.getArrayStartPosition(action); i < q.getArrayStartPosition(action) + q.getNumTerms(action); i++) {
            weightMag += Math.abs(weights[i]);
        }
        double err[] = new double[s.getState().length + 1];
        for (int i = 0; i < s.getState().length; i++) {
            err[i] = s.getState()[i];
        }
        err[err.length - 1] = delta;
        errors[action].add(err);
        boolean added = false;
        FourierBasis best = null;
        double bestVal = 0;
        int bestIndex = 0;
        for (int i = 0; i < candidateFunctions[action].size(); i++) {
            double four = candidateFunctions[action].get(i).getValue(s);
            topValsA[action].set(i, topValsA[action].get(i) + four * delta);
            botValsA[action].set(i, botValsA[action].get(i) + four * four);
            if (actionCounter[action] > 10000) {
                double val = Math.abs(topValsA[action].get(i) / Math.sqrt(botValsA[action].get(i)));
                if ((val / weightMag) < 0.01) {
                    continue;
                }
                if (val > bestVal) {
                    boolean add = true;
                    FourierBasis b = candidateFunctions[action].get(i);
                    for (int k = 0; k < q.getFAs()[action].getTerms().length; k++) {
                        if (q.getFAs()[action].getTerms()[k].equals(b)) {
                            add = false;
                        }
                    }
                    if (add) {
                        best = b;
                        bestIndex = i;
                        bestVal = val;
                    }
                }
            }
        }
        if (best != null) {
            double val = topValsA[action].get(bestIndex) / botValsA[action].get(bestIndex);
            //best = new FourierBasis(new double[]{(int)(Math.random()*100), (int)(Math.random()*100)});
            best.printBasis();
            q.addBasis(best, action, val);
            actionCounter[action] = 0;
            candidateFunctions[action].remove(bestIndex);
            topValsA[action].remove(bestIndex);
            botValsA[action].remove(bestIndex);
            addFringe(best,action);
        }
    }

    public void addFringe(FourierBasis b, int action) {
        for (int i = 0; i < b.featureArr.length; i++) {
            double f[] = b.featureArr.clone();
            f[i]+=1;
            FourierBasis n = new FourierBasis(f.clone());
            tryAdd(n,action);
            f[i]-=2;
            n = new FourierBasis(f);
            tryAdd(n,action);
        }
    }
    
    public void tryAdd(FourierBasis b, int action) {
        for (int i = 0; i < b.featureArr.length; i++) {
            if (b.featureArr[i] <0) {
                return;
            }
        }
        for (int k = 0; k < q.getFAs()[action].getTerms().length; k++) {
            if (q.getFAs()[action].getTerms()[k].equals(b)) {
                return;
            }
        }
        candidateFunctions[action].add(b);
        topValsA[action].add(0.0);
        botValsA[action].add(0.0);
    }
    
    public void addInitialCandidates() {
        for (int action = 0; action < numActions; action++) {
                for (int k = 0; k < q.getFAs()[action].getTerms().length; k++) {
            addFringe((FourierBasis)q.getFAs()[action].getTerms()[k], action);
                
            }
        }
    }

    public void saveErrors(String filename) {
        for (int i = 0; i < errors.length; i++) {
            String fname = filename + i + ".dat";
            String out = "";
            for (double[] d : errors[i]) {
                out += getArrayString(d) + "\r\n";
            }
            FileWriter f = null;
            try {
                f = new FileWriter(fname, false);
            } catch (IOException ex) {
                Logger.getLogger(SarsaLambdaTester3.class.getName()).log(Level.SEVERE, null, ex);
            }

            BufferedWriter writer = new BufferedWriter(f);
            try {
                writer.write(out);
            } catch (IOException ex) {
                Logger.getLogger(SarsaLambdaTester3.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                writer.close();
            } catch (IOException ex) {
                Logger.getLogger(SarsaLambdaTester3.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public double[] add(double arr1[], double arr2[]) {
        double returnArr[] = new double[arr1.length];
        for (int i = 0; i < arr1.length; i++) {
            returnArr[i] = arr1[i] + arr2[i];
        }
        return returnArr;
    }

    public void decayAlpha(double amount) {
        alpha = alpha * amount;
    }

    public double dot(double arr1[], double arr2[]) {
        if (arr1.length != arr2.length) {
            System.out.println("Dot product error. Arrays not equal in length");
            return Double.NaN;
        }
        double returnVal = 0;
        for (int i = 0; i < arr1.length; i++) {
            returnVal += arr1[i] * arr2[i];
        }
        return returnVal;
    }

    public double[] multiply(double arr[], double m) {
        double returnArr[] = new double[arr.length];
        for (int i = 0; i < arr.length; i++) {
            returnArr[i] = arr[i] * m;
        }
        return returnArr;
    }

    public void printArray(double arr[]) {
        String str = "";
        for (int i = 0; i < arr.length; i++) {
            str = str + arr[i] + " ";
        }
        System.out.println(str);
    }

    public String getArrayString(double arr[]) {
        String str = "";
        for (int i = 0; i < arr.length; i++) {
            str = str + arr[i] + " ";
        }
        return str;
    }
}
