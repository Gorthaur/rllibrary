/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package experimental;

import Jama.Matrix;
import Jama.CholeskyDecomposition;
import functionapproximation.BasisFunction;
import functionapproximation.QApproximator;
import functionapproximation.WaveletTensorBasis;
import java.util.LinkedList;
import rl.policy.Policy;
import rl.Sample;

/**
 *
 * @author mitch
 */
public class OMPBRM {
    Policy p;
    QApproximator q;
    double gamma, betaarr[];
    boolean adaptive;
    int betaInd=0;
    int maxScale = 10;
    double results[][];
    
    public OMPBRM(Policy p, QApproximator q, double gamma, double betaarr[], boolean adaptive){
        this.p = p;
        this.q = q;
        this.gamma = gamma;
        this.betaarr = betaarr;
        this.adaptive = adaptive;
        results = new double[2][betaarr.length];
    }
    
    public OMPBRM(Policy p, QApproximator q, double gamma, double betaarr[], boolean adaptive, int maxScale){
        this.p = p;
        this.q = q;
        this.gamma = gamma;
        this.betaarr = betaarr;
        this.maxScale = maxScale;
        this.adaptive = adaptive;
        results = new double[2][betaarr.length];
    }
    
    public Matrix makeA(LinkedList<Sample> samples) {
        System.gc();
        int n = samples.size();
        int k = q.getNumTerms();
        Sample sam;
        Matrix phi1, phi2;
        Matrix A = new Matrix(n, k);
        for(int i=0; i<n; i++){
            sam = samples.get(i);
            phi1 = (new Matrix(q.calculateStateValue(sam.s, 0), 1)); //hard coded to v function
            phi2 = (new Matrix(q.calculateStateValue(sam.nextState, 0), 1)); // hard coded to v function
            A.setMatrix(i, i, 0, k-1, phi1.minus(phi2.times(gamma)));
        }
        return A;
    }
    
    public Matrix makeB(LinkedList<Sample> samples) {
        int n = samples.size();
        int k = q.getNumTerms();
        Sample sam;
        Matrix b = new Matrix(n, 1);
        for(int i=0; i<n; i++){
            sam = samples.get(i);
            b.set(i, 0, sam.reward);
        }
        return b;
    }
    
    public Matrix makeReturns(LinkedList<Sample> samples) {
        int n = samples.size();
        Sample sam;
        Matrix b = new Matrix(n, 1);
        for(int i=0; i<n; i++){
            sam = samples.get(i);
            b.set(i, 0, sam.ret);
        }
        return b;
    }
    
    public void addChildren(BasisFunction bf) {
        String bfString = bf.getBasisString();
        int locals[] = WaveletTensorBasis.getLocals(bfString);
        int scale = WaveletTensorBasis.getScale(bfString);
        BasisFunction children[] = {};
        //children = WaveletTensorBasis.makeNextScaleTerms(WaveletTensorBasis.getG(bfString), locals, scale, locals.length, 8);
        if (WaveletTensorBasis.isScaling(bfString)) {
            //System.out.print("Adding cross terms. ");
            children = WaveletTensorBasis.makeCrossTerms(locals, scale, locals.length, 8); // hard coded!
        }
        else {
            //System.out.print("Adding next scale. ");
            if (WaveletTensorBasis.getScale(bfString) < maxScale) children = WaveletTensorBasis.makeNextScaleTerms(WaveletTensorBasis.getG(bfString), locals, scale, locals.length, 8);
        }
        for (int i=0; i<children.length; i++) {
            //System.out.println(children[i].getBasisString());
            q.addBasis(children[i]);
        }
    }
    
    public Matrix remakeW(Matrix w, int oldNumTerms) {
        int k = q.getNumTerms();
        Matrix newW = new Matrix(k,1);
        newW.setMatrix(0, oldNumTerms-1, 0, 0, w.getMatrix(0, oldNumTerms-1, 0, 0));
        return newW;
    }
    
    public boolean[] remakeI(boolean[] I, int oldNumTerms) {
        int k = q.getNumTerms();
        boolean newI[] = new boolean[k];
        for(int i=0; i<oldNumTerms; i++) newI[i] = I[i];
        return newI;
    }

    public double[][] processSamples(LinkedList<Sample> samples) {
        int n = samples.size();
        int k = q.getNumTerms();
        int oldnumterms;
        int iter=0;
        int cumdictsize=0;
        BasisFunction bf;
        Matrix A = makeA(samples);
        Matrix b = makeB(samples);
        Matrix returns = makeReturns(samples);
        boolean I[] = new boolean[k];
        Matrix w = new Matrix(k, 1);
//        double ans=0;
//        //w=(A.transpose().times(A).plus(Matrix.identity(k, k).times(0.00000000001))).solve(A.transpose().times(b));
//        //w = (((A.transpose()).times(A)).plus(Matrix.identity(k, k).times(0.00000000001)).inverse()).times(A.transpose()).times(b);
//        Matrix res = b.minus(A.times(w));
//        for (int i=0; i<res.getRowDimension(); i++) ans = ans + Math.abs(res.get(i, 0))/(double)res.getRowDimension();
//        System.out.println(ans);
        Matrix c, AI, bI;
        int cMaxIndex;
        int Icount = 0;
        //System.out.println(q.getNumTerms());
        while (true) {
            //System.out.print(q.getNumTerms() + " ");
            //System.out.println(Icount);
            cumdictsize += q.getNumTerms()-Icount;
            iter++;
            c = (A.transpose()).times(b.minus(A.times(w))).times(1.0/n);
            cMaxIndex = 0;
            while (I[cMaxIndex]) cMaxIndex++;
            for(int j=cMaxIndex+1; j<q.getNumTerms(); j++){
                if (Math.abs(c.get(j, 0)) > Math.abs(c.get(cMaxIndex, 0)) && !I[j]) cMaxIndex = j;
            }
            //System.out.println(q.getNumTerms());
            //System.out.println(c.get(cMaxIndex, 0));
            if (Math.abs(c.get(cMaxIndex, 0)) > betaarr[betaInd]) 
            {I[cMaxIndex] = true;
            Icount++;
            int colList[] = new int[Icount];
            int t2 = 0;
            for (int t=0; t<q.getNumTerms(); t++) {
                if (I[t]) {
                    colList[t2]= t;
                    t2++;
                }      
            }
            AI = A.getMatrix(0, n-1, colList);
            CholeskyDecomposition AIC = new CholeskyDecomposition((AI.transpose().times(AI)));
            //w.setMatrix(colList, 0, 0, (AI.transpose().times(AI).plus(Matrix.identity(AI.getColumnDimension(), AI.getColumnDimension()).times(0.01))).inverse().times(AI.transpose()).times(b));
            w.setMatrix(colList, 0, 0, AIC.solve(AI.transpose().times(b)));
            //for (int i=0; i<w.getRowDimension(); i++) System.out.print(w.get(i, 0) + " ");
            //System.out.println(Icount);
            //bf = q.FAs[0].terms[cMaxIndex]; 
            //System.out.println(bf.getBasisString());
            if (Icount == q.getNumTerms()) break;
            //System.out.println(q.getNumTerms()-Icount);
            if (adaptive) {
                bf = q.getFAs()[0].getTerms()[cMaxIndex]; 
                oldnumterms = q.getNumTerms();
                addChildren(bf);
                A = makeA(samples);
                b = makeB(samples);
                w = remakeW(w, oldnumterms);
                I = remakeI(I, oldnumterms);
            }
        }
            else {
                results[0][betaInd] = cumdictsize/(double)iter;
                Matrix res = returns.minus(A.times(w));
                for (int i=0; i<res.getRowDimension(); i++) results[1][betaInd] = results[1][betaInd] + Math.pow(res.get(i, 0), 2);
                results[1][betaInd] = Math.sqrt(results[1][betaInd]/res.getRowDimension());
                betaInd++;
                if (betaInd==betaarr.length) break;
            }
        }
        return results;
    }
}

