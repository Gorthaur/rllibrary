package experimental;

import functionapproximation.QApproximator;
import functionapproximation.FourierBasis;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import rl.ElegibilityTrace;
import rl.policy.Policy;
import rl.state.State;

public class SarsaLambdaTester2 {

    QApproximator q;
    Policy p;
    ElegibilityTrace trace;
    double weights[];
    double shrink[];
    int numTerms;
    double lambda;
    double gamma;
    double alpha;
    ArrayList<double[]> errors[];
    int xFrequency = 10;
    int yFrequency = 10;
    int zFrequency = 10;
    int kFrequency = 10;
    int numActions = 5;
    double topVals[][][][][] = new double[numActions][xFrequency][yFrequency][zFrequency][kFrequency];
    FourierBasis f[][][][] = new FourierBasis[xFrequency][yFrequency][zFrequency][kFrequency];
    double botVals[][][][][] = new double[numActions][xFrequency][yFrequency][zFrequency][kFrequency];
    int actionCounter[] = new int[numActions];
    double oldphi[] = null;

    public SarsaLambdaTester2(QApproximator q, Policy p, double alpha, double gamma, double lambda) {
        this.q = q;
        this.p = p;
        this.numTerms = q.getNumTerms();
        this.alpha = alpha;
        this.gamma = gamma;
        this.lambda = lambda;
        trace = q.getNewTrace();
        weights = q.getWeights();
        shrink = q.getShrink();
        errors = new ArrayList[q.getNumActions()];
        for (int i = 0; i < q.getNumActions(); i++) {
            errors[i] = new ArrayList<double[]>();
        }

        for (int i = 0; i < xFrequency; i++) {
            for (int j = 0; j < yFrequency; j++) {
                for (int z = 0; z < zFrequency; z++) {
                    for (int k = 0; k < kFrequency; k++) {
                        f[i][j][z][k] = new FourierBasis(new double[]{i, j, z, k});
                    }
                }
            }
        }
    }

    public void startEpisode() {
        trace.setTrace(new double[q.getNumTerms()]);
    }

    public int nextMove(State s) {
        return p.select(s);
    }

    public void addSample(State s, int action, State nextState, int nextAction, double reward) {
        weights = q.getWeights();
        double trace[] = this.trace.getTrace();
        shrink = q.getShrink();
        double stateVals[] = q.calculateStateValue(s);
        //printArray(stateVals);
        int start = q.getArrayStartPosition(action);
        int end = start + q.getNumTerms(action);
        for (int i = 0; i < q.getNumTerms(); i++) {
            trace[i] = gamma * lambda * 1 * trace[i];
            if (i >= start && i < end) {
                trace[i] += stateVals[i];
            }
        }


        double bot = Math.abs(dot(dotDivide(trace, shrink), add(multiply(q.calculateStateValue(nextState, nextAction), gamma), multiply(q.calculateStateValue(s, action), -1))));
        alpha = Math.min(alpha, 1 / bot);
        //System.out.println(alpha);
        double delta = reward - q.valueAt(s, action);
        if (!nextState.isTerminal()) {
            delta += gamma * q.valueAt(nextState, nextAction);
        }
        if (java.lang.Double.isNaN(delta)) {
            System.out.println("NaN trouble");
        }


        for (int i = 0; i < q.getNumTerms(); i++) {
            weights[i] += (alpha / shrink[i]) * delta * trace[i];
        }
        //====================================================================
        addError(s, action, delta);
        //printArray(weights);
    }

    public void addSample(State s, int action, State nextState, int nextAction, double reward, double trace[]) {
        double delta = reward - q.valueAt(s, action);
        if (!nextState.isTerminal()) {
            delta += gamma * q.valueAt(nextState, nextAction);
        }
        if (java.lang.Double.isNaN(delta)) {
            System.out.println("NaN trouble");
        }

        double stateVals[] = q.calculateStateValue(s);
        //printArray(stateVals);
        int start = q.getArrayStartPosition(action);
        int end = start + q.getNumTerms(action);
        for (int i = 0; i < q.getNumTerms(); i++) {
            trace[i] = gamma * lambda * 1 * trace[i];
            if (i >= start && i < end) {
                trace[i] += stateVals[i];
            }
            weights[i] += (alpha / shrink[i]) * delta * trace[i];
        }
        //printArray(weights);
    }

    public void addError(State s, int action, double delta) {
        actionCounter[action]++;
        double weightMag = 0;

        for (int i = q.getArrayStartPosition(action); i < q.getArrayStartPosition(action) + q.getNumTerms(action); i++) {
            weightMag += Math.abs(weights[i]);
        }
        double err[] = new double[s.getState().length + 1];
        for (int i = 0; i < s.getState().length; i++) {
            err[i] = s.getState()[i];
        }
        err[err.length - 1] = delta;
        errors[action].add(err);
        boolean added = false;
        FourierBasis best = null;
        double bestVal = 0;
        for (int i = 0; i < xFrequency; i++) {
            for (int j = 0; j < yFrequency; j++) {
                for (int z = 0; z < zFrequency; z++) {
                    for (int k = 0; k < kFrequency; k++) {

                        double four = f[i][j][z][k].getValue(s);
                        topVals[action][i][j][z][k] = topVals[action][i][j][z][k] + (four * delta - topVals[action][i][j][z][k]);
                        botVals[action][i][j][z][k] = botVals[action][i][j][z][k] + (four * four - botVals[action][i][j][z][k]);
                        if (actionCounter[action] > 2000) {
                            double val = Math.abs(topVals[action][i][j][z][k] / Math.sqrt(botVals[action][i][j][z][k]));
                            double val2 = Math.abs(topVals[action][i][j][z][k] / botVals[action][i][j][z][k]);
                            if ((val2 / weightMag) < 0.1) {
                                continue;
                            }
                            if (val > bestVal) {
                                boolean add = true;
                                FourierBasis b = new FourierBasis(new double[]{i, j, z, k});
                                for (int l = 0; l < q.getFAs()[action].getTerms().length; l++) {
                                    if (q.getFAs()[action].getTerms()[l].equals(b)) {
                                        add = false;
                                    }
                                }
                                if (add) {
                                    best = b;
                                    bestVal = val;
                                }
                            }

                        }
                    }
                }
            }
        }
        if (best != null) {
            int x = (int) best.featureArr[0];
            int y = (int) best.featureArr[1];
            int z = (int) best.featureArr[2];
            int k = (int) best.featureArr[3];
            double val = topVals[action][x][y][z][k] / botVals[action][x][y][z][k];
            //best = new FourierBasis(new double[]{(int)(Math.random()*100), (int)(Math.random()*100)});
            best.printBasis();
            q.addBasis(best, action, 0);
            //alpha = 1;
            actionCounter[action] = 0;
            for (int i = 0; i < xFrequency; i++) {
                for (int j = 0; j < yFrequency; j++) {
                    for (int l = 0; l < zFrequency; l++) {
                        for (int m = 0; m < kFrequency; m++) {
                            topVals[action][i][j][l][m] = 0;
                            botVals[action][i][j][l][m] = 0;
                        }
                    }
                }
            }
        }
    }

    public void saveErrors(String filename) {
        for (int i = 0; i < errors.length; i++) {
            String fname = filename + i + ".dat";
            String out = "";
            for (double[] d : errors[i]) {
                out += getArrayString(d) + "\r\n";
            }
            FileWriter f = null;
            try {
                f = new FileWriter(fname, false);
            } catch (IOException ex) {
                Logger.getLogger(SarsaLambdaTester2.class.getName()).log(Level.SEVERE, null, ex);
            }

            BufferedWriter writer = new BufferedWriter(f);
            try {
                writer.write(out);
            } catch (IOException ex) {
                Logger.getLogger(SarsaLambdaTester2.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                writer.close();
            } catch (IOException ex) {
                Logger.getLogger(SarsaLambdaTester2.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public double[] add(double arr1[], double arr2[]) {
        double returnArr[] = new double[arr1.length];
        for (int i = 0; i < arr1.length; i++) {
            returnArr[i] = arr1[i] + arr2[i];
        }
        return returnArr;
    }

    public void decayAlpha(double amount) {
        alpha = alpha * amount;
    }

    public double dot(double arr1[], double arr2[]) {
        if (arr1.length != arr2.length) {
            System.out.println("Dot product error. Arrays not equal in length");
            return Double.NaN;
        }
        double returnVal = 0;
        for (int i = 0; i < arr1.length; i++) {
            returnVal += arr1[i] * arr2[i];
        }
        return returnVal;
    }

    public double[] multiply(double arr[], double m) {
        double returnArr[] = new double[arr.length];
        for (int i = 0; i < arr.length; i++) {
            returnArr[i] = arr[i] * m;
        }
        return returnArr;
    }

    public void printArray(double arr[]) {
        String str = "";
        for (int i = 0; i < arr.length; i++) {
            str = str + arr[i] + " ";
        }
        System.out.println(str);
    }

    public String getArrayString(double arr[]) {
        String str = "";
        for (int i = 0; i < arr.length; i++) {
            str = str + arr[i] + " ";
        }
        return str;
    }
            //pairwise multiple elements in arrays and return the resulting array
    public double[] dotDivide(double arr1[], double arr2[]) {

        if (arr1.length != arr2.length) {
            System.out.println("Dot product error. Arrays not equal in length");
            return null;
        }
        double ret[] = new double[arr1.length];
        for (int i = 0; i < arr1.length; i++) {
            ret[i] = arr1[i] / arr2[i];
        }
        return ret;
    }
}
