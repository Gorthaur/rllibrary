package rl.learningalgorithms;

import Jama.Matrix;
import functionapproximation.VApproximator;
import java.io.Serializable;
import java.util.LinkedList;
import rl.Sample;
import rlmath.MatrixMath;

/**
 *
 * @author Dean
 */
public class LSTDV implements Serializable {

    VApproximator v;
    double gamma;
    double A[][];
    double B[][];
    double b[][];
    double regularisation = 0.01;

    public LSTDV(VApproximator v, double gamma) {
        this.v = v;
        this.gamma = gamma;
    }

    /**
     * Runs LSTD on the given samples using the value function this object was constructed with.
     * @param samples The samples to learn from. All they really need are state, reward and next state values.
     */
    public void fullUpdate(LinkedList<Sample> samples) {
        initialiseB();
        initialiseb();
        for (Sample samp : samples) {
            addSample(samp);
        }
        double weights[] = getWeights();
        v.setWeights(weights);
    }

    public void initialiseB() {
        int numTerms = v.getNumTerms();
        B = new double[numTerms][numTerms];
        A = new double[numTerms][numTerms];
        for (int i = 0; i < B.length; i++) {
            //B[i][i] = 1/(regularisation*v.fa.getTerms()[i].getShrink() + 0.0001);
            B[i][i] = 1/(regularisation + 0.00000001);
        }
        for (int i = 0; i < B.length; i++) {
            //A[i][i] = regularisation*v.fa.getTerms()[i].getShrink();
            A[i][i] = regularisation;
        }
    }

    public void initialiseb() {
        int numTerms = v.getNumTerms();
        b = new double[1][numTerms];
    }

    public void addSample(Sample p) {
        double Bprime[][];
        Bprime = B;

        double phisa[][] = new double[1][];
        phisa[0] = v.calculateStateValue(p.s);


        double phisprimeaprime[][] = new double[1][];

        if (p.s.isTerminal()) {
            System.out.println("shit");
        }
        if (!p.nextState.isTerminal()) {
            if (p.s.isTerminal()) {
                System.out.println("shit");
            }

            phisprimeaprime[0] = v.calculateStateValue(p.nextState);
        } else {
            phisprimeaprime[0] = new double[phisa[0].length];
        }

        double phiminusgammaphiprime[][] = MatrixMath.subtract(phisa, MatrixMath.multiply(phisprimeaprime, gamma));

        double phisaphiminusgammaphiprime[][] = MatrixMath.multiply(phisa, true, phiminusgammaphiprime, false);

        A = MatrixMath.add(A, phisaphiminusgammaphiprime);

        double top[][] = MatrixMath.multiply(Bprime, false, phisa, true);
        top = MatrixMath.multiply(top, MatrixMath.multiply(phiminusgammaphiprime, Bprime));


        double bottom[][] = MatrixMath.multiply(phiminusgammaphiprime, Bprime);
        bottom = MatrixMath.multiply(bottom, false, phisa, true);


        if (bottom.length != 1 && bottom[0].length != 1) {
            System.out.println("Error, bottom size wrong");
        }
        top = MatrixMath.divide(top, 1 + bottom[0][0]);


        double newB[][] = MatrixMath.subtract(Bprime, top);


        B = newB;

        double newb[][] = MatrixMath.add(b, MatrixMath.multiply(phisa, p.reward));
        b = newb;
    }

    public double[] getWeights() {
        //All this commented stuff is for when you'd rather not use Sherman Morrison. Either way, the A matrix is initiallised with small values down the diagonal to make it more stable as described in the LSPI paper.
        Matrix AMat = new Matrix(A);
        Matrix inv = AMat.inverse();
        Matrix bMat = new Matrix(MatrixMath.transpose(b));
        Matrix w = inv.times(bMat);
        double weights2[][] = w.getArray();
        weights2 = MatrixMath.transpose(weights2);
        return weights2[0];
        //double weights[][] = MatrixMath.multiply(B, false, b, true);
        //weights = MatrixMath.transpose(weights);
        //return weights[0];
    }
}
