package rl.learningalgorithms;

import functionapproximation.QApproximator;
import rl.ElegibilityTrace;
import rl.policy.Policy;
import rl.state.State;

public class GQLambda {
  QApproximator q;
  Policy p;
  ElegibilityTrace trace;
  double secondaryWeights[];
  double primaryWeights[];
  double shrink[];
  int numTerms;

  // double alpha = 0.0001;
  // double eta = 0.01;
  double lambda;
  double gamma;
  double alpha;

  public GQLambda(QApproximator q, Policy p, double alpha, double gamma, double lambda) {
    this.q = q;
    this.p = p;
    this.numTerms = q.getNumTerms();
    this.alpha = alpha;
    this.gamma = gamma;
    this.lambda = lambda;
    //trace = new double[numTerms];
    trace = q.getNewTrace();
    secondaryWeights = new double[numTerms];
    primaryWeights = q.getWeights();
    shrink = q.getShrink();
  }

  public void startEpisode() {
    trace.setTrace(new double[q.getNumTerms()]);
  }

  public int nextMove(State s) {
    return p.select(s);
  }

  public void addSample(State s, int action, State nextState, double reward) {
    primaryWeights = q.getWeights();
    double trace[] = this.trace.getTrace();
    shrink = q.getShrink();
    double probabilities[] = p.getProbabilities(nextState);
    double phi[] = q.calculateStateValue(s,action);
    double phi_next[] = new double[numTerms];
    for (int i = 0; i < q.getNumActions(); i++) {
      phi_next = add(phi_next, multiply(q.calculateStateValue(nextState,i), probabilities[i]));
    }
    //comment out to remove auto alpha - doesn't work.
    //double bot = Math.abs(dot(trace, add(multiply(phi_next, gamma), multiply(phi,-1))));
    //alpha = Math.min(alpha, 1/bot);
    
    int greedy = p.getGreedy(q,s);
    double pi = 0;
    if (greedy != action) {
      pi = 0;
    }
    else {
      pi = 1;
    }
    probabilities = p.getProbabilities(s);
    double rho = pi/probabilities[action];
    
    double gammaPrime;
    if (nextState.isTerminal()) {
      gammaPrime = 0;
    }
    else {
      gammaPrime = 1;
    }
    GQLearn(phi,phi_next, lambda, gammaPrime, 0, reward, rho, 1);
  }

  /**
   * @author Adam White Inputs:: phi - feature vector corresponding to action
   *         a_t in state s_t phi_next - expected next state feature vector
   *         corresponding to a \in A and s_t+1 lambda - elegibility trace
   *         parameter [0,1] gamma - discount factor [0,1] z - outcome reward r
   *         - transient reward rho - ratio of target policy to behaviour policy
   *         [0,1] I - set of interest for s_t, a_t [0,1]
   **/
  public void GQLearn(double[] phi, double[] phi_next, double lambda, double gamma, double z, double r, double rho, double I) {
    
      double trace[] = this.trace.getTrace();
      double eta = 1.0; // step size parameter
    double delta;

    delta = r + (1 - gamma) * z + gamma * dot(primaryWeights, phi_next) - dot(primaryWeights, phi);

    for (int i = 0; i < numTerms; i++)
      trace[i] = rho * trace[i] + I * phi[i];

    for (int i = 0; i < numTerms; i++) {
      primaryWeights[i] += (alpha/shrink[i]) * (delta * trace[i] - gamma * (1 - lambda) * dot(secondaryWeights, trace) * phi_next[i]);
      secondaryWeights[i] += (alpha/shrink[i]) * eta * (delta * trace[i] - dot(secondaryWeights, phi) * phi[i]);
      trace[i] *= gamma * lambda;
    }

  }

  public double[] add(double arr1[], double arr2[]) {
    double returnArr[] = new double[arr1.length];
    for (int i = 0; i < arr1.length; i++) {
      returnArr[i] = arr1[i] + arr2[i];
    }
    return returnArr;
  }

  public double dot(double arr1[], double arr2[]) {
    if (arr1.length != arr2.length) {
      System.out.println("Dot product error. Arrays not equal in length");
      return Double.NaN;
    }
    double returnVal = 0;
    for (int i = 0; i < arr1.length; i++) {
      returnVal += arr1[i] * arr2[i];
    }
    return returnVal;
  }
  
  public double[] multiply(double arr[], double m) {
    double returnArr[] = new double[arr.length];
    for (int i = 0; i < arr.length; i++) {
      returnArr[i] = arr[i]*m;
    }
    return returnArr;
  }
}
