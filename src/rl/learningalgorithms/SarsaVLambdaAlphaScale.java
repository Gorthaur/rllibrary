package rl.learningalgorithms;

import functionapproximation.VApproximator;
import rl.ElegibilityTrace;
import rl.policy.Policy;
import rl.state.State;

/**
 *
 * @author Dean
 */
public class SarsaVLambdaAlphaScale {

    VApproximator v;
    Policy p;
    ElegibilityTrace trace;
    double weights[];
    double shrink[];
    int numTerms;
    double lambda;
    double gamma;
    public double alpha;

    public SarsaVLambdaAlphaScale(VApproximator v, Policy p, double alpha, double gamma, double lambda) {
        this.v = v;
        this.p = p;
        this.numTerms = v.getNumTerms();
        this.alpha = alpha;
        this.gamma = gamma;
        this.lambda = lambda;
        trace = v.getNewTrace();
        weights = v.getWeights();
        shrink = v.getShrink();
    }

    public void startEpisode() {
        trace.setTrace(new double[v.getNumTerms()]);
    }

    public int nextMove(State s) {
        return p.select(s);
    }

    public void addSample(State s, State nextState, double reward) {
        weights = v.getWeights();
        double trace[] = this.trace.getTrace();
        shrink = v.getShrink();
        double stateVals[] = v.calculateStateValue(s);

        for (int i = 0; i < v.getNumTerms(); i++) {
            trace[i] = gamma * lambda * 1 * trace[i];
            trace[i] += stateVals[i];
            //trace[i] = Math.min(stateVals[i],1);
        }

        /* Alpha scaling include shrink */
        double bot = dot(dotDivide(trace, shrink), add(multiply(v.calculateStateValue(nextState), gamma), multiply(v.calculateStateValue(s), -1)));

        /* Alpha scaling excluding shrink */
        //double bot = Math.abs(dot(trace, add(multiply(q.calculateStateValue(nextState, nextAction), gamma), multiply(q.calculateStateValue(s, action), -1))));

        /* Identical if shrink values = 1 */
        if (bot < 0) {
           alpha = Math.min(alpha, 1 / Math.abs(bot));
        }

        double delta = reward - v.valueAt(s);
        if (!nextState.isTerminal()) {
            delta += gamma * v.valueAt(nextState);
        }
        if (java.lang.Double.isNaN(delta)) {
            System.out.println("NaN trouble");
        }

        for (int i = 0; i < v.getNumTerms(); i++) {
            weights[i] += (alpha / shrink[i]) * delta * trace[i];
            if (Double.isNaN(weights[i]) || weights[i] > 1000000) {
                System.out.println("NaN trouble 2.");
            }
        }
    }

    public double[] add(double arr1[], double arr2[]) {
        double returnArr[] = new double[arr1.length];
        for (int i = 0; i < arr1.length; i++) {
            returnArr[i] = arr1[i] + arr2[i];
        }
        return returnArr;
    }

    public void decayAlpha(double amount) {
        alpha = alpha * amount;
    }

    public double dot(double arr1[], double arr2[]) {
        if (arr1.length != arr2.length) {
            System.out.println("Dot product error. Arrays not equal in length");
            return Double.NaN;
        }
        double returnVal = 0;
        for (int i = 0; i < arr1.length; i++) {
            returnVal += arr1[i] * arr2[i];
        }
        return returnVal;
    }

    //pairwise multiple elements in arrays and return the resulting array
    public double[] dotMultiply(double arr1[], double arr2[]) {

        if (arr1.length != arr2.length) {
            System.out.println("Dot product error. Arrays not equal in length");
            return null;
        }
        double ret[] = new double[arr1.length];
        for (int i = 0; i < arr1.length; i++) {
            ret[i] = arr1[i] * arr2[i];
        }
        return ret;
    }

    //pairwise multiple elements in arrays and return the resulting array
    public double[] dotDivide(double arr1[], double arr2[]) {

        if (arr1.length != arr2.length) {
            System.out.println("Dot product error. Arrays not equal in length");
            return null;
        }
        double ret[] = new double[arr1.length];
        for (int i = 0; i < arr1.length; i++) {
            ret[i] = arr1[i] / arr2[i];
        }
        return ret;
    }

    public double[] multiply(double arr[], double m) {
        double returnArr[] = new double[arr.length];
        for (int i = 0; i < arr.length; i++) {
            returnArr[i] = arr[i] * m;
        }
        return returnArr;
    }

    public void printArray(double arr[]) {
        String str = "";
        for (int i = 0; i < arr.length; i++) {
            str = str + arr[i] + " ";
        }
        System.out.println(str);
    }

    public String getArrayString(double arr[]) {
        String str = "";
        for (int i = 0; i < arr.length; i++) {
            str = str + arr[i] + " ";
        }
        return str;
    }
}
