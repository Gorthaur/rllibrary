package rl.learningalgorithms;

import Jama.Matrix;
import Jama.SingularValueDecomposition;
import functionapproximation.BasisFunction;
import functionapproximation.VApproximator;
import functionapproximation.dictionary.GeneralFeatureDictionary;
import java.io.Serializable;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Random;
import rl.Sample;
import rl.state.State;
import rlmath.MatrixMath;

/**
 *
 * @author Dean
 */
public class RegularizedLSTDVRS implements Serializable {

    VApproximator v;
    double gamma;
    double A[][];
    double B[][];
    double b[][];
    double regularisation = 0.01;
    private final LinkedList<BasisFunction> bfs;
    private double[][] projection;
    private double[] weights;

    public RegularizedLSTDVRS(LinkedList<BasisFunction> bfs, double gamma, double regularisation) {
        this.bfs = bfs;
        this.gamma = gamma;
        this.regularisation = regularisation;
    }

    /**
     * Runs LSTD on the given samples using the value function this object was
     * constructed with.
     *
     * @param samples The samples to learn from. All they really need are state,
     * reward and next state values.
     */
    public void fullUpdate(LinkedList<Sample> samples, int reductionSize) {
        Random rand = new Random();
        projection = new double[bfs.size()][reductionSize];
        for (int i = 0; i < projection.length; i++) {
            for (int j = 0; j < projection[i].length; j++) {
                projection[i][j] = rand.nextGaussian() / Math.sqrt(reductionSize);
            }
        }
        initialiseB();
        initialiseb();
        for (Sample samp : samples) {
            addSample(samp, bfs, projection);
        }
        weights = getWeights();
    }

    public double getValue(State s) {
        double phisa[][] = new double[1][];
        phisa[0] = new double[bfs.size()];
        int count = 0;
        for (BasisFunction b : bfs) {
            phisa[0][count] = b.getValue(s);
            count++;
        }

        phisa = MatrixMath.multiply(phisa, projection);
        return MatrixMath.dot(phisa[0], weights);
    }

    public void initialiseB() {
        int numTerms = projection[0].length;
        B = new double[numTerms][numTerms];
        A = new double[numTerms][numTerms];
        for (int i = 0; i < B.length; i++) {
            //B[i][i] = 1/(regularisation*v.fa.getTerms()[i].getShrink() + 0.0001);
            B[i][i] = 1 / (regularisation + 0.00000001);
        }
        for (int i = 0; i < B.length; i++) {
            //A[i][i] = regularisation*v.fa.getTerms()[i].getShrink();
            A[i][i] = regularisation;
        }
    }

    public void initialiseb() {
        int numTerms = projection[0].length;
        b = new double[1][numTerms];
    }

    public void addSample(Sample p, LinkedList<BasisFunction> bfs, double projection[][]) {
        double Bprime[][];
        Bprime = B;

        double phisa[][] = new double[1][];
        phisa[0] = new double[bfs.size()];
        int count = 0;
        for (BasisFunction b : bfs) {
            phisa[0][count] = b.getValue(p.s);
            count++;
        }

        phisa = MatrixMath.multiply(phisa, projection);

        double phisprimeaprime[][] = new double[1][];

        if (p.s.isTerminal()) {
            System.out.println("shit");
        }
        if (!p.nextState.isTerminal()) {
            if (p.s.isTerminal()) {
                System.out.println("shit");
            }

            phisprimeaprime[0] = new double[bfs.size()];
            count = 0;
            for (BasisFunction b : bfs) {
                phisprimeaprime[0][count] = b.getValue(p.nextState);
                count++;
            }
        } else {
            phisprimeaprime[0] = new double[bfs.size()];
        }

        phisprimeaprime = MatrixMath.multiply(phisprimeaprime, projection);

        double phiminusgammaphiprime[][] = MatrixMath.subtract(phisa, MatrixMath.multiply(phisprimeaprime, gamma));

        double phisaphiminusgammaphiprime[][] = MatrixMath.multiply(phisa, true, phiminusgammaphiprime, false);

        A = MatrixMath.add(A, phisaphiminusgammaphiprime);

        double top[][] = MatrixMath.multiply(Bprime, false, phisa, true);
        top = MatrixMath.multiply(top, MatrixMath.multiply(phiminusgammaphiprime, Bprime));

        double bottom[][] = MatrixMath.multiply(phiminusgammaphiprime, Bprime);
        bottom = MatrixMath.multiply(bottom, false, phisa, true);

        if (bottom.length != 1 && bottom[0].length != 1) {
            System.out.println("Error, bottom size wrong");
        }
        top = MatrixMath.divide(top, 1 + bottom[0][0]);

        double newB[][] = MatrixMath.subtract(Bprime, top);

        B = newB;

        double newb[][] = MatrixMath.add(b, MatrixMath.multiply(phisa, p.reward));
        b = newb;
    }

    public double[] getWeights() {

        //All this commented stuff is for when you'd rather not use Sherman Morrison. Either way, the A matrix is initiallised with small values down the diagonal to make it more stable as described in the LSPI paper.
        //Jama.Matrix AMat = new Jama.Matrix(A);
        //SingularValueDecomposition s = AMat.svd();
//Matrix U = s.getU();
//Matrix S = s.getS();
//Matrix V = s.getV();
        //Jama.Matrix inv = AMat.inverse();
//Matrix inv = V.times(S.inverse()).times(U.transpose());
        //Jama.Matrix bMat = new Jama.Matrix(MatrixMath.transpose(b));
        //Jama.Matrix w = inv.times(bMat);
        //double weights2[][] = w.getArray();
        //weights2 = MatrixMath.transpose(weights2);
        //return weights2[0];
        double weights[][] = MatrixMath.multiply(B, false, b, true);
        weights = MatrixMath.transpose(weights);
        return weights[0];
    }

    public void updateWeights(LinkedList<Sample> samples, int reductionSize) {
        double r[][] = new double[1][samples.size()];
        double psi[][] = new double[samples.size()][reductionSize];
        double psiPrime[][] = new double[samples.size()][reductionSize];
        int count = 0;
        for (Sample s : samples) {
            r[0][count] = s.reward;
            psi[count] = getStateValue(s.s);
            if (!s.nextState.isTerminal()) {
                psiPrime[count] = getStateValue(s.nextState);
            } else {
                psiPrime[count] = new double[reductionSize];
            }
            count++;
        }
        double A[][] = new double[reductionSize][reductionSize];
        for (int i = 0; i < reductionSize; i++) {
            A[i][i] = regularisation;
        }

        A = MatrixMath.add(A, MatrixMath.multiply(psi, true, MatrixMath.subtract(psi, MatrixMath.multiply(psiPrime, gamma)), false));
        double b[][] = MatrixMath.multiply(psi, true, r, true);

        Jama.Matrix AMat = new Jama.Matrix(A);
        SingularValueDecomposition s = AMat.svd();
        Matrix U = s.getU();
        Matrix S = s.getS();
        Matrix V = s.getV();
        //Jama.Matrix inv = AMat.inverse();
        Matrix inv = V.times(S.inverse()).times(U.transpose());
        Jama.Matrix bMat = new Jama.Matrix(b);
        Jama.Matrix w = inv.times(bMat).transpose();
        double weights2[][] = w.getArray();
        System.out.println(Arrays.toString(weights2[0]));
        System.out.println(Arrays.toString(weights));
        weights = weights2[0];
    }

    public double[] getStateValue(State s) {
        double phisa[][] = new double[1][];
        phisa[0] = new double[bfs.size()];
        int count = 0;
        for (BasisFunction b : bfs) {
            phisa[0][count] = b.getValue(s);
            count++;
        }
        phisa = MatrixMath.multiply(phisa, projection);
        return phisa[0];
    }
}
