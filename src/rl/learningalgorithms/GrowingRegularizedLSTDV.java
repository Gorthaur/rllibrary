package rl.learningalgorithms;

import functionapproximation.VApproximator;
import java.io.Serializable;
import java.util.Arrays;
import java.util.LinkedList;
import org.mathIT.algebra.Matrix;
import rl.Sample;
import rlmath.MatrixMath;

/**
 *
 * @author Dean
 */
public class GrowingRegularizedLSTDV extends RegularizedLSTDV implements Serializable {

    double phiStore[][] = new double[100][];
    double phiNextStore[][] = new double[100][];

    public GrowingRegularizedLSTDV(VApproximator v, double gamma, double regularisation) {
        super(v, gamma, regularisation);
    }

    public void partialUpdate(LinkedList<Sample> samples) {
        if (A == null) {
            fullUpdate(samples);
            return;
        }
        if (phiStore.length > v.getNumTerms()) {
            for (int i = 0; i < v.getNumTerms(); i++) {

                if (phiStore[i] == null) {
                    double p[] = new double[samples.size()];
                    double pn[] = new double[samples.size()];
                    int count = 0;
                    for (Sample s : samples) {
                        p[count] = v.fa.terms[i].getValue(s.s);
                        pn[count] = gamma*v.fa.terms[i].getValue(s.nextState);
                        count++;
                    }
                    phiStore[i] = p;
                    phiNextStore[i] = pn;
                }
            }
        }
        double[][] newA = new double[v.getNumTerms()][v.getNumTerms()];
        double[][] newb = new double[1][v.getNumTerms()];
        for (int i = 0; i < newA.length; i++) {
            if (i < A.length) {
                newb[0][i] = b[0][i];
            } else {
                newb[0][i] = calculateSingleb(i, samples);
            }
            for (int j = 0; j < newA[i].length; j++) {
                if (i < A.length && j < A[i].length) {
                    newA[i][j] = A[i][j];
                } else {
                    newA[i][j] = calculateSingle(i, j, samples);
                    if (i == j) {
                        newA[i][j] += regularisation;
                    }
                }
            }
        }
        b = newb;
        A = newA;
        double weights[] = getWeights();
        v.setWeights(weights);
    }

    public double calculateSingle(int i, int j, LinkedList<Sample> samples) {
        double val = 0;
        int count = 0;
        for (Sample s : samples) {
            if (!s.nextState.isTerminal()) {
                //val += v.fa.terms[i].getValue(s.s) * (v.fa.terms[j].getValue(s.s) - gamma * v.fa.terms[j].getValue(s.nextState));
                val += getValue(s, count, i)*(getValue(s, count, j) - getNextValue(s,count, j));
            } else {
                //val += v.fa.terms[i].getValue(s.s) * (v.fa.terms[j].getValue(s.s));
                val += getValue(s, count, i)*(getValue(s, count, j));
            }
            count++;
        }
        return val;
    }

    public double getValue(Sample s, int sampleNum, int bf) {
        if (phiStore.length > bf && phiStore[bf] != null) {
            return phiStore[bf][sampleNum];
        } else {
            return v.fa.terms[bf].getValue(s.s);
        }
    }

    public double getNextValue(Sample s, int sampleNum, int bf) {
        if (phiStore.length > bf && phiStore[bf] != null) {
            return phiNextStore[bf][sampleNum];
        } else {
            return v.fa.terms[bf].getValue(s.nextState);
        }
    }

    public double calculateSingleb(int i, LinkedList<Sample> samples) {
        double val = 0;
        int count = 0;
        for (Sample s : samples) {
            //val += v.fa.terms[i].getValue(s.s) * s.reward;
            val += getValue(s, count, i) * s.reward;
            count++;
        }
        return val;
    }

    @Override
    public double[] getWeights() {
        //All this commented stuff is for when you'd rather not use Sherman Morrison. Either way, the A matrix is initiallised with small values down the diagonal to make it more stable as described in the LSPI paper.
        Matrix AMat = new Matrix(A);
        Matrix inv = AMat.inverse();
        Matrix bMat = new Matrix(MatrixMath.transpose(b));
        Matrix w = inv.multiply(bMat);
        double weights2[][] = w.getMatrix();
        weights2 = MatrixMath.transpose(weights2);
        return weights2[0];
        //double weights[][] = MatrixMath.multiply(B, false, b, true);
        //weights = MatrixMath.transpose(weights);
        //return weights[0];
    }
}
