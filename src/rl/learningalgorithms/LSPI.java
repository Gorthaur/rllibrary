package rl.learningalgorithms;

import domaintools.ValueFunctionPlotter;
import functionapproximation.QApproximator;
import java.util.LinkedList;
import org.mathIT.algebra.Matrix;
import rl.Sample;
import rlmath.MatrixMath;

/**
 *
 * @author Dean
 */
public class LSPI {

    double A[][];
    double B[][];
    double b[][];
    double oldb[][];
    double oldB[][];
    double gamma;
    QApproximator q;
    LinkedList<Sample> samples;

    public LSPI(QApproximator q, LinkedList<Sample> samples, double gamma) {
        this.q = q;
        this.samples = samples;
        this.gamma = gamma;
    }

    public void fullUpdate() {
        initialiseB();
        initialiseb();
        double weights[] = q.getWeights();
        LSTDQ(samples);
        double newWeights[] = getWeights();
        newWeights = q.getWeights();

        int count = 0;
        while (getError(weights, newWeights) > Math.pow(10, -40)) {
            count++;
            //((MPFourierSubsetFA) FAs[0]).printTerms();
            System.out.println("Error " + getError(weights, newWeights));
            weights = newWeights;
            //ValueFunctionPlotter vp = new ValueFunctionPlotter(q, "LSPI-mountaincar-" + count);
            // vp.writeValuesMatFile();
            LSTDQ(samples);
            newWeights = getWeights();
            newWeights = q.getWeights();
            if (count == 50) {
                System.out.print("No convergence ");
                return;
            }
        }
        System.out.println(getError(weights, newWeights));


    }

    public void LSTDQ(LinkedList<Sample> samples) {
        initialiseB();
        initialiseb();
        for (Sample sample : samples) {
            addSample(sample);
        }
    }

    public void addSample2(Sample p) {
        double phisa[][] = new double[1][];
        phisa[0] = q.calculateStateValue(p.s, p.action);
        phisa = MatrixMath.transpose(phisa);
    }

    public void addSample(Sample p) {
        double Bprime[][];
        Bprime = B;

        double phisa[][] = new double[1][];
        phisa[0] = q.calculateStateValue(p.s, p.action);


        double phisprimeaprime[][] = new double[1][];
        double maxValue = Double.NEGATIVE_INFINITY;
        int maxAction = 0;
        if (p.s.isTerminal()) {
            System.out.println("shit");
        }
        if (!p.nextState.isTerminal()) {
            if (p.s.isTerminal()) {
                System.out.println("shit");
            }

            for (int j = 0; j < q.getNumActions(); j++) {
                double tempValue = q.valueAt(p.nextState, j);
                if (tempValue > maxValue) {
                    maxAction = j;
                    maxValue = tempValue;
                }
            }
            phisprimeaprime[0] = q.calculateStateValue(p.nextState, maxAction);
            //phisprimeaprime[0] = q.calculateStateValue(p.nextState, p.nextAction);
        } else {
            phisprimeaprime[0] = new double[phisa[0].length];
        }

        double phiminusgammaphiprime[][] = MatrixMath.subtract(phisa, MatrixMath.multiply(phisprimeaprime, gamma));

        double phisaphiminusgammaphiprime[][] = MatrixMath.multiply(phisa, true, phiminusgammaphiprime, false);
        //System.out.println("length " + phisaphiminusgammaphiprime.length);

        A = MatrixMath.add(A, phisaphiminusgammaphiprime);

        double top[][] = MatrixMath.multiply(Bprime, false, phisa, true);
        top = MatrixMath.multiply(top, MatrixMath.multiply(phiminusgammaphiprime, Bprime));


        double bottom[][] = MatrixMath.multiply(phiminusgammaphiprime, Bprime);
        bottom = MatrixMath.multiply(bottom, false, phisa, true);


        if (bottom.length != 1 && bottom[0].length != 1) {
            System.out.println("Error, bottom size wrong");
        }
        top = MatrixMath.divide(top, 1 + bottom[0][0]);


        double newB[][] = MatrixMath.subtract(Bprime, top);


        B = newB;

        double newb[][] = MatrixMath.add(b, MatrixMath.multiply(phisa, p.reward));
        b = newb;
    }

    public void initialiseB() {
        int numTerms = q.getNumTerms();
        B = new double[numTerms][numTerms];
        A = new double[numTerms][numTerms];
        if (oldB == null) {
            oldB = new double[numTerms][numTerms];
        }
        double diag = 100.0;
        for (int i = 0; i < B.length; i++) {
            B[i][i] = diag;
        }
        for (int i = 0; i < B.length; i++) {
            A[i][i] = 0;
        }
    }

    public void initialiseb() {
        int numTerms = q.getNumTerms();
        if (oldb == null) {
            oldb = new double[1][numTerms];
        }
        b = new double[1][numTerms];
    }

    public double[] getWeights() {
        oldB = B;
        oldb = b;
        Matrix AMat = new Matrix(A);
        Matrix inv = AMat.inverse();
        Matrix bMat = new Matrix(MatrixMath.transpose(b));
        Matrix w = inv.multiply(bMat);
        double weights2[][] = w.getMatrix();
        weights2 = MatrixMath.transpose(weights2);
        //w = w.transpose();
        //System.out.println("B");
        //printMatrix(B);
        //System.out.println("A");
        //printMatrix(inv.getArray());
        double allWeights[][] = new double[q.getNumActions()][];
        double weights[][] = MatrixMath.multiply(B, false, b, true);
        //double weights[][] = multiply(inv.getArray(), false, b , true);
        //double weights[][] = w.getArray();
        //printMatrix(weights);
        weights = MatrixMath.transpose(weights);
        q.setWeights(weights[0]);
        return weights[0];
    }

    public double getError(double weight[], double newWeight[]) {
        double err = 0;
        for (int j = 0; j < weight.length; j++) {
            err += Math.pow(weight[j] - newWeight[j], 2);
        }
        return err;
    }
}
