package rl.learningalgorithms;

import functionapproximation.BasisFunction;
import functionapproximation.VApproximator;
import functionapproximation.dictionary.FeatureDictionary;
import java.io.Serializable;
import java.util.LinkedList;
import rl.Sample;

/**
 *
 * @author dean
 */
public class OMPTD implements Serializable {

    private VApproximator v;
    private FeatureDictionary dictionary;
    public double regularisation = 0.01;

    RegularizedLSTDV learner;

    public void setRegularisation(double regularisation) {
        this.regularisation = regularisation;
    }

    public OMPTD(VApproximator v, FeatureDictionary dictionary) {
        this.v = v;
        this.dictionary = dictionary;
    }

    public OMPTD(VApproximator v, FeatureDictionary dictionary, double regularisation) {
        this.regularisation = regularisation;
    }

    public void calculateWeights(LinkedList<Sample> samples, double gamma, double beta) {
        BasisFunction mostCorrelated = dictionary.getMostCorrelated(samples, v, gamma);
        double correlation = getCorrelation(samples, mostCorrelated, gamma);
        while (correlation > beta) {
            //System.out.print(".");
            v.addBasis(mostCorrelated);
            RegularizedLSTDV learner = new RegularizedLSTDV(v, gamma, regularisation);
            learner.fullUpdate(samples);
            mostCorrelated = dictionary.getMostCorrelated(samples, v, gamma);
            correlation = getCorrelation(samples, mostCorrelated, gamma);
        }
        System.out.println();
        return;
    }

    public void calculateWeights(LinkedList<Sample> samples, double gamma, double beta, int maxTerms) {
        for (int i = 0; i < maxTerms; i++) {
            System.out.print(".");
            RegularizedLSTDV learner = new RegularizedLSTDV(v, gamma, regularisation);
            learner.fullUpdate(samples);
            BasisFunction mostCorrelated = dictionary.getMostCorrelated(samples, v, gamma);
            double correlation = getCorrelation(samples, mostCorrelated, gamma);
            v.addBasis(mostCorrelated);
        }
        System.out.println();
        return;
    }

    public LinkedList<BasisFunction> findFirstNBasisFunctions(LinkedList<Sample> samples, double gamma, int n) {
        LinkedList<BasisFunction> bfs = new LinkedList<BasisFunction>();
        for (int i = 0; i < n; i++) {
            System.out.print(".");
            if (learner == null) {
                learner = new RegularizedLSTDV(v, gamma, regularisation);
                learner.fullUpdate(samples);
            }
            //learner.partialUpdate(samples);
            BasisFunction mostCorrelated = dictionary.getMostCorrelated(samples, v, gamma);
            if (mostCorrelated != null) {
                //double correlation = getCorrelation(samples, mostCorrelated, gamma);
                //add the basis function to the approximation
                v.addBasis(mostCorrelated);
                bfs.add(mostCorrelated);
            }
            learner = new RegularizedLSTDV(v, gamma, regularisation);
            //}
            learner.fullUpdate(samples);
        }
        return bfs;
    }

    //implement function to work out the value of v using lstd
    public void learn(LinkedList<Sample> samples, double gamma) {
        RegularizedLSTDV learner = new RegularizedLSTDV(v, gamma, regularisation);
        learner.fullUpdate(samples);
    }

    //implement function to add most correlated function.
    public LinkedList<BasisFunction> addMostCorrelatedFunction(LinkedList<Sample> samples, double gamma) {
        LinkedList<BasisFunction> bfs = new LinkedList<BasisFunction>();
        BasisFunction mostCorrelated = dictionary.getMostCorrelated(samples, v, gamma);
        if (mostCorrelated != null) {
            //double correlation = getCorrelation(samples, mostCorrelated, gamma);
            v.addBasis(mostCorrelated);
            bfs.add(mostCorrelated);
        }
        return bfs;
    }

    public double getCorrelation(LinkedList<Sample> samples, BasisFunction bf, double gamma) {
        double bot = 0;
        double top = 0;
        for (Sample s : samples) {

            double eval = bf.getValue(s.s);
            top += eval * (-v.valueAt(s.s) + gamma * v.valueAt(s.nextState) + s.reward);
            bot += eval * eval;
        }

        bot = Math.sqrt(bot);
        return Math.abs(top / bot);

    }
}
