package rl.learningalgorithms;


import functionapproximation.QApproximator;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import rl.ElegibilityTrace;
import rl.policy.Policy;
import rl.state.State;


public class SarsaLambda {
  public QApproximator q;
  public Policy p;
  public ElegibilityTrace trace;
  public double weights[];
  public double shrink[];
  public int numTerms;

  public double lambda;
  public double gamma;
  public double alpha;

  public SarsaLambda(QApproximator q, Policy p, double alpha, double gamma, double lambda) {
    this.q = q;
    this.p = p;
    this.numTerms = q.getNumTerms();
    this.alpha = alpha;
    this.gamma = gamma;
    this.lambda = lambda;
    trace = q.getNewTrace();
    weights = q.getWeights();
    shrink = q.getShrink();
  }

  public void startEpisode() {
    trace.setTrace(new double[q.getNumTerms()]);
  }

  public int nextMove(State s) {
    return p.select(s);
  }

  public void addSample(State s, int action, State nextState, int nextAction, double reward) {
    weights = q.getWeights();
    double trace[] = this.trace.getTrace();
    shrink = q.getShrink();
    double delta = reward - q.valueAt(s,action);
    if (!nextState.isTerminal()) {
      delta += gamma*q.valueAt(nextState, nextAction);
    }
    if(java.lang.Double.isNaN(delta)) {
      System.out.println("NaN trouble");
    }

    double stateVals[] = q.calculateStateValue(s);
    //printArray(stateVals);
    int start = q.getArrayStartPosition(action);
    int end = start + q.getNumTerms(action);
    for (int i = 0; i < q.getNumTerms(); i++) {
      trace[i] = gamma*lambda*1*trace[i];
      if (i >= start && i < end) {
        trace[i] += stateVals[i];
      }
      weights[i] += (alpha/shrink[i])*delta*trace[i];
    }
    //printArray(weights);
  }
  
    public void addSample(State s, int action, State nextState, int nextAction, double reward, double trace[]) {
    double delta = reward - q.valueAt(s,action);
    if (!nextState.isTerminal()) {
      delta += gamma*q.valueAt(nextState, nextAction);
    }
    if(java.lang.Double.isNaN(delta)) {
      System.out.println("NaN trouble");
    }
    
    double stateVals[] = q.calculateStateValue(s);
    //printArray(stateVals);
    int start = q.getArrayStartPosition(action);
    int end = start + q.getNumTerms(action);
    for (int i = 0; i < q.getNumTerms(); i++) {
      trace[i] = gamma*lambda*1*trace[i];
      if (i >= start && i < end) {
        trace[i] += stateVals[i];
      }
      weights[i] += (alpha/shrink[i])*delta*trace[i];
    }
    //printArray(weights);
  }
    
    


  public double[] add(double arr1[], double arr2[]) {
    double returnArr[] = new double[arr1.length];
    for (int i = 0; i < arr1.length; i++) {
      returnArr[i] = arr1[i] + arr2[i];
    }
    return returnArr;
  }
  
  public void decayAlpha(double amount) {
    alpha = alpha * amount;
  }

  public double dot(double arr1[], double arr2[]) {
    if (arr1.length != arr2.length) {
      System.out.println("Dot product error. Arrays not equal in length");
      return Double.NaN;
    }
    double returnVal = 0;
    for (int i = 0; i < arr1.length; i++) {
      returnVal += arr1[i] * arr2[i];
    }
    return returnVal;
  }

  public double[] multiply(double arr[], double m) {
    double returnArr[] = new double[arr.length];
    for (int i = 0; i < arr.length; i++) {
      returnArr[i] = arr[i]*m;
    }
    return returnArr;
  }
  
  public void printArray(double arr[]) {
    String str = "";
    for (int i = 0; i < arr.length; i++) {
      str = str + arr[i] + " ";
    }
    System.out.println(str);
  }
  
  public String getArrayString(double arr[]) {
          String str = "";
    for (int i = 0; i < arr.length; i++) {
      str = str + arr[i] + " ";
    }
    return str;
  }
}
