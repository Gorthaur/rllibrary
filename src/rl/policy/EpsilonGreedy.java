package rl.policy;

import domains.Domain;
import functionapproximation.QApproximator;
import java.util.ArrayList;
import rl.state.State;


public class EpsilonGreedy extends Policy {
  double epsilon;
  int max = 0;
  QApproximator q;
  public EpsilonGreedy(Domain domain, QApproximator q, double epsilon) {
    super(domain);
    this.epsilon = epsilon;
    this.q = q;
  }
  
    @Override
  public double[] getProbabilities(State s) {
    double probabilities[] = new double[q.getNumActions()];
    int max = -1;
    double maxValue = 0;
    ArrayList<Integer> maxValues = new ArrayList<Integer>();
    for (int i = 0; i < q.getNumActions(); i++) {
      probabilities[i] = epsilon / q.getNumActions();
      double value = q.valueAt(s,i);
      if (max == -1 || maxValue < value) {
        max = i;
        maxValue = value;
        maxValues = new ArrayList<Integer>();
        maxValues.add(i);
      }
      else if (maxValue == value) {
          maxValues.add(i);
      }
    }
    if (maxValues.size() > 1) {
        int maxIndex = (int)(Math.random()*maxValues.size());
        if (maxIndex == maxValues.size()) {
            maxIndex = maxIndex -1;
        }
        max = maxValues.get(maxIndex);
    }
    probabilities[max] += (1-epsilon);
    this.max = max;
    return probabilities;
  }
  

}
