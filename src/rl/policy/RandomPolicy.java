
package rl.policy;

import domains.Domain;
import java.util.Arrays;
import rl.state.State;

/**
 *
 * @author Dean
 */
public class RandomPolicy extends Policy {
    public RandomPolicy(Domain domain) {
        super(domain);
    }
    
    @Override
    public double[] getProbabilities(State s) {
        double probs[] = new double[domain.getTotalNumActions()];
        double probability = 1.0/domain.getTotalNumActions();
        Arrays.fill(probs, probability);
        return probs;
    }
    
}
