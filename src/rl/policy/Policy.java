package rl.policy;

import domains.Domain;
import functionapproximation.QApproximator;
import functionapproximation.VApproximator;
import rl.state.State;


public abstract class Policy<T extends Domain> {
    T domain;
  public Policy(T domain) {
    this.domain = domain;
  }
  public abstract double[] getProbabilities(State s);
  
  public T getDomain() {
      return domain;
  }
   
  public int select(State s) {
    double probabilities[] = getProbabilities(s);
    double rand = Math.random();
    double sum = 0;
    int pos = 0;
    while (rand > sum && pos < probabilities.length) {
      sum+= probabilities[pos];
      pos++;
    }
    return pos - 1;
  }
  
  public int getGreedy(QApproximator q, State s) {
    int max = -1;
    double maxValue = 0;
    for (int i = 0; i < q.getNumActions(); i++) {
      double value = q.valueAt(s,i);
      if (max == -1 || maxValue < value) {
        max = i;
        maxValue = value;
      }
    }
    return max;
  }
  
}
