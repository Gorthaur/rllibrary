package rl.policy;

import domains.Domain;
import functionapproximation.QApproximator;
import java.util.Arrays;
import rl.state.State;

/**
 *
 * @author Dean
 */
public class KEpsilonGreedy extends EpsilonGreedy {
    int k;
    int randWalk = 0;
    public KEpsilonGreedy(Domain domain, QApproximator q, double epsilon, int k) {
        super(domain, q, epsilon);
        this.k = k;
    }
    
    public double[] getProbabilities(QApproximator q, State s) {
        if (randWalk != 0 && randWalk < k) {
            double probs[] = new double[q.getNumActions()];
            Arrays.fill(probs, 1.0/q.getNumActions());
            randWalk++;
            return probs;
        }
        else if (randWalk == k) {
            randWalk = 0;
        }
        return super.getProbabilities(s);
    }
    
    public int select(State s) {
        int choice = super.select(s);
        if (choice != max && randWalk == 0) {
            randWalk++;
        }
        return super.select(s);
    }
}
