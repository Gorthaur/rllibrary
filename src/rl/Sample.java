package rl;

import java.io.Serializable;
import rl.state.State;

/**
 *
 * @author Dean
 */
public class Sample implements Serializable {
    public State s;
    public int action;
    public double reward;
    public double error;
    public State nextState;
    public int nextAction;
    public double ret;
    public double userValue = 0;
    public Sample() {
        
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + (this.s != null ? this.s.hashCode() : 0);
        hash = 67 * hash + this.action;
        hash = 67 * hash + (int) (Double.doubleToLongBits(this.reward) ^ (Double.doubleToLongBits(this.reward) >>> 32));
        hash = 67 * hash + (this.nextState != null ? this.nextState.hashCode() : 0);
        hash = 67 * hash + this.nextAction;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Sample other = (Sample) obj;
        if (this.s != other.s && (this.s == null || !this.s.equals(other.s))) {
            return false;
        }
        if (this.action != other.action) {
            return false;
        }
        if (Double.doubleToLongBits(this.reward) != Double.doubleToLongBits(other.reward)) {
            return false;
        }
        if (this.nextState != other.nextState && (this.nextState == null || !this.nextState.equals(other.nextState))) {
            return false;
        }
        if (this.nextAction != other.nextAction) {
            return false;
        }
        return true;
    }
    
    
    
    public Sample(State s, int action, State nextState, int nextAction, double reward) {
        this.s = s;
        this.action = action;
        this.nextState = nextState;
        this.nextAction = nextAction;
        this.reward = reward;
    }
    
    public Sample(State s, int action, State nextState, int nextAction, double reward, double ret) {
        this.s = s;
        this.action = action;
        this.nextState = nextState;
        this.nextAction = nextAction;
        this.reward = reward;
        this.ret = ret;
    }
}
