package rl.state;

import java.io.Serializable;
import java.util.Arrays;

public class State implements Serializable {
  
  double stateVars[];
  boolean endState;
  public State(double stateVars[]) {
    setState(stateVars);
  }
  
  public void setState(double stateVars[]) {
    this.stateVars = stateVars;
  }
  
  public double[] getState() {
    return stateVars;
  }
  
  public int getStateLength() {
    return stateVars.length;
  }
  
  public boolean isTerminal() {
    if (stateVars == null || endState) {
      return true;
    }
    else {
      return false;
    }
  }
  
  public void setTerminal(boolean terminal) {
    endState = terminal;
  }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final State other = (State) obj;
        for (int i = 0; i < stateVars.length; i++) {
            if (Math.abs(stateVars[i] - other.stateVars[i]) > 0.00000000001) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Arrays.hashCode(this.stateVars);
        hash = 79 * hash + (this.endState ? 1 : 0);
        return hash;
    }
  
  
}
