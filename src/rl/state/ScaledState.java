package rl.state;


public class ScaledState extends State {
  
  public ScaledState(double stateVars[], double minValues[], double maxValues[]) {
    super(new double[stateVars.length]);
    double scaledState[] = this.getState();
    for (int i = 0; i < stateVars.length; i++) {
      scaledState[i] = (stateVars[i] - minValues[i]) / (maxValues[i] - minValues[i]);
    }
    this.stateVars = scaledState;
  }
}
