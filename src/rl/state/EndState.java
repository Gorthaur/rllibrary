package rl.state;


public class EndState extends ScaledState {
  public EndState(double stateVars[], double minValues[], double maxValues[]) {
    super(stateVars, minValues, maxValues);
    setTerminal(true);
  }
}
